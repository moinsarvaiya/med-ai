import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/widgets/custom_chip_widget.dart';
import 'package:med_ai/widgets/space_horizontal.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_round_text_field.dart';
import '../../../../widgets/space_vertical.dart';
import 'dr_symptom_checker_step_2_controller.dart';

class DrSymptomCheckerStep2Screen extends GetView<DrSymptomCheckerStep2Controller> {
  const DrSymptomCheckerStep2Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(bottom: MediaQuery.viewInsetsOf(context).bottom + 16, top: 16, left: 16, right: 16),
        child: SizedBox(
          height: MediaQuery.sizeOf(context).height / 9,
          child: Row(
            children: [
              Expanded(
                child: SubmitButton(
                  backgroundColor: AppColors.white,
                  titleColor: AppColors.colorDarkBlue,
                  title: AppStringKey.strSkip.tr,
                  onClick: () {
                    Get.toNamed(RoutePaths.DR_ADDITIONAL_COMMENT);
                  },
                ),
              ),
              const SpaceHorizontal(10),
              Expanded(
                child: SubmitButton(
                  title: AppStringKey.strNext.tr,
                  onClick: () {
                    Get.toNamed(RoutePaths.DR_DYNAMIC_SYMPTOM);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppStringKey.titleDescSymptomChecker.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    18,
                    AppColors.colorDarkBlue,
                  ),
                ),
                const SpaceVertical(20),
                Obx(
                  () => Wrap(
                    children: controller.selectedSymptomsList
                        .map(
                          (e) => CustomChipWidget(
                            label: e,
                            callback: () {
                              controller.selectedSymptomsList.remove(e);
                              for (var element in controller.listSymptoms) {
                                if (element.serviceName == e) {
                                  element.isSelected = false;
                                  controller.listSymptoms.refresh();
                                }
                              }
                              controller.selectedSymptomsList.refresh();
                            },
                          ).marginOnly(bottom: 15, right: 10),
                        )
                        .toList(),
                  ),
                ),
                const SpaceVertical(20),
                CustomRoundTextField(
                  labelVisible: false,
                  labelText: AppStringKey.labelTime.tr,
                  isRequireField: true,
                  hintText: AppStringKey.hintSymptomChecker.tr,
                  isReadOnly: false,
                  textInputType: TextInputType.text,
                  suffixIcon: const Icon(
                    Icons.keyboard_voice,
                    size: 20,
                    color: AppColors.colorDarkBlue,
                  ),
                  onFieldSubmitted: (val) {
                    if (!controller.selectedSymptomsList.contains(val)) {
                      controller.selectedSymptomsList.add(val!);
                      controller.selectedSymptomsList.refresh();
                      controller.speakController.clear();
                    } else {
                      controller.speakController.clear();
                      Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
                    }
                  },
                  backgroundColor: AppColors.transparent,
                  textEditingController: controller.speakController,
                ),
                const SpaceVertical(30),
                Text(
                  AppStringKey.titleSelectSymptom.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    18,
                    AppColors.colorDarkBlue,
                  ),
                ),
                const SpaceVertical(30),
                SizedBox(
                  width: double.infinity,
                  child: Obx(
                    () => Wrap(
                      children: controller.listSymptoms
                          .map(
                            (element) => CustomChipWidget(
                              label: element.serviceName!,
                              cancelVisible: false,
                              viewCallback: () {
                                if (!controller.selectedSymptomsList.contains(element.serviceName)) {
                                  controller.selectedSymptomsList.add(element.serviceName!);
                                } else {
                                  Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
                                }
                                controller.listSymptoms.refresh();
                                controller.selectedSymptomsList.refresh();
                              },
                            ).marginOnly(right: 10, bottom: 15),
                          )
                          .toList(),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ItemAvailableSymptoms extends StatelessWidget {
  const ItemAvailableSymptoms({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final DrSymptomCheckerStep2Controller controller;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Obx(
        () => Wrap(
          children: controller.listSymptoms
              .map(
                (element) => GestureDetector(
                  onTap: () {
                    if (!controller.selectedSymptomsList.contains(element.serviceName)) {
                      controller.selectedSymptomsList.add(element.serviceName!);
                    } else {
                      Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
                    }
                    controller.listSymptoms.refresh();
                    controller.selectedSymptomsList.refresh();
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      vertical: 8,
                      horizontal: 15,
                    ),
                    decoration: BoxDecoration(
                      color: AppColors.colorBackground,
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          element.serviceName!,
                          style: BaseStyle.textStyleNunitoSansMedium(
                            14,
                            AppColors.colorDarkBlue,
                          ),
                        ),
                        Visibility(
                            visible: element.isSelected!,
                            child: Image.asset(
                              AppImages.checkCircle,
                              height: 16,
                              width: 16,
                            ).paddingOnly(left: 5)),
                      ],
                    ),
                  ).marginOnly(right: 10, bottom: 15),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
