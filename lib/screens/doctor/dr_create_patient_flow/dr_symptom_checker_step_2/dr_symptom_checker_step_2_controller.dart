import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';

import '../../../../constant/app_strings_key.dart';
import '../../../../models/dr_patient_symptom_model.dart';

class DrSymptomCheckerStep2Controller extends BaseController {
  var speakController = TextEditingController();
  RxList<DrPatientSymptomModel> listSymptoms = RxList();

  var selectedSymptomsList = [
    'Dry cough',
  ].obs;

  @override
  void onInit() {
    super.onInit();
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomNightSweat.tr,
        isSelected: false,
      ),
    );
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomCoughingBlood.tr,
        isSelected: false,
      ),
    );
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomPersistent.tr,
        isSelected: false,
      ),
    );
  }
}
