import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';

import '../../../../constant/app_strings_key.dart';
import '../../../../models/dr_patient_symptom_model.dart';

class DrSymptomCheckerController extends BaseController {
  var speakController = TextEditingController();
  RxList<DrPatientSymptomModel> listSymptoms = RxList();

  var selectedSymptomsList = [].obs;

  @override
  void onInit() {
    super.onInit();
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomBloating.tr,
        isSelected: false,
        isSpecialSymptom: false,
      ),
    );
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomCough.tr,
        isSelected: false,
        isSpecialSymptom: true,
      ),
    );
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomFever.tr,
        isSelected: false,
        isSpecialSymptom: true,
      ),
    );
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomNeusea.tr,
        isSelected: false,
        isSpecialSymptom: false,
      ),
    );
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomUnexplained.tr,
        isSelected: false,
        isSpecialSymptom: false,
      ),
    );
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomPain.tr,
        isSelected: false,
        isSpecialSymptom: false,
      ),
    );
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomDiarrhoea.tr,
        isSelected: false,
        isSpecialSymptom: false,
      ),
    );
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomRunny.tr,
        isSelected: false,
        isSpecialSymptom: false,
      ),
    );
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomSore.tr,
        isSelected: false,
        isSpecialSymptom: false,
      ),
    );
    listSymptoms.add(
      DrPatientSymptomModel(
        serviceName: AppStringKey.strSymptomDizziness.tr,
        isSelected: false,
        isSpecialSymptom: false,
      ),
    );
  }
}
