import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/widgets/custom_chip_widget.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_round_text_field.dart';
import '../../../../widgets/space_vertical.dart';
import 'dr_symptom_checker_controller.dart';

class DrSymptomCheckerScreen extends GetView<DrSymptomCheckerController> {
  const DrSymptomCheckerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(bottom: MediaQuery.viewInsetsOf(context).bottom + 16, top: 16, left: 16, right: 16),
        child: SizedBox(
          height: Platform.isAndroid ? BaseSize.height(15) : BaseSize.height(19),
          child: Column(
            children: [
              SubmitButton(
                backgroundColor: AppColors.white,
                titleColor: AppColors.colorDarkBlue,
                title: AppStringKey.strSkip.tr,
                onClick: () {
                  Get.toNamed(RoutePaths.DR_SYMPTOM_CHECKER_STEP_2);
                },
              ),
              Platform.isAndroid ? const SpaceVertical(10) : const SpaceVertical(00),
              SubmitButton(
                title: AppStringKey.strNext.tr,
                onClick: () {
                  Get.toNamed(RoutePaths.DR_SYMPTOM_CHECKER_STEP_2);
                },
              ),
            ],
          ),
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppStringKey.titleDescSymptomChecker.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    18,
                    AppColors.colorDarkBlue,
                  ),
                ),
                const SpaceVertical(20),
                Obx(
                  () => Wrap(
                    children: controller.selectedSymptomsList
                        .map(
                          (e) => CustomChipWidget(
                            label: e,
                            callback: () {
                              controller.selectedSymptomsList.remove(e);
                              for (var element in controller.listSymptoms) {
                                if (element.serviceName == e) {
                                  element.isSelected = false;
                                  controller.listSymptoms.refresh();
                                }
                              }
                              controller.selectedSymptomsList.refresh();
                            },
                          ).marginOnly(bottom: 15, right: 15),
                        )
                        .toList(),
                  ),
                ),
                const SpaceVertical(10),
                CustomRoundTextField(
                  labelVisible: false,
                  labelText: AppStringKey.labelTime.tr,
                  isRequireField: true,
                  hintText: AppStringKey.hintSymptomChecker.tr,
                  isReadOnly: false,
                  textInputType: TextInputType.text,
                  suffixIcon: const Icon(
                    Icons.keyboard_voice,
                    size: 20,
                    color: AppColors.colorDarkBlue,
                  ),
                  onFieldSubmitted: (val) {
                    if (!controller.selectedSymptomsList.contains(val)) {
                      controller.selectedSymptomsList.add(val!);
                      controller.selectedSymptomsList.refresh();
                      controller.speakController.clear();
                    } else {
                      controller.speakController.clear();
                      Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
                    }
                  },
                  backgroundColor: AppColors.transparent,
                  textEditingController: controller.speakController,
                ),
                const SpaceVertical(30),
                Text(
                  AppStringKey.titleSelectSymptom.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    18,
                    AppColors.colorDarkBlue,
                  ),
                ),
                const SpaceVertical(30),
                SizedBox(
                  width: double.infinity,
                  child: Obx(
                    () => Wrap(
                      children: controller.listSymptoms
                          .map(
                            (element) => CustomChipWidget(
                              label: element.serviceName!,
                              cancelVisible: false,
                              viewCallback: () {
                                if (element.isSpecialSymptom! && element.serviceName == AppStringKey.strSymptomFever.tr) {
                                  Get.toNamed(RoutePaths.DR_SPECIAL_SYMPTOM, arguments: {'symptom': AppStringKey.strSymptomFever.tr});
                                } else if (element.isSpecialSymptom! && element.serviceName == AppStringKey.strSymptomCough.tr) {
                                  Get.toNamed(RoutePaths.DR_SPECIAL_SYMPTOM, arguments: {'symptom': AppStringKey.strSymptomCough.tr});
                                } else {
                                  if (!controller.selectedSymptomsList.contains(element.serviceName)) {
                                    controller.selectedSymptomsList.add(element.serviceName!);
                                  } else {
                                    Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
                                  }
                                  controller.listSymptoms.refresh();
                                  controller.selectedSymptomsList.refresh();
                                }
                              },
                            ).marginOnly(right: 15, bottom: 15),
                          )
                          .toList(),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
