import 'package:flutter/cupertino.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

import '../../../../bindings/base_controller.dart';

class DrCreatePatientController extends BaseController {
  List<String> patientCountry = ['India', 'Canada', 'Australia', 'China', 'Usa'];
  List<String> patientDivision = ['India', 'Canada', 'Australia', 'China', 'Usa'];
  List<String> patientDistrict = ['India', 'Canada', 'Australia', 'China', 'Usa'];
  Rx<int> stepperIndex = 0.obs;
  late ScrollController scrollController;
  final _showBackToTopButton = false.obs;
  var genderController = TextEditingController().obs;
  var ageController = TextEditingController().obs;
  var areaController = TextEditingController().obs;
  var referralCodeController = TextEditingController().obs;
  var bloodGroupController = TextEditingController().obs;
  var maritalStatusController = TextEditingController().obs;
  var educationController = TextEditingController().obs;
  var occupationController = TextEditingController().obs;
  var ethnicityController = TextEditingController().obs;
  var monthlyEarningController = TextEditingController().obs;
  var selectedLanguages = [].obs;

  var arrGender = ['Male', 'Female'];
  var bloodGroup = [
    'A+',
    'A-',
    'B+',
    'A-',
    'AB+',
    'AB-',
    'O+',
    'O-',
  ];
  var arrLanguages = [
    'English',
    'Hindi',
    'Arabic',
    'Spanish',
  ];
  var maritalStatus = [
    'Single',
    'Married',
    'Divorced',
    'Separated',
  ];
  var area = [
    'Urban',
    'Rural',
    'Compact',
    'Dispersed',
  ];
  var education = [
    'Graduate',
    'Non-Graduate',
  ];
  var occupation = [
    'Job',
    'Business',
    'Employment',
    'Profession',
  ];
  var ethnicity = [
    'African',
    'Asian',
    'African American',
    'Hispanic/Latino',
  ];
  var monthlyEarning = [
    'Below 5,000',
    '5,000 - 10,000',
    '10,000 - 15,000',
    '15,000 - 20,000',
    '20,000 - 25,000',
    '25,000 - 50,000',
    '50,000 - 1L',
  ];

  @override
  void onInit() {
    scrollController = ScrollController()
      ..addListener(() {
        if (scrollController.offset >= 400) {
          _showBackToTopButton.value = true; // show the back-to-top button
        } else {
          _showBackToTopButton.value = false; // hide the back-to-top button
        }
      });
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
    scrollController.dispose();
  }

  void scrollToTop() {
    scrollController.animateTo(0, duration: const Duration(milliseconds: 500), curve: Curves.linear);
  }
}
