import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/routes/route_paths.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_dropdown_field.dart';
import '../../../../widgets/custom_round_text_field.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';
import 'dr_create_patient_controller.dart';

class DrCreatePatientScreen extends GetView<DrCreatePatientController> {
  const DrCreatePatientScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (controller.stepperIndex.value == 1) {
          controller.stepperIndex.value = 0;
          controller.scrollToTop();
          return Future.value(false);
        } else {
          return Future.value(true);
        }
      },
      child: Scaffold(
        backgroundColor: AppColors.white,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(defaultAppBarHeight),
          child: CustomAppBar(
            titleName: AppStringKey.titleCreatePatient.tr,
            isDividerVisible: true,
            onClick: () {
              if (controller.stepperIndex.value == 1) {
                controller.stepperIndex.value = 0;
                controller.scrollToTop();
              } else {
                Get.back();
              }
            },
          ),
        ),
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            controller: controller.scrollController,
            child: Column(
              children: [
                const SpaceVertical(20),
                StepperSection(controller: controller),
                const SpaceVertical(30),
                Obx(
                  () => controller.stepperIndex.value == 1
                      ? SecondStep(
                          controller: controller,
                        )
                      : FirstStep(
                          controller: controller,
                        ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class StepperSection extends StatelessWidget {
  final DrCreatePatientController controller;

  const StepperSection({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 40,
          width: 40,
          decoration: fillBoxDecoration(),
          child: Center(
            child: Text(
              "1",
              style: BaseStyle.textStyleNunitoSansMedium(
                16,
                AppColors.white,
              ),
            ),
          ),
        ),
        const SpaceHorizontal(10),
        Container(
          width: 50,
          height: 2,
          color: AppColors.colorLightSkyBlue,
        ),
        const SpaceHorizontal(10),
        Obx(
          () => Container(
            height: 40,
            width: 40,
            decoration: controller.stepperIndex.value == 0 ? borderBoxDecoration() : fillBoxDecoration(),
            child: Center(
              child: Text(
                "2",
                style: BaseStyle.textStyleNunitoSansMedium(
                  16,
                  controller.stepperIndex.value == 0 ? AppColors.colorDarkBlue : AppColors.white,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

BoxDecoration borderBoxDecoration() {
  return BoxDecoration(
    border: Border.all(color: AppColors.colorDarkBlue),
    shape: BoxShape.circle,
    color: AppColors.white,
  );
}

BoxDecoration fillBoxDecoration() {
  return const BoxDecoration(
    shape: BoxShape.circle,
    color: AppColors.colorDarkBlue,
  );
}

class FirstStep extends StatelessWidget {
  final DrCreatePatientController controller;

  const FirstStep({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(
        children: [
          Center(
            child: Stack(
              children: [
                Container(
                  height: 90,
                  width: 90,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.5),
                      width: 0.5,
                    ),
                  ),
                  child: ClipOval(
                    child: Image.network(
                      dummyImageUrl,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        return Utilities.getPlaceHolder();
                      },
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: Container(
                    padding: const EdgeInsets.all(8),
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppColors.colorDarkBlue,
                    ),
                    child: const Icon(
                      Icons.camera_alt_outlined,
                      color: AppColors.white,
                      size: 18,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SpaceVertical(20),
          CustomRoundTextField(
            labelText: AppStringKey.labelFullName.tr,
            isRequireField: true,
            hintText: AppStringKey.hintFullName.tr,
            textInputType: TextInputType.text,
            backgroundColor: AppColors.transparent,
            textEditingController: TextEditingController(),
          ),
          const SpaceVertical(20),
          CustomDropDownField(
            labelText: AppStringKey.labelCountry.tr,
            validationText: AppStringKey.validationEnterYourCountry.tr,
            hintTitle: AppStringKey.hintSelectCountry.tr,
            onChanged: (countryValue) {},
            items: controller.patientCountry,
          ),
          const SpaceVertical(20),
          CustomDropDownField(
            labelText: AppStringKey.labelDivision.tr,
            validationText: AppStringKey.validationEnterYourCountry.tr,
            hintTitle: AppStringKey.hintSelectDivision.tr,
            onChanged: (countryValue) {},
            items: controller.patientDivision,
          ),
          const SpaceVertical(20),
          CustomDropDownField(
            labelText: AppStringKey.labelDistrict.tr,
            validationText: AppStringKey.validationEnterYourCountry.tr,
            hintTitle: AppStringKey.hintSelectDistrict.tr,
            onChanged: (countryValue) {},
            items: controller.patientDistrict,
          ),
          const SpaceVertical(20),
          CustomRoundTextField(
            labelText: AppStringKey.labelAddress.tr,
            isRequireField: true,
            hintText: AppStringKey.hintAddress.tr,
            textInputType: TextInputType.text,
            backgroundColor: AppColors.transparent,
            textEditingController: TextEditingController(),
          ),
          const SpaceVertical(20),
          CustomRoundTextField(
            labelText: AppStringKey.labelPostCode.tr,
            isRequireField: true,
            hintText: AppStringKey.hintPostCode.tr,
            textInputType: TextInputType.number,
            backgroundColor: AppColors.transparent,
            textEditingController: TextEditingController(),
          ),
          const SpaceVertical(20),
          CustomRoundTextField(
            labelText: AppStringKey.labelMobile.tr,
            isRequireField: true,
            hintText: AppStringKey.hintMobile.tr,
            textInputType: TextInputType.number,
            backgroundColor: AppColors.transparent,
            textEditingController: TextEditingController(),
          ),
          const SpaceVertical(40),
          SubmitButton(
            title: AppStringKey.strNext.tr,
            onClick: () {
              controller.scrollToTop();
              controller.stepperIndex.value = 1;
            },
          ).paddingOnly(
            left: 0,
            right: 0,
            top: 20,
            bottom: MediaQuery.viewInsetsOf(context).bottom + 20,
          ),
        ],
      ),
    );
  }
}

class SecondStep extends StatelessWidget {
  final DrCreatePatientController controller;

  const SecondStep({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(
        children: [
          Center(
            child: Text(
              AppStringKey.strCreatePatientDesc.tr,
              style: BaseStyle.textStyleDomineBold(
                16,
                AppColors.colorDarkBlue,
              ),
            ),
          ),
          const SpaceVertical(30),
          CustomDropDownField(
            hintTitle: AppStringKey.labelProfileGender.tr,
            items: controller.arrGender,
            validationText: '',
            onChanged: (String? val) {
              controller.genderController.value.text = val!;
            },
            labelText: AppStringKey.labelProfileGender.tr,
          ),
          const SpaceVertical(20),
          CustomRoundTextField(
            labelText: AppStringKey.labelProfileAge.tr,
            isRequireField: true,
            keyBoardAction: TextInputAction.next,
            hintText: AppStringKey.hintAge.tr,
            textInputType: TextInputType.number,
            inputFormatter: inputFormatterDigit,
            backgroundColor: AppColors.transparent,
            textEditingController: controller.ageController.value,
          ),
          const SpaceVertical(20),
          CustomDropDownField(
            hintTitle: AppStringKey.labelBloodGroup.tr,
            items: controller.bloodGroup,
            validationText: '',
            onChanged: (String? val) {
              controller.bloodGroupController.value.text = val!;
            },
            labelText: AppStringKey.labelBloodGroup.tr,
          ),
          const SpaceVertical(20),
          CustomDropDownField(
            hintTitle: AppStringKey.hintSelectLanguages.tr,
            items: controller.arrLanguages,
            validationText: '',
            onChanged: (String? val) {
              if (!controller.selectedLanguages.contains(val!)) {
                controller.selectedLanguages.add(val);
              }
            },
            labelText: AppStringKey.labelLanguages.tr,
          ),
          const SpaceVertical(10),
          Obx(
            () => Align(
              alignment: Alignment.centerLeft,
              child: Wrap(
                spacing: 10,
                runSpacing: 10,
                children: controller.selectedLanguages
                    .map(
                      (element) => ItemLanguage(language: element),
                    )
                    .toList(),
              ),
            ),
          ),
          const SpaceVertical(10),
          CustomDropDownField(
            hintTitle: AppStringKey.hintSelectArea.tr,
            items: controller.area,
            validationText: '',
            onChanged: (String? val) {
              controller.areaController.value.text = val!;
            },
            labelText: AppStringKey.labelArea.tr,
          ),
          const SpaceVertical(20),
          CustomDropDownField(
            hintTitle: AppStringKey.hintSelectMaritalStatus.tr,
            items: controller.maritalStatus,
            validationText: '',
            onChanged: (String? val) {
              controller.maritalStatusController.value.text = val!;
            },
            labelText: AppStringKey.labelMaritalStatus.tr,
          ),
          const SpaceVertical(20),
          CustomDropDownField(
            hintTitle: AppStringKey.hintSelectEducation.tr,
            items: controller.education,
            validationText: '',
            onChanged: (String? val) {
              controller.educationController.value.text = val!;
            },
            labelText: AppStringKey.labelEducation.tr,
          ),
          const SpaceVertical(20),
          CustomDropDownField(
            hintTitle: AppStringKey.hintSelectOccupation.tr,
            items: controller.occupation,
            validationText: '',
            onChanged: (String? val) {
              controller.occupationController.value.text = val!;
            },
            labelText: AppStringKey.labelOccupation.tr,
          ),
          const SpaceVertical(20),
          CustomDropDownField(
            hintTitle: AppStringKey.hintSelectEthnicity.tr,
            items: controller.ethnicity,
            validationText: '',
            onChanged: (String? val) {
              controller.ethnicityController.value.text = val!;
            },
            labelText: AppStringKey.labelEthnicity.tr,
          ),
          const SpaceVertical(20),
          CustomDropDownField(
            hintTitle: AppStringKey.hintSelectMonthlyEarning.tr,
            items: controller.monthlyEarning,
            validationText: '',
            onChanged: (String? val) {
              controller.monthlyEarningController.value.text = val!;
            },
            labelText: AppStringKey.labelMonthlyEarning.tr,
          ),
          const SpaceVertical(20),
          CustomRoundTextField(
            labelText: AppStringKey.labelReferralCodeCreatePatient.tr,
            isRequireField: true,
            keyBoardAction: TextInputAction.done,
            hintText: AppStringKey.hintReferralCode.tr,
            backgroundColor: AppColors.transparent,
            textEditingController: controller.referralCodeController.value,
          ),
          const SpaceVertical(20),
          SubmitButton(
            title: AppStringKey.strNext.tr,
            onClick: () {
              Get.toNamed(RoutePaths.DR_VITAL_HEALTH_METRICS);
            },
          ).paddingOnly(
            left: 0,
            right: 0,
            top: 20,
            bottom: 00,
          ),
          Platform.isAndroid ? const SpaceVertical(20) : const SpaceVertical(00),
          SubmitButton(
            title: AppStringKey.strNextSubmit.tr,
            onClick: () {
              Get.until((route) => Get.currentRoute == RoutePaths.DR_BOTTOM_TAB);
            },
          ).paddingOnly(
            left: 0,
            right: 0,
            top: 0,
            bottom: Platform.isAndroid ? MediaQuery.viewInsetsOf(context).bottom + 20 : MediaQuery.viewInsetsOf(context).bottom,
          ),
        ],
      ),
    );
  }
}

class ItemLanguage extends StatelessWidget {
  final String language;
  final int index;

  const ItemLanguage({
    Key? key,
    required this.language,
    this.index = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 8, bottom: 8, left: 15, right: 10),
      decoration: const BoxDecoration(
        color: AppColors.colorGrayBackground,
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            language,
            style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
          ),
          InkWell(
            onTap: () {
              Get.find<DrCreatePatientController>().selectedLanguages.remove(language);
            },
            child: const Icon(
              Icons.cancel,
              color: AppColors.colorDarkBlue,
              size: 15,
            ).paddingAll(5),
          ),
        ],
      ),
    );
  }
}
