import 'package:med_ai/bindings/base_controller.dart';

class DrPatientReviewController extends BaseController {
  var dummyText =
      'Please not that the information provided by MedAi is provided solely for guideline purposes and is not a qualified medical opinion.'
      ' This information should not be considered advice or an opinion. of doctor or other health professional about your actual medical state and you should see a doctor for any symptoms you may have. '
      'If you are experiencing a health emergency, you should call your local emergency number immediately to request emergency medical assistance.';
}
