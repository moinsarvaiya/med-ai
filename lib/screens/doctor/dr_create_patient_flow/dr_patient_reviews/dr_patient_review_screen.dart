import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/doctor/dr_create_patient_flow/dr_patient_reviews/dr_patient_review_controller.dart';
import 'package:med_ai/widgets/space_horizontal.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/space_vertical.dart';
import '../../dr_home/dr_home_screen.dart';

class DrPatientReviewScreen extends GetView<DrPatientReviewController> {
  const DrPatientReviewScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.strNext.tr,
        onClick: () {
          Get.toNamed(RoutePaths.DR_IDENTIFIED_DISEASE);
        },
      ).paddingOnly(left: 20, right: 20, top: 20, bottom: MediaQuery.viewInsetsOf(context).bottom + 20),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    AppStringKey.titleReview.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      18,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                ),
                const SpaceVertical(20),
                Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                  decoration: BoxDecoration(
                    color: AppColors.colorGrayBackground,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.strSymptoms.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(10),
                      Text(
                        AppStringKey.strReviewDoctorSuggest.tr,
                        style: BaseStyle.textStyleNunitoSansRegular(
                          14,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(24),
                      GestureDetector(
                        onTap: () {
                          Get.until((route) => Get.currentRoute == RoutePaths.DR_SYMPTOM_CHECKER);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: 24,
                              height: 24,
                              padding: const EdgeInsets.all(0),
                              child: Image.asset(
                                AppImages.editQuestions,
                              ),
                            ),
                            const SpaceHorizontal(5),
                            Text(
                              AppStringKey.strReviewEditQuestions.tr,
                              style: BaseStyle.textStyleNunitoSansBold(
                                14,
                                AppColors.colorDarkBlue,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                const SpaceVertical(20),
                Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                  decoration: BoxDecoration(
                    color: AppColors.colorGrayBackground,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.strPredication.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(15),
                      const PredicationWidget(
                        title: "Name",
                        percentage: "20",
                        progress: 0.2,
                      ),
                      const SpaceVertical(3),
                      const Divider(
                        color: Colors.grey,
                      ),
                      const SpaceVertical(3),
                      const PredicationWidget(
                        title: "Name",
                        percentage: "60",
                        progress: 0.6,
                      ),
                      const SpaceVertical(3),
                      const Divider(
                        color: Colors.grey,
                      ),
                      const SpaceVertical(3),
                      const PredicationWidget(
                        title: "Name",
                        percentage: "80",
                        progress: 0.8,
                      ),
                      const SpaceVertical(3),
                    ],
                  ),
                ),
                const SpaceVertical(20),
                Center(
                  child: Text(
                    AppStringKey.strDisclaimer.tr,
                    style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
                  ),
                ),
                const SpaceVertical(10),
                Text(
                  controller.dummyText,
                  style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
                ),
                const SpaceVertical(20),
                /*    const SpaceVertical(20),
                Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                  decoration: BoxDecoration(
                    color: AppColors.colorGrayBackground,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.strRecommendation.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(5),
                      Text(
                        'Consult a Doctor',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          14,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(5),
                      Text(
                        'Based on the symptoms provided media believes that you \nneed to consult a doctor from the following specialty',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(20),
                      Text(
                        AppStringKey.strRecommendationSpecialist.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(5),
                      Text(
                        'Gastro Liver',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          14,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    ],
                  ),
                ),*/
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PredicationWidget extends StatelessWidget {
  final String title;
  final String percentage;
  final double progress;

  const PredicationWidget({
    Key? key,
    required this.title,
    required this.percentage,
    required this.progress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: BaseStyle.textStyleNunitoSansRegular(
            14,
            AppColors.colorDarkBlue,
          ),
        ),
        Row(
          children: [
            SizedBox(
              width: 100,
              child: ItemProgressBar(
                percentage: progress,
                progressColor: AppColors.colorGreen,
              ),
            ),
            Text(
              '$percentage%',
              style: BaseStyle.textStyleNunitoSansRegular(
                14,
                AppColors.colorDarkBlue,
              ),
            ),
          ],
        )
      ],
    );
  }
}
