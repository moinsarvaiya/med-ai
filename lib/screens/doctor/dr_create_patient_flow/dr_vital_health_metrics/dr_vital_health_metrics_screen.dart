import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/widgets/custom_temperature_widget.dart';
import 'package:med_ai/widgets/space_horizontal.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_dropdown_field.dart';
import '../../../../widgets/custom_round_text_field.dart';
import '../../../../widgets/space_vertical.dart';
import 'dr_vital_health_metrics_controller.dart';

class DrVitalHealthMetricsScreen extends GetView<DrVitalHealthMetricsController> {
  const DrVitalHealthMetricsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppStringKey.titleDescVitalHealth.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    16,
                    AppColors.colorDarkBlue,
                  ),
                ),
                const SpaceVertical(25),
                CustomRoundTextField(
                  labelText: AppStringKey.labelWeight.tr,
                  isRequireField: true,
                  hintText: AppStringKey.hintKG.tr,
                  textInputType: TextInputType.number,
                  keyBoardAction: TextInputAction.next,
                  backgroundColor: AppColors.transparent,
                  textEditingController: controller.weightController.value,
                ),
                const SpaceVertical(20),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      child: CustomRoundTextField(
                        labelText: AppStringKey.labelHeight.tr,
                        isRequireField: true,
                        hintText: AppStringKey.hintFeet.tr,
                        keyBoardAction: TextInputAction.next,
                        textInputType: TextInputType.text,
                        backgroundColor: AppColors.transparent,
                        textEditingController: controller.heightFeetController.value,
                      ),
                    ),
                    const SpaceHorizontal(20),
                    Expanded(
                      child: CustomRoundTextField(
                        labelVisible: false,
                        labelText: AppStringKey.labelInch.tr,
                        keyBoardAction: TextInputAction.next,
                        isRequireField: true,
                        hintText: AppStringKey.hintInch.tr,
                        textInputType: TextInputType.text,
                        backgroundColor: AppColors.transparent,
                        textEditingController: controller.heightInchController.value,
                      ),
                    ),
                  ],
                ),
                const SpaceVertical(20),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      child: CustomRoundTextField(
                        labelText: AppStringKey.labelBloodPressure.tr,
                        isRequireField: true,
                        keyBoardAction: TextInputAction.next,
                        hintText: AppStringKey.hintLowest.tr,
                        textInputType: TextInputType.text,
                        backgroundColor: AppColors.transparent,
                        textEditingController: controller.lowestBloodPressureController.value,
                      ),
                    ),
                    const SpaceHorizontal(20),
                    Expanded(
                      child: CustomRoundTextField(
                        labelVisible: false,
                        labelText: AppStringKey.hintHighest.tr,
                        isRequireField: true,
                        keyBoardAction: TextInputAction.next,
                        hintText: AppStringKey.hintHighest.tr,
                        textInputType: TextInputType.text,
                        backgroundColor: AppColors.transparent,
                        textEditingController: controller.highestBloodPressureController.value,
                      ),
                    ),
                  ],
                ),
                const SpaceVertical(20),
                CustomRoundTextField(
                  labelText: AppStringKey.labelPulse.tr,
                  isRequireField: true,
                  hintText: AppStringKey.hintType.tr,
                  textInputType: TextInputType.text,
                  keyBoardAction: TextInputAction.done,
                  backgroundColor: AppColors.transparent,
                  textEditingController: controller.pulseController.value,
                ),
                const SpaceVertical(20),
                CustomDropDownField(
                  labelText: AppStringKey.labelBreathing.tr,
                  validationText: AppStringKey.validationSelectBreathing.tr,
                  hintTitle: AppStringKey.hintSelect.tr,
                  onChanged: (countryValue) {
                    controller.breathingSelectedValue = countryValue!;
                  },
                  items: controller.breathing,
                ),
                const SpaceVertical(20),
                CustomTemperatureWidget(
                  onChanged: (value) {
                    controller.temperatureSelectedValue = value;
                  },
                ),
                const SpaceVertical(40),
                Row(
                  children: [
                    Expanded(
                      child: SubmitButton(
                        title: AppStringKey.strSkip.tr,
                        backgroundColor: AppColors.white,
                        titleColor: AppColors.colorDarkBlue,
                        onClick: () {
                          Get.toNamed(RoutePaths.DR_SYMPTOM_CHECKER);
                        },
                      ),
                    ),
                    const SpaceHorizontal(20),
                    Expanded(
                      child: SubmitButton(
                        title: AppStringKey.strNext.tr,
                        onClick: () {
                          Get.toNamed(RoutePaths.DR_SYMPTOM_CHECKER);
                        },
                      ),
                    ),
                  ],
                ).paddingOnly(
                  bottom: 20,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
