import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:med_ai/constant/app_strings_key.dart';

import '../../../../bindings/base_controller.dart';
import '../../../../models/dr_patient_temperture_model.dart';

class DrSpecialSymptomController extends BaseController {
  RxList<Rx<DrPatientTemperatureModel>> temperatures = List.generate(
    11,
    (index) => DrPatientTemperatureModel(temperature: '${96 + index}°F', isSelected: false).obs,
  ).obs;
  var symptomName = AppStringKey.strSymptomFever.obs;
  RxList<String> durationList = [
    '5min',
    '10min',
    '15min',
    '20min',
    '25min',
    '30min',
  ].obs;
  RxList<String> oftenItComesList = [
    'One time',
    'One time',
    'One time',
  ].obs;
  RxList<String> traveledRecentlyList = [
    'Yes',
    'No',
  ].obs;

  var isTemperatureSelected = false.obs;
  var temperatureValue = ''.obs;
  var isDurationExpand = false.obs;

  var isDurationSelected = false.obs;
  var durationValue = ''.obs;
  var oftenItComesExpand = false.obs;

  var isOftenItComesSelected = false.obs;
  var oftenIsComesValue = ''.obs;
  var traveledRecentlyExpand = false.obs;

  var isTraveledRecentlySelected = false.obs;
  var traveledRecentlyValue = ''.obs;
  var completedButtonExpand = false.obs;
}
