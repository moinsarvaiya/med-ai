import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/widgets/custom_chip_widget.dart';
import 'package:med_ai/widgets/space_horizontal.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_size.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/space_vertical.dart';
import 'dr_dynamic_symptom_controller.dart';

class DrDynamicSymptomScreen extends GetView<DrDynamicSymptomController> {
  const DrDynamicSymptomScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (controller.queIndex.value == 1) {
          Get.back();
          return Future.value(true);
        } else {
          controller.queIndex.value = controller.queIndex.value - 1;
          return Future.value(false);
        }
      },
      child: Scaffold(
        backgroundColor: AppColors.white,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(defaultAppBarHeight),
          child: CustomAppBar(
            isDividerVisible: true,
            onClick: () {
              if (controller.queIndex.value == 1) {
                Get.back();
              } else {
                controller.queIndex.value = controller.queIndex.value - 1;
              }
            },
          ),
        ),
        bottomNavigationBar: Padding(
          padding: EdgeInsets.only(bottom: MediaQuery.viewInsetsOf(context).bottom + 16, top: 16, left: 16, right: 16),
          child: SizedBox(
            height: MediaQuery.sizeOf(context).height / 9,
            child: Row(
              children: [
                Expanded(
                  child: SubmitButton(
                    backgroundColor: AppColors.white,
                    titleColor: AppColors.colorDarkBlue,
                    title: AppStringKey.strSkip.tr,
                    onClick: () {
                      Get.toNamed(RoutePaths.DR_ADDITIONAL_COMMENT);
                    },
                  ),
                ),
                const SpaceHorizontal(10),
                Expanded(
                  child: SubmitButton(
                    title: AppStringKey.strNext.tr,
                    onClick: () {
                      if (controller.queIndex.value == controller.queListSize.value) {
                        Get.toNamed(RoutePaths.DR_ADDITIONAL_COMMENT);
                      } else {
                        controller.queIndex.value++;
                        // controller.queIndex.value = controller.queIndex.value + 1;
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.titleDynamicSymptom.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(25),
                  StepperWidget(controller: controller),
                  const SpaceVertical(7),
                  Align(
                    alignment: Alignment.center,
                    child: Obx(() => Text(
                          "${controller.queIndex}/${controller.queListSize}",
                          style: BaseStyle.textStyleNunitoSansBold(
                            16,
                            AppColors.colorDarkBlue,
                          ),
                        )),
                  ),
                  const SpaceVertical(30),
                  CustomChipWidget(
                    label: AppStringKey.strSymptomPersistent.tr,
                    callback: () {},
                  ),
                  const SpaceVertical(30),
                  Text(
                    AppStringKey.strDoYouHavePersistentCough.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(20),
                  Obx(
                    () => Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            controller.isYesSelected.value = true;
                            controller.isNoSelected.value = false;
                            controller.isDoNotKnowSelected.value = false;
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: controller.isYesSelected.value ? AppColors.colorBackground : AppColors.white,
                              borderRadius: BorderRadius.circular(5),
                              border: controller.isYesSelected.value ? null : Border.all(color: AppColors.colorDarkBlue, width: 1),
                            ),
                            width: 80,
                            height: 45,
                            child: Center(
                              child: Text(
                                AppStringKey.deleteYes.tr,
                                style: BaseStyle.textStyleNunitoSansRegular(
                                  14,
                                  AppColors.colorDarkBlue,
                                ),
                              ),
                            ),
                          ),
                        ),
                        const SpaceHorizontal(15),
                        GestureDetector(
                          onTap: () {
                            controller.isYesSelected.value = false;
                            controller.isNoSelected.value = true;
                            controller.isDoNotKnowSelected.value = false;
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: controller.isNoSelected.value ? AppColors.colorBackground : AppColors.white,
                              borderRadius: BorderRadius.circular(5),
                              border: controller.isNoSelected.value ? null : Border.all(color: AppColors.colorDarkBlue, width: 1),
                            ),
                            width: 80,
                            height: 45,
                            child: Center(
                              child: Text(
                                AppStringKey.deleteNo.tr,
                                style: BaseStyle.textStyleNunitoSansRegular(
                                  14,
                                  AppColors.colorDarkBlue,
                                ),
                              ),
                            ),
                          ),
                        ),
                        const SpaceHorizontal(15),
                        GestureDetector(
                          onTap: () {
                            controller.isYesSelected.value = false;
                            controller.isNoSelected.value = false;
                            controller.isDoNotKnowSelected.value = true;
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: controller.isDoNotKnowSelected.value ? AppColors.colorBackground : AppColors.white,
                              borderRadius: BorderRadius.circular(5),
                              border: controller.isDoNotKnowSelected.value
                                  ? null
                                  : Border.all(
                                      color: AppColors.colorDarkBlue,
                                      width: 1,
                                    ),
                            ),
                            width: 130,
                            height: 45,
                            child: Center(
                              child: Text(
                                AppStringKey.dontKnow.tr,
                                style: BaseStyle.textStyleNunitoSansRegular(
                                  14,
                                  AppColors.colorDarkBlue,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class StepperWidget extends StatelessWidget {
  final DrDynamicSymptomController controller;

  const StepperWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: 18,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(
            controller.queListSize.value,
            (index) => Obx(
                  () => controller.queIndex.value > index
                      ? Container(
                          width: BaseSize.width(68 / controller.queListSize.value),
                          margin: const EdgeInsets.only(right: 10),
                          height: 20,
                          decoration: BoxDecoration(
                            color: AppColors.colorGreen,
                            borderRadius: BorderRadius.circular(40),
                          ),
                        )
                      : Container(
                          width: BaseSize.width(68 / controller.queListSize.value),
                          margin: const EdgeInsets.only(right: 10),
                          height: 20,
                          decoration: BoxDecoration(
                            color: AppColors.colorBackground,
                            borderRadius: BorderRadius.circular(40),
                          ),
                        ),
                )),
      ),
    );
  }
}
