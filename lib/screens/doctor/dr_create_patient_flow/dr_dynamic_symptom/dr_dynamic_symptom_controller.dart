import 'package:get/get.dart';

import '../../../../bindings/base_controller.dart';

class DrDynamicSymptomController extends BaseController {
  Rx<int> queIndex = 1.obs;
  Rx<int> queListSize = 6.obs;
  var isYesSelected = true.obs;
  var isNoSelected = false.obs;
  var isDoNotKnowSelected = false.obs;
}
