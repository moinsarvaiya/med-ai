import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_images.dart';

class DrAdditionalCommentController extends BaseController {
  var additionalCommentController = TextEditingController();

  var listImages = [
    AppImages.images,
    AppImages.images,
  ].obs;
}
