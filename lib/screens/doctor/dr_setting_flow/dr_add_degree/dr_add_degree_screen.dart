import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_round_text_field.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../widgets/custom_dropdown_field.dart';
import 'dr_add_degree_controller.dart';

class DrAddDegreeScreen extends GetView<DrAddDegreeController> {
  const DrAddDegreeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var fromAdd = Get.arguments['fromAdd'];
    return Scaffold(
      backgroundColor: AppColors.white,
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.strSave.tr,
        onClick: () {
          controller.passDataToPreviousPage();
        },
      ).paddingOnly(
        left: 20,
        right: 20,
        top: 20,
        bottom: MediaQuery.viewInsetsOf(context).bottom + 20,
      ),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          titleName: fromAdd ? AppStringKey.titleAddDegree.tr : AppStringKey.titleEditDegree.tr,
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SpaceVertical(25),
              CustomDropDownField(
                labelText: AppStringKey.labelDegreeName.tr,
                validationText: AppStringKey.validationEnterYourDegree.tr,
                hintTitle: AppStringKey.hintSelectDegreeName.tr,
                onChanged: (degreeValue) {
                  controller.degreeNameController.value.text = degreeValue!;
                },
                items: controller.degreeItems,
              ),
              const SpaceVertical(20),
              CustomRoundTextField(
                hintText: AppStringKey.hintInstitutionName.tr,
                labelText: AppStringKey.labelInstitutionName.tr,
                isRequireField: true,
                backgroundColor: AppColors.white,
                keyBoardAction: TextInputAction.next,
                textEditingController: controller.institutionNameController.value,
              ),
              const SpaceVertical(20),
              CustomRoundTextField(
                hintText: AppStringKey.hintAccreditationBody.tr,
                labelText: AppStringKey.labelAccreditationBody.tr,
                isRequireField: true,
                backgroundColor: AppColors.white,
                keyBoardAction: TextInputAction.done,
                textEditingController: controller.accreditationBodyController.value,
              ),
              const SpaceVertical(20),
              CustomDropDownField(
                labelText: AppStringKey.labelCountry.tr,
                validationText: AppStringKey.validationEnterYourCountry.tr,
                hintTitle: AppStringKey.hintSelectCountry.tr,
                onChanged: (countryValue) {
                  controller.countryController.value.text = countryValue!;
                },
                items: controller.countryItems,
              ),
            ],
          ).paddingSymmetric(horizontal: 20),
        ),
      ),
    );
  }
}
