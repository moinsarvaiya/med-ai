import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';

class DrAddDegreeController extends BaseController {
  var institutionNameController = TextEditingController().obs;
  var accreditationBodyController = TextEditingController().obs;
  var countryController = TextEditingController().obs;
  var degreeNameController = TextEditingController().obs;
  List<String> degreeItems = [
    'BCA',
    'MCA',
    'MBBS',
    'BSC-IT',
    'MSC-IT',
  ];
  List<String> countryItems = [
    'Bangladesh',
    'India',
    'Australia',
    'Usa',
    'Canada',
    'China',
  ];

  void passDataToPreviousPage() {
    if (institutionNameController.value.text.isNotEmpty) {
      if (accreditationBodyController.value.text.isNotEmpty) {
        if (countryController.value.text.isNotEmpty) {
          if (degreeNameController.value.text.isNotEmpty) {
            Get.back(result: {
              "institution": institutionNameController.value.text,
              "accreditation": accreditationBodyController.value.text,
              "country": countryController.value.text,
              "degree": degreeNameController.value.text,
            });
          } else {
            "All Fields are required".showErrorSnackBar();
          }
        } else {
          "All Fields are required".showErrorSnackBar();
        }
      } else {
        "All Fields are required".showErrorSnackBar();
      }
    } else {
      "All Fields are required".showErrorSnackBar();
    }
  }
}
