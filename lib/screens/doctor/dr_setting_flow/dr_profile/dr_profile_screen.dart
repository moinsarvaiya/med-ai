import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_extension.dart';
// import 'package:med_ai/screens/doctor/dr_setting_flow/dr_profile/dr_profile_controller.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/utilities.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';
import 'dr_profile_controller.dart';

class DrProfileScreen extends GetView<DrProfileController> {
  const DrProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        backgroundColor: AppColors.colorBackground,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(defaultAppBarHeight),
          child: CustomAppBar(
            displayLogo: false,
            isDividerVisible: true,
            titleName: AppStringKey.titleMyProfile.tr,
            onClick: () {
              Get.back();
            },
          ),
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 10,
          ),
          child: ScrollConfiguration(
            behavior: MyBehavior(),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  // Obx(() => Text('${controller.apiCalled.value}')),
                  Obx(
                    () => DoctorProfileDetail(
                      doctorProfileImage: controller.profileImageURL.value,
                      doctorName: controller.name.value,
                      doctorState: controller.state.value,
                      doctorCountry: controller.country.value,
                      doctorApprovalStatus: controller.doctorApprovalStatus.value,
                    ),
                  ),
                  SpaceVertical(15),
                  Obx(
                    () => ProfileDetailSection(
                      age: controller.age.value,
                      registeredNo: controller.regNo.value,
                      degree: controller.degrees.value,
                      specialization: controller.specialization.value,
                      affiliatedWith: controller.affiliation.value,
                      workingAt: controller.currentInstitute.value,
                      trainedIn: controller.trainingCourse.value,
                    ),
                  ),
                  SpaceVertical(10),
                  Obx(
                    () => VerifiedAccountSection(
                      emailValue: controller.email.value,
                      mobileValue: controller.mobile.value,
                      isEmailVerified: controller.email.value.length > 10 ? true : false,
                      isMobileVerified: controller.mobile.value.length > 10 ? true : false,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      onWillPop: () {
        Get.back(result: {
          "profileUpdated": controller.profileUpdated,
        });
        return Future.value(true);
      },
    );
  }
}

class VerifiedAccountSection extends StatelessWidget {
  final String emailValue;
  final String mobileValue;
  final bool isEmailVerified;
  final bool isMobileVerified;

  const VerifiedAccountSection({
    Key? key,
    required this.emailValue,
    required this.mobileValue,
    required this.isEmailVerified,
    required this.isMobileVerified,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 10,
      ),
      width: double.maxFinite,
      padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 3,
            blurRadius: 3,
            offset: const Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppStringKey.strEmail.tr,
                style: BaseStyle.textStyleNunitoSansRegular(
                  12,
                  AppColors.colorDarkBlue,
                ),
              ),
              Text(
                emailValue,
                style: BaseStyle.textStyleNunitoSansRegular(
                  12,
                  Colors.grey,
                ),
              ),
              isEmailVerified
                  ? Text(
                      AppStringKey.strVerified.tr,
                      style: BaseStyle.textStyleNunitoSansSemiBold(
                        12,
                        AppColors.green,
                      ),
                    )
                  : Visibility(
                      visible: false,
                      child: GestureDetector(
                        onTap: () {
                          // AppStrings.labelTryAgain.showSnackBar;
                          Get.toNamed(RoutePaths.DR_VERIFY_EMAIL_NUMBER, arguments: {'isFromNumber': false});
                        },
                        child: Container(
                          width: 45,
                          height: 20,
                          decoration: BoxDecoration(
                            color: AppColors.colorDarkBlue,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Center(
                            child: Text(
                              'ADD',
                              style: BaseStyle.textStyleNunitoSansSemiBold(
                                12,
                                AppColors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
            ],
          ),
          const SpaceVertical(20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppStringKey.strMobile.tr,
                style: BaseStyle.textStyleNunitoSansRegular(
                  12,
                  AppColors.colorDarkBlue,
                ),
              ),
              Text(
                mobileValue,
                style: BaseStyle.textStyleNunitoSansRegular(
                  12,
                  Colors.grey,
                ),
              ),
              isMobileVerified
                  ? Text(
                      AppStringKey.strVerified.tr,
                      style: BaseStyle.textStyleNunitoSansSemiBold(
                        12,
                        AppColors.green,
                      ),
                    )
                  : Visibility(
                      visible: false,
                      child: GestureDetector(
                        onTap: () {
                          AppStrings.labelTryAgain.showSnackBar;
                          Get.toNamed(RoutePaths.DR_VERIFY_EMAIL_NUMBER, arguments: {'isFromNumber': true});
                        },
                        child: Container(
                          width: 45,
                          height: 20,
                          decoration: BoxDecoration(
                            color: AppColors.colorDarkBlue,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Center(
                            child: Text(
                              'ADD',
                              style: BaseStyle.textStyleNunitoSansSemiBold(
                                12,
                                AppColors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
            ],
          ),
        ],
      ),
    );
  }
}

class DoctorProfileDetail extends StatelessWidget {
  final String doctorProfileImage;
  final String doctorName;
  final String doctorState;
  final String doctorCountry;
  final String doctorApprovalStatus;

  const DoctorProfileDetail({
    Key? key,
    required this.doctorProfileImage,
    required this.doctorName,
    required this.doctorState,
    required this.doctorCountry,
    required this.doctorApprovalStatus,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 10,
      ),
      width: double.maxFinite,
      padding: const EdgeInsets.symmetric(vertical: 14),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 3,
            blurRadius: 3,
            offset: const Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 94,
            width: 94,
            decoration: doctorProfileImage.contains("http")
                ? BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(
                        doctorProfileImage,
                      ),
                    ),
                  )
                : BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage(
                        doctorProfileImage,
                      ),
                    ),
                  ),
          ),
          const SpaceVertical(10),
          Text(
            doctorName,
            style: BaseStyle.textStyleDomineBold(
              16,
              AppColors.colorDarkBlue,
            ),
          ),
          const SpaceVertical(4),
          Text(
            '$doctorState, $doctorCountry',
            style: BaseStyle.textStyleNunitoSansSemiBold(
              10,
              AppColors.colorDarkBlue,
            ),
          ),
          const SpaceVertical(4),
          doctorApprovalStatus.toLowerCase().contains("approved")
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      AppImages.verified,
                      width: 16,
                      height: 16,
                    ),
                    const SpaceHorizontal(3),
                    Text(
                      'Verified Doctor',
                      style: BaseStyle.textStyleNunitoSansSemiBold(
                        10,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                  ],
                )
              : const SpaceVertical(4)
        ],
      ),
    );
  }
}

class ProfileDetailSection extends StatelessWidget {
  final String age;
  final String registeredNo;
  final String degree;
  final String specialization;
  final String affiliatedWith;
  final String workingAt;
  final String trainedIn;

  const ProfileDetailSection({
    Key? key,
    required this.age,
    required this.registeredNo,
    required this.degree,
    required this.specialization,
    required this.affiliatedWith,
    required this.workingAt,
    required this.trainedIn,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      width: double.maxFinite,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 3,
            blurRadius: 3,
            offset: const Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 20,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelProfileAge.tr,
              labelValue: age,
            ),
            const SpaceVertical(15),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelRegisteredNo.tr,
              labelValue: registeredNo,
            ),
            const SpaceVertical(15),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelDegree.tr,
              labelValue: degree,
            ),
            const SpaceVertical(15),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelSpecialization.tr,
              labelValue: specialization,
            ),
            const SpaceVertical(15),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelAffiliatedWith.tr,
              labelValue: affiliatedWith,
            ),
            const SpaceVertical(15),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelWorkingAt.tr,
              labelValue: workingAt,
            ),
            const SpaceVertical(15),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelTrainedIn.tr,
              labelValue: trainedIn,
            ),
            const SpaceVertical(15),
            Padding(
              padding: const EdgeInsets.only(
                bottom: 0,
                top: 15,
                left: 20,
                right: 20,
              ),
              child: SubmitButton(
                title: AppStringKey.labelEditProfile.tr,
                onClick: () async {
                  var controller = Get.find<DrProfileController>();
                  var data = await Get.toNamed(RoutePaths.DR_EDIT_PROFILE, arguments: {"doctorProfile": controller.responseBody});
                  try {
                    controller.profileUpdatedData(data);
                  } catch (e) {
                    print(e);
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ProfileDetailItemWidget extends StatelessWidget {
  final String labelName;
  final String labelValue;

  const ProfileDetailItemWidget({
    Key? key,
    required this.labelName,
    required this.labelValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          width: MediaQuery.sizeOf(context).width / 3.5,
          child: Text(
            labelName,
            style: BaseStyle.textStyleNunitoSansRegular(
              12,
              AppColors.colorDarkBlue,
            ),
          ),
        ),
        Expanded(
          child: Text(
            ': $labelValue',
            style: BaseStyle.textStyleNunitoSansRegular(
              12,
              AppColors.colorDarkBlue,
            ),
          ),
        )
      ],
    );
  }
}
