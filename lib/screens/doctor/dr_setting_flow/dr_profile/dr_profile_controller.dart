import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_endpoints.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/models/dr_profile_view_model.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';

import '../../../../api/api_interface/api_interface.dart';
import '../../../../constant/app_images.dart';

class DrProfileController extends BaseController {
  var TAG = "DrProfileController";
  var arguments = Get.arguments;
  var profileUpdated = false;

  var apiProfileImageFromServer = false.obs;
  var apiProfileDetailsFromServer = false.obs;

  ApiCallObject api = ApiCallObject();
  String token = LoginPreferences().sharedPrefRead(StorageKeys.token);
  String personId = LoginPreferences().sharedPrefRead(StorageKeys.personId);
  var profileImageURL = AppImages.dummyProfile.obs;

  var responseBody = DrProfileViewModel();
  var name = "-".obs;
  var state = "-".obs;
  var country = "-".obs;

  var age = "-".obs;
  var gender = "-".obs;
  var regNo = "-".obs;
  var registrationYear = "-".obs;
  var specialization = "-".obs;
  var designation = "-".obs;
  var regBody = "-".obs;
  var currentInstitute = "-".obs;
  var mobile = "-".obs;
  var affiliation = "-".obs;
  var trainingCourse = "-".obs;
  var email = "-".obs;
  var degreesList = [""].obs;
  var degrees = "".obs;

  var doctorApprovalStatus = "".obs;

  @override
  void onInit() {
    super.onInit();
    getProfileImageFromServer();
    getProfileDetailsFromServer();
    doctorApprovalStatus.value = LoginPreferences().sharedPrefRead(StorageKeys.approvalStatus);
  }

  void profileUpdatedData(dynamic data) {
    profileUpdated = data['profileUpdated'];
    print("$TAG: $profileUpdated");
    if (profileUpdated) {
      getProfileDetailsFromServer();
    }
  }

  Future<void> getProfileImageFromServer() async {
    http.Response response;
    Object data = {"person_id": personId};
    response = await api.postData(ApiEndpoints.urlGetProfileImage, data);
    if (response.statusCode == 200) {
      profileImageURL.value = jsonDecode(response.body)['profile_img'];
      apiProfileImageFromServer.value = true;
      print(profileImageURL.value.toString());
    } else {
      print(response.statusCode);
    }
  }

  Future<void> getProfileDetailsFromServer() async {
    http.Response response;
    Object data = {"person_id": personId};
    response = await api.postData(ApiEndpoints.urlGetDoctorProfile, data);
    if (response.statusCode == 200) {
      responseBody = DrProfileViewModel.fromJson(jsonDecode(response.body));
      apiProfileDetailsFromServer.value = true;

      degreesList.value = [];
      degrees.value = "";
      if (responseBody.code == "200") {
        name.value = "${responseBody.doctorProfile!.firstName} ${responseBody.doctorProfile?.lastName}";
        state.value = responseBody.doctorProfile!.district.toString();
        country.value = responseBody.doctorProfile!.country.toString();

        age.value = responseBody.doctorProfile!.age.toString();
        gender.value = responseBody.doctorProfile!.gender.toString();
        regNo.value = responseBody.doctorProfile!.regNo.toString();
        registrationYear.value = responseBody.doctorProfile!.registrationYear.toString();
        specialization.value = responseBody.doctorProfile!.specialization.toString();
        designation.value = responseBody.doctorProfile!.designation.toString();
        regBody.value = responseBody.doctorProfile!.regBody.toString();
        currentInstitute.value = responseBody.doctorProfile!.currentInstitute.toString();
        mobile.value = responseBody.doctorProfile!.mobile.toString();
        affiliation.value = responseBody.doctorProfile!.affiliation.toString();
        trainingCourse.value = responseBody.doctorProfile!.trainingCourse.toString();
        email.value = responseBody.doctorProfile!.email.toString();
        degreesList.value = responseBody.doctorProfile!.degrees!;

        var count = 0;
        for (String data in degreesList) {
          List<String> degree = data.split("|");
          if (count > 0) {
            degrees.value += "\n";
          }
          degrees.value += "${degree[0]}, ${degree[1]}";
          count++;
        }
      } else {
        apiProfileDetailsFromServer.value = false;
      }
    } else {
      print(response.statusCode);
    }
  }
}
