import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_interface/api_interface.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/screens/doctor/dr_home/dr_home_controller.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../models/dr_availability_days_model.dart';
import '../../../../models/dr_profile_view_model.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/app_preference/login_preferences.dart';

class DrSettingsController extends BaseController {
  var TAG = "DrSettingsController";
  var arguments = Get.arguments;
  String token = LoginPreferences().sharedPrefRead(StorageKeys.token);
  String username = LoginPreferences().sharedPrefRead(StorageKeys.username);
  String personId = LoginPreferences().sharedPrefRead(StorageKeys.personId);
  String referralCode = LoginPreferences().sharedPrefRead(StorageKeys.referralCode);

  RxBool showCircularProgressBar = true.obs;

  var profileImageURL = AppImages.dummyProfile.obs;
  ApiCallObject api = ApiCallObject();

  var name = "-".obs;
  var state = "-".obs;
  var country = "-".obs;

  var profileUpdated = false;
  var doctorApprovalStatus = "".obs;
  var doctorAvailabilityHours = "-".obs;

  List<DocAvailabilityDaysModel> dayModel = [
    DocAvailabilityDaysModel(dayName: "Sun", isAvailable: false),
    DocAvailabilityDaysModel(dayName: "Mon", isAvailable: false),
    DocAvailabilityDaysModel(dayName: "Tue", isAvailable: false),
    DocAvailabilityDaysModel(dayName: "Wed", isAvailable: false),
    DocAvailabilityDaysModel(dayName: "Thu", isAvailable: false),
    DocAvailabilityDaysModel(dayName: "Fri", isAvailable: false),
    DocAvailabilityDaysModel(dayName: "Sat", isAvailable: false)
  ];
  Rx<bool> switchChangeAvailability = false.obs;
  Rx<bool> switchChangeLanguage = false.obs;

  void profileUpdatedData(dynamic data) {
    // print(TAG + profileUpdated.toString());
    profileUpdated = data['profileUpdated'];
    print("$TAG: $profileUpdated");
    // if(profileUpdated) {
    //   getProfileDetailsFromServer();
    // }
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    doctorApprovalStatus.value = LoginPreferences().sharedPrefRead(StorageKeys.approvalStatus);

    getProfileImageFromServer();
    getProfileDetailsFromServer();
  }

  void setDefaultDays() {
    dayModel[0].isAvailable = false;
    dayModel[1].isAvailable = false;
    dayModel[2].isAvailable = false;
    dayModel[3].isAvailable = false;
    dayModel[4].isAvailable = false;
    dayModel[5].isAvailable = false;
    dayModel[6].isAvailable = false;
  }

  void logout() {
    CustomFunctions.resetToDefaultLanguage();
    CustomFunctions.clearAllPreferences();
    Get.offNamedUntil(RoutePaths.SELECT_USER, (route) => false);
  }

  Future<void> getProfileImageFromServer() async {
    http.Response response;
    Object data = {"person_id": personId};
    response = await api.postData(ApiEndpoints.urlGetProfileImage, data);
    if (response.statusCode == 200) {
      profileImageURL.value = jsonDecode(response.body)['profile_img'];
      // apiProfileImageFromServer.value = true;
      print(profileImageURL.value.toString());
    } else {
      print(response.statusCode);
    }
  }

  Future<void> getProfileDetailsFromServer() async {
    http.Response response;
    DrProfileViewModel responseBody;
    Object data = {"person_id": personId};
    try {
      response = await api.postData(ApiEndpoints.urlGetDoctorProfile, data);
      if (response.statusCode == 200) {
        responseBody = DrProfileViewModel.fromJson(jsonDecode(response.body));
        if (responseBody.code == "200") {
          name.value = "${responseBody.doctorProfile!.firstName} ${responseBody.doctorProfile?.lastName}";
          Get.find<DrHomeController>().name.value = name.value;

          state.value = responseBody.doctorProfile!.district.toString();
          country.value = responseBody.doctorProfile!.country.toString();

          doctorApprovalStatus.value = responseBody.doctorProfile!.approvalStatus.toString();

          var schedule = responseBody.doctorProfile?.schedule;
          doctorAvailabilityHours.value = schedule?.consultationHour![0] ?? "-";

          var availableDays = responseBody.doctorProfile?.schedule!.days;
          setDefaultDays();
          for (String day in availableDays!) {
            if (day.toLowerCase().contains("sun")) {
              dayModel[0].isAvailable = true;
            } else if (day.toLowerCase().contains("mon")) {
              dayModel[1].isAvailable = true;
            } else if (day.toLowerCase().contains("tue")) {
              dayModel[2].isAvailable = true;
            } else if (day.toLowerCase().contains("wed")) {
              dayModel[3].isAvailable = true;
            } else if (day.toLowerCase().contains("thu")) {
              dayModel[4].isAvailable = true;
            } else if (day.toLowerCase().contains("fri")) {
              dayModel[5].isAvailable = true;
            } else if (day.toLowerCase().contains("sat")) {
              dayModel[6].isAvailable = true;
            }
          }

          var availability = schedule?.availability.toString() ?? "No";
          if (availability == "Yes") {
            switchChangeAvailability.value = true;
            Get.find<DrHomeController>().availability.value = switchChangeAvailability.value;
          }
        } else {
          AppStrings.somethingWentWrong.showErrorSnackBar();
        }
      } else {
        print(response.statusCode);
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> changeDoctorAvailability() async {
    http.Response response;
    dynamic object = {};

    var availability = "No";
    if (switchChangeAvailability.value) {
      availability = "Yes";
    } else {
      availability = "No";
    }

    object["requested_by"] = personId;
    object["availability_status"] = availability;

    if (personId.isNotEmpty) {
      try {
        response = await api.postData(ApiEndpoints.urlDoctorChangeAvailabilityStatus, object);
        var responseBody = jsonDecode(response.body);
        if (responseBody['code'] == "200") {
          var message = responseBody['success_msg'];
          Get.find<DrHomeController>().availability.value = switchChangeAvailability.value;
          message.toString().showSuccessSnackBar();
          await Future.delayed(const Duration(seconds: 3));
        } else {
          var message = responseBody['error_msg'];
          message.showErrorSnackBar();
          switchChangeAvailability.value = !switchChangeAvailability.value;
          await Future.delayed(const Duration(seconds: 3));
        }
      } catch (e) {
        print(e);
        AppStrings.somethingWentWrong.showErrorSnackBar();
        switchChangeAvailability.value = !switchChangeAvailability.value;
        await Future.delayed(const Duration(seconds: 3));
      }
    } else {
      "Person ID Missing".showErrorSnackBar();
      // "something went wrong".showErrorSnackBar;
      switchChangeAvailability.value = !switchChangeAvailability.value;
      await Future.delayed(const Duration(seconds: 3));
    }
  }

  Future<void> deleteProfileSendToServer(context) async {
    http.Response response;
    dynamic object = {};

    object["username"] = username;

    if (username.isNotEmpty) {
      try {
        response = await api.postData(ApiEndpoints.urlDeleteUserAccount, object);
        var responseBody = jsonDecode(response.body);
        if (responseBody['code'] == "200") {
          var message = responseBody['success_msg'];

          message.toString().showSuccessSnackBar();
          await Future.delayed(const Duration(seconds: 3));
          logout();
        } else {
          var message = responseBody['error_msg'];
          message.toString().showErrorSnackBar();
          await Future.delayed(const Duration(seconds: 3));
        }
      } catch (e) {
        print(e);
        AppStrings.somethingWentWrong.showErrorSnackBar();
        await Future.delayed(const Duration(seconds: 3));
      }
    } else {
      AppStrings.somethingWentWrong.showErrorSnackBar();
      await Future.delayed(const Duration(seconds: 3));
    }
  }
}
