import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/doctor/dr_setting_flow/dr_settings/dr_settings_controller.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../models/dr_availability_days_model.dart';
import '../../../../widgets/custom_appbar.dart';

class DrSettingsScreen extends GetView<DrSettingsController> {
  const DrSettingsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isBackButtonVisible: false,
          displayLogo: false,
          isDividerVisible: true,
          titleName: AppStringKey.titleSettings.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Obx(
                  () => DoctorProfileDetail(
                    doctorProfileImage: controller.profileImageURL.value,
                    doctorName: controller.name.value,
                    doctorState: controller.state.value,
                    doctorCountry: controller.country.value,
                    doctorApprovalStatus: controller.doctorApprovalStatus.value,
                  ),
                ),
                const SpaceVertical(20),
                Obx(
                  () => DoctorAvailabilitySection(
                    docAvailabilityHours: controller.doctorAvailabilityHours.value,
                    controller: controller,
                  ),
                ),
                const SpaceVertical(20),
                SettingOptionSection(controller: controller),
                const SpaceVertical(30),
                Text(
                  AppStringKey.appVersion.tr,
                  style: BaseStyle.textStyleNunitoSansMedium(
                    12,
                    AppColors.colorDarkBlue,
                  ),
                ),
                const SpaceVertical(20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class DoctorProfileDetail extends StatelessWidget {
  final String doctorProfileImage;
  final String doctorName;
  final String doctorState;
  final String doctorCountry;
  final String doctorApprovalStatus;

  const DoctorProfileDetail({
    Key? key,
    required this.doctorProfileImage,
    required this.doctorName,
    required this.doctorState,
    required this.doctorCountry,
    required this.doctorApprovalStatus,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      padding: const EdgeInsets.symmetric(vertical: 14),
      width: double.maxFinite,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 3,
            blurRadius: 3,
            offset: const Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 94,
            width: 94,
            decoration: doctorProfileImage.contains("http")
                ? BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(
                        doctorProfileImage,
                      ),
                    ),
                  )
                : BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage(
                        doctorProfileImage,
                      ),
                    ),
                  ),
          ),
          const SpaceVertical(10),
          Text(
            doctorName,
            style: BaseStyle.textStyleDomineBold(
              16,
              AppColors.colorDarkBlue,
            ),
          ),
          const SpaceVertical(4),
          Text(
            '$doctorState, $doctorCountry',
            style: BaseStyle.textStyleNunitoSansRegular(
              10,
              AppColors.colorDarkBlue,
            ),
          ),
          doctorApprovalStatus.toLowerCase().contains("approved")
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      AppImages.verified,
                      width: 16,
                      height: 16,
                    ),
                    const SpaceHorizontal(3),
                    Text(
                      AppStringKey.verifyDoc.tr,
                      style: BaseStyle.textStyleNunitoSansRegular(
                        10,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                  ],
                )
              : const SpaceVertical(4)
        ],
      ),
    );
  }
}

class DoctorAvailabilitySection extends StatelessWidget {
  final String docAvailabilityHours;
  final DrSettingsController controller;

  const DoctorAvailabilitySection({Key? key, required this.docAvailabilityHours, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Align(
            alignment: Alignment.topLeft,
            child: Text(
              AppStringKey.labelAvailability.tr,
              style: BaseStyle.textStyleNunitoSansSemiBold(
                14,
                AppColors.colorDarkBlue,
              ),
            ),
          ),
        ),
        const SpaceVertical(10),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 10),
          width: double.maxFinite,
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 3,
                blurRadius: 3,
                offset: const Offset(0, 1), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    AppStringKey.labelAvailabilityHours.tr,
                    style: BaseStyle.textStyleNunitoSansRegular(
                      12,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceHorizontal(10),
                  Text(
                    docAvailabilityHours,
                    style: BaseStyle.textStyleNunitoSansBold(
                      12,
                      AppColors.colorDarkBlue,
                    ),
                  )
                ],
              ),
              const SpaceVertical(10),
              Text(
                AppStringKey.labelAvailabilityDays.tr,
                style: BaseStyle.textStyleNunitoSansRegular(
                  12,
                  AppColors.colorDarkBlue,
                ),
              ),
              const SpaceVertical(10),
              DocAvailabilityDaysItem(
                docAvailabilityDays: controller.dayModel,
              )
            ],
          ),
        )
      ],
    );
  }
}

class DocAvailabilityDaysItem extends StatelessWidget {
  final List<DocAvailabilityDaysModel> docAvailabilityDays;

  const DocAvailabilityDaysItem({Key? key, required this.docAvailabilityDays}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 40,
      alignment: Alignment.center,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: docAvailabilityDays.length,
        itemBuilder: (context, index) {
          return Container(
            margin: const EdgeInsets.symmetric(horizontal: 5),
            width: 32,
            height: 32,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: docAvailabilityDays[index].isAvailable! ? AppColors.colorOrange : AppColors.colorLightSkyBlue,
            ),
            child: Center(
              child: Text(
                docAvailabilityDays[index].dayName!,
                style: BaseStyle.textStyleNunitoSansSemiBold(
                  12,
                  AppColors.colorDarkBlue,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

class SettingOptionSection extends StatelessWidget {
  final DrSettingsController controller;

  const SettingOptionSection({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      width: double.maxFinite,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 3,
            blurRadius: 3,
            offset: const Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: [
          InkWell(
            splashColor: AppColors.colorDarkBlue,
            onTap: () async {
              var personId = LoginPreferences().sharedPrefRead(StorageKeys.personId);
              if (personId != "0") {
                var data = await Get.toNamed(RoutePaths.DR_PROFILE);
                try {
                  controller.profileUpdatedData(data);
                } catch (e) {
                  print(e);
                }
              } else {
                var data = await Get.toNamed(RoutePaths.DR_EDIT_PROFILE);
                try {
                  controller.profileUpdatedData(data);
                } catch (e) {
                  print(e);
                }
              }
            },
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SettingOptionItemWidget(
                    icon: AppImages.setProfile,
                    optionName: AppStringKey.labelMyProfile.tr,
                  ),
                  const Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                  )
                ],
              ),
            ),
          ),
          const CustomDivider(),
          InkWell(
            onTap: () {
              controller.personId != "0"
                  ? Get.toNamed(RoutePaths.DR_SET_YOUR_CALENDAR_VIRTUAL)
                  : Utilities.pendingProfileCompleteDialog(context);
              // Get.toNamed(RoutePaths.DR_SET_YOUR_SCHEDULE);
            },
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SettingOptionItemWidget(
                    icon: AppImages.setSchedule,
                    optionName: AppStringKey.labelSetYourSchedule.tr,
                  ),
                  const Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                  )
                ],
              ),
            ),
          ),
          const CustomDivider(),
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SettingOptionItemWidget(
                  icon: AppImages.setChangeAvailability,
                  optionName: AppStringKey.labelChangeAvailability.tr,
                ),
                Obx(() => FlutterSwitch(
                      width: 30,
                      height: 20,
                      valueFontSize: 0.0,
                      toggleSize: 15.0,
                      value: controller.switchChangeAvailability.value,
                      borderRadius: 30.0,
                      padding: 2.0,
                      activeColor: AppColors.colorGreen,
                      showOnOff: false,
                      onToggle: (val) {
                        // controller.getDoctorDataFromServer();
                        controller.switchChangeAvailability.value = !controller.switchChangeAvailability.value;
                        controller.changeDoctorAvailability();
                      },
                    ))
              ],
            ),
          ),
          // const CustomDivider(),
          Visibility(
            visible: true,
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SettingOptionItemWidget(
                    icon: AppImages.setChangeLanguage,
                    optionName: AppStringKey.labelChangeLanguage.tr,
                  ),
                  Row(
                    children: [
                      Text(
                        "বন।",
                        style: BaseStyle.textStyleNunitoSansSemiBold(14, AppColors.colorDarkBlue),
                      ),
                      const SpaceHorizontal(5),
                      Obx(() => FlutterSwitch(
                            width: 30,
                            height: 20,
                            valueFontSize: 0.0,
                            toggleSize: 15.0,
                            value: controller.switchChangeLanguage.value,
                            borderRadius: 30.0,
                            padding: 2.0,
                            activeColor: AppColors.colorGreen,
                            showOnOff: false,
                            onToggle: (val) {
                              if (val) {
                                Get.updateLocale(const Locale('bn', 'BN'));
                              } else {
                                Get.updateLocale(const Locale('en', 'EN'));
                              }
                              controller.switchChangeLanguage.value = !controller.switchChangeLanguage.value;
                            },
                          ))
                    ],
                  )
                ],
              ),
            ),
          ),
          const CustomDivider(),
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SettingOptionItemWidget(
                  icon: AppImages.setReferralCode,
                  optionName: "${AppStringKey.labelReferralCodeWithoutValue.tr} ${controller.referralCode.toString()}",
                ),
                controller.referralCode.isEmpty
                    ? const SizedBox()
                    : GestureDetector(
                        onTap: () {
                          Utilities.addTextToClipBoard(context, controller.referralCode.toString());
                        },
                        child: Image.asset(AppImages.setCopy, width: 24, height: 24)),
              ],
            ),
          ),
          const CustomDivider(),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SettingOptionItemWidget(
                  icon: AppImages.setShare,
                  optionName: AppStringKey.labelShareApp.tr,
                ),
              ],
            ),
          ),
          const CustomDivider(),
          InkWell(
            onTap: () {
              Utilities.commonDialog(
                context,
                AppStringKey.deleteWarning.tr,
                AppStringKey.deleteDesc.tr,
                AppStringKey.deleteYes.tr,
                AppStringKey.deleteNo.tr,
                () {
                  controller.deleteProfileSendToServer(context);
                },
                // positive button
                () {
                  Get.back();
                }, // negative button
              );
            },
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SettingOptionItemWidget(
                    icon: AppImages.setDeleteAccount,
                    optionName: AppStringKey.labelDeleteAcc.tr,
                    labelColor: AppColors.colorPink,
                  ),
                ],
              ),
            ),
          ),
          const CustomDivider(),
          InkWell(
            onTap: () {
              Utilities.commonDialog(
                context,
                AppStringKey.logoutTitle.tr,
                AppStringKey.logoutDesc.tr,
                AppStringKey.deleteYes.tr,
                AppStringKey.deleteNo.tr,
                () {
                  controller.logout();
                },
                () {
                  Get.back();
                },
              );
            },
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SettingOptionItemWidget(
                    icon: AppImages.setLogout,
                    optionName: AppStringKey.labelLogout.tr,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class SettingOptionItemWidget extends StatelessWidget {
  final String icon;
  final String optionName;
  final Color labelColor;

  const SettingOptionItemWidget({
    Key? key,
    required this.icon,
    required this.optionName,
    this.labelColor = AppColors.colorDarkBlue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image.asset(icon, width: 24, height: 24),
        const SpaceHorizontal(16),
        Text(
          optionName,
          style: BaseStyle.textStyleNunitoSansMedium(
            14,
            labelColor,
          ),
        ),
      ],
    );
  }
}

class CustomDivider extends StatelessWidget {
  const CustomDivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 2,
      color: AppColors.colorBackground,
    );
  }
}

class DeleteAccountDialog extends StatelessWidget {
  const DeleteAccountDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
