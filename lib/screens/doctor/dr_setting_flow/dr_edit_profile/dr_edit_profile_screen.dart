import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/models/dr_institution_model.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/validators/validators.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_dropdown_field.dart';
import '../../../../widgets/custom_dropdown_field_selected.dart';
import '../../../../widgets/custom_round_text_field.dart';
import 'dr_edit_profile_controller.dart';

class DrEditProfileScreen extends GetView<DrEditProfileController> {
  const DrEditProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          backgroundColor: AppColors.white,
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(defaultAppBarHeight),
            child: CustomAppBar(
              titleName: AppStringKey.titleEditProfile.tr,
              isDividerVisible: true,
              onClick: () {
                Get.back(result: {
                  "profileUpdated": false,
                });
              },
            ),
          ),
          body: ScrollConfiguration(
            behavior: MyBehavior(),
            child: SingleChildScrollView(
              child: Form(
                key: controller.form,
                autovalidateMode: AutovalidateMode.disabled,
                child: Column(
                  children: [
                    const SpaceVertical(40),
                    Center(
                      child: Stack(
                        children: [
                          Container(
                            height: 90,
                            width: 90,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                            ),
                            child: Image.network(
                              'https://picsum.photos/200/300',
                              fit: BoxFit.cover,
                            ),
                          ),
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: Container(
                              padding: const EdgeInsets.all(8),
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              decoration: const BoxDecoration(shape: BoxShape.circle, color: AppColors.colorDarkBlue),
                              child: const Icon(
                                Icons.camera_alt_outlined,
                                color: AppColors.white,
                                size: 18,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    const SpaceVertical(20),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          flex: 1,
                          child: CustomRoundTextField(
                            labelText: AppStringKey.labelFirstName.tr,
                            isRequireField: true,
                            keyBoardAction: TextInputAction.next,
                            hintText: AppStringKey.hintFirstName.tr,
                            backgroundColor: AppColors.transparent,
                            textEditingController: controller.firstNameController.value,
                            validator: Validators.emptyValidator,
                          ),
                        ),
                        const SpaceHorizontal(20),
                        Flexible(
                          flex: 1,
                          child: CustomRoundTextField(
                            labelText: AppStringKey.labelLastName.tr,
                            isRequireField: true,
                            keyBoardAction: TextInputAction.done,
                            hintText: AppStringKey.hintLastName.tr,
                            backgroundColor: AppColors.transparent,
                            textEditingController: controller.lastNameController.value,
                            validator: Validators.emptyValidator,
                          ),
                        )
                      ],
                    ),
                    const SpaceVertical(20),
                    CustomDropDownFieldSelected(
                      selectedValue: controller.genderController.value.text,
                      hintTitle: AppStringKey.labelProfileGender.tr,
                      items: controller.arrGender,
                      validationText: '',
                      onChanged: (String? val) {
                        controller.genderController.value.text = val!;
                      },
                      labelText: AppStringKey.labelProfileGender.tr,
                    ),
                    controller.personId != "0" ? const SizedBox() : const SpaceVertical(20),
                    controller.personId != "0"
                        ? const SizedBox()
                        : CustomRoundTextField(
                            isReadOnly: controller.registeredNoController.value.text.isNotEmpty ? true : false,
                            labelText: AppStringKey.labelRegisteredNo.tr,
                            isRequireField: true,
                            inputFormatter: inputFormatterDigit,
                            hintText: AppStringKey.hintRegisteredNo.tr,
                            textInputType: TextInputType.number,
                            keyBoardAction: TextInputAction.next,
                            backgroundColor: AppColors.transparent,
                            textEditingController: controller.registeredNoController.value,
                            validator: Validators.emptyValidator,
                          ),
                    controller.personId != "0" ? const SizedBox() : const SpaceVertical(20),
                    controller.personId != "0"
                        ? const SizedBox()
                        : CustomRoundTextField(
                            isReadOnly: controller.registeredBodyController.value.text.isNotEmpty ? true : false,
                            labelText: AppStringKey.labelRegisteredBody.tr,
                            isRequireField: true,
                            inputFormatter: inputFormatterDigit,
                            hintText: AppStringKey.hintRegisteredBody.tr,
                            textInputType: TextInputType.number,
                            keyBoardAction: TextInputAction.next,
                            backgroundColor: AppColors.transparent,
                            textEditingController: controller.registeredBodyController.value,
                            validator: Validators.emptyValidator,
                          ),
                    controller.personId != "0" ? const SizedBox() : const SpaceVertical(20),
                    controller.personId != "0"
                        ? const SizedBox()
                        : CustomRoundTextField(
                            isReadOnly: controller.registeredYearController.value.text.isNotEmpty ? true : false,
                            labelText: AppStringKey.labelRegisteredYear.tr,
                            isRequireField: true,
                            inputFormatter: inputFormatterDigit,
                            hintText: AppStringKey.hintRegisteredYear.tr,
                            textInputType: TextInputType.number,
                            keyBoardAction: TextInputAction.next,
                            backgroundColor: AppColors.transparent,
                            textEditingController: controller.registeredYearController.value,
                            validator: Validators.emptyValidator,
                          ),
                    const SpaceVertical(20),
                    CustomRoundTextField(
                      labelText: AppStringKey.labelWorking.tr,
                      isRequireField: true,
                      hintText: AppStringKey.hintWorking.tr,
                      keyBoardAction: TextInputAction.done,
                      backgroundColor: AppColors.transparent,
                      textEditingController: controller.workingController.value,
                      validator: Validators.emptyValidator,
                    ),
                    const SpaceVertical(20),
                    CustomDropDownField(
                      hintTitle: AppStringKey.hintLanguages.tr,
                      items: controller.arrLanguages,
                      validationText: '',
                      onChanged: (String? val) {
                        if (!controller.selectedLanguages.contains(val!)) {
                          controller.selectedLanguages.add(val);
                        }
                      },
                      labelText: AppStringKey.labelLanguages.tr,
                    ),
                    const SpaceVertical(10),
                    Obx(
                      () => Align(
                        alignment: Alignment.centerLeft,
                        child: Wrap(
                          spacing: 10,
                          runSpacing: 10,
                          children: controller.selectedLanguages
                              .map(
                                (element) => ItemLanguage(language: element),
                              )
                              .toList(),
                        ),
                      ),
                    ),
                    const SpaceVertical(20),
                    CustomRoundTextField(
                      labelText: AppStringKey.labelProfileAge.tr,
                      isRequireField: true,
                      hintText: AppStringKey.hintAge.tr,
                      inputFormatter: inputFormatterDigit,
                      textInputType: TextInputType.number,
                      keyBoardAction: TextInputAction.next,
                      backgroundColor: AppColors.transparent,
                      textEditingController: controller.ageController.value,
                    ),
                    controller.personId == "0" ? const SpaceVertical(20) : const SizedBox(),
                    Visibility(
                      visible: controller.personId == "0" ? true : false,
                      child: CustomRoundTextField(
                        isReadOnly: controller.mobileController.value.text.isNotEmpty ? true : false,
                        labelText: AppStringKey.labelMobile.tr,
                        isRequireField: true,
                        inputFormatter: inputFormatterDigit,
                        hintText: AppStringKey.hintMobile.tr,
                        textInputType: TextInputType.number,
                        keyBoardAction: TextInputAction.next,
                        backgroundColor: AppColors.transparent,
                        textEditingController: controller.mobileController.value,
                        validator: Validators.emptyValidator,
                      ),
                    ),
                    controller.personId == "0" ? const SpaceVertical(20) : const SizedBox(),
                    Visibility(
                      visible: controller.personId == "0" ? true : false,
                      child: CustomRoundTextField(
                        isReadOnly: controller.emailController.value.text.isNotEmpty ? true : false,
                        labelText: AppStringKey.labelEmail.tr,
                        isRequireField: true,
                        hintText: AppStringKey.hintEmail.tr,
                        keyBoardAction: TextInputAction.next,
                        backgroundColor: AppColors.transparent,
                        textEditingController: controller.emailController.value,
                      ),
                    ),
                    const SpaceVertical(20),
                    CustomRoundTextField(
                      labelText: AppStringKey.labelSpecialization.tr,
                      isRequireField: true,
                      hintText: AppStringKey.hintSpecialization.tr,
                      keyBoardAction: TextInputAction.next,
                      backgroundColor: AppColors.transparent,
                      textEditingController: controller.specializationController.value,
                      validator: Validators.emptyValidator,
                    ),
                    const SpaceVertical(20),
                    CustomRoundTextField(
                      labelText: AppStringKey.labelDesignation.tr,
                      isRequireField: true,
                      keyBoardAction: TextInputAction.next,
                      hintText: AppStringKey.hintDesignation.tr,
                      backgroundColor: AppColors.transparent,
                      textEditingController: controller.designationController.value,
                    ),
                    const SpaceVertical(20),
                    CustomRoundTextField(
                      labelText: AppStringKey.labelAffiliation.tr,
                      isRequireField: true,
                      keyBoardAction: TextInputAction.next,
                      hintText: AppStringKey.hintAffiliation.tr,
                      backgroundColor: AppColors.transparent,
                      textEditingController: controller.affiliationController.value,
                    ),
                    const SpaceVertical(20),
                    CustomRoundTextField(
                      labelText: AppStringKey.labelTraining.tr,
                      isRequireField: true,
                      keyBoardAction: TextInputAction.done,
                      hintText: AppStringKey.hintTraining.tr,
                      backgroundColor: AppColors.transparent,
                      textEditingController: controller.trainingController.value,
                    ),
                    controller.personId != "0" ? const SizedBox() : const SpaceVertical(20),
                    controller.personId != "0"
                        ? const SizedBox()
                        : CustomRoundTextField(
                            labelText: AppStringKey.labelDistrict.tr,
                            isRequireField: true,
                            keyBoardAction: TextInputAction.done,
                            hintText: AppStringKey.hintDistrict.tr,
                            backgroundColor: AppColors.transparent,
                            textEditingController: controller.districtController.value,
                            validator: Validators.emptyValidator,
                          ),
                    controller.personId != "0" ? const SizedBox() : const SpaceVertical(20),
                    controller.personId != "0"
                        ? const SizedBox()
                        : CustomRoundTextField(
                            labelText: AppStringKey.labelCountry.tr,
                            isRequireField: true,
                            keyBoardAction: TextInputAction.done,
                            hintText: AppStringKey.hintCountry.tr,
                            backgroundColor: AppColors.transparent,
                            textEditingController: controller.countryController.value,
                            validator: Validators.emptyValidator,
                          ),
                    controller.personId != "0" ? const SizedBox() : const SpaceVertical(20),
                    controller.personId != "0"
                        ? const SizedBox()
                        : CustomRoundTextField(
                            labelText: AppStringKey.labelReferralCodeWithoutValue.tr,
                            isRequireField: false,
                            keyBoardAction: TextInputAction.done,
                            hintText: AppStringKey.hintReferralCode.tr,
                            backgroundColor: AppColors.transparent,
                            textEditingController: controller.referralCodeController.value,
                          ),
                    Obx(
                      () => Visibility(
                          visible: controller.listInstitution.isNotEmpty,
                          child: Column(
                            children: [
                              const SpaceVertical(20),
                              Container(
                                width: double.infinity,
                                color: AppColors.colorBackground,
                                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                child: Text(
                                  AppStringKey.labelAllDegrees.tr,
                                  style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                                ),
                              ),
                              const SpaceVertical(15),
                              Card(
                                elevation: 5,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: SizedBox(
                                  width: double.infinity,
                                  child: Column(
                                    children: [
                                      Text(
                                        AppStringKey.labelInstitution.tr,
                                        style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
                                      ).paddingOnly(top: 10, bottom: 5),
                                      const Divider(
                                        color: AppColors.colorLightSkyBlue,
                                        thickness: 1,
                                      ),
                                      Obx(
                                        () => ListView.builder(
                                          shrinkWrap: true,
                                          scrollDirection: Axis.vertical,
                                          itemCount: controller.listInstitution.length,
                                          physics: const NeverScrollableScrollPhysics(),
                                          itemBuilder: (context, i) {
                                            return ItemInstitution(
                                                index: i, listInstitution: controller.listInstitution, controller: controller);
                                          },
                                        ).paddingOnly(left: 15, right: 15, bottom: 20),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          )),
                    ),
                    const SpaceVertical(20),
                    InkWell(
                      splashColor: AppColors.white,
                      onTap: () async {
                        var data = await Get.toNamed(RoutePaths.DR_ADD_DEGREE, arguments: {
                          'fromAdd': true,
                        });
                        try {
                          controller.newDegreeAdd(data);
                        } catch (e) {
                          print(e);
                        }
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        color: AppColors.colorDarkBlue,
                        child: Text(
                          AppStringKey.labelAddDegree.tr,
                          style: BaseStyle.textStyleNunitoSansBold(14, AppColors.white),
                        ),
                      ),
                    ),
                    const SpaceVertical(35),
                    SubmitButton(
                      title: AppStringKey.strSave.tr,
                      onClick: () {
                        if (controller.personId != "0") {
                          controller.updateProfileSendToServer();
                        } else {
                          if (controller.form.currentState!.validate()) {
                            controller.createProfileSendToServer();
                          } else {
                            print("Validation Required.");
                          }
                        }
                      },
                    ),
                    const SpaceVertical(20),
                  ],
                ).marginSymmetric(horizontal: 20),
              ),
            ),
          ),
        ),
        onWillPop: () {
          Get.back(result: {
            "profileUpdated": false,
          });
          return Future.value(true);
        });
  }
}

class ItemLanguage extends StatelessWidget {
  final String language;
  final int index;

  const ItemLanguage({
    Key? key,
    required this.language,
    this.index = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 8, bottom: 8, left: 15, right: 10),
      decoration: const BoxDecoration(
        color: AppColors.colorGrayBackground,
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            language,
            style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
          ),
          InkWell(
            onTap: () {
              Get.find<DrEditProfileController>().selectedLanguages.remove(language);
            },
            child: const Icon(
              Icons.cancel,
              color: AppColors.colorDarkBlue,
              size: 15,
            ).paddingAll(5),
          ),
        ],
      ),
    );
  }
}

class ItemInstitution extends StatelessWidget {
  final int index;
  final List<DrInstitutionModel> listInstitution;
  final DrEditProfileController controller;

  const ItemInstitution({Key? key, required this.index, required this.listInstitution, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SpaceVertical(15),
        SubItemInstitution(
          title: AppStringKey.labelDegree.tr,
          value: listInstitution[index].degree!,
        ).marginOnly(bottom: 5),
        SubItemInstitution(
          title: AppStringKey.labelInstitutionName.tr,
          value: listInstitution[index].institutionName!,
        ).marginOnly(bottom: 5),
        SubItemInstitution(
          title: AppStringKey.labelAccreditationBody.tr,
          value: listInstitution[index].accreditationBody!,
        ).marginOnly(bottom: 5),
        SubItemInstitution(
          title: AppStringKey.labelCountry.tr,
          value: listInstitution[index].country!,
        ).marginOnly(bottom: 5),
        const SpaceVertical(15),
        const Divider(
          color: AppColors.colorLightSkyBlue,
          thickness: 1,
        ),
        Visibility(
          visible: false,
          child: Container(
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () {
                    Get.toNamed(RoutePaths.DR_ADD_DEGREE, arguments: {
                      'fromAdd': false,
                    });
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    color: AppColors.colorBackground,
                    child: Text(
                      AppStringKey.strEdit.tr,
                      style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    listInstitution.removeAt(index);
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    color: AppColors.colorBackground,
                    child: Text(
                      AppStringKey.strDelete.tr,
                      style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class SubItemInstitution extends StatelessWidget {
  final String title;
  final String value;

  const SubItemInstitution({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Text(
            title,
            style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
          ),
        ),
        Expanded(
          child: Text(
            ': $value',
            style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
          ),
        ),
      ],
    );
  }
}
