import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_endpoints.dart';
import 'package:med_ai/api/api_interface/api_interface.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/models/dr_institution_model.dart';
import 'package:med_ai/models/dr_profile_view_model.dart';
import 'package:med_ai/screens/doctor/dr_setting_flow/dr_settings/dr_settings_controller.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';

import '../../../../routes/route_paths.dart';

class DrEditProfileController extends BaseController {
  var TAG = "DrEditProfileController";
  GlobalKey<FormState> form = GlobalKey();

  String personId = LoginPreferences().sharedPrefRead(StorageKeys.personId);
  String username = LoginPreferences().sharedPrefRead(StorageKeys.username);
  var token = LoginPreferences().sharedPrefRead(StorageKeys.token);

  var fullNameController = TextEditingController().obs;
  var firstNameController = TextEditingController().obs;
  var lastNameController = TextEditingController().obs;
  var genderController = TextEditingController().obs;
  var registeredNoController = TextEditingController().obs;
  var registeredBodyController = TextEditingController().obs;
  var registeredYearController = TextEditingController().obs;
  var workingController = TextEditingController().obs;
  var languageController = TextEditingController().obs;
  var ageController = TextEditingController().obs;
  var mobileController = TextEditingController().obs;
  var emailController = TextEditingController().obs;
  var specializationController = TextEditingController().obs;
  var designationController = TextEditingController().obs;
  var affiliationController = TextEditingController().obs;
  var trainingController = TextEditingController().obs;

  var districtController = TextEditingController().obs;
  var countryController = TextEditingController().obs;
  var referralCodeController = TextEditingController().obs;

  var languagesSpeakController = "".obs;
  var degreeListController = [""].obs;
  DrProfileViewModel doctorProfileViewModel = DrProfileViewModel();

  var newDegreeInstitution = "".obs;
  var newDegreeAccreditation = "".obs;
  var newDegreeCountry = "".obs;
  var newDegreeDegree = "".obs;

  var arrGender = ['Male', 'Female'];

  var arrLanguages = ['Bengali', 'English', 'Hindi', 'Arabic', 'Spanish'];

  var selectedLanguages = [].obs;
  RxList<DrInstitutionModel> listInstitution = RxList();

  @override
  void onInit() {
    super.onInit();
    try {
      doctorProfileViewModel = Get.arguments['doctorProfile'] ?? "";
      setData();
    } catch (e) {
      username.contains("@") ? emailController.value.text = username : mobileController.value.text = username;
      print(e);
    }

    // listInstitution.add(
    //   DrInstitutionModel(id: 1, degree: 'MBBS', institutionName: 'Faridpur Medical College', accreditationBody: 'NWCCU', country: 'NWCCU'),
    // );
    //
    // listInstitution.add(
    //   DrInstitutionModel(id: 1, degree: 'FCPS', institutionName: 'Glasglow', accreditationBody: 'NWCCU', country: 'NWCCU'),
    // );
  }

  void newDegreeAdd(dynamic data) {
    newDegreeInstitution.value = data['institution'];
    newDegreeAccreditation.value = data['accreditation'];
    newDegreeCountry.value = data['country'];
    newDegreeDegree.value = data['degree'];
    listInstitution.add(
      DrInstitutionModel(
        id: 1,
        degree: newDegreeDegree.value,
        institutionName: newDegreeInstitution.value,
        accreditationBody: newDegreeAccreditation.value,
        country: newDegreeCountry.value,
      ),
    );

    print("--------list of Degrees--------");
    for (data in listInstitution) {
      print("${data.id}, ${data.degree}, ${data.institutionName}, ${data.accreditationBody}, ${data.country}");
    }
    print("-------------------------------");
  }

  void setData() {
    firstNameController.value.text = doctorProfileViewModel.doctorProfile!.firstName ?? "";
    lastNameController.value.text = doctorProfileViewModel.doctorProfile!.lastName ?? "";
    genderController.value.text = doctorProfileViewModel.doctorProfile!.gender ?? "";
    registeredNoController.value.text = doctorProfileViewModel.doctorProfile!.regNo ?? "";
    registeredBodyController.value.text = doctorProfileViewModel.doctorProfile!.regNo ?? "";
    registeredYearController.value.text = doctorProfileViewModel.doctorProfile!.regNo ?? "";

    workingController.value.text = doctorProfileViewModel.doctorProfile!.currentInstitute ?? "";
    ageController.value.text = doctorProfileViewModel.doctorProfile!.age.toString();
    mobileController.value.text = doctorProfileViewModel.doctorProfile!.mobile ?? "";
    emailController.value.text = doctorProfileViewModel.doctorProfile!.email ?? "";
    specializationController.value.text = doctorProfileViewModel.doctorProfile!.specialization ?? "";
    designationController.value.text = doctorProfileViewModel.doctorProfile!.designation ?? "";
    affiliationController.value.text = doctorProfileViewModel.doctorProfile!.affiliation ?? "";
    trainingController.value.text = doctorProfileViewModel.doctorProfile!.trainingCourse ?? "";

    districtController.value.text = doctorProfileViewModel.doctorProfile!.district ?? "";
    countryController.value.text = doctorProfileViewModel.doctorProfile!.country ?? "";

    languagesSpeakController.value = doctorProfileViewModel.doctorProfile!.languagesSpeaks ?? "";
    degreeListController.value = doctorProfileViewModel.doctorProfile!.degrees ?? [];

    // print(languagesSpeakController.value.length);
    List<String> languageList = languagesSpeakController.value.split("|");
    for (String data in languageList) {
      // print(data.trim());
      selectedLanguages.add(data.trim());
    }

    // print(degreeListController.length);
    for (String data in degreeListController) {
      List<String> degreeDetails = data.split("|");
      // print(degreeDetails);
      if (degreeDetails.length < 4) {
        degreeDetails.add("");
        degreeDetails.add("");
      }
      listInstitution.add(DrInstitutionModel(
        id: 1,
        degree: degreeDetails[0],
        institutionName: degreeDetails[1],
        accreditationBody: degreeDetails[2],
        country: degreeDetails[3],
      ));
    }
  }

  Future<void> updateProfileSendToServer() async {
    var degree = {};
    var degreeList = [];

    for (DrInstitutionModel data in listInstitution) {
      degree = {};
      degree["degree_name"] = data.degree;
      degree["institute"] = data.institutionName;
      degree["accreditation_body"] = data.accreditationBody;
      degree["country"] = data.country;
      degreeList.add(degree);
    }

    // var requestBody = {};
    // requestBody["person_id"] = int.parse(personId);
    // requestBody["first_name"] = firstNameController.value.text;
    // requestBody["last_name"] = lastNameController.value.text;
    // requestBody["age"] = int.parse(ageController.value.text);
    // requestBody["gender"] = genderController.value.text;
    // requestBody["specialization"] = specializationController.value.text;
    // requestBody["designation"] = designationController.value.text;
    // requestBody["current_institute"] = workingController.value.text;
    // requestBody["phone"] = mobileController.value.text;
    // requestBody["email"] = emailController.value.text;
    // requestBody["affiliation"] = affiliationController.value.text;
    // requestBody["training_course"] = trainingController.value.text;
    // requestBody["degrees"] = degreeList;
    Object requestBody = {
      "person_id": int.parse(personId),
      "first_name": firstNameController.value.text,
      "last_name": lastNameController.value.text,
      "age": int.parse(ageController.value.text),
      "gender": genderController.value.text,
      "specialization": specializationController.value.text,
      "designation": designationController.value.text,
      "current_institute": workingController.value.text,
      "phone": mobileController.value.text,
      "email": emailController.value.text,
      "affiliation": affiliationController.value.text,
      "training_course": trainingController.value.text,
      "degrees": degreeList,
      "languages_speaks": selectedLanguages
    };

    try {
      var responseBody;
      http.Response response = await ApiCallObject().postData(ApiEndpoints.urlDoctorUpdateProfile, requestBody);
      responseBody = jsonDecode(response.body.toString());

      if (responseBody['code'] == "200") {
        String message = responseBody['success_msg'] ?? "";
        message.showSuccessSnackBar();
        await Future.delayed(const Duration(seconds: 3));
        print(message);
        Get.find<DrSettingsController>().getProfileDetailsFromServer();
        Get.back(result: {
          'profileUpdated': true,
        });
      } else {
        var message = responseBody['error_msg'];
        message.toString().showErrorSnackBar();
      }
    } on Exception catch (error) {
      print("Exception: " + error.toString());
      AppStrings.somethingWentWrong.showErrorSnackBar();
      await Future.delayed(const Duration(seconds: 3));
    }
  }

  Future<void> createProfileSendToServer() async {
    var degree = {};
    var degreeList = [];

    if (username.contains("@")) {
      emailController.value.text = username;
    } else {
      mobileController.value.text = username;
    }

    for (DrInstitutionModel data in listInstitution) {
      degree = {};
      degree["degree_name"] = data.degree;
      degree["institute"] = data.institutionName;
      degree["accreditation_body"] = data.accreditationBody;
      degree["country"] = data.country;
      degreeList.add(degree);
    }

    Object requestBody = {
      "username": username,
      "first_name": firstNameController.value.text,
      "last_name": lastNameController.value.text,
      "age": int.parse(ageController.value.text),
      "gender": genderController.value.text,
      "reg_no": registeredNoController.value.text,
      "reg_body": registeredBodyController.value.text,
      "specialization": specializationController.value.text,
      "designation": designationController.value.text,
      "country": countryController.value.text,
      "district": districtController.value.text,
      "languages_speaks": selectedLanguages,
      "phone": mobileController.value.text,
      "affiliation": affiliationController.value.text,
      "training_course": trainingController.value.text,
      "registration_year": registeredYearController.value.text,
      "current_institute": workingController.value.text,
      "email": emailController.value.text,
      "degrees": degreeList,
      "referral_code": referralCodeController.value.text
    };

    try {
      var responseBody;
      http.Response response = await ApiCallObject().postData(ApiEndpoints.urlDoctorCreateProfile, requestBody);
      responseBody = jsonDecode(response.body.toString());

      if (responseBody['code'] == "200") {
        String message = responseBody['success_msg'] ?? "";
        LoginPreferences().sharedPrefWrite(StorageKeys.personId, responseBody['person_id']);
        message.showSuccessSnackBar();
        Get.find<DrSettingsController>().getProfileDetailsFromServer();
        await Future.delayed(const Duration(seconds: 3));
        Get.offNamedUntil(RoutePaths.DR_BOTTOM_TAB, (route) => false);
        print(message);
        // Get.back(result: {
        //   'profileUpdated': true,
        // });
      } else {
        var message = responseBody['error_msg'];
        message.toString().showErrorSnackBar();
      }
    } on Exception catch (error) {
      print("Exception: " + error.toString());
      AppStrings.somethingWentWrong.showErrorSnackBar();
      await Future.delayed(const Duration(seconds: 3));
    }
  }
}
