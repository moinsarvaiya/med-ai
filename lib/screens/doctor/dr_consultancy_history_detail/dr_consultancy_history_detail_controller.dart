import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';

import '../../../models/dr_prescribe_medicine_model.dart';

class DrConsultancyHistoryDetailController extends BaseController {
  final RxList<DrPrescribeMedicineModel> listMedicines = RxList();

  @override
  void onInit() {
    super.onInit();

    listMedicines.add(
      DrPrescribeMedicineModel(
        brandName: 'Napasin',
        dosageType: "Tablet",
        threeTimesDay: 'Hour Difference/ Day',
        hourDifference: '',
        mealInstruction: 'After',
        duration: '7 Days',
        specialInstruction: 'Lorem Ipsum is simply dummy text of the printing and typeset',
      ),
    );
    listMedicines.add(
      DrPrescribeMedicineModel(
        brandName: 'Napasin',
        dosageType: "Tablet",
        threeTimesDay: 'Hour Difference/ Day',
        hourDifference: '',
        mealInstruction: 'After',
        duration: '7 Days',
        specialInstruction: 'Lorem Ipsum is simply dummy text of the printing and typeset',
      ),
    );
    listMedicines.add(
      DrPrescribeMedicineModel(
        brandName: 'Napasin',
        dosageType: "Tablet",
        threeTimesDay: 'Hour Difference/ Day',
        hourDifference: '',
        mealInstruction: 'After',
        duration: '7 Days',
        specialInstruction: 'Lorem Ipsum is simply dummy text of the printing and typeset',
      ),
    );
  }
}
