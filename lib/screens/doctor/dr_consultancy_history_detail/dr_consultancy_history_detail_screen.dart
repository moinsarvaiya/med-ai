import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/space_horizontal.dart';

import '../../../constant/app_images.dart';
import '../../../constant/app_strings_key.dart';
import '../../../constant/base_style.dart';
import '../../../constant/ui_constant.dart';
import '../../../utils/utilities.dart';
import '../../../widgets/custom_appbar.dart';
import '../../../widgets/custom_pager_indicator.dart';
import '../../../widgets/space_vertical.dart';
import 'dr_consultancy_history_detail_controller.dart';

class DrConsultancyHistoryDetailScreen extends GetView<DrConsultancyHistoryDetailController> {
  const DrConsultancyHistoryDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          titleName: AppStringKey.titleConsultancyHistoryDetail.tr,
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            child: Column(
              children: [
                const PatientProfileDetail(
                  patientProfileImage: AppImages.dummyProfile,
                  patientName: "Ricky J",
                  patientState: 'Noakhali',
                  patientCountry: 'Bangladesh',
                  patientAge: '32 Years Old',
                  patientBloodGroup: 'B+ve',
                  patientMobileNumber: '+882854578526',
                ),
                const SpaceVertical(10),
                const ComplaintsSymptomsSection(),
                const SpaceVertical(10),
                const MedicalHistorySection(),
                const SpaceVertical(10),
                const PreExistingConditionSection(),
                const SpaceVertical(10),
                const VitalSignsSection(),
                const SpaceVertical(10),
                const DecisionSection(),
                const SpaceVertical(10),
                CustomPagerIndicator(
                  listMedicines: controller.listMedicines,
                  visibleEdit: false,
                ),
                const SpaceVertical(10),
                const PrescribedDiagnosticsTestSection(),
                const SpaceVertical(25),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 5,
                  ),
                  child: SubmitButton(
                    title: AppStringKey.back.tr,
                    onClick: () {
                      Get.back();
                    },
                  ),
                ),
                const SpaceVertical(10),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PrescribedDiagnosticsTestSection extends StatelessWidget {
  const PrescribedDiagnosticsTestSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardSection(
      title: AppStringKey.strPrescribedDiagnostics.tr,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          valueText('CBC'),
          GestureDetector(
            onTap: () {
              Get.toNamed(RoutePaths.DR_DIAGNOSTICS_TEST_IMAGES);
            },
            child: Image.asset(
              AppImages.diagnosticTest,
              height: 20,
              width: 20,
            ),
          )
        ],
      ),
    );
  }
}

class DecisionSection extends StatelessWidget {
  const DecisionSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardSection(
      title: AppStringKey.strDecision.tr,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PatientHistoryItemWidget(
            titleText: AppStringKey.strIdentifiedDisease.tr,
            value: 'XYZ',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strClinicalNote.tr,
            value: 'XYZ',
          ),
        ],
      ),
    );
  }
}

class RecommendationsSection extends StatelessWidget {
  const RecommendationsSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardSection(
      title: AppStringKey.strRecommendations.tr,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PatientHistoryItemWidget(
            titleText: AppStringKey.strProvisionalDiagnosis.tr,
            value: 'Chronic obstructive pulmonary disease ',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strRecommendedMedicines.tr,
            value: 'Unknown',
          ),
        ],
      ),
    );
  }
}

class VitalSignsSection extends StatelessWidget {
  const VitalSignsSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardSection(
      title: AppStringKey.strVitalSigns.tr,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PatientHistoryItemWidget(
            titleText: AppStringKey.strBP.tr,
            value: '80/120',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strTemp.tr,
            value: '98’ C',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strPulseRate.tr,
            value: 'Unknown',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strBreathingsRate.tr,
            value: 'Unknown',
          ),
        ],
      ),
    );
  }
}

class PreExistingConditionSection extends StatelessWidget {
  const PreExistingConditionSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardSection(
      title: AppStringKey.strPreConditions.tr,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PatientHistoryItemWidget(
            titleText: AppStringKey.strMedicalCondition.tr,
            value: 'No available data',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strMedication.tr,
            value: '',
          ),
        ],
      ),
    );
  }
}

class MedicalHistorySection extends StatelessWidget {
  const MedicalHistorySection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardSection(
      title: AppStringKey.strMedicalHistory.tr,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PatientHistoryItemWidget(
            titleText: AppStringKey.strMedicalCondition.tr,
            value: 'No available data',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strAllergies.tr,
            value: 'Unknown',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strFamilyHistory.tr,
            value: 'No available data',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strPersonalHistory.tr,
            value: 'No available data',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strMedicalCondition.tr,
            value: 'Unknown',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strMedication.tr,
            value: 'No available data',
          ),
        ],
      ),
    );
  }
}

class ComplaintsSymptomsSection extends StatelessWidget {
  const ComplaintsSymptomsSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardSection(
      title: AppStringKey.strComplaintsSymptoms.tr,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          labelText(AppStringKey.strChiefComplaints.tr),
          const SpaceVertical(5),
          valueText('Dry cough'),
          const SpaceVertical(10),
          labelText(AppStringKey.strSymptoms.tr),
          const SpaceVertical(5),
          valueText('Dry cough'),
        ],
      ),
    );
  }
}

class PatientHistoryItemWidget extends StatelessWidget {
  final String titleText;
  final String value;

  const PatientHistoryItemWidget({
    Key? key,
    required this.titleText,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(child: labelText(titleText)),
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(': '),
              Expanded(child: valueText(value)),
            ],
          ),
        ),
      ],
    );
  }
}

class PatientProfileDetail extends StatelessWidget {
  final String patientProfileImage;
  final String patientName;
  final String patientState;
  final String patientCountry;
  final String patientAge;
  final String patientMobileNumber;
  final String patientBloodGroup;

  const PatientProfileDetail({
    Key? key,
    required this.patientProfileImage,
    required this.patientName,
    required this.patientState,
    required this.patientCountry,
    required this.patientAge,
    required this.patientMobileNumber,
    required this.patientBloodGroup,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 5,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 20,
      ),
      width: double.maxFinite,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 3,
            blurRadius: 3,
            offset: const Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 55,
            width: 55,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              image: const DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(
                  AppImages.dummyProfile,
                ),
              ),
            ),
          ),
          const SpaceVertical(10),
          Text(
            patientName,
            style: BaseStyle.textStyleDomineBold(
              16,
              AppColors.colorDarkBlue,
            ),
          ),
          const SpaceVertical(4),
          Text(
            '$patientAge, $patientBloodGroup',
            style: BaseStyle.textStyleNunitoSansRegular(
              12,
              AppColors.colorDarkBlue,
            ),
          ),
          const SpaceVertical(4),
          Text(
            '$patientState, $patientCountry',
            style: BaseStyle.textStyleNunitoSansRegular(
              12,
              AppColors.colorDarkBlue,
            ),
          ),
          const SpaceVertical(4),
          IntrinsicWidth(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    CustomCardWidget(
                      shadowOpacity: shadow,
                      widget: const Padding(
                        padding: EdgeInsets.all(5),
                        child: Icon(
                          Icons.phone_android_outlined,
                          color: AppColors.colorDarkBlue,
                          size: 22,
                        ),
                      ),
                    ),
                    const SpaceHorizontal(10),
                    Text(
                      patientMobileNumber,
                      style: BaseStyle.textStyleNunitoSansRegular(
                        12,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                  ],
                ),
                const SpaceVertical(4),
                Row(
                  children: [
                    CustomCardWidget(
                      shadowOpacity: shadow,
                      widget: const Padding(
                        padding: EdgeInsets.all(5),
                        child: Icon(
                          Icons.email,
                          color: AppColors.colorDarkBlue,
                          size: 22,
                        ),
                      ),
                    ),
                    const SpaceHorizontal(10),
                    Text(
                      '-',
                      style: BaseStyle.textStyleNunitoSansRegular(
                        12,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class CardSection extends StatelessWidget {
  final String title;
  final Widget child;

  const CardSection({
    Key? key,
    required this.title,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Container(
        width: double.maxFinite,
        padding: const EdgeInsets.symmetric(
          vertical: 15,
          horizontal: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            headingText(title),
            const SizedBox(height: 10),
            child,
          ],
        ),
      ),
    );
  }
}

Text headingText(String text) {
  return Text(
    text,
    style: BaseStyle.textStyleNunitoSansBold(
      14,
      AppColors.colorSkyBlue,
    ),
  );
}

Text labelText(String text) {
  return Text(
    text,
    style: BaseStyle.textStyleNunitoSansBold(
      12,
      AppColors.colorDarkBlue,
    ),
  );
}

Text valueText(String text) {
  return Text(
    text,
    style: BaseStyle.textStyleNunitoSansRegular(
      12,
      AppColors.colorDarkBlue,
    ),
  );
}
