import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/screens/doctor/dr_home/dr_home_screen.dart';

import '../dr_appointments/dr_new_consultancy/dr_new_consultancy_screen.dart';
import '../dr_setting_flow/dr_settings/dr_settings_screen.dart';
import '../dr_wallet_flow/dr_my_wallet/dr_my_wallet_screen.dart';

class DrBottomNavigationController extends BaseController {
  var tabIndex = 0.obs;
  double iconSize = 24;
  var backPressDetected = false.obs;
  EdgeInsetsGeometry margin = const EdgeInsets.only(bottom: 5);

  void changeTabIndex(int index) {
    tabIndex.value = index;
    if (tabIndex.value == 1) {
      consultancyFrom = EnumConsultancyFrom.APPOINTMENT_TAB;
    }
  }

  var pages = [
    const DrHomeScreen(),
    const DrNewConsultancyScreen(),
    const DrSettingsScreen(),
    const DrMyWalletScreen(),
  ];

  timer() async {
    var duration = const Duration(seconds: 2);
    return Timer(duration, data);
  }

  data() {
    backPressDetected.value = false;
  }
}
