import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';

import '../../../utils/utilities.dart';
import 'dr_bottom_navigation_controller.dart';

class DrBottomNavigation extends GetView<DrBottomNavigationController> {
  const DrBottomNavigation({super.key});

  buildBottomNavigationMenu(context, landingPageController) {
    return Obx(
      () => MediaQuery(
        data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        child: SizedBox(
          height: Platform.isAndroid ? BaseSize.height(9) : BaseSize.height(12),
          child: BottomNavigationBar(
            showUnselectedLabels: true,
            showSelectedLabels: true,
            onTap: landingPageController.changeTabIndex,
            currentIndex: landingPageController.tabIndex.value,
            backgroundColor: AppColors.colorDarkBlue,
            type: BottomNavigationBarType.fixed,
            unselectedItemColor: AppColors.white,
            selectedItemColor: AppColors.colorSky,
            unselectedLabelStyle: BaseStyle.textStyleNunitoSansRegular(8, AppColors.white),
            selectedLabelStyle: BaseStyle.textStyleNunitoSansRegular(8, AppColors.colorSky),
            items: [
              BottomNavigationBarItem(
                icon: Container(
                  margin: controller.margin,
                  child: Image.asset(
                    AppImages.home,
                    height: controller.iconSize,
                    color: controller.tabIndex.value == 0 ? AppColors.colorSky : AppColors.white,
                  ),
                ),
                label: AppStringKey.labelHome.tr.toUpperCase(),
              ),
              BottomNavigationBarItem(
                icon: Container(
                  margin: controller.margin,
                  child: Image.asset(
                    AppImages.appointment,
                    height: controller.iconSize,
                    color: controller.tabIndex.value == 1 ? AppColors.colorSky : AppColors.white,
                  ),
                ),
                label: AppStringKey.labelAppointment.tr.toUpperCase(),
              ),
              BottomNavigationBarItem(
                icon: Container(
                  margin: controller.margin,
                  child: Image.asset(
                    AppImages.settings,
                    height: controller.iconSize,
                    color: controller.tabIndex.value == 2 ? AppColors.colorSky : AppColors.white,
                  ),
                ),
                label: AppStringKey.labelSettings.tr.toUpperCase(),
              ),
              BottomNavigationBarItem(
                icon: Container(
                  margin: controller.margin,
                  child: Image.asset(
                    AppImages.walletTab,
                    height: controller.iconSize,
                    color: controller.tabIndex.value == 3 ? AppColors.colorSky : AppColors.white,
                  ),
                ),
                label: AppStringKey.labelMyWallet.tr.toUpperCase(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (controller.tabIndex.value != 0) {
          controller.tabIndex.value = 0;
          return Future.value(false);
        } else {
          controller.timer();
          if (controller.backPressDetected.value) {
            return Future.value(true);
          }
          Utilities.showExitMessage(AppStringKey.labelPressAgainToExit.tr);
          controller.backPressDetected.value = true;
          return Future.value(false);
        }
      },
      child: Scaffold(
        bottomNavigationBar: buildBottomNavigationMenu(context, controller),
        body: Obx(
          () => IndexedStack(
            index: controller.tabIndex.value,
            children: controller.pages,
          ),
        ),
      ),
    );
  }
}
