import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../bindings/base_controller.dart';
import '../../../constant/app_images.dart';
import '../../../constant/app_strings_key.dart';
import '../../../utils/functions/permission_check.dart';
import '../../../utils/video_call.dart';

class DrHomeController extends BaseController {
  var isRevenueUp = true.obs;

  var name = ''.obs;
  RxBool availability = false.obs;

  var listImages = [
    AppImages.drMyPatients,
    AppImages.drAddPatients,
    AppImages.drConsultancyHistory,
  ];
  var tagList = [
    AppStringKey.labelMyPatients.tr,
    AppStringKey.labelAddNewPatients.tr,
    AppStringKey.titleConsultancyHistory.tr,
  ];
  var pagesList = [
    RoutePaths.DR_MY_PATIENT,
    RoutePaths.DR_CREATE_PATIENT,
    RoutePaths.DR_CONSULTANCY_HISTORY,
  ];

  @override
  void onInit() {
    super.onInit();
    name.value = LoginPreferences().sharedPrefRead(StorageKeys.name);
  }

  Future<void> onJoin(BuildContext context) async {
    CheckPermission.check([Permission.camera, Permission.microphone], "storage");
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const VideoCall(),
      ),
    );
  }
}
