import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/doctor/dr_home/dr_home_controller.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/home_items.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:med_ai/widgets/space_vertical.dart';
import 'package:med_ai/widgets/subtitle_widget.dart';
import 'package:pie_chart/pie_chart.dart';

import '../../../constant/ui_constant.dart';
import '../../../utils/local_notification_service.dart';

class DrHomeScreen extends GetView<DrHomeController> {
  const DrHomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(65),
          child: Column(
            children: [
              const SpaceVertical(10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 32,
                    height: 32,
                    child: ClipOval(
                      child: Image.network(
                        dummyImageUrl,
                        width: 32,
                        height: 32,
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return Utilities.getPlaceHolder(
                            width: 32,
                            height: 32,
                          );
                        },
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      // Get.toNamed(RoutePaths.DR_NOTIFICATION);
                      // Get.put(CallController());
                      // Get.find<CallController>().initilize();
                      // controller.onJoin(context);
                      LocalNotificationService.display();
                    },
                    child: Stack(
                      children: [
                        Image.asset(
                          AppImages.notification,
                          height: 24,
                          width: 24,
                        ),
                        Positioned(
                          right: 2,
                          top: 0,
                          child: Image.asset(
                            AppImages.notificationDot,
                            height: 8,
                            width: 8,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ).paddingSymmetric(horizontal: 20),
              const Divider(
                color: AppColors.colorLightSkyBlue,
              ),
            ],
          ),
        ),
        backgroundColor: AppColors.white,
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SpaceVertical(BaseSize.height(1)),
                Column(
                  children: [
                    Obx(
                      () => Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                controller.name.value,
                                style: BaseStyle.textStyleNunitoSansBold(
                                  14,
                                  AppColors.colorDarkBlue,
                                ),
                              ),
                              const SpaceVertical(10),
                              controller.availability.value.toString() == "true"
                                  ? Container(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 15,
                                        vertical: 8,
                                      ),
                                      decoration: const BoxDecoration(
                                        color: AppColors.colorGreen,
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(3),
                                        ),
                                      ),
                                      child: Text(
                                        AppStringKey.strAvailable.tr,
                                      ),
                                    )
                                  : Container(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 15,
                                        vertical: 8,
                                      ),
                                      decoration: const BoxDecoration(
                                        color: AppColors.colorYellow,
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(3),
                                        ),
                                      ),
                                      child: Text(
                                        AppStringKey.strUnavailable.tr,
                                      ),
                                    )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                Utilities.getDay(),
                                style: BaseStyle.textStyleNunitoSansSemiBold(
                                  14,
                                  AppColors.colorDarkBlue,
                                ),
                              ),
                              const SpaceVertical(5),
                              Text(
                                Utilities.getTodayDate(),
                                style: BaseStyle.textStyleNunitoSansBold(
                                  14,
                                  AppColors.colorDarkBlue,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const SpaceVertical(18),
                    Container(
                      height: 45,
                      decoration: BoxDecoration(
                        color: AppColors.colorGrayBackground,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: TextField(
                        showCursor: false,
                        onTap: () {
                          Get.toNamed(RoutePaths.GLOBAL_SEARCH);
                        },
                        cursorColor: AppColors.colorDarkBlue,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(
                            Icons.search,
                            color: AppColors.colorGreen,
                            size: 20,
                          ),
                          hintText: AppStringKey.hintSearchMedicine.tr,
                          fillColor: AppColors.transparent,
                          hintStyle: BaseStyle.textStyleNunitoSansRegular(
                            12,
                            AppColors.hintColor,
                          ),
                        ),
                        readOnly: true,
                        autocorrect: false,
                        enableSuggestions: false,
                        style: BaseStyle.textStyleNunitoSansSemiBold(
                          14,
                          AppColors.colorDarkBlue,
                        ),
                        maxLines: 1,
                      ),
                    ),
                    const SpaceVertical(20),
                    SubTitleWidget(
                      subTitle: AppStringKey.labelStatisticalSummary.tr,
                    ),
                    const SpaceVertical(20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Expanded(child: DrPieChart()),
                        SpaceHorizontal(BaseSize.width(4)),
                        const Expanded(child: DrProgressBar()),
                      ],
                    ),
                    const SpaceVertical(20),
                    SubTitleWidget(
                      subTitle: AppStringKey.labelMyPatientsAndAppointment.tr,
                    ),
                    const SpaceVertical(20),
                    SizedBox(
                      height: BaseSize.height(14),
                      child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          final finalIndex = index % controller.listImages.length;
                          return HomeItems(
                            label: controller.tagList[finalIndex].tr,
                            image: controller.listImages[finalIndex],
                            onClick: () {
                              Get.toNamed(controller.pagesList[finalIndex]);
                            },
                          );
                        },
                      ),
                    )
                  ],
                ).paddingSymmetric(horizontal: 20),
              ],
            ).paddingOnly(bottom: 20),
          ),
        ),
      ),
    );
  }
}

class DrPieChart extends StatelessWidget {
  const DrPieChart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: BaseSize.height(25),
      decoration: BoxDecoration(
        color: AppColors.colorGrayBackground,
        borderRadius: BorderRadius.circular(9),
      ),
      padding: EdgeInsets.symmetric(vertical: BaseSize.height(1), horizontal: BaseSize.width(2)),
      child: Column(
        children: [
          PieChart(
            dataMap: const {
              'Male': 3,
              'Female': 5,
            },
            animationDuration: const Duration(milliseconds: 1000),
            chartLegendSpacing: 32,
            chartRadius: BaseSize.width(25),
            colorList: const [
              AppColors.colorPink,
              AppColors.colorYellow,
            ],
            initialAngleInDegree: 80,
            chartType: ChartType.ring,
            ringStrokeWidth: 8,
            centerText: "",
            legendOptions: const LegendOptions(
              showLegendsInRow: false,
              legendPosition: LegendPosition.right,
              showLegends: false,
              legendTextStyle: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            chartValuesOptions: const ChartValuesOptions(
              showChartValueBackground: true,
              showChartValues: false,
              showChartValuesInPercentage: false,
              showChartValuesOutside: false,
              decimalPlaces: 1,
            ),
            degreeOptions: const DegreeOptions(
              initialAngle: 0,
              totalDegrees: 360,
            ),
          ),
          SpaceVertical(BaseSize.height(1)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Legends(
                    title: AppStringKey.strMale.tr,
                    color: AppColors.colorPink,
                  ),
                  SpaceVertical(BaseSize.height(1)),
                  Text(
                    '60',
                    style: BaseStyle.textStyleNunitoSansBold(
                      14,
                      AppColors.colorDarkBlue,
                    ),
                  )
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Legends(
                    title: AppStringKey.strFemale.tr,
                    color: AppColors.colorYellow,
                  ),
                  const SpaceVertical(5),
                  Text(
                    '42',
                    style: BaseStyle.textStyleNunitoSansBold(
                      14,
                      AppColors.colorDarkBlue,
                    ),
                  )
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}

class DrProgressBar extends StatelessWidget {
  const DrProgressBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: BaseSize.height(25),
      decoration: BoxDecoration(
        color: AppColors.colorGrayBackground,
        borderRadius: BorderRadius.circular(9),
      ),
      padding: const EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const ItemProgressBar(
            percentage: 0.4,
            progressColor: AppColors.colorDarkBlue,
          ),
          SpaceVertical(BaseSize.height(2)),
          const ItemProgressBar(
            percentage: 0.8,
            progressColor: AppColors.colorGreen,
          ),
          SpaceVertical(BaseSize.height(2)),
          const ItemProgressBar(
            percentage: 0.6,
            progressColor: AppColors.colorYellow,
          ),
          SpaceVertical(BaseSize.height(3)),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Legends(
                    title: AppStringKey.strPatients.tr,
                    color: AppColors.colorDarkBlue,
                  ),
                  Text(
                    '+5.20%',
                    style: BaseStyle.textStyleNunitoSansBold(
                      10,
                      AppColors.colorDarkBlue,
                    ),
                  )
                ],
              ),
              SpaceVertical(BaseSize.height(1)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Legends(
                    title: AppStringKey.strRevenue.tr,
                    color: AppColors.colorGreen,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '1254',
                        style: BaseStyle.textStyleNunitoSansBold(
                          10,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      Icon(Get.find<DrHomeController>().isRevenueUp.value ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                          size: 12, color: AppColors.colorDarkBlue),
                    ],
                  )
                ],
              ),
              SpaceVertical(BaseSize.height(1)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Legends(
                    title: AppStringKey.strCapacity.tr,
                    color: AppColors.colorYellow,
                  ),
                  Text(
                    '25.03%',
                    style: BaseStyle.textStyleNunitoSansBold(
                      10,
                      AppColors.colorDarkBlue,
                    ),
                  )
                ],
              ),
            ],
          ).paddingSymmetric(horizontal: 6)
        ],
      ),
    );
  }
}

class ItemProgressBar extends StatelessWidget {
  final double percentage;
  final Color progressColor;

  const ItemProgressBar({
    Key? key,
    required this.percentage,
    required this.progressColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GFProgressBar(
      percentage: percentage,
      lineHeight: 10,
      animationDuration: 1000,
      backgroundColor: AppColors.colorDarkBlue.withOpacity(0.2),
      progressBarColor: progressColor,
    );
  }
}

class Legends extends StatelessWidget {
  final String title;
  final Color color;

  const Legends({Key? key, required this.title, required this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircleAvatar(
          minRadius: 3,
          backgroundColor: color,
        ),
        const SpaceHorizontal(5),
        Text(
          title,
          style: BaseStyle.textStyleNunitoSansBold(
            10,
            AppColors.colorDarkBlue,
          ),
        )
      ],
    );
  }
}
