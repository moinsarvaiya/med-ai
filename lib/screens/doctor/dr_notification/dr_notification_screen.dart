import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/screens/doctor/dr_notification/dr_notification_controller.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/space_vertical.dart';

class DrNotificationScreen extends GetView<DrNotificationController> {
  const DrNotificationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          titleName: AppStringKey.titleNotifications.tr,
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: 5,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, i) {
                    return NotificationItem(
                      title: 'Your blood test is ready',
                      content:
                          'Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                      date: '23 Jan 2023',
                      imageUrl: controller.imageUrls[i],
                      isRead: controller.arrRead[i],
                    );
                  }),
              const Divider(
                height: 1,
                color: AppColors.colorLightSkyBlue,
              )
            ],
          ).marginOnly(bottom: 30, top: 20),
        ),
      ),
    );
  }
}

class NotificationItem extends StatelessWidget {
  final String title;
  final String content;
  final String date;
  final String imageUrl;
  final bool isRead;

  const NotificationItem({
    Key? key,
    required this.title,
    required this.content,
    required this.date,
    this.imageUrl = '',
    this.isRead = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: isRead ? AppColors.white : AppColors.colorGrayBackground,
      child: Column(
        children: [
          const Divider(
            height: 1,
            color: AppColors.colorLightSkyBlue,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: BaseStyle.textStyleDomineBold(
                        14,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    const SpaceVertical(10),
                    Text(
                      content,
                      style: BaseStyle.textStyleNunitoSansRegular(
                        12,
                        AppColors.colorTextDarkGray,
                      ),
                      maxLines: 5,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: BaseSize.width(30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    imageUrl != ''
                        ? CustomCardWidget(
                            borderRadius: 10,
                            widget: Image.network(
                              imageUrl,
                              height: 80,
                              width: 80,
                              fit: BoxFit.cover,
                              errorBuilder: (context, error, stackTrace) {
                                return Utilities.getPlaceHolder(
                                  height: 80,
                                  width: 80,
                                );
                              },
                            ),
                          )
                        : Container(
                            padding: const EdgeInsets.all(12),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: isRead ? AppColors.colorGrayBackground : AppColors.white,
                            ),
                            child: Image.asset(
                              AppImages.notificationFill,
                              height: 15,
                              width: 15,
                            ),
                          ),
                  ],
                ),
              ),
            ],
          ).paddingOnly(top: 15, bottom: 8, left: 20, right: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                date,
                style: BaseStyle.textStyleNunitoSansRegular(
                  10,
                  AppColors.colorTextDarkGray,
                ),
                maxLines: 10,
                overflow: TextOverflow.ellipsis,
              ),
              isRead
                  ? Container()
                  : Container(
                      padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                      decoration: BoxDecoration(
                        color: AppColors.colorMint,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Text(
                        AppStringKey.strUnread.tr,
                        style: BaseStyle.textStyleNunitoSansRegular(
                          10,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    )
            ],
          ).paddingOnly(
            top: 8,
            bottom: 15,
            left: 20,
            right: 20,
          ),
        ],
      ),
    );
  }
}
