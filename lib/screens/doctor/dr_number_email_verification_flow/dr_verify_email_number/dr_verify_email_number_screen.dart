import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../utils/validators/validators.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_round_text_field.dart';
import 'dr_verify_email_number_controller.dart';

class DrVerifyEmailNumberScreen extends GetView<DrVerifyEmailNumberController> {
  const DrVerifyEmailNumberScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(left: 20, right: 20, bottom: MediaQuery.viewInsetsOf(context).bottom + 15, top: 10),
        child: SubmitButton(
          title: AppStringKey.labelGetOtp.tr,
          onClick: () {
            Get.toNamed(RoutePaths.DR_OTP_VERIFICATION);
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SpaceVertical(40),
                Text(
                  controller.isFromNumber ? AppStringKey.strEnterYourMobile.tr : AppStringKey.strEnterYourEmail.tr,
                  style: BaseStyle.textStyleNunitoSansRegular(
                    14,
                    AppColors.colorDarkBlue,
                  ),
                ),
                const SpaceVertical(15),
                CustomRoundTextField(
                  labelText: '',
                  labelVisible: false,
                  isRequireField: true,
                  textEditingController: controller.emailNumberController.value,
                  hintText: controller.isFromNumber ? AppStringKey.hintEnterYourMobile.tr : AppStringKey.hintEnterYourEmail.tr,
                  backgroundColor: AppColors.transparent,
                  validator: Validators.emailValidator,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
