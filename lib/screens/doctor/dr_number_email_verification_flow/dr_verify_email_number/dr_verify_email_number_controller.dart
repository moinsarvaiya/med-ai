import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';

class DrVerifyEmailNumberController extends BaseController {
  var emailNumberController = TextEditingController().obs;
  var isFromNumber = false;

  @override
  void onInit() {
    super.onInit();
    isFromNumber = Get.arguments['isFromNumber'];
  }
}
