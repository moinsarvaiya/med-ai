import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/space_vertical.dart';
import 'package:otp_pin_field/otp_pin_field.dart';

import 'dr_otp_verification_controller.dart';

class DrOtpVerificationScreen extends GetView<DrOtpVerificationController> {
  const DrOtpVerificationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(left: 20, right: 20, bottom: MediaQuery.viewInsetsOf(context).bottom + 15, top: 10),
        child: SubmitButton(
          title: AppStringKey.labelVerify.tr,
          onClick: () {
            Get.close(2);
            Get.offNamed(RoutePaths.DR_PROFILE);
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SpaceVertical(MediaQuery.sizeOf(context).height / 10),
              Center(
                child: Text(
                  AppStringKey.hintOtp.tr,
                  style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorTextDarkGray),
                ),
              ),
              SpaceVertical(MediaQuery.sizeOf(context).height / 25),
              OtpPinField(
                key: controller.otpPinFieldController,
                onSubmit: (text) {
                  controller.verificationCode.value = text;
                },
                onChange: (text) {},
                autoFillEnable: true,
                textInputAction: TextInputAction.done,
                onCodeChanged: (code) {},
                maxLength: 6,
                otpPinFieldStyle: OtpPinFieldStyle(
                  activeFieldBorderColor: AppColors.colorDarkBlue,
                  defaultFieldBorderColor: AppColors.colorDarkBlue,
                  fieldPadding: 15,
                  textStyle: BaseStyle.textStyleNunitoSansRegular(
                    20,
                    AppColors.colorDarkBlue,
                  ),
                ),
                showCursor: true,
                cursorColor: AppColors.colorDarkBlue,
                showCustomKeyboard: false,
                cursorWidth: 1.5,
                mainAxisAlignment: MainAxisAlignment.center,
                fieldWidth: 25,
                fieldHeight: 40,
                otpPinFieldDecoration: OtpPinFieldDecoration.underlinedPinBoxDecoration,
              ),
              SpaceVertical(MediaQuery.sizeOf(context).height / 25),
              Obx(
                () => controller.duration.value == 0
                    ? GestureDetector(
                        onTap: () {
                          controller.duration.value = controller.startingDuration;
                          controller.startTimer();
                        },
                        child: Center(
                          child: Text(
                            AppStringKey.labelResendOtp.tr,
                            style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorTextDarkGray),
                          ),
                        ),
                      )
                    : Center(
                        child: Text(
                          '(${Utilities.formatHHMMSSTimer(controller.duration.value)})',
                          style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorTextDarkGray),
                        ),
                      ),
              ),
              SpaceVertical(MediaQuery.sizeOf(context).height / 20),
              Center(
                child: Text(
                  AppStringKey.labelDoNotShareOTP.tr,
                  style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorTextDarkGray),
                ),
              ),
            ],
          ).paddingSymmetric(horizontal: 20),
        ),
      ),
    );
  }
}
