import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/utils/validators/validators.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_dropdown_field.dart';
import 'package:med_ai/widgets/custom_round_text_field.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../widgets/custom_buttons.dart';
import 'dr_set_withdrawal_method_controller.dart';

class DrSetWithdrawalMethodScreen extends GetView<DrSetWithdrawalMethodController> {
  const DrSetWithdrawalMethodScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          onClick: () {
            Get.back();
          },
          isDividerVisible: true,
        ),
      ),
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.btnSetMethod.tr,
        onClick: () {
          //Get.close(1);
          //Get.toNamed(RoutePaths.DR_SEE_WITHDRAWL_METHODS);
          if (controller.methodTypeController.value.text.isEmpty) {
            CustomFunctions.customSnackBar(AppStringKey.labelSelectMethod.tr, AppStringKey.chooseFromDropDown.tr);
          } else if (controller.accountNameController.value.text.isEmpty) {
            CustomFunctions.customSnackBar(AppStringKey.labelAccountName.tr, AppStringKey.cantBeEmpty.tr);
          } else if (controller.accountNumberController.value.text.isEmpty) {
            CustomFunctions.customSnackBar(AppStringKey.labelAccountNumber.tr, AppStringKey.cantBeEmpty.tr);
          } else if (controller.methodSaveAsController.value.text.isEmpty) {
            CustomFunctions.customSnackBar(AppStringKey.labelMethodSaveAs.tr, AppStringKey.cantBeEmpty.tr);
          } else {
            //controller.setWithdrawalMethodDetails();
          }
        },
      ).paddingOnly(left: 20, right: 20, top: 10, bottom: MediaQuery.viewInsetsOf(context).bottom + 20),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStringKey.titleSetWithdrawalMethod.tr,
                style: BaseStyle.textStyleNunitoSansBold(18, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(15),
              CustomDropDownField(
                hintTitle: AppStringKey.labelSelectMethod.tr,
                items: controller.arrMethods,
                validationText: AppStringKey.validationSelectMethod.tr,
                onChanged: (val) {
                  controller.methodTypeController.value.text = val!;
                  controller.setMethodTypeDetails(val);
                  //print("SetMetod: ValueChanged: "+val.toString());
                },
                labelText: AppStringKey.labelSelectMethod.tr,
              ),
              const SpaceVertical(20),
              CustomRoundTextField(
                  labelText: AppStringKey.labelAccountName.tr,
                  isRequireField: true,
                  hintText: AppStringKey.labelAccountName.tr,
                  keyBoardAction: TextInputAction.next,
                  backgroundColor: AppColors.transparent,
                  validator: Validators.emptyValidator,
                  textEditingController: controller.accountNameController.value),
              const SpaceVertical(20),
              CustomRoundTextField(
                  labelText: AppStringKey.labelAccountNumber.tr,
                  isRequireField: true,
                  hintText: AppStringKey.labelAccountNumber.tr,
                  keyBoardAction: TextInputAction.next,
                  textInputType: TextInputType.number,
                  inputFormatter: inputFormatterDigit,
                  backgroundColor: AppColors.transparent,
                  validator: Validators.emptyValidator,
                  textEditingController: controller.accountNumberController.value),
              const SpaceVertical(20),
              CustomRoundTextField(
                  labelText: AppStringKey.labelMethodSaveAs.tr,
                  isRequireField: true,
                  hintText: AppStringKey.hintMethodSaveAs.tr,
                  keyBoardAction: TextInputAction.done,
                  backgroundColor: AppColors.transparent,
                  validator: Validators.emptyValidator,
                  textEditingController: controller.methodSaveAsController.value),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}
