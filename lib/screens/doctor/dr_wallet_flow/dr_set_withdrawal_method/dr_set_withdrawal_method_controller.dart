import 'dart:convert';
import 'dart:developer' as devLog;

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_endpoints.dart';
import 'package:med_ai/api/api_interface/api_interface.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/screens/doctor/dr_wallet_flow/dr_see_withdrawal_methods/dr_see_withdrawal_methods_controller.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';
import 'package:quickalert/quickalert.dart';

class DrSetWithdrawalMethodController extends BaseController {
  static const String TAG = "SetWithdrawalMethod: ";
  var arrMethods = [
    'MFS - BKASH',
  ];
  var methodTypeController = TextEditingController().obs;
  var accountNameController = TextEditingController().obs;
  var accountNumberController = TextEditingController().obs;
  var methodSaveAsController = TextEditingController().obs;

  ApiCallObject api = ApiCallObject();
  String token = LoginPreferences().sharedPrefRead(StorageKeys.token);
  String personID = LoginPreferences().sharedPrefRead(StorageKeys.personId);

  var bankName = "";
  var branchName = "";
  var routingNumber = "";
  var mfsName = "";

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  void setMethodTypeDetails(methodName) {
    if (methodName.toString() == "MFS - BKASH") {
      mfsName = "bKash";
      methodTypeController.value.text = "mfs";
    }
  }

  Future<void> setWithdrawalMethodDetails() async {
    http.Response serverResponse;
    Object requestBody = {
      "person_id": personID,
      "payment_method_name": methodSaveAsController.value.text,
      "payment_method_type": methodTypeController.value.text,
      "account_name": accountNameController.value.text,
      "account_number": accountNumberController.value.text,
      "bank_name": bankName,
      "branch_name": branchName,
      "routing_number": routingNumber,
      "mfs_name": mfsName
    };

    serverResponse = await api.postData(ApiEndpoints.urlDoctorSetWalletMethod, requestBody);
    devLog.log("StatusCode: ${serverResponse.statusCode}", name: TAG);
    if (serverResponse.statusCode == 200) {
      var responseBody = jsonDecode(serverResponse.body);
      var resCode = responseBody['code'];
      if (resCode == "200") {
        CustomFunctions.customSnackBar("Congratulations!", "Wallet Added Successfully!");
        Get.find<DrSeeWithdrawalMethodsController>().getWithdrawalMethodListFromServer();
        Get.back();
      } else {
        QuickAlert.show(
            context: Get.context!,
            type: QuickAlertType.error,
            text: responseBody['error_msg'],
            onConfirmBtnTap: () {
              Get.close(0);
            });
      }
    }
  }
}
