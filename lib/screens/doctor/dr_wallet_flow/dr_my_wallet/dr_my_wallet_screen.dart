import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../models/dr_earning_history_model.dart';
import '../../../../models/dr_withdrawal_history_model.dart';
import 'dr_my_wallet_controller.dart';

class DrMyWalletScreen extends GetView<DrMyWalletController> {
  const DrMyWalletScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          titleName: AppStringKey.labelMyWallet.tr,
          isBackButtonVisible: false,
          isDividerVisible: true,
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 25),
        child: SubmitButton(
          title: AppStringKey.btnWithdrawBalance.tr,
          onClick: () {
            Get.toNamed(RoutePaths.DR_SEE_WITHDRAWAL_METHODS);
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SpaceVertical(20),
              Text(
                AppStringKey.titleMyWallet.tr,
                style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(15),
              Obx(
                () => Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ItemWallet(
                      image: AppImages.walletEarning,
                      title: AppStringKey.labelTotalEarning.tr,
                      amount: "${controller.totalEarned.value} BDT",
                    ),
                    ItemWallet(
                      image: AppImages.walletWithdrawal,
                      title: AppStringKey.labelTotalWithdrawal.tr,
                      amount: "${controller.totalWithdrawn.value} BDT",
                    ),
                    ItemWallet(
                      image: AppImages.walletImage,
                      title: AppStringKey.labelAvailableBalance.tr,
                      amount: "${controller.totalBalance.value} BDT",
                    ),
                  ],
                ),
              ),
              const SpaceVertical(20),
              Obx(
                () => Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          controller.earningTabSelected.value = true;
                        },
                        child: Container(
                          height: BaseSize.height(5),
                          color: controller.earningTabSelected.value ? AppColors.colorDarkBlue : AppColors.colorBackground,
                          alignment: Alignment.center,
                          child: Text(
                            AppStringKey.labelEarning.tr.toUpperCase(),
                            style: BaseStyle.textStyleNunitoSansBold(
                                12, controller.earningTabSelected.value ? AppColors.white : AppColors.colorDarkBlue),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          controller.earningTabSelected.value = false;
                        },
                        child: Container(
                          height: BaseSize.height(5),
                          color: !controller.earningTabSelected.value ? AppColors.colorDarkBlue : AppColors.colorBackground,
                          alignment: Alignment.center,
                          child: Text(
                            AppStringKey.labelWithdrawals.tr.toUpperCase(),
                            style: BaseStyle.textStyleNunitoSansBold(
                                12, !controller.earningTabSelected.value ? AppColors.white : AppColors.colorDarkBlue),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                color: AppColors.white,
                child: Obx(
                  () => ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: controller.earningTabSelected.value
                        ? controller.drEarningHistoryList.length
                        : controller.drWithdrawalHistoryList.length,
                    itemBuilder: (context, i) {
                      return ListItemWallet(
                        index: i,
                        earningList: controller.drEarningHistoryList,
                        withdrawalList: controller.drWithdrawalHistoryList,
                        controller: controller,
                        selectedTab: controller.earningTabSelected.value,
                      );
                    },
                  ),
                ),
              ),
            ],
          ).paddingSymmetric(horizontal: 20),
        ),
      ),
    );
  }
}

class ItemWallet extends StatelessWidget {
  final String image;
  final String title;
  final String amount;

  const ItemWallet({Key? key, required this.image, required this.title, required this.amount}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: BaseSize.width(28),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        color: AppColors.colorDarkBlue,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        children: [
          Image.asset(
            image,
            height: BaseSize.height(10),
            width: BaseSize.width(10),
          ),
          Text(
            title,
            style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.white),
            textAlign: TextAlign.center,
          ),
          const SpaceVertical(8),
          Text(
            amount,
            style: BaseStyle.textStyleNunitoSansSemiBold(14, AppColors.colorGreen),
          ),
          const SpaceVertical(15),
        ],
      ),
    );
  }
}

class ListItemWallet extends StatelessWidget {
  final int index;
  final List<DrEarningHistoryModel> earningList;
  final List<DrWithdrawalHistoryModel> withdrawalList;
  final DrMyWalletController controller;
  final dynamic selectedTab;

  const ListItemWallet({
    Key? key,
    required this.index,
    required this.earningList,
    required this.withdrawalList,
    required this.controller,
    required this.selectedTab,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${selectedTab ? earningList[index].date : withdrawalList[index].date}',
                  style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
                ),
                Text(
                  '${selectedTab ? earningList[index].paymentId : withdrawalList[index].trxID}',
                  style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  '${selectedTab ? earningList[index].amount : withdrawalList[index].amount} BDT',
                  style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
                ),
                Text(
                  '${selectedTab ? earningList[index].status : withdrawalList[index].status}',
                  style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
                ),
              ],
            ),
          ],
        ),
        const SpaceVertical(10),
        const Divider(
          color: AppColors.colorLightSkyBlue,
          height: 1,
        )
      ],
    ).paddingOnly(left: 10, right: 10, top: 10);
  }
}
