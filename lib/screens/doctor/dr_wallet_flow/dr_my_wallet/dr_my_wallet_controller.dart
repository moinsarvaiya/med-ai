import 'dart:convert';
import 'dart:developer' as devLog;

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_endpoints.dart';
import 'package:med_ai/api/api_interface/api_interface.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/models/dr_earning_history_model.dart';
import 'package:med_ai/models/dr_withdrawal_history_model.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';

class DrMyWalletController extends BaseController {
  final String tag = "DrMyWalletController: ";
  var earningTabSelected = true.obs;
  RxList<DrWithdrawalHistoryModel> drWithdrawalHistoryList = <DrWithdrawalHistoryModel>[].obs;
  RxList<DrEarningHistoryModel> drEarningHistoryList = <DrEarningHistoryModel>[].obs;

  ApiCallObject api = ApiCallObject();
  String token = LoginPreferences().sharedPrefRead(StorageKeys.token);
  String personID = LoginPreferences().sharedPrefRead(StorageKeys.personId);

  var code = "".obs;
  var totalEarned = "".obs;
  var totalWithdrawn = "".obs;
  var totalBalance = "".obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();

    devLog.log("$tag PersonID: $personID");
    getWalletDashboardFromServer();
    getWithdrawalHistoryFromServer();
    getEarningHistoryFromServer();
  }

  var listEarning = [];

  var listWithdrawals = [];

  Future<void> getEarningHistoryFromServer() async {
    http.Response serverResponse;
    Object requestBody = {"person_id": personID, "start_date": "", "end_date": ""};

    serverResponse = await api.postData(ApiEndpoints.urlDoctorEarningHistory, requestBody);
    devLog.log("StatusCodeEarning: ${serverResponse.statusCode}", name: tag);
    if (serverResponse.statusCode == 200) {
      drEarningHistoryList.value = [];

      Map<String, dynamic> responseBody = jsonDecode(serverResponse.body);
      var earningHistoryList = responseBody['earning_history'];
      devLog.log("EarningArray: ${jsonEncode(earningHistoryList)}", name: tag);
      for (var item in earningHistoryList) {
        drEarningHistoryList.add(DrEarningHistoryModel.fromJson(item));
      }
    }
  }

  Future<void> getWithdrawalHistoryFromServer() async {
    http.Response serverResponse;
    Object requestBody = {"person_id": personID, "start_date": "", "end_date": ""};

    serverResponse = await api.postData(ApiEndpoints.urlDoctorWithdrawalHistory, requestBody);
    devLog.log("StatusCodeWithdraw: ${serverResponse.statusCode}", name: tag);
    if (serverResponse.statusCode == 200) {
      drWithdrawalHistoryList.value = [];

      Map<String, dynamic> responseBody = jsonDecode(serverResponse.body);
      var withdrawalHistoryList = responseBody['payment_withdrawal_data'];
      devLog.log("PaymentArray: ${jsonEncode(withdrawalHistoryList)}", name: tag);
      for (var item in withdrawalHistoryList) {
        drWithdrawalHistoryList.add(DrWithdrawalHistoryModel.fromJson(item));
      }
    }
  }

  Future<void> getWalletDashboardFromServer() async {
    http.Response serverResponse;
    Object requestBody = {"person_id": personID};

    serverResponse = await api.postData(ApiEndpoints.urlDoctorWalletDashboard, requestBody);
    devLog.log("StatusCode: ${serverResponse.statusCode}", name: tag);
    if (serverResponse.statusCode == 200) {
      Map<String, dynamic> responseBody = jsonDecode(serverResponse.body);

      Map<String, dynamic> walletObject = responseBody['wallet'];
      devLog.log("WalletObject: $walletObject", name: tag);

      totalEarned.value = walletObject['total_earned'] ?? "";
      totalWithdrawn.value = walletObject['total_withdrawn'] ?? "";
      totalBalance.value = walletObject['current_balance'] ?? "";

      devLog.log("Earned: $totalEarned", name: tag);
      devLog.log("Withdrawn: $totalWithdrawn", name: tag);
      devLog.log("Balance: $totalBalance", name: tag);
    }
  }
}
