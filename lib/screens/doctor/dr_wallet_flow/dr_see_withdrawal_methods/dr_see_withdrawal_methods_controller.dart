import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/models/dr_withdrawal_method_model.dart';
import 'package:quickalert/quickalert.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../api/api_interface/api_interface.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../utils/app_preference/login_preferences.dart';

class DrSeeWithdrawalMethodsController extends BaseController {
  RxList<DrWithdrawalMethodModel> drWithdrawalMethodList = <DrWithdrawalMethodModel>[].obs;
  static const String TAG = "WithdrawalMethodList: ";
  ApiCallObject api = ApiCallObject();
  String token = LoginPreferences().sharedPrefRead(StorageKeys.token);
  String personID = LoginPreferences().sharedPrefRead(StorageKeys.personId);
  bool _isLoading = false;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();

    getWithdrawalMethodListFromServer();

    /*drWithdrawalMethodList.add(DrWithdrawalMethodModel(
        accountName: "Hasan",
        mfsName: "Bkash",
        txMethodId: 0,
        insertDateStamp: "2023-07-30T11:57:40",
        accountNumber: "01234567890",
        updateDataStamp: "2023-07-30T11:57:40",
        txMethodType: "mfs",
        txMethodName: "Test Method",
        bankName: "",
        branchName: "",
        routingNumber: "")
    );*/
  }

  Future<void> getWithdrawalMethodListFromServer() async {
    _isLoading = true;
    http.Response serverResponse;
    Object requestBody = {"person_id": personID};

    serverResponse = await api.postData(ApiEndpoints.urlDoctorGetWalletMethods, requestBody);

    if (serverResponse.statusCode == 200) {
      drWithdrawalMethodList.value = [];
      Map<String, dynamic> responseBody = jsonDecode(serverResponse.body);

      var withdrawalMethodList = responseBody['payment_method_list'];

      for (var item in withdrawalMethodList) {
        drWithdrawalMethodList.add(DrWithdrawalMethodModel.fromJson(item));
      }

      print("${TAG} WithdrawalList: $withdrawalMethodList");
      print("${TAG} WithdrawalListSize: ${drWithdrawalMethodList.length}");
    }
  }

  void deleteAlert(methodID, index) {
    QuickAlert.show(
        context: Get.context!,
        type: QuickAlertType.confirm,
        text: "Do you want to delete this withdrawal method?",
        confirmBtnText: "Yes",
        onConfirmBtnTap: () async {
          deleteMethodFromServer(methodID, index);
        },
        cancelBtnText: "No");
  }

  void deleteMethodFromServer(methodID, index) async {
    http.Response serverResponse;
    Object requestBody = {
      "person_id": personID,
      "payment_method_id": methodID,
    };
    serverResponse = await api.postData(ApiEndpoints.urlDoctorDeleteWalletMethod, requestBody);

    if (serverResponse.statusCode == 200) {
      drWithdrawalMethodList.removeAt(index);
      Get.close(0);
      print("inMethodListSize: " + drWithdrawalMethodList.length.toString());
    }
  }
}
