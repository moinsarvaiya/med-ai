import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/ripple_effect_widget.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_strings.dart';
import '../../../../constant/base_size.dart';
import '../../../../models/dr_withdrawal_method_model.dart';
import 'dr_see_withdrawal_methods_controller.dart';

class DrSeeWithdrawalMethodsScreen extends GetView<DrSeeWithdrawalMethodsController> {
  const DrSeeWithdrawalMethodsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(left: 24, right: 24, top: 5, bottom: 20),
        child: SubmitButton(
          title: AppStringKey.btnAddNewMethod.tr,
          onClick: () {
            Get.toNamed(RoutePaths.DR_SET_WITHDRAWAL_METHOD);
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Text(
                AppStringKey.titleSeeWithdrawalMethods.tr,
                style: BaseStyle.textStyleNunitoSansBold(18, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(20),
              Obx(
                () => ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: controller.drWithdrawalMethodList.length,
                    itemBuilder: (context, i) {
                      return ItemWithdrawalMethod(
                        index: i,
                        withdrawalList: controller.drWithdrawalMethodList,
                        controller: controller,
                      );
                    }),
              ),
              const SpaceVertical(15),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemWithdrawalMethod extends StatelessWidget {
  final int index;
  final List<DrWithdrawalMethodModel> withdrawalList;
  final DrSeeWithdrawalMethodsController controller;

  const ItemWithdrawalMethod({
    Key? key,
    required this.index,
    required this.withdrawalList,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        children: [
          SubItemWithdrawalMethod(
            title: AppStringKey.labelAlias.tr,
            value: withdrawalList[index].txMethodName!,
          ).paddingSymmetric(horizontal: 15),
          SubItemWithdrawalMethod(
            title: AppStringKey.labelMethodType.tr,
            value: withdrawalList[index].mfsName!,
          ).paddingSymmetric(horizontal: 15),
          SubItemWithdrawalMethod(
            title: AppStringKey.labelAccountName.tr,
            value: withdrawalList[index].accountName!,
          ).paddingSymmetric(horizontal: 15),
          SubItemWithdrawalMethod(
            title: AppStringKey.labelAccountNo.tr,
            value: withdrawalList[index].accountNumber!,
          ).paddingSymmetric(horizontal: 15),
          const SpaceVertical(15),
          Row(
            children: [
              Expanded(
                flex: 5,
                child: SizedBox(
                  height: BaseSize.height(6),
                  child: CustomCardWidget(
                    shadowOpacity: shadow,
                    widget: RippleEffectWidget(
                        borderRadius: 5,
                        callBack: () {
                          var intentObject = {
                            'methodName': withdrawalList[index].txMethodName!,
                            'accountNumber': withdrawalList[index].accountNumber!,
                            'methodID': withdrawalList[index].txMethodId!,
                            'methodTypeName': withdrawalList[index].mfsName!
                          };
                          Get.toNamed(RoutePaths.DR_WITHDRAW_BALANCE, arguments: {AppStrings.intentArgument: jsonEncode(intentObject)});
                        },
                        widget: Center(
                            child: Text(
                          AppStringKey.btnSelectThisMethod.tr,
                          style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                        ))),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: SizedBox(
                  height: BaseSize.height(6),
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: RippleEffectWidget(
                      borderRadius: 5,
                      callBack: () {
                        var methodID = withdrawalList[index].txMethodId;
                        print("Delete Icon Pressed. $methodID");
                        controller.deleteAlert(methodID, index);
                        //withdrawalList.removeAt(index);
                      },
                      widget: Center(
                        child: Image.asset(AppImages.delete).paddingAll(8),
                      ),
                    ),
                  ),
                ),
              )
              //child: Image.asset(AppImages.delete).paddingAll(8),
            ],
          ).paddingSymmetric(horizontal: 10)
        ],
      ).paddingSymmetric(vertical: 15),
    ).marginOnly(bottom: 5);
  }
}

class SubItemWithdrawalMethod extends StatelessWidget {
  final String title;
  final String value;

  const SubItemWithdrawalMethod({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Text(
            title,
            style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
          ),
        ),
        Expanded(
          child: Text(
            ': $value',
            style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
          ),
        ),
      ],
    ).marginOnly(bottom: 5);
  }
}
