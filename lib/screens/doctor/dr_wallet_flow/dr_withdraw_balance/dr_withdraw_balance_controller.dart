import 'dart:convert';
import 'dart:developer' as devLog;

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings.dart';
import 'package:med_ai/screens/doctor/dr_wallet_flow/dr_my_wallet/dr_my_wallet_controller.dart';
import 'package:quickalert/quickalert.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../api/api_interface/api_interface.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../utils/app_preference/login_preferences.dart';

class DrWithdrawBalanceController extends BaseController {
  final String tag = "DrWithdrawalBalance::: ";
  final ApiCallObject _api = ApiCallObject();
  final String token = LoginPreferences().sharedPrefRead(StorageKeys.token);
  final String personID = LoginPreferences().sharedPrefRead(StorageKeys.personId);
  dynamic intentObject = Get.arguments[AppStrings.intentArgument];

  var methodController = TextEditingController().obs;
  var nameController = TextEditingController().obs;
  var accountController = TextEditingController().obs;
  var amountController = TextEditingController().obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    devLog.log("IntentData: $intentObject", name: tag);
    var receivedIntentData = jsonDecode(intentObject);

    methodController.value.text = receivedIntentData['methodName'];
    nameController.value.text = receivedIntentData['methodTypeName'];
    accountController.value.text = receivedIntentData['accountNumber'];
  }

  void submitWithdrawalRequest() async {
    http.Response serverResponse;
    var amount = amountController.value.text;
    var receivedIntentData = jsonDecode(intentObject);

    Object requestBody = {
      "person_id": personID,
      "withdrawal_amount": amount,
      "tx_method_id": receivedIntentData['methodID']!,
      "currency": "BDT"
    };

    serverResponse = await _api.postData(ApiEndpoints.urlDoctorRequestWithdrawal, requestBody);
    devLog.log("ServerResponse: ${serverResponse.statusCode}", name: tag);
    if (serverResponse.statusCode == 200) {
      var responseBody = jsonDecode(serverResponse.body);
      var responseCode = responseBody['code'];
      devLog.log("ResponseCode: $responseCode", name: tag);
      if (responseCode == "200") {
        QuickAlert.show(
          context: Get.context!,
          type: QuickAlertType.success,
          text: "Request Successful!",
          confirmBtnText: "Back to Dashboard",
          onConfirmBtnTap: () => {
            Get.find<DrMyWalletController>().drWithdrawalHistoryList,
            Get.find<DrMyWalletController>().drEarningHistoryList,
            Get.close(3),
          },
        );
      } else {
        var errorMsg = responseBody['error_msg'];
        QuickAlert.show(
          context: Get.context!,
          type: QuickAlertType.error,
          text: errorMsg,
          confirmBtnText: "Back to Dashboard",
          onConfirmBtnTap: () => {
            Get.close(3),
          },
        );
      }
    }
  }

  bool isAmountEmpty() {
    var amount = amountController.value.text;
    if (amount.isEmpty) {
      return true;
    } else {
      return false;
    }
  }
}
