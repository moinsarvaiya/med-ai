import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';
import 'package:med_ai/utils/validators/validators.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_round_text_field.dart';
import '../../../../widgets/space_vertical.dart';
import 'dr_withdraw_balance_controller.dart';

class DrWithDrawBalanceScreen extends GetView<DrWithdrawBalanceController> {
  const DrWithDrawBalanceScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          onClick: () {
            Get.back();
          },
          isDividerVisible: true,
        ),
      ),
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.btnWithdrawalRequest.tr,
        onClick: () {
          if (controller.isAmountEmpty()) {
            CustomFunctions.customSnackBar(AppStringKey.labelAmount.tr, AppStringKey.cantBeEmpty.tr);
          } else {
            controller.submitWithdrawalRequest();
          }
          //Get.toNamed(RoutePaths.DR_WITHDRAW_REQUEST_SUBMITTED);
        },
      ).paddingOnly(left: 20, right: 20, top: 10, bottom: MediaQuery.of(context).viewInsets.bottom + 20),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStringKey.titleSetWithdrawBalance.tr,
                style: BaseStyle.textStyleNunitoSansBold(18, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(15),
              CustomRoundTextField(
                  labelText: AppStringKey.labelMethodSelected.tr,
                  isRequireField: true,
                  isReadOnly: true,
                  keyBoardAction: TextInputAction.next,
                  textInputType: TextInputType.number,
                  inputFormatter: inputFormatterDigit,
                  backgroundColor: AppColors.transparent,
                  textEditingController: controller.methodController.value),
              const SpaceVertical(15),
              CustomRoundTextField(
                  labelText: AppStringKey.labelAccountType.tr,
                  isRequireField: true,
                  isReadOnly: true,
                  keyBoardAction: TextInputAction.next,
                  textInputType: TextInputType.number,
                  inputFormatter: inputFormatterDigit,
                  backgroundColor: AppColors.transparent,
                  textEditingController: controller.nameController.value),
              const SpaceVertical(20),
              CustomRoundTextField(
                  labelText: AppStringKey.labelAccountNumber.tr,
                  isRequireField: true,
                  isReadOnly: true,
                  hintText: AppStringKey.labelAccountNumber.tr,
                  keyBoardAction: TextInputAction.next,
                  textInputType: TextInputType.number,
                  inputFormatter: inputFormatterDigit,
                  backgroundColor: AppColors.transparent,
                  textEditingController: controller.accountController.value),
              const SpaceVertical(15),
              CustomRoundTextField(
                  labelText: AppStringKey.labelAmount.tr,
                  isRequireField: true,
                  hintText: AppStringKey.hintAmount.tr,
                  keyBoardAction: TextInputAction.next,
                  textInputType: TextInputType.number,
                  inputFormatter: inputFormatterDigit,
                  backgroundColor: AppColors.transparent,
                  validator: Validators.emptyValidator,
                  textEditingController: controller.amountController.value),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}
