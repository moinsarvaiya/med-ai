import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../routes/route_paths.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/space_vertical.dart';
import 'dr_withdraw_request_submitted_controller.dart';

class DrWithdrawRequestSubmittedScreen extends GetView<DrWithdrawRequestSubmittedController> {
  const DrWithdrawRequestSubmittedScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              AppImages.success,
              height: 80,
              width: 80,
            ),
            const SpaceVertical(20),
            Text(
              AppStringKey.labelWithDrawRequestSubmit.tr,
              style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorDarkBlue),
              textAlign: TextAlign.center,
            ),
            const SpaceVertical(50),
            SubmitButton(
              title: AppStringKey.btnBackToDashboard.tr,
              onClick: () {
                Get.offAllNamed(RoutePaths.DR_BOTTOM_TAB);
              },
              titleFontSize: 14,
            ),
          ],
        ).paddingAll(20),
      ),
    );
  }
}
