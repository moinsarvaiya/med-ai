import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_followup_appointment/dr_followup_appointment_controller.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../api/api_interface/api_interface.dart';
import '../../../../constant/app_strings.dart';

class DrFollowupUpdateSymptomsController extends BaseController {
  var api = ApiCallObject();
  dynamic bundle;
  String selectedLanguage = "eng";
  String consultationType = "";
  String consultationId = "";

  var symptomsController = TextEditingController();
  var selectedSymptomsList = [].obs;
  var listGivenSymptoms = [
    'Night Sweat',
    'Coughing up blood',
    'Persistent cough',
  ].obs;
  final FocusNode searchFocusNode = FocusNode();
  GlobalKey<TextFieldAutoCompleteState<String>> searchFieldAutoCompleteKey = GlobalKey();
  final List<String> searchOptions = <String>['Sore throat', 'Dry throat', 'Fever', 'Cold', 'Dry cough'];

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    setData();
    getSymptomSuggestions();
  }

  void setData() {
    try {
      bundle = jsonDecode(Get.arguments[AppStrings.forwardArgument]);

      print("patientId: ${bundle['consultationType']}");
      print("patientId: ${bundle['consultationId']}");
      consultationType = bundle['consultationType'];
      consultationId = bundle['consultationId'];

      selectedSymptomsList.value = [];
      // print("patientId: ${bundle['selectedSymptoms']}");
      var bundleSymptomList = bundle['selectedSymptoms'];
      for (String data in bundleSymptomList) {
        selectedSymptomsList.add(data);
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> getSymptomSuggestions() async {
    http.Response response;
    dynamic object = {};

    object["symptoms"] = selectedSymptomsList;
    object["lang"] = selectedLanguage;

    try {
      response = await api.postData(ApiEndpoints.urlGetSymptomSuggestions, object);
      if (response.statusCode == 200) {
        listGivenSymptoms.value = [];
        var responseBody = jsonDecode(response.body);
        List<dynamic> suggestedSymptoms = responseBody['suggested_symptoms'];
        for (String data in suggestedSymptoms) {
          listGivenSymptoms.add(data);
        }
      } else {
        String message = jsonDecode(response.body)['error_msg'] ?? AppStrings.somethingWentWrong;
        message.showErrorSnackBar();
        await Future.delayed(const Duration(seconds: 3));
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateSymptoms() async {
    http.Response response;
    dynamic object = {};

    object["fid"] = consultationId;
    object["consultation_id"] = consultationId;
    object["id_type"] = consultationType;
    object["symptoms"] = selectedSymptomsList;
    object["user_typed_symptom"] = "";

    try {
      response = await api.postData(ApiEndpoints.urlFollowupAddSymptoms, object);
      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        String message = responseBody['success_msg'];
        message.showSuccessSnackBar();
        await Future.delayed(const Duration(seconds: 3));
        Get.find<DrFollowUpAppointmentController>().currentSymptomsList.value = selectedSymptomsList;
        Get.back();
      } else {
        String message = jsonDecode(response.body)['error_msg'] ?? AppStrings.somethingWentWrong;
        message.showErrorSnackBar();
        await Future.delayed(const Duration(seconds: 3));
      }
    } catch (e) {
      print(e);
    }
  }
}
