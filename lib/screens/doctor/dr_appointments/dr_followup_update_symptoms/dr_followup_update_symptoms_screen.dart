import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_followup_update_symptoms/dr_followup_update_symptoms_controller.dart';
import 'package:med_ai/widgets/custom_chip_widget.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_auto_complete_textfield.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';

class DrFollowupUpdateSymptomsScreen extends GetView<DrFollowupUpdateSymptomsController> {
  const DrFollowupUpdateSymptomsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.btnUpdate.tr,
        onClick: () {
          controller.updateSymptoms();
          // Get.back();
        },
      ).paddingOnly(
          left: 20, right: 20, top: MediaQuery.viewInsetsOf(context).bottom, bottom: MediaQuery.viewInsetsOf(context).bottom + 15),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStringKey.labelFollowUpUpdateSymptomsHere.tr,
                style: BaseStyle.textStyleNunitoSansBold(20, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(20),
              CustomAutoCompleteTextField(
                label: AppStringKey.labelSymptoms.tr,
                hint: AppStringKey.hintSymptoms.tr,
                focusNode: controller.searchFocusNode,
                textEditingController: controller.symptomsController,
                onSelectedValue: (val) {
                  if (!controller.selectedSymptomsList.contains(val)) {
                    controller.selectedSymptomsList.add(val);
                    controller.selectedSymptomsList.refresh();
                    controller.symptomsController.clear();
                  } else {
                    Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
                  }
                },
                searchOptions: controller.searchOptions,
                searchFieldAutoCompleteKey: controller.searchFieldAutoCompleteKey,
              ),
              const SpaceVertical(20),
              Obx(
                () => Wrap(
                  children: controller.selectedSymptomsList
                      .map(
                        (e) => CustomChipWidget(
                          label: e,
                          callback: () {
                            controller.selectedSymptomsList.remove(e);
                            controller.selectedSymptomsList.refresh();
                          },
                        ).marginOnly(right: 10, bottom: 10),
                      )
                      .toList(),
                ),
              ),
              const SpaceVertical(30),
              Text(
                AppStringKey.labelFollowUpUpdateExtraSymptomsHere.tr,
                style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(20),
              Obx(
                () => Wrap(
                  children: controller.listGivenSymptoms
                      .map(
                        (e) => CustomChipWidget(
                          label: e,
                          cancelVisible: false,
                          viewCallback: () {
                            if (!controller.selectedSymptomsList.contains(e)) {
                              controller.selectedSymptomsList.add(e);
                              controller.selectedSymptomsList.refresh();
                              controller.symptomsController.clear();
                              controller.getSymptomSuggestions();
                            } else {
                              controller.symptomsController.clear();
                              Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
                            }
                          },
                        ).marginOnly(right: 10, bottom: 10),
                      )
                      .toList(),
                ),
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemSymptoms extends StatelessWidget {
  final String title;
  final DrFollowupUpdateSymptomsController controller;
  final bool visibleIcon;
  final Function(String val)? callBack;

  const ItemSymptoms({
    Key? key,
    required this.title,
    required this.controller,
    this.visibleIcon = true,
    required this.callBack,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        callBack!(title);
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
        decoration: BoxDecoration(
          color: AppColors.colorBackground,
          borderRadius: BorderRadius.circular(30),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              title,
              style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
            ),
            const SpaceHorizontal(3),
            visibleIcon
                ? GestureDetector(
                    onTap: () {
                      controller.selectedSymptomsList.remove(title);
                      controller.selectedSymptomsList.refresh();
                      controller.getSymptomSuggestions();
                    },
                    child: const Icon(
                      Icons.cancel,
                      size: 15,
                    ),
                  )
                : Container(),
          ],
        ),
      ).marginOnly(right: 10, bottom: 10),
    );
  }
}
