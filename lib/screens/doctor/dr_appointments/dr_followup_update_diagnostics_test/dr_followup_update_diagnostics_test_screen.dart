import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';

import '../../../../widgets/custom_auto_complete_textfield.dart';
import '../../../../widgets/space_vertical.dart';
import 'dr_followup_update_diagnostics_test_controller.dart';

class DrFollowUpUpdateDiagnosticsTestScreen extends GetView<DrFollowUpUpdateDiagnosticsTestController> {
  const DrFollowUpUpdateDiagnosticsTestScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: AppStringKey.titlePrescribe.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
          top: 15,
          bottom: MediaQuery.viewInsetsOf(context).bottom + 15,
        ),
        child: SubmitButton(
          title: Get.previousRoute == RoutePaths.DR_FOLLOWUP_COMPLETE_APPOINTMENT ? AppStringKey.btnUpdate.tr : AppStringKey.strNext.tr,
          onClick: () {
            if (Get.previousRoute == RoutePaths.DR_FOLLOWUP_COMPLETE_APPOINTMENT) {
              Get.back();
            } else {
              controller.addDiagnosisToServer();
              // Get.toNamed(RoutePaths.DR_FOLLOWUP_DATE);
            }
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomAutoCompleteTextField(
                label: AppStringKey.labelDiagnosticsTests.tr,
                hint: AppStringKey.hintAddDiagnosticsTests.tr,
                focusNode: controller.searchFocusNode,
                textEditingController: controller.diagnosticsTestController,
                onSelectedValue: (val) {
                  if (!controller.listDiagnostics.contains(val)) {
                    controller.listDiagnostics.add(val);
                    controller.listDiagnostics.refresh();
                    controller.diagnosticsTestController.clear();
                  } else {
                    Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
                  }
                },
                searchOptions: controller.searchOptions,
                searchFieldAutoCompleteKey: controller.searchFieldAutoCompleteKey2,
              ),
              const SpaceVertical(40),
              Text(
                AppStringKey.labelDiagnosticsList.tr,
                style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(20),
              Obx(
                () => ListView.separated(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: controller.listDiagnostics.length,
                  shrinkWrap: true,
                  itemBuilder: (context, i) {
                    return ItemDiagnosticTest(
                      controller: controller,
                      name: controller.listDiagnostics[i],
                      index: i,
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const Divider(
                      color: AppColors.colorLightSkyBlue,
                      height: 30,
                    );
                  },
                ),
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemDiagnosticTest extends StatelessWidget {
  final DrFollowUpUpdateDiagnosticsTestController controller;
  final String name;
  final int index;

  const ItemDiagnosticTest({
    Key? key,
    required this.name,
    required this.index,
    required this.controller,
  }) : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              name,
              style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
            ),
            InkWell(
              onTap: () {
                Utilities.commonDialog(
                    context,
                    AppStringKey.strAlert.tr,
                    '${AppStringKey.labelRemoveDiagnosticTestPart1.tr} "$name" ${AppStringKey.labelRemoveDiagnosticTestPart2.tr}',
                    AppStringKey.deleteYes.tr,
                    AppStringKey.deleteNo.tr, () {
                  Get.back();
                  controller.listDiagnostics.removeAt(index);
                  controller.listDiagnostics.refresh();
                }, () {
                  Get.back();
                });
              },
              child: Text(
                AppStringKey.strRemove.tr,
                style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
              ),
            ),
          ],
        ),
        /*  controller.listDiagnostics.length - 1 == index
            ? Container(
                height: 30,
              )
            : ,*/
      ],
    );
  }
}
