import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/utils/app_preference/followup_preferences.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../api/api_interface/api_interface.dart';
import '../../../../constant/app_strings.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../routes/route_paths.dart';

class DrFollowUpUpdateDiagnosticsTestController extends BaseController {
  var api = ApiCallObject();

  String consultationId = "";

  var diagnosticsTestController = TextEditingController();
  var listDiagnostics = <String>[].obs;
  var searchOptions = [''].obs;

  final FocusNode searchFocusNode = FocusNode();
  GlobalKey<TextFieldAutoCompleteState<String>> searchFieldAutoCompleteKey2 = GlobalKey();

  @override
  void onInit() {
    super.onInit();
    consultationId = FollowupPreferences().sharedPrefRead(StorageKeys.followupID);

    getDiagnosticTestListFromServer();
  }

  Future<void> getDiagnosticTestListFromServer() async {
    http.Response response;
    try {
      response = await api.getData(ApiEndpoints.urlGetDiagnosticTestList);
      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        var diagnosticTestListFromApi = responseBody['procedure_list'];
        searchOptions.value = [];
        for (String data in diagnosticTestListFromApi) {
          searchOptions.add(data);
        }
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> addDiagnosisToServer() async {
    http.Response response;
    dynamic object = {};
    object["fid"] = consultationId;
    object["prescribed_test"] = listDiagnostics;

    if (consultationId != "0" && consultationId.isNotEmpty) {
      try {
        response = await api.postData(ApiEndpoints.urlFollowupAddPrescribedDiagnosis, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            Get.toNamed(RoutePaths.DR_FOLLOWUP_DATE);
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print(e);
      }
    } else {
      // print(AppStrings.somethingWentWrong);
    }
  }
}
