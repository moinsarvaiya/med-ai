import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_follow_up_date/dr_follow_up_date_controller.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_round_text_field.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../utils/utilities.dart';

class DrFollowUpDateScreen extends GetView<DrFollowUpDateController> {
  const DrFollowUpDateScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.strNext.tr,
        onClick: () {
          controller.adviceAndDateToServer();
          // if (appointmentType == EnumAppointmentType.VISIT) {
          //   Get.toNamed(RoutePaths.DR_VISIT_COMPLETE_APPOINTMENT);
          // } else {
          //   controller.adviceAndDateToServer();
          //   // Get.toNamed(RoutePaths.DR_FOLLOWUP_COMPLETE_APPOINTMENT);
          // }
        },
      ).paddingOnly(
        left: 20,
        right: 20,
        top: 15,
        bottom: MediaQuery.viewInsetsOf(context).bottom + 15,
      ),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStringKey.titleAdviceAndNextFollowUpDate.tr,
                style: BaseStyle.textStyleNunitoSansBold(20, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(25),
              CustomRoundTextField(
                labelText: AppStringKey.labelDoctorAdvise.tr,
                isRequireField: true,
                hintText: AppStringKey.hintNote.tr,
                maxLines: 5,
                backgroundColor: AppColors.transparent,
                textEditingController: controller.doctorAdviseController,
              ),
              const SpaceVertical(20),
              CustomRoundTextField(
                onTap: () {
                  controller.dateTimePicker.show(context);
                },
                labelText: AppStringKey.labelNextFollowUp.tr,
                isRequireField: true,
                hintText: AppStringKey.hintSelectDay.tr,
                maxLines: 1,
                isReadOnly: true,
                suffixIcon: const Icon(
                  Icons.calendar_today_outlined,
                  size: 20,
                  color: AppColors.colorDarkBlue,
                ),
                backgroundColor: AppColors.transparent,
                textEditingController: controller.nextFollowUpController,
              ),
              const SpaceVertical(20),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}
