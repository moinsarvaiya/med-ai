import 'dart:convert';

import 'package:bottom_picker/bottom_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/utils/app_preference/followup_preferences.dart';
import 'package:med_ai/utils/utilities.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../api/api_interface/api_interface.dart';
import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../routes/route_paths.dart';

class DrFollowUpDateController extends BaseController {
  var api = ApiCallObject();

  String consultationId = "";
  String consultationType = "";

  var doctorAdviseController = TextEditingController();
  var nextFollowUpController = TextEditingController();
  var dateTimePicker;

  @override
  void onInit() {
    super.onInit();
    consultationId = FollowupPreferences().sharedPrefRead(StorageKeys.followupID);
    consultationType = FollowupPreferences().sharedPrefRead('consultationType');

    dateTimePicker = BottomPicker.date(
      title: '',
      titleStyle: BaseStyle.textStyleNunitoSansSemiBold(14, AppColors.colorDarkBlue),
      onSubmit: (date) {
        // var data = DateFormat('dd-MM-yyyy hh:mm a').format(date);
        var data = DateFormat('yyyy-MM-dd').format(date);
        if (date.isAfter(DateTime.now())) {
          nextFollowUpController.text = data;
        } else {
          Utilities.showErrorMessage("Please select valid time", "InValid Time");
        }
      },
      onClose: () {},
      dismissable: true,
      iconColor: AppColors.white,
      minDateTime: DateTime.now(),
      maxDateTime: DateTime.now().add(
        const Duration(days: 60),
      ),
      initialDateTime: DateTime.now(),
      gradientColors: const [AppColors.colorDarkBlue, AppColors.colorDarkBlue],
    );
  }

  Future<void> adviceAndDateToServer() async {
    http.Response response;
    dynamic object = {};
    object["consultation_id"] = consultationId;
    object["id_type"] = consultationType;
    object["advice"] = doctorAdviseController.value.text.toString();
    object["next_consultation_date"] = nextFollowUpController.value.text.toString();

    print(jsonEncode(object));

    if (consultationId != "0" && consultationId.isNotEmpty) {
      try {
        response = await api.postData(ApiEndpoints.urlAdviseNextDate, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            updateFollowupStatusToServer();
            // Get.toNamed(RoutePaths.DR_FOLLOWUP_COMPLETE_APPOINTMENT);
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print(e);
      }
    } else {
      // print(AppStrings.somethingWentWrong);
    }
  }

  Future<void> updateFollowupStatusToServer() async {
    http.Response response;
    dynamic object = {};
    object["fid"] = consultationId;

    print(jsonEncode(object));

    if (consultationId != "0" && consultationId.isNotEmpty) {
      try {
        response = await api.postData(ApiEndpoints.urlDoctorFollowupUpdateConsultationStatus, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            Get.toNamed(RoutePaths.DR_FOLLOWUP_COMPLETE_APPOINTMENT);
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print(e);
      }
    } else {
      // print(AppStrings.somethingWentWrong);
    }
  }
}
