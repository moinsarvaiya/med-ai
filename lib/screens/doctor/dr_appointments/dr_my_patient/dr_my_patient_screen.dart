import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_consultancy_widget.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import 'dr_my_patient_controller.dart';

class DrMyPatientScreen extends GetView<DrMyPatientController> {
  const DrMyPatientScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          titleName: AppStringKey.labelMyPatient.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: Column(
        children: [
          CustomConsultancyWidget(
            tag: AppStringKey.labelNewConsultancy.tr,
            onClick: () {
              consultancyFrom = EnumConsultancyFrom.MY_PATIENT;
              Get.toNamed(
                RoutePaths.DR_NEW_CONSULTANCY,
                //arguments: {'consultancyFrom': EnumConsultancyFrom.MY_PATIENT},
              );
            },
          ),
          const SpaceVertical(15),
          CustomConsultancyWidget(
            tag: AppStringKey.labelExistingPatientConsultancy.tr,
            onClick: () {
              Get.toNamed(RoutePaths.DR_EC_PATIENT_LIST);
            },
          ),
        ],
      ).paddingAll(20),
    );
  }
}
