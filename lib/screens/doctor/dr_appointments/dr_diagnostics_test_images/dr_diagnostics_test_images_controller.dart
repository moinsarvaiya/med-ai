import 'dart:convert';

import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings.dart';

class DrDiagnosticsTestImagesController extends BaseController {
  dynamic arguments = Get.arguments[AppStrings.forwardArgument];

  var listImages = [
    // AppImages.dImage1,
    // AppImages.dImage2,
  ].obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var bundle = jsonDecode(arguments);
    listImages.value = [];
    for (String data in bundle["imageList"]) {
      print(data);
      listImages.add(data);
    }
    print(listImages.length);
  }
}
