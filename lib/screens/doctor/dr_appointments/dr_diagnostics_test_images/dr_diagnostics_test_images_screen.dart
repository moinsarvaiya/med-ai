import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_diagnostics_test_images/dr_diagnostics_test_images_controller.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../widgets/custom_buttons.dart';

class DrDiagnosticsTestImagesScreen extends GetView<DrDiagnosticsTestImagesController> {
  const DrDiagnosticsTestImagesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStringKey.labelViewImageHere.tr,
                style: BaseStyle.textStyleNunitoSansBold(20, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(20),
              ListView.builder(
                shrinkWrap: true,
                itemCount: controller.listImages.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, i) {
                  return ItemTestImages(
                    image: controller.listImages[i],
                  );
                },
              ),
              const SpaceVertical(15),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  width: 100,
                  height: 33,
                  child: SubmitButton(
                    title: AppStringKey.back.tr,
                    onClick: () {
                      Get.back();
                    },
                  ),
                ),
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemTestImages extends StatelessWidget {
  final String image;

  const ItemTestImages({
    Key? key,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.colorBackground,
        borderRadius: BorderRadius.circular(5),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      child: Container(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
        ),
        child: Image.network(image),
      ),
    ).marginOnly(bottom: 10);
  }
}
