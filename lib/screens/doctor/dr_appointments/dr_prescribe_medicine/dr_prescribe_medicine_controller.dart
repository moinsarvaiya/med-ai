import 'dart:convert';
import 'dart:developer' as developer;

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_endpoints.dart';
import 'package:med_ai/api/api_interface/api_interface.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/models/dr_prescribe_medicine_model.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';
import 'package:med_ai/utils/app_preference/visit_preferences.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';

class DrPrescribeMedicineController extends BaseController {
  final String tag = 'MedicineList';
  ApiCallObject api = ApiCallObject();
  String token = LoginPreferences().sharedPrefRead(StorageKeys.token);
  String visitID = VisitPreferences().sharedPrefRead(StorageKeys.visitID);
  RxList<DrPrescribeMedicineModel> medicineList = RxList();
  var remarks = 'Based on the symptoms patient has provided, the patient may have developer a stomach ulcer related problem'.obs;

  @override
  void onInit() {
    super.onInit();
    developer.log('Remarks: $remarks');
    developer.log('VisitID: $visitID');
  }

  void addNewMedicine(DrPrescribeMedicineModel prescribeMedicineModel) {
    medicineList.add(prescribeMedicineModel);
    //var data = prescribeMedicineModel.threeTimesDay!.isEmpty ? prescribeMedicineModel.hourDifference: prescribeMedicineModel.threeTimesDay;
    //developer.log('Data: $data', name: tag);
    var data = CustomFunctions.getScheduleSorted(prescribeMedicineModel.threeTimesDay!);
    developer.log('DataToString: ${data.breakfast}', name: tag);
    developer.log('DataToString: ${data.lunch}', name: tag);
    developer.log('DataToString: ${data.dinner}', name: tag);
  }

  Future<void> addMedicinePrescriptionToServer() async {
    http.Response serverResponse;
    Object requestBody = {
      "vid": int.parse(visitID),
      "drug_info": [
        for (var item in medicineList)
          {
            "drug_name": item.brandName,
            "schedule": CustomFunctions.getScheduleSorted(item.threeTimesDay!),
            "time_interval": int.parse(item.hourDifference!),
            "food_intake": item.mealInstruction,
            "duration": item.duration,
            "special_instruction": item.specialInstruction,
            "administration_method": item.dosageType
          }
      ]
    };
    //developer.log("MedicineObject: ${jsonEncode(requestBody)}", name: tag);
    serverResponse = await api.postData(ApiEndpoints.urlVisitAddPrescribeMedicine, requestBody);
    developer.log("StatusCode: ${serverResponse.statusCode}", name: tag);
    if (serverResponse.statusCode == 200) {
      var responseBody = jsonDecode(serverResponse.body);
      var resCode = responseBody['code'];
      if (resCode == "200") {
        var successMsg = responseBody['success_msg'];
        CustomFunctions.customSnackBar(AppStrings.success, successMsg);
        Get.toNamed(RoutePaths.DR_VISIT_UPDATE_DIAGNOSTIC_TEST);
      } else {
        var errorMsg = responseBody['error_msg'];
        CustomFunctions.customErrorSnackBar(errorMsg);
      }
    }
  }
}
