import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_prescribe_medicine/dr_prescribe_medicine_controller.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../models/dr_prescribe_medicine_model.dart';
import '../../../../routes/route_paths.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/space_horizontal.dart';
import '../dr_followup_appointment/dr_followup_appointment_screen.dart';

class DrPrescribeMedicineScreen extends GetView<DrPrescribeMedicineController> {
  const DrPrescribeMedicineScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: AppStringKey.titlePrescribe.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: SubmitButton(
        title: Get.previousRoute == RoutePaths.DR_VISIT_COMPLETE_APPOINTMENT ||
                Get.previousRoute == RoutePaths.DR_FOLLOWUP_COMPLETE_APPOINTMENT
            ? AppStringKey.btnUpdate.tr
            : AppStringKey.strComplete.tr,
        onClick: () {
          /*if (Get.previousRoute == RoutePaths.DR_IDENTIFIED_DISEASE) {
            Get.toNamed(RoutePaths.DR_FOLLOWUP_UPDATE_DIAGNOSTICS_TEST);
          } else {
            Get.back();
          }*/
          controller.addMedicinePrescriptionToServer();
        },
      ).paddingOnly(left: 20, right: 20, top: 15, bottom: MediaQuery.viewInsetsOf(context).bottom + 15),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const ItemRemarks(),
              Obx(
                () => ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: controller.medicineList.length,
                    itemBuilder: (context, i) {
                      return ItemPrescription(
                        medicines: controller.medicineList[i],
                        callback: () {
                          Utilities.commonDialog(
                            context,
                            AppStringKey.strAlert.tr,
                            'Do you want to remove ${controller.medicineList[i].brandName} from the prescription?',
                            AppStringKey.deleteYes.tr,
                            AppStringKey.deleteNo.tr,
                            () {
                              Get.back();
                              controller.medicineList.removeAt(i);
                              controller.medicineList.refresh();
                            },
                            () {
                              Get.back();
                            },
                          );
                        },
                      ).marginOnly(bottom: 5);
                    }),
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemRemarks extends StatelessWidget {
  static DrPrescribeMedicineController controller = Get.find<DrPrescribeMedicineController>();

  const ItemRemarks({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppStringKey.labelRemarks.tr,
            style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
          ),
          const SpaceVertical(5),
          Obx(
            () => Text(
              '${controller.remarks}',
              style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorTextDarkGray),
            ),
          ),
          const SpaceVertical(14),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.strMedication.tr,
                    style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                  ),
                  const SpaceVertical(5),
                  Text(
                    AppStringKey.noMedicineAdded.tr,
                    style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorTextDarkGray),
                  ),
                ],
              ),
              CustomCardWidget(
                shadowOpacity: shadow,
                widget: InkWell(
                  onTap: () async {
                    var result = await Get.toNamed(RoutePaths.DR_ADD_NEW_MEDICINE);
                    //if (result == '1') {
                    //Get.find<DrPrescribeMedicineController>()
                    //  .addNewMedicine();
                    //}
                  },
                  child: const Icon(
                    Icons.add,
                    size: 30,
                    color: AppColors.colorDarkBlue,
                  ).paddingAll(5),
                ),
              ),
            ],
          )
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 10),
    );
  }
}

class ItemPrescription extends StatelessWidget {
  final Function()? callback;
  final DrPrescribeMedicineModel medicines;

  const ItemPrescription({
    Key? key,
    this.callback,
    required this.medicines,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      elevation: 1,
      widget: SizedBox(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: SubItemPreviousDecision(title: AppStringKey.strBrand.tr, value: medicines.brandName!)),
                Expanded(
                  child: SubItemPreviousDecision(
                      title: AppStringKey.labelMedicineTiming.tr,
                      value:
                          medicines.threeTimesDay!.isNotEmpty ? '${medicines.threeTimesDay!} / Day' : '${medicines.hourDifference!} Hours'),
                ),
              ],
            ),
            const SpaceVertical(15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: SubItemPreviousDecision(title: AppStringKey.labelMealInstruction.tr, value: medicines.mealInstruction!)),
                Expanded(child: SubItemPreviousDecision(title: AppStringKey.strDuration.tr, value: medicines.duration!)),
              ],
            ),
            const SpaceVertical(15),
            SubItemPreviousDecision(title: AppStringKey.labelSpecialInstruction.tr, value: medicines.specialInstruction!),
            const SpaceVertical(15),
            Center(
              child: CustomCardWidget(
                shadowOpacity: shadow,
                widget: InkWell(
                  onTap: callback,
                  splashColor: AppColors.colorDarkBlue.withOpacity(0.4),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.cancel,
                        color: AppColors.colorPink,
                        size: 18,
                      ),
                      const SpaceHorizontal(5),
                      Text(
                        AppStringKey.strRemove.tr,
                        style: BaseStyle.textStyleNunitoSansBold(10, AppColors.colorDarkBlue),
                      ),
                    ],
                  ).paddingSymmetric(vertical: 8, horizontal: 40),
                ),
              ),
            ),
          ],
        ).paddingSymmetric(horizontal: 8, vertical: 15),
      ),
    );
  }
}
