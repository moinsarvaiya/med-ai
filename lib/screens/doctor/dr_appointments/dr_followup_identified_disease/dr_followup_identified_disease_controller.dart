import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/utils/app_preference/followup_preferences.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../api/api_interface/api_interface.dart';
import '../../../../constant/app_strings.dart';
import '../../../../routes/route_paths.dart';

class DrFollowupIdentifiedDiseaseController extends BaseController {
  var api = ApiCallObject();

  String consultationId = "";

  var identifiedDiseaseController = TextEditingController();
  var clinicalNoteController = TextEditingController();

  var identifiedDiseaseFocusNode = FocusNode();
  GlobalKey<TextFieldAutoCompleteState<String>> identifiedDiseaseAutoCompleteKey = GlobalKey();

  RxList<String> selectedDiseaseList = RxList<String>();
  RxList<String> diseaseList = RxList<String>();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    setData();
    getIdentifiedDiseaseListFromServer();
    // print(consultationId);
    // print(selectedSymptoms);
  }

  void setData() {
    try {
      consultationId = FollowupPreferences().sharedPrefRead(StorageKeys.followupID);
      selectedDiseaseList.value = [];
      diseaseList.value = [];
      print("ConsultationID: $consultationId");
      // for (String data in selectedSymptomsFromPref) {
      //   selectedDiseaseList.add(data);
      // }
    } catch (e) {
      print("ExceptionSetData: $e");
    }
  }

  Future<void> getIdentifiedDiseaseListFromServer() async {
    http.Response response;
    try {
      response = await api.getData(ApiEndpoints.urlGetDiseaseList);
      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        var diseaseListFromApi = responseBody['disease_list'];
        for (String data in diseaseListFromApi) {
          diseaseList.add(data);
        }
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> submitDecisionToServer() async {
    http.Response response;
    dynamic object = {};
    object["fid"] = consultationId;
    object["clinical_note"] = clinicalNoteController.value.text.toString();
    object["diagnosed_disease"] = selectedDiseaseList;

    if (consultationId != "0" && consultationId.isNotEmpty) {
      try {
        response = await api.postData(ApiEndpoints.urlDoctorFollowupAddSubmitDecision, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            Get.toNamed(RoutePaths.DR_FOLLOWUP_PRESCRIBE_MEDICINE);
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print(e);
      }
    } else {
      print("ConsultationID: $consultationId");
      print(AppStrings.somethingWentWrong);
      Get.toNamed(RoutePaths.DR_FOLLOWUP_PRESCRIBE_MEDICINE);
    }
  }
}
