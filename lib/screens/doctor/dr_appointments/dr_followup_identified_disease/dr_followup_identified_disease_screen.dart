import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_chip_widget.dart';
import 'package:med_ai/widgets/custom_round_text_field.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/functions/custom_functions.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_auto_complete_textfield.dart';
import 'dr_followup_identified_disease_controller.dart';

class DrFollowupIdentifiedDiseaseScreen extends GetView<DrFollowupIdentifiedDiseaseController> {
  const DrFollowupIdentifiedDiseaseScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: AppStrings.titleDecision,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: SubmitButton(
        title: AppStrings.strNext,
        onClick: () {
          controller.submitDecisionToServer();
          // Get.toNamed(RoutePaths.DR_PRESCRIBE_MEDICINE);
        },
      ).paddingOnly(left: 20, right: 20, top: 15, bottom: MediaQuery.viewInsetsOf(context).bottom + 15),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Obx(
                () => Wrap(
                  children: controller.selectedDiseaseList
                      .map(
                        (element) => CustomChipWidget(
                          label: element,
                          callback: () {
                            controller.selectedDiseaseList.remove(element);
                            controller.selectedDiseaseList.refresh();
                          },
                        ).marginOnly(right: 10, bottom: 10),
                      )
                      .toList(),
                ),
              ),

              const SpaceVertical(20),
              CustomAutoCompleteTextField(
                label: AppStrings.strIdentifiedDisease,
                hint: AppStrings.hintDiseaseName,
                focusNode: controller.identifiedDiseaseFocusNode,
                textEditingController: controller.identifiedDiseaseController,
                onSelectedValue: (val) {
                  if (!(controller.selectedDiseaseList.contains(val))) {
                    controller.selectedDiseaseList.add(val);
                    controller.identifiedDiseaseController.clear();
                  } else {
                    CustomFunctions.customSnackBar(AppStrings.labelWarning, "$val is already selected!");
                    controller.identifiedDiseaseController.clear();
                  }
                  controller.identifiedDiseaseAutoCompleteKey = GlobalKey();
                },
                searchOptions: controller.diseaseList,
                searchFieldAutoCompleteKey: controller.identifiedDiseaseAutoCompleteKey,
              ),
              // const SpaceVertical(20),
              // CustomRoundTextField(
              //   hintText: AppStrings.hintDiseaseName,
              //   labelText: AppStrings.strIdentifiedDisease,
              //   isRequireField: true,
              //   keyBoardAction: TextInputAction.next,
              //   backgroundColor: AppColors.white,
              //   textEditingController: controller.identifiedDiseaseController,
              // ),
              const SpaceVertical(20),
              CustomRoundTextField(
                hintText: AppStrings.hintNote,
                labelText: AppStrings.strClinicalNote,
                isRequireField: true,
                backgroundColor: AppColors.white,
                maxLines: 4,
                textEditingController: controller.clinicalNoteController,
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}
