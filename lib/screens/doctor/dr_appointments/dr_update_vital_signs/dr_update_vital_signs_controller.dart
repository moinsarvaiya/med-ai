import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../../bindings/base_controller.dart';
import '../../../../models/dr_patient_temperture_model.dart';

class DrUpdateVitalSignsController extends BaseController {
  var weightController = TextEditingController().obs;
  var weightFeetController = TextEditingController().obs;
  var weightInchController = TextEditingController().obs;
  var lowestBloodPressureController = TextEditingController().obs;
  var highestBloodPressureController = TextEditingController().obs;
  var pulseController = TextEditingController().obs;
  List<String> breathing = ['Low', 'High'];
  RxList<Rx<DrPatientTemperatureModel>> temperatures = List.generate(
    11,
    (index) => DrPatientTemperatureModel(temperature: '${96 + index}°F', isSelected: false).obs,
  ).obs;
}
