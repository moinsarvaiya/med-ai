import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_update_vital_signs/dr_update_vital_signs_controller.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_round_text_field.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../widgets/custom_dropdown_field.dart';
import '../../../../widgets/custom_temperature_widget.dart';
import '../../../../widgets/space_horizontal.dart';

class DrUpdateVitalSignsScreen extends GetView<DrUpdateVitalSignsController> {
  const DrUpdateVitalSignsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.btnUpdate.tr,
        onClick: () {
          Get.back();
        },
      ).paddingOnly(left: 20, right: 20, top: 15, bottom: MediaQuery.viewInsetsOf(context).bottom + 15),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStringKey.titleUpdateVitalSignsHere.tr,
                style: BaseStyle.textStyleNunitoSansBold(
                  20,
                  AppColors.colorDarkBlue,
                ),
              ),
              const SpaceVertical(25),
              CustomRoundTextField(
                labelText: AppStringKey.labelWeight.tr,
                isRequireField: true,
                hintText: AppStringKey.hintKG.tr,
                textInputType: TextInputType.number,
                keyBoardAction: TextInputAction.next,
                backgroundColor: AppColors.transparent,
                textEditingController: controller.weightController.value,
              ),
              const SpaceVertical(20),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                    child: CustomRoundTextField(
                      labelText: AppStringKey.labelHeight.tr,
                      isRequireField: true,
                      hintText: AppStringKey.hintFeet.tr,
                      keyBoardAction: TextInputAction.next,
                      textInputType: TextInputType.text,
                      backgroundColor: AppColors.transparent,
                      textEditingController: controller.weightFeetController.value,
                    ),
                  ),
                  const SpaceHorizontal(20),
                  Expanded(
                    child: CustomRoundTextField(
                      labelVisible: false,
                      labelText: AppStringKey.labelInch.tr,
                      keyBoardAction: TextInputAction.next,
                      isRequireField: true,
                      hintText: AppStringKey.hintInch.tr,
                      textInputType: TextInputType.text,
                      backgroundColor: AppColors.transparent,
                      textEditingController: controller.weightInchController.value,
                    ),
                  ),
                ],
              ),
              const SpaceVertical(20),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                    child: CustomRoundTextField(
                      labelText: AppStringKey.labelBloodPressure.tr,
                      isRequireField: true,
                      keyBoardAction: TextInputAction.next,
                      hintText: AppStringKey.hintLowest.tr,
                      textInputType: TextInputType.text,
                      backgroundColor: AppColors.transparent,
                      textEditingController: controller.lowestBloodPressureController.value,
                    ),
                  ),
                  const SpaceHorizontal(20),
                  Expanded(
                    child: CustomRoundTextField(
                      labelVisible: false,
                      labelText: AppStringKey.hintHighest.tr,
                      isRequireField: true,
                      keyBoardAction: TextInputAction.next,
                      hintText: AppStringKey.hintHighest.tr,
                      textInputType: TextInputType.text,
                      backgroundColor: AppColors.transparent,
                      textEditingController: controller.highestBloodPressureController.value,
                    ),
                  ),
                ],
              ),
              const SpaceVertical(20),
              CustomRoundTextField(
                labelText: AppStringKey.labelPulse.tr,
                isRequireField: true,
                hintText: AppStringKey.hintType.tr,
                textInputType: TextInputType.text,
                keyBoardAction: TextInputAction.done,
                backgroundColor: AppColors.transparent,
                textEditingController: controller.pulseController.value,
              ),
              const SpaceVertical(20),
              CustomDropDownField(
                labelText: AppStringKey.labelBreathing.tr,
                validationText: AppStringKey.validationSelectBreathing.tr,
                hintTitle: AppStringKey.hintSelect.tr,
                onChanged: (countryValue) {},
                items: controller.breathing,
              ),
              const SpaceVertical(20),
              CustomTemperatureWidget(
                onChanged: (value) {},
              )
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}
