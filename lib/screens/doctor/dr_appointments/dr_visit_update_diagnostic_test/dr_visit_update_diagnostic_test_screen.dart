import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_visit_update_diagnostic_test/dr_visit_update_diagnostic_test_controller.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../widgets/custom_auto_complete_textfield.dart';

class DrVisitUpdateDiagnosticTestScreen extends GetView<DrVisitUpdateDiagnosticTestController> {
  const DrVisitUpdateDiagnosticTestScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.btnUpdate.tr,
        onClick: () {
          controller.addDiagnosticsToServer();
        },
      ).paddingOnly(left: 20, right: 20, top: 15, bottom: MediaQuery.viewInsetsOf(context).bottom + 15),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStringKey.titlePrescribe.tr,
                style: BaseStyle.textStyleNunitoSansBold(20, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(20),
              CustomAutoCompleteTextField(
                label: AppStringKey.labelDiagnosticsTests.tr,
                hint: AppStringKey.hintAddDiagnosticsTests.tr,
                focusNode: controller.searchFocusNode,
                textEditingController: controller.addDiagnosticsTestsController,
                onSelectedValue: (val) {
                  if (!controller.diagnosticList.contains(val)) {
                    controller.diagnosticList.add(val);
                    controller.diagnosticList.refresh();
                    controller.addDiagnosticsTestsController.clear();
                    controller.searchFieldAutoCompleteKey = GlobalKey();
                  } else {
                    controller.addDiagnosticsTestsController.clear();
                    controller.searchFieldAutoCompleteKey = GlobalKey();
                    CustomFunctions.customErrorSnackBar('$val already Added!');
                    //Utilities.showErrorMessage('It\'s already added!');
                  }
                },
                searchOptions: controller.testList,
                searchFieldAutoCompleteKey: controller.searchFieldAutoCompleteKey,
              ),
              const SpaceVertical(40),
              Text(
                AppStringKey.labelDiagnosticsList.tr,
                style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(20),
              Obx(
                () => ListView.separated(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: controller.diagnosticList.length,
                  shrinkWrap: true,
                  itemBuilder: (context, i) {
                    return ItemDiagnosticTest(
                      controller: controller,
                      name: controller.diagnosticList[i],
                      index: i,
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const Divider(
                      color: AppColors.colorLightSkyBlue,
                      height: 30,
                    );
                  },
                ),
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemDiagnosticTest extends StatelessWidget {
  final DrVisitUpdateDiagnosticTestController controller;
  final String name;
  final int index;

  const ItemDiagnosticTest({
    Key? key,
    required this.name,
    required this.index,
    required this.controller,
  }) : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              name,
              style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
            ),
            InkWell(
              onTap: () {
                Utilities.commonDialog(
                    context,
                    AppStringKey.strAlert.tr,
                    '${AppStringKey.labelRemoveDiagnosticTestPart1.tr} "$name" ${AppStringKey.labelRemoveDiagnosticTestPart2.tr}',
                    AppStringKey.deleteYes.tr,
                    AppStringKey.deleteNo.tr, () {
                  Get.back();
                  controller.diagnosticList.removeAt(index);
                  controller.diagnosticList.refresh();
                }, () {
                  Get.back();
                });
              },
              child: Text(
                AppStringKey.strRemove.tr,
                style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
