import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/cupertino.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_endpoints.dart';
import 'package:med_ai/api/api_interface/api_interface.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/app_preference/visit_preferences.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';

class DrVisitUpdateDiagnosticTestController extends BaseController {
  final String tag = "VisitDiagnosticsTest";
  var api = ApiCallObject();
  var addDiagnosticsTestsController = TextEditingController();
  var visitID = "";
  RxList<String> testList = [''].obs;

  var diagnosticList = [].obs;
  final FocusNode searchFocusNode = FocusNode();
  GlobalKey<TextFieldAutoCompleteState<String>> searchFieldAutoCompleteKey = GlobalKey();

  /*final List<String> searchOptions = <String>[
    'CT',
    'CBC',
    'CRP',
    'CT scan',
    'CSF study',
  ];*/

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    setData();
    getTestListFromServer();
  }

  void setData() {
    try {
      developer.log("VisitPref ${VisitPreferences().sharedPrefRead(StorageKeys.visitID)}", name: tag);
      visitID = VisitPreferences().sharedPrefRead(StorageKeys.visitID);
      //personID = LoginPreferences().sharedPrefRead(StorageKeys.personId);
      //doctorName = LoginPreferences().sharedPrefRead(StorageKeys.name);
      developer.log('VisitID: $visitID', name: tag);
    } catch (e) {
      developer.log("SetDataError: ${e.toString()}", name: tag);
    }
  }

  Future<void> getTestListFromServer() async {
    http.Response response;
    try {
      response = await api.getData(ApiEndpoints.urlGetDiagnosticTestList);
      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        var testListFromApi = responseBody['procedure_list'];
        for (String data in testListFromApi) {
          testList.add(data);
        }
        developer.log("TotalTestCount: ${testList.length}");
      }
    } catch (e) {
      developer.log('TestListException $e');
    }
  }

  Future<void> addDiagnosticsToServer() async {
    http.Response response;
    dynamic object = {"vid": int.parse(visitID), "tests": diagnosticList, "test_instructions": ""};
    //developer.log("Object: ${jsonEncode(object)}", name: tag);
    try {
      response = await api.postData(ApiEndpoints.urlVisitAddPrescribeDiagnostics, object);
      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        if (responseBody['code'] == "200") {
          CustomFunctions.customSnackBar(AppStrings.success, responseBody['success_msg']);
          Get.toNamed(RoutePaths.DR_VISIT_NEXT_DATE);
        } else {
          String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
          CustomFunctions.customSnackBar(AppStrings.error, message);
          //await Future.delayed(const Duration(seconds: 3));
        }
      }
    } catch (e) {
      developer.log("SubmitDecisionCatch: $e", name: tag);
    }
  }
}
