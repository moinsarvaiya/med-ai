import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../api/api_interface/api_interface.dart';
import '../../../../constant/app_strings.dart';
import '../../../../models/dr_appointments_model.dart';

class DrNewConsultancyController extends BaseController {
  ApiCallObject api = ApiCallObject();
  String personId = LoginPreferences().sharedPrefRead(StorageKeys.personId);

  RxInt selected = 0.obs;
  RxList<DrAppointmentsModel> listDateAppointments = RxList();
  Enum consultancyFrom = EnumConsultancyFrom.APPOINTMENT_TAB;

  void setDefaultData() async {
    var context = Get.context!;
    await Future.delayed(const Duration(seconds: 3));
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text("Default Value Is Set For Upcoming Consultancy"),
    ));

    listDateAppointments.add(
      DrAppointmentsModel(
        date: '2023-08-06',
        count: 1,
        appointments: [
          Appointments(
              name: "Ricky J",
              age: 39,
              gender: "Male",
              personId: "230605001700001",
              consultationId: 2306050017000010004,
              consultationIdStr: "2306050017000010004",
              prevRecordId: "",
              idType: "visit",
              consultationStatus: "Pending",
              date: "2023-08-06",
              time: "10:24 AM",
              division: "Khulna",
              country: "Bangladesh",
              phone: "01234567890",
              chiefComplaints: ["dizziness"],
              symptoms: ["sneezing"])
        ],
      ),
    );
    listDateAppointments.add(
      DrAppointmentsModel(
        date: '2023-08-07',
        count: 1,
        appointments: [
          Appointments(
              name: "Ricky J",
              age: 39,
              gender: "Male",
              personId: "230605001700002",
              consultationId: 2306050017000010005,
              consultationIdStr: "2306050017000010005",
              prevRecordId: "",
              idType: "followup",
              consultationStatus: "Pending",
              date: "2023-08-07",
              time: "10:24 AM",
              division: "Khulna",
              country: "Bangladesh",
              phone: "01234567890",
              chiefComplaints: ["dizziness"],
              symptoms: ["sneezing"])
        ],
      ),
    );
    listDateAppointments.add(
      DrAppointmentsModel(
        date: '2023-08-08',
        count: 1,
        appointments: [
          Appointments(
              name: "Ricky J",
              age: 39,
              gender: "Male",
              personId: "230605001700003",
              consultationId: 2306050017000010006,
              consultationIdStr: "2306050017000010006",
              prevRecordId: "",
              idType: "followup",
              consultationStatus: "Pending",
              date: "2023-08-08",
              time: "10:24 AM",
              division: "Khulna",
              country: "Bangladesh",
              phone: "01234567890",
              chiefComplaints: ["dizziness"],
              symptoms: ["sneezing"])
        ],
      ),
    );
    listDateAppointments.add(
      DrAppointmentsModel(
        date: '2023-08-09',
        count: 2,
        appointments: [
          Appointments(
              name: "Ricky J",
              age: 39,
              gender: "Male",
              personId: "230605001700004",
              consultationId: 2306050017000010007,
              consultationIdStr: "2306050017000010007",
              prevRecordId: "",
              idType: "visit",
              consultationStatus: "Pending",
              date: "2023-08-09",
              time: "10:24 AM",
              division: "Khulna",
              country: "Bangladesh",
              phone: "01234567890",
              chiefComplaints: ["dizziness"],
              symptoms: ["sneezing"]),
          Appointments(
              name: "Ricky J",
              age: 39,
              gender: "Male",
              personId: "230605001700005",
              consultationId: 2306050017000010008,
              consultationIdStr: "2306050017000010008",
              prevRecordId: "",
              idType: "followup",
              consultationStatus: "Pending",
              date: "2023-08-09",
              time: "10:24 AM",
              division: "Khulna",
              country: "Bangladesh",
              phone: "01234567890",
              chiefComplaints: ["dizziness"],
              symptoms: ["sneezing"]),
          // Appointments(
          //   age: '39',
          //   appointmentDate: '11-07-2023',
          //   appointmentTime: '08:20 PM',
          //   appointmentType: 'Follow up',
          //   bloodGroup: 'O+ve',
          //   chiefComplaint: 'Dizziness',
          //   image: '',
          //   location: 'Noakhali, Bangladesh',
          //   name: 'Ricky J',
          //   symptoms: 'Runny nose, Postnasal drip, sore throat',
          // ),
        ],
      ),
    );
    // listDateAppointments.add(
    //   DrAppointmentsModel(
    //     date: '14-07-2023',
    //     count: 1,
    //     appointments: [
    //       Appointments(
    //         age: '39',
    //         appointmentDate: '11-07-2023',
    //         appointmentTime: '08:20 PM',
    //         appointmentType: 'Visit',
    //         bloodGroup: 'O+ve',
    //         chiefComplaint: 'Dizziness',
    //         image: '',
    //         location: 'Noakhali, Bangladesh',
    //         name: 'Ricky J',
    //         symptoms: 'Runny nose, Postnasal drip, sore throat',
    //       ),
    //     ],
    //   ),
    // );
    // listDateAppointments.add(
    //   DrAppointmentsModel(
    //     date: '15-07-2023',
    //     count: 1,
    //     appointments: [
    //       Appointments(
    //         age: '39',
    //         appointmentDate: '11-07-2023',
    //         appointmentTime: '08:20 PM',
    //         appointmentType: 'Visit',
    //         bloodGroup: 'O+ve',
    //         chiefComplaint: 'Dizziness',
    //         image: '',
    //         location: 'Noakhali, Bangladesh',
    //         name: 'Ricky J',
    //         symptoms: 'Runny nose, Postnasal drip, sore throat',
    //       ),
    //     ],
    //   ),
    // );
    // listDateAppointments.add(
    //   DrAppointmentsModel(
    //     date: '16-07-2023',
    //     count: 1,
    //     appointments: [
    //       Appointments(
    //         age: '39',
    //         appointmentDate: '11-07-2023',
    //         appointmentTime: '08:20 PM',
    //         appointmentType: 'Visit',
    //         bloodGroup: 'O+ve',
    //         chiefComplaint: 'Dizziness',
    //         image: '',
    //         location: 'Noakhali, Bangladesh',
    //         name: 'Ricky J',
    //         symptoms: 'Runny nose, Postnasal drip, sore throat',
    //       ),
    //     ],
    //   ),
    // );
    // listDateAppointments.add(
    //   DrAppointmentsModel(
    //     date: '17-07-2023',
    //     count: 1,
    //     appointments: [
    //       Appointments(
    //         age: '39',
    //         appointmentDate: '11-07-2023',
    //         appointmentTime: '08:20 PM',
    //         appointmentType: 'Visit',
    //         bloodGroup: 'O+ve',
    //         chiefComplaint: 'Dizziness',
    //         image: '',
    //         location: 'Noakhali, Bangladesh',
    //         name: 'Ricky J',
    //         symptoms: 'Runny nose, Postnasal drip, sore throat',
    //       ),
    //     ],
    //   ),
    // );
    // listDateAppointments.add(
    //   DrAppointmentsModel(
    //     date: '18-07-2023',
    //     count: 1,
    //     appointments: [
    //       Appointments(
    //         age: '39',
    //         appointmentDate: '11-07-2023',
    //         appointmentTime: '08:20 PM',
    //         appointmentType: 'Visit',
    //         bloodGroup: 'O+ve',
    //         chiefComplaint: 'Dizziness',
    //         image: '',
    //         location: 'Noakhali, Bangladesh',
    //         name: 'Ricky J',
    //         symptoms: 'Runny nose, Postnasal drip, sore throat',
    //       ),
    //     ],
    //   ),
    // );
  }

  @override
  void onInit() {
    super.onInit();
    selected.value = -1;
    // setDefaultData();
    getUpcomingAppointments();

    if (Get.arguments != null) consultancyFrom = Get.arguments['consultancyFrom'];
  }

  String setCount(int count) {
    if (count < 10) {
      return '0$count';
    } else {
      return count.toString();
    }
  }

  String setDate(String date, bool match) {
    var dateList = date.split("-");

    var now = DateTime(int.parse(dateList[0]), int.parse(dateList[1]), int.parse(dateList[2]));
    var formatter = DateFormat('dd MMM, yyyy');
    var updatedTime = formatter.format(now);
    // print("FormattedDate $updatedTime");
    if (match) {
      updatedTime = dateMatch(updatedTime);
    }

    return updatedTime;
  }

  String dateMatch(String date) {
    var day = "";
    final now = DateTime.now();
    final yesterday = now.subtract(const Duration(days: 1));
    final tomorrow = now.add(const Duration(days: 1));
    final test = now.add(const Duration(days: 2));

    // print(now.toString());
    // print(yesterday.toString());
    // print(tomorrow.toString());

    var formatter = DateFormat('dd MMM, yyyy');
    // print("Now: ${formatter.format(now)}");
    // print("Yesterday: ${formatter.format(yesterday)}");
    // print("Tomorrow: ${formatter.format(tomorrow)}");
    // print("test: ${formatter.format(test)}");

    if (date == formatter.format(now)) {
      day = "Today";
    } else if (date == formatter.format(tomorrow)) {
      day = "Tomorrow";
    } else {
      day = date;
    }
    // print("MatchingDateResult: $day");
    return day;
  }

  Future<void> getUpcomingAppointments() async {
    http.Response response;
    dynamic object = {};

    object["person_id"] = LoginPreferences().sharedPrefRead(StorageKeys.personId);
    object["consultancy_type"] = ApiEndpoints.consultancyType;

    if (LoginPreferences().sharedPrefRead(StorageKeys.personId) != "0") {
      try {
        response = await api.postData(ApiEndpoints.urlDoctorGetUpcomingAppointments, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            listDateAppointments.value = [];

            print("------------------------------------------------------------");
            // print(jsonEncode(responseBody["upcoming_appointments"][0]).toString());
            var listAppointments = [];
            listAppointments = responseBody["upcoming_appointments"];
            // print(jsonEncode(listAppointments));
            // print(jsonEncode(listAppointments[0]));
            // DrAppointmentsModel appointment = DrAppointmentsModel.fromJson((listAppointments[0]));

            for (dynamic data in listAppointments) {
              DrAppointmentsModel appointment = DrAppointmentsModel.fromJson(data);
              // print(jsonEncode(appointment));
              print(appointment.toJson());
              // print(appointment.date.toString());
              listDateAppointments.add(appointment);
            }
            // print(listAppointments.toString());
            print("------------------------------------------------------------");
          } else if (responseBody["code"] == "404") {
            print("1");
            listDateAppointments.value = [];
            AppStrings.noUpcomingAppointment.showSnackBar;
            await Future.delayed(const Duration(seconds: 3));
          } else {
            print("2");
            AppStrings.somethingWentWrong.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        } else {
          print("3");
          AppStrings.somethingWentWrong.showErrorSnackBar();
          await Future.delayed(const Duration(seconds: 3));
        }
      } catch (e) {
        // TODO
        print(e);
        AppStrings.somethingWentWrong.showErrorSnackBar();
        await Future.delayed(const Duration(seconds: 3));
      }
    } else {
      "Complete your profile First".showErrorSnackBar();
      // AppStrings.somethingWentWrong.showErrorSnackBar();
      await Future.delayed(const Duration(seconds: 3));
    }
  }

  Future<void> cancelSingleAppointment(String consultationId, String idType) async {
    http.Response response;
    dynamic object = {};

    object["consultation_id"] = consultationId;
    object["id_type"] = idType;
    object["requested_by"] = LoginPreferences().sharedPrefRead(StorageKeys.personId);

    if (LoginPreferences().sharedPrefRead(StorageKeys.personId) != "0") {
      try {
        response = await api.postData(ApiEndpoints.urlDoctorCancelOneAppointment, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            String message = responseBody['success_msg'];
            message.showSuccessSnackBar();
            getUpcomingAppointments();
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print(e);
      }
    } else {
      print(AppStrings.somethingWentWrong);
    }
  }

  Future<void> cancelAllNewConsultancy() async {
    http.Response response;
    dynamic object = {};

    object["requested_by"] = LoginPreferences().sharedPrefRead(StorageKeys.personId);

    if (LoginPreferences().sharedPrefRead(StorageKeys.personId) != "0") {
      try {
        response = await api.postData(ApiEndpoints.urlDoctorCancelAllNewConsultancy, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          cancelAllFollowupConsultancy();
          if (responseBody['code'] == "200") {
            String message = responseBody['success_msg'];
            message.showSuccessSnackBar();
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print(e);
      }
    } else {
      print(AppStrings.somethingWentWrong);
    }
  }

  Future<void> cancelAllFollowupConsultancy() async {
    http.Response response;
    dynamic object = {};

    object["requested_by"] = LoginPreferences().sharedPrefRead(StorageKeys.personId);

    if (LoginPreferences().sharedPrefRead(StorageKeys.personId) != "0") {
      try {
        response = await api.postData(ApiEndpoints.urlDoctorCancelAllFollowupConsultancy, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            String message = responseBody['success_msg'];
            message.showSuccessSnackBar();
            getUpcomingAppointments();
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print(e);
      }
    } else {
      print(AppStrings.somethingWentWrong);
    }
  }
}
