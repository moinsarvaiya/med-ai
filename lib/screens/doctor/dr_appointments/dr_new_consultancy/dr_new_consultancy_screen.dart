import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/app_strings.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../../../../constant/app_colors.dart';
import '../../../../models/dr_appointments_model.dart';
import '../../../../utils/functions/custom_functions.dart';
import '../../../../widgets/ripple_effect_widget.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';
import 'dr_new_consultancy_controller.dart';

class DrNewConsultancyScreen extends GetView<DrNewConsultancyController> {
  const DrNewConsultancyScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.colorGrayBackground,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(defaultAppBarHeight),
          child: CustomAppBar(
            titleName: AppStringKey.labelNewConsultancy.tr,
            onClick: () {
              Get.back();
            },
            isBackButtonVisible: consultancyFrom == EnumConsultancyFrom.MY_PATIENT ? true : false,
            isDividerVisible: true,
          ),
        ),
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Obx(
                  () => controller.listDateAppointments.isEmpty
                      ? const SizedBox()
                      : CustomCardWidget(
                          shadowOpacity: shadow,
                          widget: InkWell(
                            onTap: () {
                              Utilities.commonDialog(
                                context,
                                AppStringKey.labelWarning.tr,
                                AppStringKey.cancelAllAppointmentDesc.tr,
                                AppStringKey.deleteYes.tr,
                                AppStringKey.deleteNo.tr,
                                () {
                                  controller.cancelAllNewConsultancy();
                                  Get.back();
                                },
                                () {
                                  Get.back();
                                },
                              );
                            },
                            splashColor: AppColors.colorDarkBlue.withOpacity(0.4),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  AppStringKey.labelCancelAllAppointment.tr,
                                  style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                                ),
                                const SpaceHorizontal(10),
                                const Icon(
                                  Icons.cancel,
                                  color: AppColors.colorPink,
                                  size: 20,
                                ),
                              ],
                            ).paddingSymmetric(vertical: 8),
                          ),
                        ),
                ),
                const SpaceVertical(10),
                Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Obx(
                    () => ListView.builder(
                      key: Key('builder ${controller.selected.value.toString()}'),
                      itemCount: controller.listDateAppointments.length,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, i) {
                        return Theme(
                          data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
                          child: ExpansionTile(
                            key: Key(i.toString()),
                            title: ItemHeading(
                              listAppointmentDate: controller.listDateAppointments,
                              index: i,
                            ),
                            initiallyExpanded: controller.selected.value == i,
                            iconColor: AppColors.colorGreen,
                            //childrenPadding: const EdgeInsets.symmetric(horizontal: 1),
                            collapsedIconColor: AppColors.colorDarkBlue,
                            onExpansionChanged: (newState) {
                              /*if(newState){
                            controller.selected.value = i;
                          }else{
                            controller.selected.value = -1;
                          }*/
                            },
                            children: <Widget>[
                              ItemNewConsultancySection(listAppointmentDetails: controller.listDateAppointments[i].appointments!)
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ),
                Obx(
                  () => controller.listDateAppointments.isEmpty
                      ? SizedBox(
                          child: Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Center(
                                child: Text(
                                  AppStringKey.noUpcomingAppointment.tr,
                                  style: BaseStyle.textStyleNunitoSansSemiBold(16, AppColors.colorDarkBlue),
                                ).paddingAll(20),
                              )),
                        )
                      : const SizedBox(),
                )
              ],
            ),
          ),
        ).marginAll(20));
  }
}

class ItemHeading extends StatelessWidget {
  final RxList<DrAppointmentsModel> listAppointmentDate;
  final int index;

  const ItemHeading({
    Key? key,
    required this.listAppointmentDate,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          Get.find<DrNewConsultancyController>().setDate(listAppointmentDate[index].date!, true),
          // listAppointmentDate[index].date!,
          style: BaseStyle.textStyleNunitoSansSemiBold(16, AppColors.colorDarkBlue),
        ),
        Container(
          padding: const EdgeInsets.all(5),
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: AppColors.colorLightSkyBlue,
          ),
          child: Text(
            Get.find<DrNewConsultancyController>().setCount(listAppointmentDate[index].count!),
            style: BaseStyle.textStyleNunitoSansSemiBold(16, AppColors.colorDarkBlue),
          ),
        ),
      ],
    );
  }
}

class ItemNewConsultancySection extends StatelessWidget {
  final List<Appointments> listAppointmentDetails;
  final controller = Get.find<DrNewConsultancyController>();

  ItemNewConsultancySection({
    Key? key,
    required this.listAppointmentDetails,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: listAppointmentDetails.length,
        itemBuilder: (context, i) {
          return Container(
            clipBehavior: Clip.antiAliasWithSaveLayer,
            decoration: BoxDecoration(
              color: AppColors.colorBackground,
              borderRadius: BorderRadius.circular(5),
            ),
            child: RippleEffectWidget(
              borderRadius: 5,
              callBack: () {
                if (listAppointmentDetails[i].idType!.toLowerCase() == 'visit') {
                  appointmentType = EnumAppointmentType.VISIT;
                  var consultationType = listAppointmentDetails[i].idType;
                  var consultationId = listAppointmentDetails[i].consultationIdStr;
                  var patientId = listAppointmentDetails[i].personId;
                  var schedule = "${controller.setDate(listAppointmentDetails[i].date!, false)} | ${listAppointmentDetails[i].time}";
                  var bundle = {
                    "consultationType": consultationType,
                    "consultationId": consultationId,
                    "patientId": patientId,
                    "schedule": schedule
                  };
                  Get.toNamed(RoutePaths.DR_VISIT_APPOINTMENT, arguments: {
                    AppStrings.forwardArgument: jsonEncode(bundle),
                  });
                } else {
                  appointmentType = EnumAppointmentType.FOLLOWUP;
                  var consultationType = listAppointmentDetails[i].idType;
                  var consultationId = listAppointmentDetails[i].consultationIdStr;
                  var patientId = listAppointmentDetails[i].personId;
                  var schedule = "${controller.setDate(listAppointmentDetails[i].date!, false)} | ${listAppointmentDetails[i].time}";
                  var bundle = {
                    "consultationType": consultationType,
                    "consultationId": consultationId,
                    "patientId": patientId,
                    "schedule": schedule
                  };
                  Get.toNamed(RoutePaths.DR_FOLLOWUP_APPOINTMENT, arguments: {
                    AppStrings.forwardArgument: jsonEncode(bundle),
                  });
                }

                // Get.find<DrNewConsultancyController>().getUpcomingAppointments();
                // Get.find<DrNewConsultancyController>().dateMatch(listAppointmentDetails[i].date!);
                // Get.find<DrNewConsultancyController>().setDate(listAppointmentDetails[i].date!);
                //  Get.toNamed(RoutePaths.DR_CONSULTANCY_HISTORY_DETAIL);
              },
              widget: Container(
                width: double.maxFinite,
                padding: const EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: AppColors.colorDarkBlue,
                        // color: listAppointmentDetails[i].idType!.toLowerCase() == 'visit' ? AppColors.colorSkyBlue : AppColors.colorDarkOrange,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        // crossAxisAlignment: listAppointmentDetails[i].idType!.toLowerCase() == 'visit' ? CrossAxisAlignment.start : CrossAxisAlignment.end,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppStringKey.strAppointmentSchdule.tr,
                                style: BaseStyle.textStyleNunitoSansRegular(
                                  12,
                                  AppColors.white,
                                ),
                              ),
                              const SpaceVertical(5),
                              Row(
                                children: [
                                  Image.asset(
                                    AppImages.calender,
                                    width: 20,
                                    height: 20,
                                  ),
                                  const SpaceHorizontal(5),
                                  Text(
                                    '${Get.find<DrNewConsultancyController>().setDate(listAppointmentDetails[i].date!, false)} | ${listAppointmentDetails[i].time}',
                                    style: BaseStyle.textStyleNunitoSansBold(
                                      14,
                                      AppColors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          GestureDetector(
                            onTap: () {
                              if (listAppointmentDetails[i].idType!.toLowerCase() == 'visit') {
                                Get.toNamed(RoutePaths.DR_VISIT_APPOINTMENT);
                              } else {
                                Get.toNamed(RoutePaths.DR_FOLLOWUP_APPOINTMENT);
                              }
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 3),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                // color: AppColors.colorDarkBlue,
                                color: listAppointmentDetails[i].idType!.toLowerCase() == 'visit'
                                    ? AppColors.colorDarkOrange
                                    : AppColors.colorSkyBlue,
                              ),
                              child: Center(
                                child: Text(
                                  listAppointmentDetails[i].idType!.toLowerCase() == 'visit' ? 'Visit' : 'Follow up',
                                  style: BaseStyle.textStyleDomineSemiBold(
                                    10,
                                    AppColors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SpaceVertical(14),
                    Row(
                      children: [
                        Container(
                          height: 55,
                          width: 55,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            image: const DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage(
                                AppImages.dummyProfile,
                              ),
                            ),
                          ),
                        ),
                        const SpaceHorizontal(10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              listAppointmentDetails[i].name!,
                              style: BaseStyle.textStyleDomineBold(
                                16,
                                AppColors.colorDarkBlue,
                              ),
                            ),
                            Text(
                              '${listAppointmentDetails[i].age} ${AppStringKey.yearsOld.tr}',
                              // '${listAppointmentDetails[i].age} Years Old, ${listAppointmentDetails[i].bloodGroup}',
                              style: BaseStyle.textStyleDomineBold(
                                12,
                                AppColors.colorDarkBlue,
                              ),
                            ),
                            Text(
                              '${listAppointmentDetails[i].division!}, ${listAppointmentDetails[i].country!}',
                              style: BaseStyle.textStyleNunitoSansRegular(
                                12,
                                AppColors.colorDarkBlue,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SpaceVertical(10),
                    Text(
                      AppStringKey.labelChiefComplaint.tr,
                      style: BaseStyle.textStyleDomineBold(
                        16,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    Text(
                      CustomFunctions.setStringFromListString(listAppointmentDetails[i].chiefComplaints!),
                      // listAppointmentDetails[i].chiefComplaints!.toString(),
                      style: BaseStyle.textStyleNunitoSansRegular(
                        14,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    const SpaceVertical(15),
                    Text(
                      AppStringKey.labelSymptoms.tr,
                      style: BaseStyle.textStyleDomineBold(
                        16,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    Text(
                      CustomFunctions.setStringFromListString(listAppointmentDetails[i].symptoms!),
                      // listAppointmentDetails[i].symptoms!.toString(),
                      style: BaseStyle.textStyleNunitoSansRegular(
                        14,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    const SpaceVertical(15),
                    CustomCardWidget(
                      shadowOpacity: shadow,
                      widget: InkWell(
                        onTap: () {
                          Utilities.commonDialog(
                            context,
                            AppStringKey.labelWarning.tr,
                            AppStringKey.cancelSingleAppointmentDesc.tr,
                            AppStringKey.deleteYes.tr,
                            AppStringKey.deleteNo.tr,
                            () {
                              controller.cancelSingleAppointment(
                                listAppointmentDetails[i].consultationIdStr.toString(),
                                listAppointmentDetails[i].idType!,
                              );
                              Get.back();
                            },
                            () {
                              Get.back();
                            },
                          );
                        },
                        splashColor: AppColors.colorDarkBlue.withOpacity(0.4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppStringKey.labelCancelAppointment.tr,
                              style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                            ),
                            const SpaceHorizontal(10),
                            const Icon(
                              Icons.cancel,
                              color: AppColors.colorPink,
                              size: 20,
                            ),
                          ],
                        ).paddingSymmetric(vertical: 8),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ).marginOnly(bottom: 8);
        });
  }
}
