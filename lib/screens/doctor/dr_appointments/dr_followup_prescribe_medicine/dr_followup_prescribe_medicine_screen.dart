import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../models/dr_prescribe_medicine_model.dart';
import '../../../../routes/route_paths.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/space_horizontal.dart';
import '../dr_followup_appointment/dr_followup_appointment_screen.dart';
import 'dr_followup_prescribe_medicine_controller.dart';

class DrFollowupPrescribeMedicineScreen extends GetView<DrFollowupPrescribeMedicineController> {
  const DrFollowupPrescribeMedicineScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: AppStrings.titlePrescribe,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: SubmitButton(
        title: Get.previousRoute == RoutePaths.DR_FOLLOWUP_COMPLETE_APPOINTMENT ? AppStrings.btnUpdate : AppStrings.strNext,
        onClick: () {
          controller.addMedicinePrescriptionToServer();
          /*print(Get.previousRoute.toString());
          if (Get.previousRoute == RoutePaths.DR_FOLLOWUP_COMPLETE_APPOINTMENT) {
            Get.back();
          } else {
            Get.toNamed(RoutePaths.DR_FOLLOWUP_UPDATE_DIAGNOSTICS_TEST);
          }*/
        },
      ).paddingOnly(left: 20, right: 20, top: 15, bottom: MediaQuery.viewInsetsOf(context).bottom + 15),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const ItemRemarks(),
              Obx(
                () => ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: controller.medicineList.length,
                    itemBuilder: (context, i) {
                      return ItemPrescription(
                        medicines: controller.medicineList[i],
                        callback: () {
                          Utilities.commonDialog(
                            context,
                            AppStrings.strAlert,
                            'Do you want to remove Napasin from the prescription?',
                            AppStrings.deleteYes,
                            AppStrings.deleteNo,
                            () {
                              Get.back();
                              controller.medicineList.removeAt(i);
                              controller.medicineList.refresh();
                            },
                            () {
                              Get.back();
                            },
                          );
                        },
                      );
                    }),
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemRemarks extends StatelessWidget {
  const ItemRemarks({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppStrings.labelRemarks,
            style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
          ),
          const SpaceVertical(5),
          Text(
            'Based on the symptomps patient has provided, the patient may have developer a stomach ulcer related problem',
            style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorTextDarkGray),
          ),
          const SpaceVertical(14),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStrings.strMedication,
                    style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                  ),
                  const SpaceVertical(5),
                  Text(
                    'No medication added yet',
                    style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorTextDarkGray),
                  ),
                ],
              ),
              Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                child: InkWell(
                  onTap: () async {
                    var result = await Get.toNamed(RoutePaths.DR_ADD_NEW_MEDICINE);
                    /*if (result == '1') {
                      Get.find<DrFollowupPrescribeMedicineController>()
                          .addNewMedicine();
                    }*/
                  },
                  child: const Icon(
                    Icons.add,
                    size: 30,
                    color: AppColors.colorDarkBlue,
                  ).paddingAll(5),
                ),
              ),
            ],
          )
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 10),
    );
  }
}

class ItemPrescription extends StatelessWidget {
  final Function()? callback;
  final DrPrescribeMedicineModel medicines;

  const ItemPrescription({
    Key? key,
    this.callback,
    required this.medicines,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: SizedBox(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: SubItemPreviousDecision(title: AppStrings.strBrand, value: medicines.brandName!)),
                Expanded(
                  child: SubItemPreviousDecision(
                      title: AppStrings.labelMedicineTiming,
                      value:
                          medicines.threeTimesDay!.isNotEmpty ? '${medicines.threeTimesDay!} / Day' : '${medicines.hourDifference!} Hours'),
                ),
              ],
            ),
            const SpaceVertical(15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: SubItemPreviousDecision(title: AppStrings.labelMealInstruction, value: medicines.mealInstruction!)),
                Expanded(child: SubItemPreviousDecision(title: AppStrings.strDuration, value: medicines.duration!)),
              ],
            ),
            const SpaceVertical(15),
            SubItemPreviousDecision(title: AppStrings.labelSpecialInstruction, value: medicines.specialInstruction!),
            const SpaceVertical(15),
            Center(
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                child: InkWell(
                  onTap: callback,
                  splashColor: AppColors.colorDarkBlue.withOpacity(0.4),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.cancel,
                        color: AppColors.colorPink,
                        size: 18,
                      ),
                      const SpaceHorizontal(5),
                      Text(
                        AppStrings.strRemove,
                        style: BaseStyle.textStyleNunitoSansBold(10, AppColors.colorDarkBlue),
                      ),
                    ],
                  ).paddingSymmetric(vertical: 8, horizontal: 40),
                ),
              ),
            ),
          ],
        ).paddingSymmetric(horizontal: 8, vertical: 15),
      ),
    );
  }
}
