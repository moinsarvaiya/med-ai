import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_interface/api_interface.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/models/dr_followup_consultation_details_model.dart';
import 'package:med_ai/models/dr_followup_soap_model.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_new_consultancy/dr_new_consultancy_controller.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../models/dr_prescribe_medicine_model.dart';
import '../../../../utils/app_preference/login_preferences.dart';

class DrFollowUpAppointmentController extends BaseController {
  dynamic bundle;
  var api = ApiCallObject();
  var personId = "";
  var consultationId = "";
  var consultationType = "";
  var consultationSchedule = "".obs;
  var isMedicineListUpdated = false.obs;

  var name = "-".obs;
  var ageGender = "-".obs;
  var districtCountry = "-".obs;

  var previousIdentifiedDisease = "-".obs;
  var previousClientNote = "-".obs;

  var medicineStatusNameList = [].obs;
  var medicineStatusStatusList = [].obs;

  var diagnosticStatusNameList = [].obs;
  var diagnosticStatusResultList = [].obs;
  var diagnosticStatusImgList = [].obs;

  var previousComplaintsList = [].obs;
  var previousSymptomsList = [].obs;
  var currentComplaintsList = [].obs;
  var currentSymptomsList = [].obs;

  var vitalSignBp = "".obs;
  var vitalSignTemp = "".obs;
  var vitalSignPulseRate = "".obs;
  var vitalSignBreathingRate = "".obs;

  var medicalHistoryCondition = "".obs;
  var medicalHistoryAllergies = "".obs;
  var medicalHistoryFamilyHistory = "".obs;

  var preExistingCondition = "".obs;
  var preExistingMedication = "".obs;

  late DrFollowupConsultationDetailsModel consultationDetailModel;
  late DrFollowupSoapModel soapModel;

  final RxList<DrPrescribeMedicineModel> listMedicines = RxList();
  var dummyText = 'Please not that the information provided by MedAi is provided solely for guideline purposes and is not '
      'a qualified medical opinion. This information should not be considered advice or an opinion. of doctor or other health'
      ' professional about your actual medical state and you should see a doctor for any symptoms you may have. If you are experiencing a health emergency,'
      ' you should call your local emergency number immediately to request emergency medical assistance.';

  @override
  void onInit() {
    super.onInit();
    setData();
    personId = LoginPreferences().sharedPrefRead(StorageKeys.personId);

    getConsultationDetails();
    getSoapInfo();

    // listMedicines.add(
    //   DrPrescribeMedicineModel(
    //     brandName: 'Napasin',
    //     medicineTiming: 'Hour Difference/ Day',
    //     mealInstruction: 'After',
    //     duration: '7 Days',
    //     specialInstruction: 'Lorem Ipsum is simply dummy text of the printing and typeset',
    //   ),
    // );
    // listMedicines.add(
    //   DrPrescribeMedicineModel(
    //     brandName: 'Napasin',
    //     medicineTiming: 'Hour Difference/ Day',
    //     mealInstruction: 'After',
    //     duration: '7 Days',
    //     specialInstruction: 'Lorem Ipsum is simply dummy text of the printing and typeset',
    //   ),
    // );
  }

  void setData() {
    try {
      bundle = jsonDecode(Get.arguments[AppStrings.forwardArgument]);
      consultationId = bundle['consultationId'];
      consultationType = bundle['consultationType'];
      consultationSchedule.value = bundle['schedule'];
      print("FollowUp Appointment");
      print("consultationType: $consultationType");
      print("consultationId: $consultationId");
      print("consultationSchedule: $consultationSchedule");
      print("patientId: ${bundle['patientId']}");
    } catch (e) {
      print(e);
    }
  }

  Future<void> cancelSingleAppointment() async {
    http.Response response;
    dynamic object = {};

    object["consultation_id"] = consultationId;
    object["id_type"] = consultationType;
    object["requested_by"] = personId;

    if (personId != "0") {
      try {
        response = await api.postData(ApiEndpoints.urlDoctorCancelOneAppointment, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            String message = responseBody['success_msg'];
            Get.back();
            message.showSuccessSnackBar();
            await Future.delayed(const Duration(seconds: 3));
            Get.find<DrNewConsultancyController>().getUpcomingAppointments();
            Get.close(1);
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print(e);
      }
    } else {
      print(AppStrings.somethingWentWrong);
    }
  }

  Future<void> getConsultationDetails() async {
    http.Response response;
    dynamic object = {};

    object["consultation_id"] = consultationId;
    object["id_type"] = consultationType;

    if (personId != "0") {
      try {
        response = await api.postData(ApiEndpoints.urlGetConsultationDetails, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody["code"] == "200") {
            consultationDetailModel = DrFollowupConsultationDetailsModel.fromJson(responseBody);
            var consultationDetail = consultationDetailModel.consultationDetail;

            //patient details
            name.value = consultationDetail?.name ?? "Guest user";
            ageGender.value = "${consultationDetail?.age} ${AppStringKey.yearsOld.tr}, ${consultationDetail?.gender}";
            districtCountry.value = "${consultationDetail?.district}, ${consultationDetail?.country}";

            print("name: $name");
            print("name: $ageGender");
            print("name: $districtCountry");

            //prescribed medication
            var prescribedMedicine = consultationDetail?.prevMedicineList ?? [];
            for (MedicineList medicineData in prescribedMedicine) {
              var medicineTiming = "";
              if (medicineData.hourlyInterval!.isNotEmpty) {
                medicineTiming = "After every ${medicineData.hourlyInterval} hours";
              } else {
                if (medicineData.breakfast.toString().toLowerCase() == "yes") {
                  medicineTiming += "1+";
                } else {
                  medicineTiming += "0+";
                }
                if (medicineData.lunch.toString().toLowerCase() == "yes") {
                  medicineTiming += "1+";
                } else {
                  medicineTiming += "0+";
                }
                if (medicineData.dinner.toString().toLowerCase() == "yes") {
                  medicineTiming += "1";
                } else {
                  medicineTiming += "0";
                }
              }

              var data = DrPrescribeMedicineModel(
                brandName: medicineData.medicine,
                duration: medicineData.duration,
                dosageType: '',
                hourDifference: '',
                mealInstruction: medicineData.foodIntake == "Yes" ? "After" : "Before",
                threeTimesDay: medicineTiming,
                specialInstruction: medicineData.specialInstruction,
              );
              listMedicines.add(data);
            }
            print("medicineListSize: ${listMedicines.length}");
            listMedicines.refresh();
            isMedicineListUpdated.value = true;

            //medicine status
            var medicineStatus = consultationDetail?.medicationStatus;
            for (MedicationStatus data in medicineStatus!) {
              medicineStatusNameList.add(data.drugName);
              medicineStatusStatusList.add(data.status);
            }

            //diagnostic status
            var diagnosticStatus = consultationDetail?.diagnosticsStatus ?? [];
            for (DiagnosticsStatus data in diagnosticStatus) {
              diagnosticStatusNameList.add(data.testName);
              diagnosticStatusResultList.add(data.testResult);
              diagnosticStatusImgList.add(data.testImg);
            }
            // print(jsonEncode(diagnosticStatusImgList));

            //previous complaints and symptoms
            var previousSymptoms = consultationDetail?.previousSymptoms! ?? [];
            for (String data in previousSymptoms) {
              previousSymptomsList.add(data);
            }

            //new complaints and symptoms
            var newSymptoms = consultationDetail?.currentSymptoms! ?? [];
            for (String data in newSymptoms) {
              currentSymptomsList.add(data);
            }

            //vital signs
            var vitalSigns = consultationDetail?.newVitalSigns;
            vitalSignBp.value = vitalSigns?.newBp ?? "-";
            vitalSignTemp.value = vitalSigns?.newTemperature ?? "-";
            vitalSignPulseRate.value = vitalSigns?.newPulse ?? "-";
            vitalSignBreathingRate.value = vitalSigns?.newBreathing ?? "-";
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print("Error Occurred");
        print(e);
      }
    } else {
      print(AppStrings.somethingWentWrong);
    }
  }

  Future<void> getSoapInfo() async {
    http.Response response;
    dynamic object = {};

    object["consultation_id"] = consultationId;
    object["id_type"] = consultationType;

    if (personId != "0") {
      try {
        response = await api.postData(ApiEndpoints.urlDoctorGetFollowupSoap, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            soapModel = DrFollowupSoapModel.fromJson(responseBody);
            var soapInfo = soapModel.soapInfo;

            //previous decision
            var prescriptionInfo = soapModel.prescriptionInfo;
            previousIdentifiedDisease.value = CustomFunctions.setStringFromListString(prescriptionInfo?.decision?.identifiedDisease ?? []);
            previousClientNote.value = prescriptionInfo?.decision?.clinicalNote ?? "-";

            //medical history
            var medicalHistory = soapInfo?.medicalHistory;
            medicalHistoryCondition.value = CustomFunctions.setStringFromListString(medicalHistory?.medicalCondition);
            medicalHistoryAllergies.value = medicalHistory!.allergy ?? "-";
            medicalHistoryFamilyHistory.value = CustomFunctions.setStringFromListString(medicalHistory?.familyHistory);

            //pre existing condition
            var preExistingCond = soapInfo?.preExistingCond;
            preExistingCondition.value = preExistingCond?.medicalCondition ?? "-";
            preExistingMedication.value = preExistingCond?.medication ?? "-";
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print(e);
      }
    } else {
      print(AppStrings.somethingWentWrong);
    }
  }

  void showData() {
    print("showData");
    print(jsonEncode(consultationDetailModel.consultationDetail!.prevMedicineList![0]).toString());
  }
}
