import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/app_preference/followup_preferences.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_content_visit_followup_widget.dart';
import '../../../../widgets/custom_heading_visit_followup.dart';
import '../../../../widgets/custom_pager_indicator.dart';
import '../../../../widgets/ripple_effect_widget.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';
import '../dr_visit_complete_appointment/dr_visit_complete_appointment_screen.dart';
import 'dr_followup_appointment_controller.dart';

class DrFollowUpAppointmentScreen extends GetView<DrFollowUpAppointmentController> {
  const DrFollowUpAppointmentScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const ItemPatientDetail(),
              const SpaceVertical(10),
              const ItemPreviousDecision(),
              const SpaceVertical(10),
              // Obx(() => CustomPagerIndicator(
              //   listMedicines: controller.isDataLoad.value ? controller.listMedicines : controller.listMedicines,
              //   visibleEdit: false,
              // )),
              Obx(
                () => controller.isMedicineListUpdated.value && controller.listMedicines.isNotEmpty
                    ? CustomPagerIndicator(
                        listMedicines: controller.listMedicines,
                        visibleEdit: false,
                      )
                    : CardEditSection(
                        title: AppStringKey.strPrescribedMedication.tr,
                        visibleEdit: false,
                        callBack: () {},
                        child: const SizedBox(),
                      ),
              ),
              const SpaceVertical(10),
              const ItemMedicineStatus(),
              const SpaceVertical(10),
              const ItemDiagnosticTestStatus(),
              const SpaceVertical(10),
              const ItemPreviousComplaintAndSymptoms(),
              const SpaceVertical(10),
              const ItemNewComplaintAndSymptoms(),
              const SpaceVertical(10),
              const ItemMedicalHistory(),
              const SpaceVertical(10),
              const ItemPreExistingConditions(),
              const SpaceVertical(10),
              const ItemVitalSigns(),
              const SpaceVertical(50),
              Center(
                child: Text(
                  AppStringKey.strDisclaimer.tr,
                  style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
                ),
              ),
              const SpaceVertical(10),
              Text(
                controller.dummyText,
                style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(30),
              SubmitButton(
                title: AppStringKey.strNext.tr,
                onClick: () {
                  print("FollowupID: ${controller.consultationId}");
                  FollowupPreferences().sharedPrefWrite(StorageKeys.followupID, controller.consultationId);
                  FollowupPreferences().sharedPrefWrite("consultationType", controller.consultationType);
                  FollowupPreferences().sharedPrefWrite("selectedSymptoms", controller.currentSymptomsList);
                  // Get.toNamed(RoutePaths.DR_IDENTIFIED_DISEASE);
                  Get.toNamed(RoutePaths.DR_FOLLOWUP_IDENTIFIED_DISEASE);
                },
              )
            ],
          ).paddingAll(15),
        ),
      ),
    );
  }
}

class ItemPatientDetail extends StatelessWidget {
  static dynamic controller = Get.find<DrFollowUpAppointmentController>();

  const ItemPatientDetail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Container(
        width: double.maxFinite,
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
              width: double.maxFinite,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: AppColors.colorDarkBlue,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.strAppointmentSchdule.tr,
                        style: BaseStyle.textStyleNunitoSansRegular(
                          12,
                          AppColors.white,
                        ),
                      ),
                      const SpaceVertical(5),
                      Row(
                        children: [
                          Image.asset(
                            AppImages.calender,
                            width: 20,
                            height: 20,
                          ),
                          const SpaceHorizontal(5),
                          Obx(
                            () => Text(
                              controller.consultationSchedule.value,
                              style: BaseStyle.textStyleNunitoSansBold(
                                14,
                                AppColors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 3),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: AppColors.colorSkyBlue,
                    ),
                    child: Center(
                      child: Text(
                        AppStringKey.followUp.tr,
                        style: BaseStyle.textStyleDomineSemiBold(
                          10,
                          AppColors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SpaceVertical(14),
            Obx(
              () => Row(
                children: [
                  Container(
                    height: 55,
                    width: 55,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      image: const DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(
                          AppImages.dummyProfile,
                        ),
                      ),
                    ),
                  ),
                  const SpaceHorizontal(10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        controller.name.value,
                        // 'Ricky J',
                        style: BaseStyle.textStyleDomineBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      Text(
                        controller.ageGender.value,
                        // '32 Years Old, B+ve',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      Text(
                        controller.districtCountry.value,
                        // 'Noakhali, Bangladesh',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SpaceVertical(15),
            Row(
              children: [
                Expanded(
                  child: CustomCardWidget(
                    shadowOpacity: shadow,
                    widget: RippleEffectWidget(
                      callBack: () {
                        // controller.showData();
                      },
                      borderRadius: 5,
                      widget: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.videocam_outlined,
                            color: AppColors.colorOrange,
                            size: 20,
                          ),
                          const SpaceHorizontal(8),
                          Text(
                            AppStringKey.labelInAppVideoCall.tr,
                            style: BaseStyle.textStyleNunitoSansBold(10, AppColors.colorDarkBlue),
                          ),
                        ],
                      ).paddingSymmetric(vertical: 8),
                    ),
                  ),
                ),
                Expanded(
                  child: CustomCardWidget(
                    shadowOpacity: shadow,
                    widget: RippleEffectWidget(
                      callBack: () {
                        Utilities.commonDialog(
                          context,
                          AppStringKey.labelWarning.tr,
                          AppStringKey.cancelSingleAppointmentDesc.tr,
                          AppStringKey.deleteYes.tr,
                          AppStringKey.deleteNo.tr,
                          () {
                            Get.find<DrFollowUpAppointmentController>().cancelSingleAppointment();
                          },
                          () {
                            Get.back();
                          },
                        );
                      },
                      borderRadius: 5,
                      widget: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.cancel,
                            color: AppColors.colorPink,
                            size: 20,
                          ),
                          const SpaceHorizontal(8),
                          Text(
                            AppStringKey.labelCancelAppointment.tr,
                            style: BaseStyle.textStyleNunitoSansBold(10, AppColors.colorDarkBlue),
                          ),
                        ],
                      ).paddingSymmetric(vertical: 8),
                    ),
                  ),
                ),
              ],
            ),
            const SpaceVertical(10),
          ],
        ),
      ),
    );
  }
}

class ItemPreviousDecision extends StatelessWidget {
  static DrFollowUpAppointmentController controller = Get.find<DrFollowUpAppointmentController>();

  const ItemPreviousDecision({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: SizedBox(
        width: double.infinity,
        child: Obx(
          () => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomHeadingVisitFollowUp(
                heading: AppStringKey.labelPreviousDecision.tr,
              ),
              SpaceVertical(15),
              SubItemPreviousDecision(
                title: AppStringKey.strIdentifiedDisease.tr,
                value: controller.previousIdentifiedDisease.value.isNotEmpty ? controller.previousIdentifiedDisease.value : "-",
              ),
              SpaceVertical(20),
              SubItemPreviousDecision(
                title: AppStringKey.labelClientNote.tr,
                value: controller.previousClientNote.value.isNotEmpty ? controller.previousClientNote.value : "-",
              ),
            ],
          ).paddingSymmetric(horizontal: 8, vertical: 15),
        ),
      ),
    );
  }
}

class ItemMedicineStatus extends StatelessWidget {
  static DrFollowUpAppointmentController controller = Get.find<DrFollowUpAppointmentController>();

  const ItemMedicineStatus({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomHeadingVisitFollowUp(
            heading: AppStringKey.labelMedicineStatus.tr,
          ),
          const SpaceVertical(10),
          Obx(
            () => ListView.builder(
              itemCount: controller.medicineStatusNameList.length,
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    const SpaceVertical(10),
                    CustomContentVisitFollowUpWidget(
                      title: controller.medicineStatusNameList[index],
                      value: controller.medicineStatusStatusList[index],
                    ),
                  ],
                );
              },
            ),
          ),
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}

class ItemDiagnosticTestStatus extends StatelessWidget {
  static DrFollowUpAppointmentController controller = Get.find<DrFollowUpAppointmentController>();

  const ItemDiagnosticTestStatus({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomHeadingVisitFollowUp(
              heading: AppStringKey.labelDiagnosticTestStatus.tr,
            ),
            // SpaceVertical(10),
            Obx(
              () => ListView.builder(
                itemCount: controller.diagnosticStatusNameList.length,
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return SubItemDiagnosticTestStatus(
                    title: controller.diagnosticStatusNameList[index],
                    value: controller.diagnosticStatusResultList[index],
                    imageList: controller.diagnosticStatusImgList[index],
                  );
                },
              ),
            ),
            // SubItemDiagnosticTestStatus(
            //   title: 'Defera 500 mg',
            //   value: 'Normal',
            // ),
            // SpaceVertical(10),
            // SubItemDiagnosticTestStatus(
            //   title: 'Blood cultures',
            //   value: '',
            // ),
          ],
        ).paddingSymmetric(horizontal: 8, vertical: 15),
      ),
    );
  }
}

class ItemPreviousComplaintAndSymptoms extends StatelessWidget {
  const ItemPreviousComplaintAndSymptoms({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomHeadingVisitFollowUp(
            heading: AppStringKey.labelPreviousComplaintsAndSymptoms.tr,
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.labelChiefComplaint.tr,
            value: '-',
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.labelSymptoms.tr,
              value: CustomFunctions.setStringFromListString(Get.find<DrFollowUpAppointmentController>().previousSymptomsList),
            ),
          ),
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}

class ItemNewComplaintAndSymptoms extends StatelessWidget {
  static DrFollowUpAppointmentController controller = Get.find<DrFollowUpAppointmentController>();

  const ItemNewComplaintAndSymptoms({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CustomHeadingVisitFollowUp(
                heading: AppStringKey.labelNewComplaintsAndSymptoms.tr,
              ),
              RippleEffectWidget(
                borderRadius: 5,
                callBack: () {
                  var bundle = {
                    'consultationType': controller.consultationType,
                    'consultationId': controller.consultationId,
                    'selectedSymptoms': controller.currentSymptomsList,
                  };
                  Get.toNamed(
                    RoutePaths.DR_FOLLOWUP_UPDATE_SYMPTOMS,
                    arguments: {
                      AppStrings.forwardArgument: jsonEncode(bundle),
                    },
                  );
                },
                widget: Image.asset(
                  AppImages.edit,
                  height: 20,
                  width: 20,
                ).paddingAll(5),
              )
            ],
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strChiefComplaints.tr,
            value: '-',
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.strSymptoms.tr,
              value: CustomFunctions.setStringFromListString(controller.currentSymptomsList),
            ),
          ),
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}

class ItemMedicalHistory extends StatelessWidget {
  static DrFollowUpAppointmentController controller = Get.find<DrFollowUpAppointmentController>();

  const ItemMedicalHistory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomHeadingVisitFollowUp(
              heading: AppStringKey.strMedicalHistory.tr,
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strMedicalCondition.tr,
              value: controller.medicalHistoryCondition.value.isNotEmpty ? controller.medicalHistoryCondition.value : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strAllergies.tr,
              value: controller.medicalHistoryAllergies.value.isNotEmpty ? controller.medicalHistoryAllergies.value : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strFamilyHistory.tr,
              value: controller.medicalHistoryFamilyHistory.value.isNotEmpty ? controller.medicalHistoryFamilyHistory.value : "-",
            ),
          ],
        ).paddingSymmetric(horizontal: 8, vertical: 15),
      ),
    );
  }
}

class ItemPreExistingConditions extends StatelessWidget {
  static DrFollowUpAppointmentController controller = Get.find<DrFollowUpAppointmentController>();

  const ItemPreExistingConditions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomHeadingVisitFollowUp(
              heading: AppStringKey.labelPreExistingConditions.tr,
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strMedicalCondition.tr,
              value: controller.preExistingCondition.value.isNotEmpty ? controller.preExistingCondition.value : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strMedication.tr,
              value: controller.preExistingMedication.value.isNotEmpty ? controller.preExistingMedication.value : "-",
            ),
          ],
        ).paddingSymmetric(horizontal: 8, vertical: 15),
      ),
    );
  }
}

class ItemVitalSigns extends StatelessWidget {
  static DrFollowUpAppointmentController controller = Get.find<DrFollowUpAppointmentController>();

  const ItemVitalSigns({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomHeadingVisitFollowUp(
              heading: AppStringKey.strVitalSigns.tr,
            ),
            const SpaceVertical(15),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strBP.tr,
              value: controller.vitalSignBp.value.isNotEmpty ? controller.vitalSignBp.value : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strTemp.tr,
              value: controller.vitalSignTemp.value.isNotEmpty ? "${controller.vitalSignTemp.value}°c" : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strPulseRate.tr,
              value: controller.vitalSignPulseRate.value.isNotEmpty ? controller.vitalSignPulseRate.value : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strBreathingsRate.tr,
              value: controller.vitalSignBreathingRate.value.isNotEmpty ? controller.vitalSignBreathingRate.value : "-",
            ),
          ],
        ).paddingSymmetric(horizontal: 8, vertical: 15),
      ),
    );
  }
}

class SubItemPreviousDecision extends StatelessWidget {
  final String title;
  final String value;

  const SubItemPreviousDecision({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
        ),
        const SpaceVertical(3),
        Text(
          value,
          style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
        ),
      ],
    );
  }
}

class SubItemDiagnosticTestStatus extends StatelessWidget {
  final String title;
  final String value;
  final List<String> imageList;

  const SubItemDiagnosticTestStatus({
    Key? key,
    required this.title,
    required this.value,
    required this.imageList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SpaceVertical(10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 5,
              child: Text(
                title,
                style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
              ),
            ),
            Expanded(
              flex: 4,
              child: Text(
                ' : $value',
                style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
              ),
            ),
            // Text(imageList.first.length.toString()),
            imageList.isNotEmpty && imageList.first.isNotEmpty
                ? Expanded(
                    child: GestureDetector(
                      onTap: () {
                        // var msg = imageList.length.toString();
                        // msg.showSnackBar;
                        var bundle = {
                          "imageList": imageList,
                        };
                        Get.toNamed(
                          RoutePaths.DR_DIAGNOSTICS_TEST_IMAGES,
                          arguments: {
                            AppStrings.forwardArgument: jsonEncode(bundle),
                          },
                        );
                      },
                      child: Image.asset(
                        AppImages.diagnosticTest,
                        height: 25,
                        width: 25,
                      ),
                    ),
                  )
                : const Expanded(
                    child: SizedBox(width: 25, height: 25),
                  ),
          ],
        ),
      ],
    );
  }
}
