import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_follow_up_complete_appointment/dr_follow_up_complete_appointment_controller.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_content_visit_followup_widget.dart';
import '../../../../widgets/custom_heading_visit_followup.dart';
import '../../../../widgets/custom_pager_indicator.dart';
import '../../../../widgets/ripple_effect_widget.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';
import '../../dr_consultancy_history_detail/dr_consultancy_history_detail_screen.dart';
import '../dr_visit_complete_appointment/dr_visit_complete_appointment_screen.dart';

class DrFollowUpCompleteAppointmentScreen extends GetView<DrFollowUpCompleteAppointmentController> {
  const DrFollowUpCompleteAppointmentScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const ItemPatientDetail(),
              const SpaceVertical(10),
              const ItemComplaintsAndSymptoms(),
              const SpaceVertical(10),
              const ItemMedicalHistory(),
              const SpaceVertical(10),
              const ItemPreExistingConditions(),
              const SpaceVertical(10),
              const ItemVitalSigns(),
              const SpaceVertical(10),
              const DecisionSection(),
              const SpaceVertical(10),
              const ItemMedicineStatus(),
              const SpaceVertical(10),
              const ItemDiagnosticTestStatus(),
              const SpaceVertical(10),
              const ItemPrescribedDiagnosticsTest(),
              const SpaceVertical(10),
              Obx(
                () => controller.isMedicineListUpdated.value && controller.listMedicines.isNotEmpty
                    ? CustomPagerIndicator(
                        listMedicines: controller.listMedicines,
                        visibleEdit: true,
                        onEditClick: (index) {
                          Get.toNamed(RoutePaths.DR_FOLLOWUP_PRESCRIBE_MEDICINE);
                        })
                    : CardEditSection(
                        title: AppStringKey.strPrescribedMedication.tr,
                        visibleEdit: false,
                        callBack: () {},
                        child: const SizedBox(),
                      ),
              ),
              const SpaceVertical(30),
              SubmitButton(
                title: AppStringKey.strComplete.tr,
                onClick: () {
                  Get.toNamed(RoutePaths.DR_APPOINTMENT_DATA_SUBMITTED);
                },
              )
            ],
          ).paddingAll(15),
        ),
      ),
    );
  }
}

class ItemPatientDetail extends StatelessWidget {
  static DrFollowUpCompleteAppointmentController controller = Get.find<DrFollowUpCompleteAppointmentController>();

  const ItemPatientDetail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Container(
        width: double.maxFinite,
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Obx(
              () => Row(
                children: [
                  Container(
                    height: 55,
                    width: 55,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      image: const DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(
                          AppImages.dummyProfile,
                        ),
                      ),
                    ),
                  ),
                  const SpaceHorizontal(10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        controller.name.value,
                        // 'Ricky J',
                        style: BaseStyle.textStyleDomineBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      Text(
                        controller.ageGender.value,
                        // '32 Years Old, B+ve',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      Text(
                        controller.districtCountry.value,
                        // 'Noakhali, Bangladesh',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SpaceVertical(15),
            CustomCardWidget(
                shadowOpacity: shadow,
                widget: RippleEffectWidget(
                  borderRadius: 5,
                  callBack: () {},
                  widget: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.videocam_outlined,
                        color: AppColors.colorOrange,
                        size: 20,
                      ),
                      const SpaceHorizontal(8),
                      Text(
                        AppStringKey.labelInAppVideoCall.tr,
                        style: BaseStyle.textStyleNunitoSansBold(10, AppColors.colorDarkBlue),
                      ),
                    ],
                  ).paddingSymmetric(vertical: 8),
                )),
            const SpaceVertical(10),
          ],
        ),
      ),
    );
  }
}

class ItemComplaintsAndSymptoms extends StatelessWidget {
  static DrFollowUpCompleteAppointmentController controller = Get.find<DrFollowUpCompleteAppointmentController>();

  const ItemComplaintsAndSymptoms({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: SizedBox(
        width: double.infinity,
        child: Obx(
          () => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomHeadingVisitFollowUp(
                heading: AppStringKey.strComplaintsSymptoms.tr,
              ),
              const SpaceVertical(15),
              SubItemPreviousDecision(
                title: AppStringKey.strChiefComplaints.tr,
                value: CustomFunctions.setStringFromListString(controller.currentComplaintsList),
              ),
              const SpaceVertical(10),
              SubItemPreviousDecision(
                title: AppStringKey.labelPreviousSymptoms.tr,
                value: CustomFunctions.setStringFromListString(controller.previousSymptomsList),
              ),
              const SpaceVertical(10),
              SubItemPreviousDecision(
                title: AppStringKey.labelCurrentSymptoms.tr,
                value: CustomFunctions.setStringFromListString(controller.currentSymptomsList),
              ),
            ],
          ).paddingSymmetric(horizontal: 8, vertical: 15),
        ),
      ),
    );
  }
}

class DecisionSection extends StatelessWidget {
  static DrFollowUpCompleteAppointmentController controller = Get.find<DrFollowUpCompleteAppointmentController>();

  const DecisionSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardEditSection(
      title: AppStringKey.strDecision.tr,
      callBack: () {
        Get.toNamed(RoutePaths.DR_CLINICAL_DECISION);
      },
      child: Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            PatientHistoryItemWidget(
              titleText: AppStringKey.strIdentifiedDisease.tr,
              value: controller.newIdentifiedDisease.value.isNotEmpty ? controller.newIdentifiedDisease.value : "-",
            ),
            const SpaceVertical(10),
            PatientHistoryItemWidget(
              titleText: AppStringKey.strClinicalNote.tr,
              value: controller.newClinicalNote.value.isNotEmpty ? controller.newClinicalNote.value : "-",
            ),
          ],
        ),
      ),
    );
  }
}

class ItemMedicineStatus extends StatelessWidget {
  static DrFollowUpCompleteAppointmentController controller = Get.find<DrFollowUpCompleteAppointmentController>();

  const ItemMedicineStatus({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomHeadingVisitFollowUp(
            heading: AppStringKey.labelMedicineStatus.tr,
          ),
          const SpaceVertical(10),
          Obx(
            () => ListView.builder(
              itemCount: controller.medicineStatusNameList.length,
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    const SpaceVertical(10),
                    CustomContentVisitFollowUpWidget(
                      title: controller.medicineStatusNameList[index],
                      value: controller.medicineStatusStatusList[index],
                    ),
                  ],
                );
              },
            ),
          ),
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}

class ItemDiagnosticTestStatus extends StatelessWidget {
  static DrFollowUpCompleteAppointmentController controller = Get.find<DrFollowUpCompleteAppointmentController>();

  const ItemDiagnosticTestStatus({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      child: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomHeadingVisitFollowUp(
              heading: AppStringKey.labelDiagnosticTestStatus.tr,
            ),
            Obx(
              () => ListView.builder(
                itemCount: controller.diagnosticStatusNameList.length,
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return SubItemDiagnosticTestStatus(
                    title: controller.diagnosticStatusNameList[index],
                    value: controller.diagnosticStatusResultList[index],
                    imageList: controller.diagnosticStatusImgList[index],
                  );
                },
              ),
            ),
            // SpaceVertical(10),
            // SubItemDiagnosticTestStatus(
            //   title: 'Defera 500 mg',
            //   value: 'Normal',
            // ),
            // SpaceVertical(10),
            // SubItemDiagnosticTestStatus(
            //   title: 'Blood cultures',
            //   value: '',
            // ),
          ],
        ).paddingSymmetric(horizontal: 8, vertical: 15),
      ),
    );
  }
}

class ItemPreviousComplaintAndSymptoms extends StatelessWidget {
  const ItemPreviousComplaintAndSymptoms({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomHeadingVisitFollowUp(
            heading: AppStringKey.labelPreviousComplaintsAndSymptoms.tr,
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.labelChiefComplaint.tr,
            value: '-',
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.labelSymptoms.tr,
            value: 'runny nose, Mild fever',
          ),
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}

class ItemPrescribedDiagnosticsTest extends StatelessWidget {
  static DrFollowUpCompleteAppointmentController controller = Get.find<DrFollowUpCompleteAppointmentController>();

  const ItemPrescribedDiagnosticsTest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CustomHeadingVisitFollowUp(
                heading: AppStringKey.strPrescribedDiagnostics.tr,
              ),
              RippleEffectWidget(
                borderRadius: 5,
                callBack: () {
                  Get.toNamed(RoutePaths.DR_FOLLOWUP_UPDATE_DIAGNOSTICS_TEST);
                },
                widget: Image.asset(
                  AppImages.edit,
                  height: 20,
                  width: 20,
                ).paddingAll(5),
              )
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                valueText(CustomFunctions.setStringFromListString(controller.newDiagnosticTestList)),
                // GestureDetector(
                //   onTap: () {
                //     Get.toNamed(RoutePaths.DR_DIAGNOSTICS_TEST_IMAGES);
                //   },
                //   child: Image.asset(
                //     AppImages.diagnosticTest,
                //     height: 20,
                //     width: 20,
                //   ),
                // )
              ],
            ).marginOnly(right: 5),
          ),
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}

class ItemMedicalHistory extends StatelessWidget {
  static DrFollowUpCompleteAppointmentController controller = Get.find<DrFollowUpCompleteAppointmentController>();

  const ItemMedicalHistory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomHeadingVisitFollowUp(
              heading: AppStringKey.strMedicalHistory.tr,
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strMedicalCondition.tr,
              value: controller.medicalHistoryCondition.value.isNotEmpty ? controller.medicalHistoryCondition.value : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strAllergies.tr,
              value: controller.medicalHistoryAllergies.value.isNotEmpty ? controller.medicalHistoryAllergies.value : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strFamilyHistory.tr,
              value: controller.medicalHistoryFamilyHistory.value.isNotEmpty ? controller.medicalHistoryFamilyHistory.value : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strPersonalHistory.tr,
              value: controller.medicalHistoryPersonalHistory.value.isNotEmpty ? controller.medicalHistoryPersonalHistory.value : "-",
            ),
          ],
        ).paddingSymmetric(horizontal: 8, vertical: 15),
      ),
    );
  }
}

class ItemPreExistingConditions extends StatelessWidget {
  static DrFollowUpCompleteAppointmentController controller = Get.find<DrFollowUpCompleteAppointmentController>();

  const ItemPreExistingConditions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomHeadingVisitFollowUp(
              heading: AppStringKey.labelPreExistingConditions.tr,
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strMedicalCondition.tr,
              value: controller.preExistingCondition.value.isNotEmpty ? controller.preExistingCondition.value : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strMedication.tr,
              value: controller.preExistingMedication.value.isNotEmpty ? controller.preExistingMedication.value : "-",
            ),
          ],
        ).paddingSymmetric(horizontal: 8, vertical: 15),
      ),
    );
  }
}

class ItemVitalSigns extends StatelessWidget {
  static DrFollowUpCompleteAppointmentController controller = Get.find<DrFollowUpCompleteAppointmentController>();

  const ItemVitalSigns({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomHeadingVisitFollowUp(
              heading: AppStringKey.strVitalSigns.tr,
            ),
            const SpaceVertical(15),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strBP.tr,
              value: controller.vitalSignBp.value.isNotEmpty ? controller.vitalSignBp.value : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strTemp.tr,
              value: controller.vitalSignTemp.value.isNotEmpty ? "${controller.vitalSignTemp.value}°c" : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strPulseRate.tr,
              value: controller.vitalSignPulseRate.value.isNotEmpty ? controller.vitalSignPulseRate.value : "-",
            ),
            const SpaceVertical(10),
            CustomContentVisitFollowUpWidget(
              title: AppStringKey.strBreathingsRate.tr,
              value: controller.vitalSignBreathingRate.value.isNotEmpty ? controller.vitalSignBreathingRate.value : "-",
            ),
          ],
        ).paddingSymmetric(horizontal: 8, vertical: 15),
      ),
    );
  }
}

class SubItemPreviousDecision extends StatelessWidget {
  final String title;
  final String value;

  const SubItemPreviousDecision({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
        ),
        const SpaceVertical(3),
        Text(
          value,
          style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
        ),
      ],
    );
  }
}

class SubItemDiagnosticTestStatus extends StatelessWidget {
  final String title;
  final String value;
  final List<String> imageList;

  const SubItemDiagnosticTestStatus({
    Key? key,
    required this.title,
    required this.value,
    required this.imageList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 5,
          child: Text(
            title,
            style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
          ),
        ),
        Expanded(
          flex: 4,
          child: Text(
            ' : $value',
            style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
          ),
        ),
        imageList.isNotEmpty && imageList.first.isNotEmpty
            ? Expanded(
                child: GestureDetector(
                  onTap: () {
                    // var msg = imageList.length.toString();
                    // msg.showSnackBar;
                    var bundle = {
                      "imageList": imageList,
                    };
                    Get.toNamed(
                      RoutePaths.DR_DIAGNOSTICS_TEST_IMAGES,
                      arguments: {
                        AppStrings.forwardArgument: jsonEncode(bundle),
                      },
                    );
                  },
                  child: Image.asset(
                    AppImages.diagnosticTest,
                    height: 25,
                    width: 25,
                  ),
                ),
              )
            : const Expanded(
                child: SizedBox(width: 25, height: 25),
              ),
      ],
    );
  }
}

class ItemRecommendations extends StatelessWidget {
  const ItemRecommendations({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomHeadingVisitFollowUp(
            heading: AppStringKey.strRecommendations.tr,
          ),
          const SpaceVertical(15),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strProvisionalDiagnosis.tr,
            value: 'Flu',
          ),
        ],
      ),
    );
  }
}
