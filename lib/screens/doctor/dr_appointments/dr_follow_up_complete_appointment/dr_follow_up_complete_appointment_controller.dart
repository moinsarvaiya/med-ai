import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../api/api_interface/api_interface.dart';
import '../../../../constant/app_strings.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../models/dr_followup_consultation_details_model.dart';
import '../../../../models/dr_followup_soap_model.dart';
import '../../../../models/dr_prescribe_medicine_model.dart';
import '../../../../utils/app_preference/followup_preferences.dart';
import '../../../../utils/app_preference/login_preferences.dart';
import '../../../../utils/functions/custom_functions.dart';

class DrFollowUpCompleteAppointmentController extends BaseController {
  var api = ApiCallObject();
  var personId = "";
  var consultationId = "";
  var consultationType = "";
  RxBool isMedicineListUpdated = false.obs;

  var name = "-".obs;
  var ageGender = "-".obs;
  var districtCountry = "-".obs;

  var previousIdentifiedDisease = "-".obs;
  var previousClientNote = "-".obs;
  var newIdentifiedDisease = "-".obs;
  var newClinicalNote = "-".obs;

  var medicineStatusNameList = [].obs;
  var medicineStatusStatusList = [].obs;

  var diagnosticStatusNameList = [].obs;
  var diagnosticStatusResultList = [].obs;
  var diagnosticStatusImgList = [].obs;

  var newDiagnosticTestList = [].obs;

  var previousComplaintsList = [].obs;
  var previousSymptomsList = [].obs;
  var currentComplaintsList = [].obs;
  var currentSymptomsList = [].obs;

  var vitalSignBp = "".obs;
  var vitalSignTemp = "".obs;
  var vitalSignPulseRate = "".obs;
  var vitalSignBreathingRate = "".obs;

  var medicalHistoryCondition = "".obs;
  var medicalHistoryAllergies = "".obs;
  var medicalHistoryFamilyHistory = "".obs;
  var medicalHistoryPersonalHistory = "".obs;

  var preExistingCondition = "".obs;
  var preExistingMedication = "".obs;

  final RxList<DrPrescribeMedicineModel> listMedicines = RxList();

  late DrFollowupConsultationDetailsModel consultationDetailModel;
  late DrFollowupSoapModel soapModel;

  @override
  void onInit() {
    super.onInit();
    setData();
    personId = LoginPreferences().sharedPrefRead(StorageKeys.personId);

    getConsultationDetails();
    getSoapInfo();

    // listMedicines.add(
    //   DrPrescribeMedicineModel(
    //     brandName: 'Napasin',
    //     dosageType: "Tablet",
    //     threeTimesDay: 'Hour Difference/ Day',
    //     hourDifference: '',
    //     mealInstruction: 'After',
    //     duration: '7 Days',
    //     specialInstruction: 'Lorem Ipsum is simply dummy text of the printing and typeset',
    //   ),
    // );
    // listMedicines.add(
    //   DrPrescribeMedicineModel(
    //     brandName: 'Napasin',
    //     dosageType: "Tablet",
    //     threeTimesDay: 'Hour Difference/ Day',
    //     hourDifference: '',
    //     mealInstruction: 'After',
    //     duration: '7 Days',
    //     specialInstruction: 'Lorem Ipsum is simply dummy text of the printing and typeset',
    //   ),
    // );
    // listMedicines.add(
    //   DrPrescribeMedicineModel(
    //     brandName: 'Napasin',
    //     dosageType: "Tablet",
    //     threeTimesDay: 'Hour Difference/ Day',
    //     hourDifference: '',
    //     mealInstruction: 'After',
    //     duration: '7 Days',
    //     specialInstruction: 'Lorem Ipsum is simply dummy text of the printing and typeset',
    //   ),
    // );
  }

  void setData() {
    try {
      consultationId = FollowupPreferences().sharedPrefRead(StorageKeys.followupID);
      consultationType = FollowupPreferences().sharedPrefRead("consultationType");
      // consultationId = "2306050017000010001";
      // consultationType = "follow_up";
      print("FollowUp Appointment");
      print("consultationType: $consultationType");
      print("consultationId: $consultationId");
    } catch (e) {
      print(e);
    }
  }

  Future<void> getConsultationDetails() async {
    http.Response response;
    dynamic object = {};

    object["consultation_id"] = consultationId;
    object["id_type"] = consultationType;

    if (personId != "0") {
      try {
        response = await api.postData(ApiEndpoints.urlGetConsultationDetails, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody["code"] == "200") {
            consultationDetailModel = DrFollowupConsultationDetailsModel.fromJson(responseBody);
            var consultationDetail = consultationDetailModel.consultationDetail;

            //patient details
            name.value = consultationDetail?.name ?? "Guest user";
            ageGender.value = "${consultationDetail?.age} ${AppStringKey.yearsOld.tr}, ${consultationDetail?.gender}";
            districtCountry.value = "${consultationDetail?.district}, ${consultationDetail?.country}";

            // print("name: $name");
            // print("name: $ageGender");
            // print("name: $districtCountry");

            //prescribed medication
            var prescribedMedicine = consultationDetail?.prevMedicineList ?? [];
            for (MedicineList medicineData in prescribedMedicine) {
              var medicineTiming = "";
              if (medicineData.hourlyInterval!.isNotEmpty) {
                medicineTiming = "After every ${medicineData.hourlyInterval} hours";
              } else {
                if (medicineData.breakfast.toString().toLowerCase() == "yes") {
                  medicineTiming += "1+";
                } else {
                  medicineTiming += "0+";
                }
                if (medicineData.lunch.toString().toLowerCase() == "yes") {
                  medicineTiming += "1+";
                } else {
                  medicineTiming += "0+";
                }
                if (medicineData.dinner.toString().toLowerCase() == "yes") {
                  medicineTiming += "1";
                } else {
                  medicineTiming += "0";
                }
              }

              var data = DrPrescribeMedicineModel(
                brandName: medicineData.medicine,
                duration: medicineData.duration,
                dosageType: '',
                hourDifference: '',
                mealInstruction: medicineData.foodIntake == "Yes" ? "After" : "Before",
                threeTimesDay: medicineTiming,
                specialInstruction: medicineData.specialInstruction,
              );
              listMedicines.add(data);
            }
            print("medicineListSize: ${listMedicines.length}");
            listMedicines.refresh();
            isMedicineListUpdated.value = true;

            //decision
            newIdentifiedDisease.value = CustomFunctions.setStringFromListString(consultationDetail?.diagnosedDisease);
            newClinicalNote.value = consultationDetail?.clinicalNote ?? "-";
            print("newIdentifiedDisease: $newIdentifiedDisease");
            print("newClinicalNote: $newClinicalNote");

            //medicine status
            var medicineStatus = consultationDetail?.medicationStatus;
            for (MedicationStatus data in medicineStatus!) {
              medicineStatusNameList.add(data.drugName);
              medicineStatusStatusList.add(data.status);
            }

            //diagnostic status
            var diagnosticStatus = consultationDetail?.diagnosticsStatus ?? [];
            for (DiagnosticsStatus data in diagnosticStatus) {
              diagnosticStatusNameList.add(data.testName);
              diagnosticStatusResultList.add(data.testResult);
              diagnosticStatusImgList.add(data.testImg);
            }
            // print(jsonEncode(diagnosticStatusImgList));

            //new diagnostic test list
            var diagnosticList = consultationDetail?.diagnosticsList ?? [];
            for (String data in diagnosticList) {
              newDiagnosticTestList.add(data);
            }
            print(jsonEncode(newDiagnosticTestList));

            //previous complaints and symptoms
            var previousSymptoms = consultationDetail?.previousSymptoms! ?? [];
            for (String data in previousSymptoms) {
              previousSymptomsList.add(data);
            }

            //new complaints and symptoms
            var newSymptoms = consultationDetail?.currentSymptoms! ?? [];
            for (String data in newSymptoms) {
              currentSymptomsList.add(data);
            }

            //vital signs
            var vitalSigns = consultationDetail?.newVitalSigns;
            vitalSignBp.value = vitalSigns?.newBp ?? "-";
            vitalSignTemp.value = vitalSigns?.newTemperature ?? "-";
            vitalSignPulseRate.value = vitalSigns?.newPulse ?? "-";
            vitalSignBreathingRate.value = vitalSigns?.newBreathing ?? "-";
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print("Error Occurred");
        print(e);
      }
    } else {
      print(AppStrings.somethingWentWrong);
    }
  }

  Future<void> getSoapInfo() async {
    http.Response response;
    dynamic object = {};

    object["consultation_id"] = consultationId;
    object["id_type"] = consultationType;

    if (personId != "0") {
      try {
        response = await api.postData(ApiEndpoints.urlDoctorGetFollowupSoap, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            soapModel = DrFollowupSoapModel.fromJson(responseBody);
            var soapInfo = soapModel.soapInfo;

            //previous decision
            var prescriptionInfo = soapModel.prescriptionInfo;
            previousIdentifiedDisease.value = CustomFunctions.setStringFromListString(prescriptionInfo?.decision?.identifiedDisease ?? []);
            previousClientNote.value = prescriptionInfo?.decision?.clinicalNote ?? "-";

            //medical history
            var medicalHistory = soapInfo?.medicalHistory;
            medicalHistoryCondition.value = CustomFunctions.setStringFromListString(medicalHistory?.medicalCondition);
            medicalHistoryAllergies.value = medicalHistory!.allergy ?? "-";
            medicalHistoryFamilyHistory.value = CustomFunctions.setStringFromListString(medicalHistory.familyHistory);
            medicalHistoryPersonalHistory.value = CustomFunctions.setStringFromListString(medicalHistory.personalHistory);

            //pre existing condition
            var preExistingCond = soapInfo?.preExistingCond;
            preExistingCondition.value = preExistingCond?.medicalCondition ?? "-";
            preExistingMedication.value = preExistingCond?.medication ?? "-";
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            message.showErrorSnackBar();
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        print(e);
      }
    } else {
      print(AppStrings.somethingWentWrong);
    }
  }

  void showData() {
    print("showData");
    print(jsonEncode(consultationDetailModel.consultationDetail!.prevMedicineList![0]).toString());
  }
}
