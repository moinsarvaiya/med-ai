import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_appointment_medicine_list/dr_appointment_medicine_list_controller.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/space_vertical.dart';

class DrAppointmentMedicineListScreen extends GetView<DrAppointmentMedicineListController> {
  const DrAppointmentMedicineListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: AppStringKey.strServiceMedicine.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStringKey.titleMedicineAppointment.tr,
                style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(15),
              ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: 4,
                  itemBuilder: (context, i) {
                    return ItemMedicineAppointment(
                      index: i,
                    );
                  }),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemMedicineAppointment extends StatelessWidget {
  final int index;

  const ItemMedicineAppointment({
    Key? key,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: InkWell(
        onTap: () {
          Get.toNamed(RoutePaths.SEARCH_MEDICINE_DETAILS);
        },
        splashColor: AppColors.colorDarkBlue.withOpacity(0.3),
        borderRadius: BorderRadius.circular(5),
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              SubItemMedicineAppointment(
                title: AppStringKey.labelBrand.tr,
                value: 'Napa, 500mg',
                color: AppColors.colorSkyBlue,
              ),
              SubItemMedicineAppointment(
                title: AppStringKey.labelGeneric.tr,
                value: 'paracetamol',
              ),
              SubItemMedicineAppointment(
                title: AppStringKey.labelCompany.tr,
                value: 'Beximco Pharmaceuticals Ltd.',
              ),
              const SpaceVertical(10),
              Row(
                children: [
                  Expanded(
                    child: SubmitButton(
                      title: AppStringKey.labelSelectThis.tr,
                      backgroundColor: AppColors.white,
                      borderColor: AppColors.colorDarkBlue,
                      onClick: () {
                        Get.back();
                      },
                      displayBorder: true,
                      titleFontSize: 14,
                      titleColor: AppColors.colorDarkBlue,
                    ).marginOnly(right: 5),
                  ),
                  Expanded(
                    child: SubmitButton(
                      title: AppStringKey.labelViewDetails.tr,
                      backgroundColor: AppColors.colorDarkBlue,
                      onClick: () {
                        Get.toNamed(RoutePaths.DR_APPOINTMENT_MEDICINE_DETAILS);
                      },
                      titleFontSize: 14,
                      titleColor: AppColors.white,
                    ).marginOnly(left: 5),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    ).marginOnly(bottom: 5);
  }
}

class SubItemMedicineAppointment extends StatelessWidget {
  final String title;
  final String value;
  final Color color;

  const SubItemMedicineAppointment({Key? key, required this.title, this.color = AppColors.colorDarkBlue, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          '$title : ',
          style: BaseStyle.textStyleNunitoSansSemiBold(14, color),
        ),
        Text(
          value,
          style: BaseStyle.textStyleNunitoSansBold(14, color),
        ),
      ],
    ).marginOnly(bottom: 10);
  }
}
