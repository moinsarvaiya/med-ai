import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/base_style.dart';
import '../../../../widgets/custom_auto_complete_textfield.dart';
import 'dr_visit_update_symptoms_controller.dart';

class DrVisitUpdateSymptomsScreen extends GetView<DrVisitUpdateSymptomsController> {
  const DrVisitUpdateSymptomsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.btnUpdate.tr,
        onClick: () {
          Get.back();
        },
      ).paddingOnly(left: 20, right: 20, top: 15, bottom: MediaQuery.viewInsetsOf(context).bottom + 15),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStringKey.labelUpdateSymptomsHere.tr,
                style: BaseStyle.textStyleNunitoSansBold(20, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(20),
              CustomAutoCompleteTextField(
                label: AppStringKey.labelSymptoms.tr,
                hint: AppStringKey.hintSymptoms.tr,
                focusNode: controller.searchFocusNode,
                textEditingController: controller.symptomsController,
                onSelectedValue: (val) {
                  if (!controller.listSymptoms.contains(val)) {
                    controller.listSymptoms.add(val);
                    controller.listSymptoms.refresh();
                    controller.symptomsController.clear();
                  } else {
                    Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
                  }
                },
                searchOptions: controller.searchOptions,
                searchFieldAutoCompleteKey: controller.searchFieldAutoCompleteKey,
              ),
              const SpaceVertical(20),
              Obx(
                () => Wrap(
                  children: controller.listSymptoms
                      .map(
                        (e) => ItemSymptoms(
                          title: e,
                          controller: controller,
                        ),
                      )
                      .toList(),
                ),
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemSymptoms extends StatelessWidget {
  final String title;
  final DrVisitUpdateSymptomsController controller;

  const ItemSymptoms({
    Key? key,
    required this.title,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
      decoration: BoxDecoration(
        color: AppColors.colorBackground,
        borderRadius: BorderRadius.circular(30),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            title,
            style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
          ),
          const SpaceHorizontal(3),
          GestureDetector(
            onTap: () {
              controller.listSymptoms.remove(title);
              controller.listSymptoms.refresh();
            },
            child: const Icon(
              Icons.cancel,
              size: 15,
            ),
          ),
        ],
      ),
    ).marginOnly(right: 10, bottom: 10);
  }
}
