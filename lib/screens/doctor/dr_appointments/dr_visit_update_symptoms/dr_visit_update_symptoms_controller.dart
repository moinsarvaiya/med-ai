import 'package:flutter/cupertino.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';

class DrVisitUpdateSymptomsController extends BaseController {
  var symptomsController = TextEditingController();
  var listSymptoms = [
    'Dry cough',
    'Persistent cough',
  ].obs;
  final FocusNode searchFocusNode = FocusNode();
  GlobalKey<TextFieldAutoCompleteState<String>> searchFieldAutoCompleteKey = GlobalKey();
  final List<String> searchOptions = <String>[
    'Swelling of Hands and feet',
    'The feeling that you need to urinate often',
    'Clicking or snapping feeling in elbow',
    'Swelling in your legs, ankles or feet',
    'consistent fever',
    'Weakness',
  ];
}
