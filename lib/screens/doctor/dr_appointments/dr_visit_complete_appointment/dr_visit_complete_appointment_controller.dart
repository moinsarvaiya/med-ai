import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:med_ai/bindings/base_controller.dart';

import '../../../../models/dr_prescribe_medicine_model.dart';

class DrVisitCompleteAppointmentController extends BaseController {
  final RxList<DrPrescribeMedicineModel> listMedicines = RxList();
  var dummyText =
      'Please not that the information provided by MedAi is provided solely for guideline purposes and is not a qualified medical opinion.'
      ' This information should not be considered advice or an opinion. of doctor or other health professional about your actual medical state and you should see a doctor for any symptoms you may have. '
      'If you are experiencing a health emergency, you should call your local emergency number immediately to request emergency medical assistance.';

  @override
  void onInit() {
    super.onInit();

    listMedicines.add(
      DrPrescribeMedicineModel(
        brandName: 'Napasin',
        dosageType: "Tablet",
        threeTimesDay: 'Hour Difference/ Day',
        hourDifference: '',
        mealInstruction: 'After',
        duration: '7 Days',
        specialInstruction: 'Lorem Ipsum is simply dummy text of the printing and typeset',
      ),
    );
    listMedicines.add(
      DrPrescribeMedicineModel(
        brandName: 'Napasin',
        dosageType: "Tablet",
        threeTimesDay: 'Hour Difference/ Day',
        hourDifference: '',
        mealInstruction: 'After',
        duration: '7 Days',
        specialInstruction: 'Lorem Ipsum is simply dummy text of the printing and typeset',
      ),
    );
    listMedicines.add(
      DrPrescribeMedicineModel(
        brandName: 'Napasin',
        dosageType: "Tablet",
        threeTimesDay: 'Hour Difference/ Day',
        hourDifference: '',
        mealInstruction: 'After',
        duration: '7 Days',
        specialInstruction: 'Lorem Ipsum is simply dummy text of the printing and typeset',
      ),
    );
  }
}
