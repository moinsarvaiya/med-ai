import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_content_visit_followup_widget.dart';
import '../../../../widgets/custom_heading_visit_followup.dart';
import '../../../../widgets/custom_pager_indicator.dart';
import '../../../../widgets/ripple_effect_widget.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';
import '../../dr_consultancy_history_detail/dr_consultancy_history_detail_screen.dart';
import 'dr_visit_complete_appointment_controller.dart';

class DrVisitCompleteAppointmentScreen extends GetView<DrVisitCompleteAppointmentController> {
  const DrVisitCompleteAppointmentScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const ItemPatientDetail(),
              const SpaceVertical(10),
              const ItemComplaintsSymptoms(),
              const SpaceVertical(10),
              const ItemMedicalHistory(),
              const SpaceVertical(10),
              const PreExistingConditionSection(),
              const SpaceVertical(10),
              const ItemVitalSigns(),
              const SpaceVertical(10),
              const DecisionSection(),
              const SpaceVertical(10),
              CustomPagerIndicator(
                listMedicines: controller.listMedicines,
                onEditClick: (index) {
                  Get.toNamed(RoutePaths.DR_PRESCRIBE_MEDICINE);
                },
              ),
              const SpaceVertical(10),
              const PrescribedDiagnosticsTestSection(),
              const SpaceVertical(30),
              SubmitButton(
                title: AppStringKey.strComplete.tr,
                onClick: () {
                  Get.offNamed(RoutePaths.DR_APPOINTMENT_DATA_SUBMITTED);
                },
              )
            ],
          ).paddingAll(15),
        ),
      ),
    );
  }
}

class ItemPatientDetail extends StatelessWidget {
  const ItemPatientDetail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Container(
        width: double.maxFinite,
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  height: 55,
                  width: 55,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    image: const DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage(
                        AppImages.dummyProfile,
                      ),
                    ),
                  ),
                ),
                const SpaceHorizontal(10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Ricky J',
                      style: BaseStyle.textStyleDomineBold(
                        16,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    Text(
                      '32 Years Old, B+ve',
                      style: BaseStyle.textStyleNunitoSansRegular(
                        12,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    Text(
                      'Noakhali, Bangladesh',
                      style: BaseStyle.textStyleNunitoSansRegular(
                        12,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SpaceVertical(10),
            CustomCardWidget(
              shadowOpacity: shadow,
              widget: RippleEffectWidget(
                callBack: () {},
                borderRadius: 5,
                widget: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.videocam_outlined,
                      color: AppColors.colorOrange,
                      size: 20,
                    ),
                    const SpaceHorizontal(8),
                    Text(
                      AppStringKey.labelInAppVideoCall.tr,
                      style: BaseStyle.textStyleNunitoSansBold(10, AppColors.colorDarkBlue),
                    ),
                  ],
                ).paddingSymmetric(vertical: 8),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ItemComplaintsSymptoms extends StatelessWidget {
  const ItemComplaintsSymptoms({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomHeadingVisitFollowUp(
              heading: AppStringKey.strComplaintsSymptoms.tr,
            ),
            const SpaceVertical(10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppStringKey.labelChiefComplaint.tr,
                  style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
                ),
                const SpaceVertical(3),
                Text(
                  'Dry cough',
                  style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
                ),
              ],
            ),
            const SpaceVertical(20),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppStringKey.labelSymptoms.tr,
                  style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
                ),
                const SpaceVertical(3),
                Text(
                  'Dry cough',
                  style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
                ),
              ],
            )
          ],
        ).paddingSymmetric(horizontal: 8, vertical: 15),
      ),
    );
  }
}

class ItemMedicalHistory extends StatelessWidget {
  const ItemMedicalHistory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomHeadingVisitFollowUp(
            heading: AppStringKey.strMedicalHistory.tr,
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strMedicalCondition.tr,
            value: 'No available data',
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strAllergies.tr,
            value: 'Unknown',
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strFamilyHistory.tr,
            value: 'No available data',
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strPersonalHistory.tr,
            value: 'No available data',
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strMedicalCondition.tr,
            value: 'Unknown',
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strMedication.tr,
            value: 'No available data',
          ),
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}

class ItemVitalSigns extends StatelessWidget {
  const ItemVitalSigns({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomHeadingVisitFollowUp(
            heading: AppStringKey.strVitalSigns.tr,
          ),
          const SpaceVertical(15),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strBP.tr,
            value: '80/20',
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strTemp.tr,
            value: '98\' C',
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strPulseRate.tr,
            value: 'Unknown',
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strBreathingsRate.tr,
            value: 'Normal',
          ),
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}

class PreExistingConditionSection extends StatelessWidget {
  const PreExistingConditionSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardSection(
      title: AppStringKey.strPreConditions.tr,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PatientHistoryItemWidget(
            titleText: AppStringKey.strMedicalCondition.tr,
            value: 'No available data',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strMedication.tr,
            value: '',
          ),
        ],
      ),
    );
  }
}

class DecisionSection extends StatelessWidget {
  const DecisionSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardEditSection(
      title: AppStringKey.strDecision.tr,
      callBack: () {
        Get.toNamed(RoutePaths.DR_CLINICAL_DECISION);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PatientHistoryItemWidget(
            titleText: AppStringKey.strIdentifiedDisease.tr,
            value: 'XYZ',
          ),
          const SpaceVertical(10),
          PatientHistoryItemWidget(
            titleText: AppStringKey.strClinicalNote.tr,
            value: 'XYZ',
          ),
        ],
      ),
    );
  }
}

class PrescribedDiagnosticsTestSection extends StatelessWidget {
  const PrescribedDiagnosticsTestSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardEditSection(
      title: AppStringKey.strPrescribedDiagnostics.tr,
      callBack: () {
        Get.toNamed(RoutePaths.DR_VISIT_UPDATE_DIAGNOSTIC_TEST);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              valueText('CBC'),
              GestureDetector(
                onTap: () {
                  Get.toNamed(RoutePaths.DR_DIAGNOSTICS_TEST_IMAGES);
                },
                child: Image.asset(
                  AppImages.diagnosticTest,
                  height: 20,
                  width: 20,
                ),
              )
            ],
          ).marginOnly(right: 5),
        ],
      ),
    );
  }
}

class CardEditSection extends StatelessWidget {
  final String title;
  final Widget child;
  final bool visibleEdit;
  final Function()? callBack;

  const CardEditSection({
    Key? key,
    required this.title,
    this.callBack,
    this.visibleEdit = true,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      elevation: 10,
      widget: Container(
        width: double.maxFinite,
        padding: const EdgeInsets.symmetric(
          vertical: 12,
          horizontal: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                headingText(title),
                visibleEdit
                    ? RippleEffectWidget(
                        borderRadius: 5,
                        callBack: callBack,
                        widget: Image.asset(
                          AppImages.edit,
                          height: 20,
                          width: 20,
                        ).paddingAll(5),
                      )
                    : Container(
                        height: 25,
                      ),
              ],
            ),
            const SizedBox(height: 10),
            child,
          ],
        ),
      ),
    );
  }
}
