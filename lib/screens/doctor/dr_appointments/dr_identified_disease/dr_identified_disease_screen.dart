import 'dart:developer' as devLog;

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_round_text_field.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_auto_complete_textfield.dart';
import '../../../../widgets/space_horizontal.dart';
import 'dr_identified_disease_controller.dart';

class DrIdentifiedDiseaseScreen extends GetView<DrIdentifiedDiseaseController> {
  const DrIdentifiedDiseaseScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: AppStringKey.titleDecision.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.strNext.tr,
        onClick: () {
          controller.submitDecisionToServer();
          // Get.toNamed(RoutePaths.DR_PRESCRIBE_MEDICINE);
        },
      ).paddingOnly(left: 20, right: 20, top: 15, bottom: MediaQuery.viewInsetsOf(context).bottom + 15),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Obx(
                () => Wrap(
                  children: controller.selectedDiseaseList
                      .map(
                        (e) => GestureDetector(
                          onTap: () {
                            devLog.log("ListSize: ${controller.selectedDiseaseList.length}");
                            if (controller.selectedDiseaseList.isNotEmpty) {
                              devLog.log("Wrap: $e", name: controller.tag);
                              devLog.log("SelectedDiseaseList: ${controller.selectedDiseaseList}");
                              controller.selectedDiseaseList.remove(e);
                              controller.selectedDiseaseList.refresh();
                            }
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                            decoration: BoxDecoration(
                              color: AppColors.colorBackground,
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  e,
                                  style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
                                ),
                                const SpaceHorizontal(3),
                                const Icon(
                                  Icons.cancel,
                                  size: 15,
                                ),
                              ],
                            ),
                          ).marginOnly(right: 15, bottom: 10),
                        ),
                      )
                      .toList(),
                ),
              ),
              const SpaceVertical(20),
              CustomAutoCompleteTextField(
                label: AppStringKey.strIdentifiedDisease.tr,
                hint: AppStringKey.hintDiseaseName.tr,
                focusNode: controller.identifiedDiseaseFocusNode,
                textEditingController: controller.identifiedDiseaseController,
                onSelectedValue: (val) {
                  if (!(controller.selectedDiseaseList.contains(val))) {
                    controller.selectedDiseaseList.add(val);
                    controller.identifiedDiseaseController.clear();
                  } else {
                    CustomFunctions.customSnackBar(AppStringKey.labelWarning.tr, "$val is already selected!");
                    controller.identifiedDiseaseController.clear();
                  }
                  controller.identifiedDiseaseAutoCompleteKey = GlobalKey();
                },
                searchOptions: controller.diseaseList,
                searchFieldAutoCompleteKey: controller.identifiedDiseaseAutoCompleteKey,
              ),
              const SpaceVertical(20),
              CustomRoundTextField(
                hintText: AppStringKey.hintNote.tr,
                labelText: AppStringKey.strClinicalNote.tr,
                isRequireField: true,
                backgroundColor: AppColors.white,
                maxLines: 4,
                textEditingController: controller.clinicalNoteController,
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}
