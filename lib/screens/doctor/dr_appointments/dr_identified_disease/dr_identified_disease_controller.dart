import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/cupertino.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_endpoints.dart';
import 'package:med_ai/api/api_interface/api_interface.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';
import 'package:med_ai/utils/app_preference/visit_preferences.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';

import '../../../../routes/route_paths.dart';

class DrIdentifiedDiseaseController extends BaseController {
  final String tag = "DoctorNote";
  var api = ApiCallObject();

  String consultationId = "";
  String personID = "";
  String doctorName = "";

  var identifiedDiseaseController = TextEditingController();
  var clinicalNoteController = TextEditingController();

  var identifiedDiseaseFocusNode = FocusNode();
  GlobalKey<TextFieldAutoCompleteState<String>> identifiedDiseaseAutoCompleteKey = GlobalKey();

  RxList selectedDiseaseList = [].obs;
  RxList<String> diseaseList = [''].obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    setData();
    getIdentifiedDiseaseListFromServer();
  }

  void setData() {
    try {
      developer.log("VisitPref ${VisitPreferences().sharedPrefRead(StorageKeys.visitID)}", name: tag);
      consultationId = VisitPreferences().sharedPrefRead(StorageKeys.visitID);
      personID = LoginPreferences().sharedPrefRead(StorageKeys.personId);
      doctorName = LoginPreferences().sharedPrefRead(StorageKeys.name);
      developer.log('VisitID: $consultationId', name: tag);
    } catch (e) {
      developer.log("SetDataError: ${e.toString()}", name: tag);
    }
  }

  Future<void> getIdentifiedDiseaseListFromServer() async {
    http.Response response;
    try {
      response = await api.getData(ApiEndpoints.urlGetDiseaseList);
      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        var diseaseListFromApi = responseBody['disease_list'];
        for (String data in diseaseListFromApi) {
          diseaseList.add(data);
        }
      }
    } catch (e) {
      developer.log('SetDataException: $e', name: tag);
    }
  }

  Future<void> submitDecisionToServer() async {
    http.Response response;
    dynamic object = {
      "vid": consultationId,
      "doctor_id": personID,
      "hospital_name": "",
      "diagnosed_disease": selectedDiseaseList,
      "doctor_name": doctorName,
      "clinical_note": clinicalNoteController.value.text
    };

    try {
      response = await api.postData(ApiEndpoints.urlVisitDoctorDecision, object);
      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        if (responseBody['code'] == "200") {
          CustomFunctions.customSnackBar(AppStrings.success, responseBody['success_msg']);
          Get.toNamed(RoutePaths.DR_PRESCRIBE_MEDICINE);
        } else {
          String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
          CustomFunctions.customSnackBar(AppStrings.error, message);
          //await Future.delayed(const Duration(seconds: 3));
        }
      }
    } catch (e) {
      developer.log("SubmitDecisionCatch: $e", name: tag);
    }
  }
}
