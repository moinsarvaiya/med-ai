import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_appointment_medicine_details/dr_appointment_medicine_details_controller.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/space_vertical.dart';
import '../../../common/global_search_flow/search_medicine_list/search_medicine_list_screen.dart';

class DrAppointmentMedicineDetailsScreen extends GetView<DrAppointmentMedicineDetailsController> {
  const DrAppointmentMedicineDetailsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: AppStringKey.labelMedicineDetails.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: CustomCardWidget(
            widget: ScrollConfiguration(
              behavior: MyBehavior(),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SubItemMedicineSearch(
                      title: AppStringKey.labelBrand.tr,
                      value: 'Napa, 500mg',
                      color: AppColors.colorSkyBlue,
                    ),
                    SubItemMedicineSearch(
                      title: AppStringKey.labelGeneric.tr,
                      value: 'paracetamol',
                    ),
                    SubItemMedicineSearch(
                      title: AppStringKey.labelDesignAdministration.tr,
                      value: 'Tablet',
                    ),
                    const SpaceVertical(20),
                    Text(
                      '${controller.strDummyTextMedicineDetails}\n\n${controller.strDummyTextMedicineDetails} ',
                      style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorTextDarkGray),
                    ),
                    const SpaceVertical(20),
                    Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        width: 100,
                        height: 33,
                        child: SubmitButton(
                          title: AppStringKey.back.tr,
                          isBottomMargin: false,
                          onClick: () {
                            Get.back();
                          },
                        ),
                      ),
                    ),
                    const SpaceVertical(20),
                  ],
                ).paddingAll(10),
              ),
            ),
          ).marginAll(15),
        ),
      ),
    );
  }
}
