import 'dart:convert';
import 'dart:developer' as developer;

import 'package:bottom_picker/bottom_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/utils/app_preference/visit_preferences.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';
import 'package:med_ai/utils/utilities.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../api/api_interface/api_interface.dart';
import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings.dart';
import '../../../../routes/route_paths.dart';

class DrVisitNextDateController extends BaseController {
  final String tag = "VisitDoctorAdvice";
  var api = ApiCallObject();

  String consultationId = "";
  String consultationType = "";

  var doctorAdviseController = TextEditingController();
  var nextFollowUpController = TextEditingController();
  var dateTimePicker;

  @override
  void onInit() {
    super.onInit();
    consultationId = VisitPreferences().sharedPrefRead(StorageKeys.visitID);
    consultationType = 'visit';

    developer.log('ConsultationID: $consultationId');
    dateTimePicker = BottomPicker.date(
      title: '',
      titleStyle: BaseStyle.textStyleNunitoSansSemiBold(14, AppColors.colorDarkBlue),
      onSubmit: (date) {
        // var data = DateFormat('dd-MM-yyyy hh:mm a').format(date);
        var data = DateFormat('yyyy-MM-dd').format(date);
        if (date.isAfter(DateTime.now())) {
          nextFollowUpController.text = data;
        } else {
          Utilities.showErrorMessage("Please select valid time", "InValid Time");
        }
      },
      onClose: () {},
      dismissable: true,
      iconColor: AppColors.white,
      minDateTime: DateTime.now(),
      maxDateTime: DateTime.now().add(
        const Duration(days: 60),
      ),
      initialDateTime: DateTime.now(),
      gradientColors: const [AppColors.colorDarkBlue, AppColors.colorDarkBlue],
    );
  }

  Future<void> adviceAndDateToServer() async {
    http.Response response;
    dynamic requestBody = {
      "consultation_id": consultationId,
      "id_type": consultationType,
      "advice": doctorAdviseController.text.toString(),
      "next_consultation_date": nextFollowUpController.text.toString()
    };

    if (consultationId != "0" && consultationId.isNotEmpty) {
      try {
        response = await api.postData(ApiEndpoints.urlAdviseNextDate, requestBody);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            updateVisitStatusToServer();
            // Get.toNamed(RoutePaths.DR_FOLLOWUP_COMPLETE_APPOINTMENT);
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;

            CustomFunctions.customErrorSnackBar(message);
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        developer.log('ServerException: $e', name: tag);
      }
    } else {
      // print(AppStrings.somethingWentWrong);
    }
  }

  Future<void> updateVisitStatusToServer() async {
    http.Response response;
    dynamic object = {"vid": int.parse(consultationId)};

    if (consultationId != "0" && consultationId.isNotEmpty) {
      try {
        response = await api.postData(ApiEndpoints.urlVisitStatusUpdate, object);
        if (response.statusCode == 200) {
          var responseBody = jsonDecode(response.body);
          if (responseBody['code'] == "200") {
            String message = responseBody['success_msg'];
            CustomFunctions.customSnackBar(AppStrings.success, message);
            Get.toNamed(RoutePaths.DR_VISIT_COMPLETE_APPOINTMENT);
            // Get.toNamed(RoutePaths.DR_FOLLOWUP_COMPLETE_APPOINTMENT);
          } else {
            String message = responseBody['error_msg'] ?? AppStrings.somethingWentWrong;
            CustomFunctions.customErrorSnackBar(message);
            await Future.delayed(const Duration(seconds: 3));
          }
        }
      } catch (e) {
        developer.log('ExceptionUpdateStatus: $e', name: tag);
      }
    } else {
      CustomFunctions.customErrorSnackBar("Visit ID Not Found!");
    }
  }
}
