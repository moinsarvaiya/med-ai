import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_add_new_medicine/dr_add_new_medicine_controller.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_round_text_field.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../models/model_check_box.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_auto_complete_textfield.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_dropdown_field.dart';

class DrAddNewMedicineScreen extends GetView<DrAddNewMedicineController> {
  const DrAddNewMedicineScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        controller.onBack(context);
        return true;
      },
      child: Scaffold(
        backgroundColor: AppColors.white,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(defaultAppBarHeight),
          child: CustomAppBar(
            isDividerVisible: true,
            titleName: AppStringKey.titleAddNewMedicine.tr,
            onClick: () {
              controller.onBack(context);
            },
          ),
        ),
        bottomNavigationBar: SubmitButton(
          title: AppStringKey.titleAddNewMedicine.tr,
          onClick: () {
            controller.addNewMedicine();
          },
        ).paddingOnly(left: 20, right: 20, top: 15, bottom: MediaQuery.viewInsetsOf(context).bottom + 15),
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomAutoCompleteTextField(
                  label: AppStringKey.labelBrandGenericName.tr,
                  hint: AppStringKey.hintBrandGenericName.tr,
                  focusNode: controller.brandGenericFocusNode,
                  textEditingController: controller.brandGenericController,
                  onSelectedValue: (val) {
                    //controller.brandGenericName.value = val;
                    controller.brandGenericName.value = "";
                    controller.brandGenericController.text = "";
                    controller.brandGenericAutoCompleteKey = GlobalKey();
                    var intent = {'selectedName': val};
                    Get.toNamed(
                      RoutePaths.SEARCH_MEDICINE_LIST,
                      arguments: {
                        AppStrings.forwardArgument: jsonEncode(intent),
                      },
                    );
                  },
                  searchOptions: controller.medicineList,
                  searchFieldAutoCompleteKey: controller.brandGenericAutoCompleteKey,
                ),
                //brand/generic
                const SpaceVertical(20),
                CustomAutoCompleteTextField(
                  label: AppStringKey.labelMedicineDosageType.tr,
                  hint: AppStringKey.hintMedicineDosageType.tr,
                  focusNode: controller.medicineDosageFocusNode,
                  textEditingController: controller.medicineDosageController,
                  onSelectedValue: (val) {
                    controller.medicineDosageType.value = val;
                  },
                  searchOptions: controller.medicineDosageSearchOptions,
                  searchFieldAutoCompleteKey: controller.medicineDosageAutoCompleteKey,
                ), //Medicine Dosage Type
                const SpaceVertical(20),
                CustomDropDownField(
                  hintTitle: AppStringKey.labelMedicineTiming.tr,
                  items: controller.medicineTimingList,
                  validationText: '',
                  onChanged: (val) {
                    controller.medicineTiming.value = val!;
                  },
                  labelText: AppStringKey.labelMedicineTiming.tr,
                ), //medicine timing
                Obx(
                  () => Visibility(
                    visible: controller.medicineTiming.value == controller.medicineTimingList[2],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SpaceVertical(20),
                        Text(
                          AppStringKey.labelTimeOfDay.tr,
                          style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                        ),
                        const SpaceVertical(15),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            CustomMultiSelectCheckBox(
                              index: 0,
                              listTimeOfTheDay: controller.listTimeOfTheDay,
                            ),
                            CustomMultiSelectCheckBox(
                              index: 1,
                              listTimeOfTheDay: controller.listTimeOfTheDay,
                            ),
                            CustomMultiSelectCheckBox(
                              index: 2,
                              listTimeOfTheDay: controller.listTimeOfTheDay,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ), //time of the day

                Obx(
                  () => Visibility(
                    visible: controller.medicineTiming.value == controller.medicineTimingList[1],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SpaceVertical(20),
                        CustomDropDownField(
                          hintTitle: AppStringKey.hintSelect.tr,
                          items: controller.hoursList,
                          validationText: '',
                          onChanged: (val) {
                            controller.hourlyDifference.value = val!;
                          },
                          labelText: AppStringKey.labelHourDifferenceDay.tr,
                        ),
                      ],
                    ),
                  ),
                ),
                const SpaceVertical(20),
                Text(
                  AppStringKey.labelBeforeOrAfterMealInstruction.tr,
                  style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                ),
                const SpaceVertical(15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomCheckBox(
                      index: 1,
                      title: AppStringKey.strBefore.tr,
                      selectedIndex: controller.mealInstructionIndex,
                    ),
                    CustomCheckBox(
                      index: 2,
                      title: AppStringKey.strAfter.tr,
                      selectedIndex: controller.mealInstructionIndex,
                    ),
                    CustomCheckBox(
                      index: 3,
                      title: AppStringKey.strNA.tr,
                      selectedIndex: controller.mealInstructionIndex,
                    ),
                  ],
                ), //meal instruction
                const SpaceVertical(20),
                CustomRoundTextField(
                  hintText: AppStringKey.hint7days.tr,
                  labelText: AppStringKey.strDuration.tr,
                  isRequireField: true,
                  backgroundColor: AppColors.transparent,
                  textEditingController: controller.durationController,
                ), //duration
                const SpaceVertical(20),
                Center(
                  child: GestureDetector(
                    onTap: () {
                      controller.continueMedicine.value = !controller.continueMedicine.value;
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Obx(
                          () => Container(
                            width: 24.0,
                            height: 24.0,
                            decoration: BoxDecoration(
                              color: controller.continueMedicine.value ? AppColors.colorDarkBlue : Colors.transparent,
                              border: Border.all(
                                color: AppColors.colorDarkBlue,
                                width: 1.0,
                              ),
                            ),
                            child: Icon(
                              Icons.check,
                              size: 16.0,
                              color: controller.continueMedicine.value ? AppColors.colorGreen : Colors.white,
                            ),
                          ),
                        ),
                        const SpaceHorizontal(5),
                        Text(
                          AppStringKey.labelContinuesMedicine.tr,
                          style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
                        )
                      ],
                    ).marginOnly(right: 15),
                  ),
                ), // continues medicine
                const SpaceVertical(20),
                CustomRoundTextField(
                  hintText: AppStringKey.hintSpecialInstruction.tr,
                  labelText: AppStringKey.labelSpecialInstruction.tr,
                  isRequireField: true,
                  backgroundColor: AppColors.transparent,
                  textEditingController: controller.specialInstructionController,
                ), // special instruction
              ],
            ).paddingAll(20),
          ),
        ),
      ),
    );
  }
}

class CustomCheckBox extends StatelessWidget {
  late final int index;
  late final RxInt selectedIndex;
  final String title;

  CustomCheckBox({
    Key? key,
    required this.index,
    required this.selectedIndex,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        selectedIndex.value = index;
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Obx(
            () => Container(
              width: 24.0,
              height: 24.0,
              decoration: BoxDecoration(
                color: selectedIndex.value == index ? AppColors.colorDarkBlue : Colors.transparent,
                border: Border.all(
                  color: AppColors.colorDarkBlue,
                  width: 1.0,
                ),
              ),
              child: Icon(
                Icons.check,
                size: 16.0,
                color: selectedIndex.value == index ? AppColors.colorGreen : Colors.white,
              ),
            ),
          ),
          const SpaceHorizontal(5),
          Text(
            title,
            style: BaseStyle.textStyleNunitoSansRegular(
              12,
              AppColors.colorDarkBlue,
            ),
          )
        ],
      ).marginOnly(right: 15),
    );
  }
}

class CustomMultiSelectCheckBox extends StatelessWidget {
  late final int index;
  final RxList<ModelCheckBox> listTimeOfTheDay;

  CustomMultiSelectCheckBox({
    Key? key,
    required this.index,
    required this.listTimeOfTheDay,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        listTimeOfTheDay[index].selected = !listTimeOfTheDay[index].selected!;
        listTimeOfTheDay.refresh();
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Obx(
            () => Container(
              width: 24.0,
              height: 24.0,
              decoration: BoxDecoration(
                color: listTimeOfTheDay[index].selected! ? AppColors.colorDarkBlue : Colors.transparent,
                border: Border.all(
                  color: AppColors.colorDarkBlue,
                  width: 1.0,
                ),
              ),
              child: Icon(
                Icons.check,
                size: 16.0,
                color: listTimeOfTheDay[index].selected! ? AppColors.colorGreen : Colors.white,
              ),
            ),
          ),
          const SpaceHorizontal(5),
          Text(
            listTimeOfTheDay[index].name!,
            style: BaseStyle.textStyleNunitoSansRegular(
              12,
              AppColors.colorDarkBlue,
            ),
          )
        ],
      ).marginOnly(right: 15),
    );
  }
}
