import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/cupertino.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_endpoints.dart';
import 'package:med_ai/api/api_interface/api_interface.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/models/dr_prescribe_medicine_model.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_followup_prescribe_medicine/dr_followup_prescribe_medicine_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_prescribe_medicine/dr_prescribe_medicine_controller.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';

import '../../../../constant/app_strings_key.dart';
import '../../../../models/model_check_box.dart';
import '../../../../utils/utilities.dart';

class DrAddNewMedicineController extends BaseController {
  final String tag = "AddNewMedVisit";
  var durationController = TextEditingController();
  var specialInstructionController = TextEditingController();
  var brandGenericController = TextEditingController();
  var brandGenericFocusNode = FocusNode();
  var brandGenericName = ''.obs;
  var medicineDosageController = TextEditingController();
  var medicineDosageFocusNode = FocusNode();
  var medicineDosageType = ''.obs;
  var medicineTiming = ''.obs;
  var hourlyDifference = ''.obs;

  var currentRoute = Get.currentRoute;
  var previousRoute = Get.previousRoute;

  RxList<ModelCheckBox> listTimeOfTheDay = RxList();
  RxList<String> medicineList = [''].obs;
  var api = ApiCallObject();

  GlobalKey<TextFieldAutoCompleteState<String>> brandGenericAutoCompleteKey = GlobalKey();
  GlobalKey<TextFieldAutoCompleteState<String>> medicineDosageAutoCompleteKey = GlobalKey();
  final List<String> brandGenericSearchOptions = <String>[
    'Mebhydrolin Napadisylate',
    'Napasin',
    'Napa',
    'Napa Extend',
    'Napa One',
    'Napa Radpid (Actizord)',
  ];

  final List<String> medicineDosageSearchOptions = <String>[
    'Aerosol Inhalation',
    'Capsule',
    'Chewable Tablet',
    'Cr Capsule',
    'Cr Tablet',
    'Cream',
    'Drops',
    'Ear Drop',
    'Effervescent Granules',
  ];

  var brandGenericList = [
    'Mebhydrolin Napadisylate',
    'Napasin',
    'Napa',
    'Napa Extend',
    'Napa One',
    'Napa Radpid (Actizord)',
  ];

  var medicineTimingList = [
    'None',
    'Hour Difference/ Day',
    '3 Times/Day',
  ];
  var hoursList = [
    '4 Hours',
    '6 Hours',
    '8 Hours',
    '12 Hours',
    '24 Hours',
  ];

  RxInt mealInstructionIndex = 0.obs;
  RxInt timeOfDayIndex = 0.obs;
  var continueMedicine = false.obs;

  bool detailsFilled() {
    if (specialInstructionController.text != '' ||
        continueMedicine.value ||
        durationController.text != '' ||
        mealInstructionIndex.value != 0 ||
        medicineTiming.value != '' ||
        brandGenericName.value != '') {
      return true;
    } else {
      return false;
    }
  }

  @override
  void onInit() {
    super.onInit();

    getDrugFromApi();

    developer.log("CurrentRoute $currentRoute");
    developer.log("PreviousRoute $previousRoute");

    //CustomDatabase().fetchDataModel();
    //developer.log("Database: $data");

    listTimeOfTheDay.add(
      ModelCheckBox(
        name: AppStringKey.strMorning.tr,
        selected: false,
      ),
    );
    listTimeOfTheDay.add(
      ModelCheckBox(
        name: AppStringKey.strNoon.tr,
        selected: false,
      ),
    );
    listTimeOfTheDay.add(
      ModelCheckBox(
        name: AppStringKey.strNight.tr,
        selected: false,
      ),
    );
  }

  void addNewMedicine() {
    var brand = brandGenericName.toString();
    var dosageType = medicineDosageController.text;
    var threeTimesDay = "";
    var hourDifference = "0";
    var duration = "";
    var mealInstruction = getMealInstruction();
    var specialInstruction = specialInstructionController.text;

    if (medicineTiming.value == medicineTimingList[2]) {
      threeTimesDay = getThreeTimes();
    }
    if (medicineTiming.value == medicineTimingList[1]) {
      hourDifference = CustomFunctions.removeHoursFromString(hourlyDifference.toString());
    }
    if (continueMedicine.isTrue) {
      durationController.text = "";
      duration = "Continues";
    } else {
      duration = '${durationController.text} Days';
    }

    DrPrescribeMedicineModel medicineModel = DrPrescribeMedicineModel(
        brandName: brand,
        dosageType: dosageType,
        threeTimesDay: threeTimesDay,
        hourDifference: hourDifference,
        mealInstruction: mealInstruction,
        duration: duration,
        specialInstruction: specialInstruction);
    CustomFunctions.customSnackBar(AppStringKey.success.tr, '$brand Added Successfully!');

    //var data = medicineModel.threeTimesDay!.isEmpty ? medicineModel.hourDifference: medicineModel.threeTimesDay;
    //developer.log("ThreeTimesDay: ${medicineModel.threeTimesDay}");
    //developer.log("HourDifference: ${medicineModel.hourDifference}");
    //developer.log("CalculationData: $data");
    if (previousRoute == "/drFollowupPrescribeMedicine") {
      Get.find<DrFollowupPrescribeMedicineController>().addNewMedicine(medicineModel);
      Get.close(1);
      developer.log("FollowUpPrescribe");
    } else {
      developer.log("VisitPrescribe");
      Get.find<DrPrescribeMedicineController>().addNewMedicine(medicineModel);
      Get.close(1);
    }
  }

  String getThreeTimes() {
    String morning = "0";
    String noon = "0";
    String night = "0";

    if (listTimeOfTheDay[0].selected!) {
      morning = "1";
    } else {
      morning = "0";
    }

    if (listTimeOfTheDay[1].selected!) {
      noon = "1";
    } else {
      noon = "0";
    }

    if (listTimeOfTheDay[2].selected!) {
      night = "1";
    } else {
      night = "0";
    }

    return '$morning+$noon+$night';
  }

  String getMealInstruction() {
    String instruction = 'NA';
    if (mealInstructionIndex.toString() == "1") {
      instruction = "No";
    } else if (mealInstructionIndex.toString() == "2") {
      instruction = "Yes";
    }
    developer.log('Meal Instruction: $instruction');
    return instruction;
  }

  void setDataToBrandName(String name) {
    brandGenericName.value = name;
    brandGenericController.text = name;
    developer.log("SetDataToBrandName: $name", name: tag);
    //brandGenericController.value = name;
  }

  void onBack(BuildContext context) {
    if (detailsFilled()) {
      Utilities.commonDialog(
        context,
        AppStringKey.strAlert.tr,
        AppStringKey.labelGoBackAlert.tr,
        AppStringKey.strStay.tr,
        AppStringKey.strGoBack.tr,
        () async {
          Get.back();
        },
        () {
          Get.back();
          Get.back();
        },
      );
    } else {
      Get.back();
    }
  }

  Future<void> getDrugFromApi() async {
    http.Response response;
    try {
      response = await api.getData(ApiEndpoints.urlGetDrugList);
      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        var medicineListFromApi = responseBody['data']['list'];
        //developer.log("DataDetails: $medicineListFromApi",name: tag);
        for (var list in medicineListFromApi) {
          //developer.log("DataDetails: $list",name: tag);
          medicineList.add(list['brand_name']);
        }
      }
      developer.log("MedicineCount: ${medicineList.length}");
    } catch (e) {
      developer.log('SetDataException: $e', name: tag);
    }
  }
}
