import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_clinical_decision/dr_clinical_decision_controller.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_chip_widget.dart';
import 'package:med_ai/widgets/custom_round_text_field.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_strings_key.dart';
import '../../../../widgets/custom_auto_complete_textfield.dart';
import '../../../../widgets/space_horizontal.dart';

class DrClinicalDecisionScreen extends GetView<DrClinicalDecisionController> {
  const DrClinicalDecisionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.btnUpdate.tr,
        onClick: () {
          Get.back();
        },
      ).paddingOnly(left: 20, right: 20, top: 15, bottom: MediaQuery.viewInsetsOf(context).bottom + 15),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStringKey.titleDecision.tr,
                style: BaseStyle.textStyleNunitoSansBold(20, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(20),
              CustomAutoCompleteTextField(
                label: AppStringKey.strIdentifiedDisease.tr,
                hint: AppStringKey.hintDiseaseName.tr,
                focusNode: controller.searchFocusNode,
                textEditingController: controller.clinicalDecisionController,
                onSelectedValue: (val) {
                  if (!controller.listClinicalDecisions.contains(val)) {
                    controller.listClinicalDecisions.add(val);
                    controller.listClinicalDecisions.refresh();
                    controller.clinicalDecisionController.clear();
                  } else {
                    Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
                  }
                },
                searchOptions: controller.searchOptions,
                searchFieldAutoCompleteKey: controller.searchFieldAutoCompleteKey,
              ),
              const SpaceVertical(15),
              Obx(
                () => Wrap(
                  children: controller.listClinicalDecisions
                      .map(
                        (element) => CustomChipWidget(
                          label: element,
                          callback: () {
                            controller.listClinicalDecisions.remove(element);
                            controller.listClinicalDecisions.refresh();
                          },
                        ).marginOnly(right: 10, bottom: 10),
                      )
                      .toList(),
                ),
              ),
              const SpaceVertical(20),
              CustomRoundTextField(
                labelText: AppStringKey.strClinicalNote.tr,
                isRequireField: true,
                maxLines: 5,
                backgroundColor: AppColors.transparent,
                hintText: AppStringKey.hintNote,
                textEditingController: controller.clinicalNoteController,
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemClinicalDecision extends StatelessWidget {
  final String title;
  final DrClinicalDecisionController controller;
  final bool visibleIcon;

  const ItemClinicalDecision({
    Key? key,
    required this.title,
    required this.controller,
    this.visibleIcon = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
      decoration: BoxDecoration(
        color: AppColors.colorBackground,
        borderRadius: BorderRadius.circular(30),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            title,
            style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
          ),
          const SpaceHorizontal(3),
          visibleIcon
              ? GestureDetector(
                  onTap: () {
                    controller.listClinicalDecisions.remove(title);
                    controller.listClinicalDecisions.refresh();
                  },
                  child: const Icon(
                    Icons.cancel,
                    size: 15,
                  ),
                )
              : Container(),
        ],
      ),
    ).marginOnly(right: 10, bottom: 10);
  }
}
