import 'package:flutter/cupertino.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';

class DrClinicalDecisionController extends BaseController {
  var clinicalDecisionController = TextEditingController();
  var clinicalNoteController = TextEditingController();

  var listClinicalDecisions = [
    'Dengue',
    'Fever',
  ].obs;
  final FocusNode searchFocusNode = FocusNode();
  GlobalKey<TextFieldAutoCompleteState<String>> searchFieldAutoCompleteKey = GlobalKey();
  final List<String> searchOptions = <String>[
    'Typhoid',
    'Typhoid and paraTyphoid fevers',
  ];
}
