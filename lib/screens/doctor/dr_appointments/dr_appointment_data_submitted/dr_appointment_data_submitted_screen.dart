import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_new_consultancy/dr_new_consultancy_controller.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_strings_key.dart';
import 'dr_appointment_data_submitted_controller.dart';

class DrAppointmentDataSubmittedScreen extends GetView<DrAppointmentDataSubmittedController> {
  const DrAppointmentDataSubmittedScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        backgroundColor: AppColors.white,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                AppImages.success,
                height: 80,
                width: 80,
              ),
              const SpaceVertical(20),
              Text(
                AppStringKey.labelAppointmentDataSubmit.tr,
                style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorDarkBlue),
                textAlign: TextAlign.center,
              ),
              const SpaceVertical(50),
              SubmitButton(
                title: AppStringKey.btnBackToNewConsultancy.tr,
                onClick: () {
                  if (consultancyFrom == EnumConsultancyFrom.APPOINTMENT_TAB) {
                    Get.find<DrNewConsultancyController>().getUpcomingAppointments();
                    Get.until((route) => Get.currentRoute == RoutePaths.DR_BOTTOM_TAB);
                  } else {
                    Get.find<DrNewConsultancyController>().getUpcomingAppointments();
                    Get.until((route) => Get.currentRoute == RoutePaths.DR_NEW_CONSULTANCY);
                  }
                },
                titleFontSize: 14,
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}
