import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_visit_appointment/dr_visit_appointment_controller.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/custom_heading_visit_followup.dart';
import 'package:med_ai/widgets/ripple_effect_widget.dart';

import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/call_controller.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_content_visit_followup_widget.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';

class DrVisitAppointmentScreen extends GetView<DrVisitAppointmentController> {
  const DrVisitAppointmentScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const ItemPatientDetail(),
              const SpaceVertical(10),
              const ItemComplaintsSymptoms(),
              const SpaceVertical(10),
              const ItemMedicalHistory(),
              const SpaceVertical(10),
              const ItemVitalSigns(),
              const SpaceVertical(40),
              Center(
                child: Text(
                  AppStringKey.strDisclaimer.tr,
                  style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
                ),
              ),
              const SpaceVertical(10),
              Text(
                controller.declarationText,
                style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(30),
              SubmitButton(
                title: AppStringKey.strNext.tr,
                onClick: () {
                  Get.toNamed(RoutePaths.DR_IDENTIFIED_DISEASE);
                },
              )
            ],
          ).paddingAll(15),
        ),
      ),
    );
  }
}

class ItemPatientDetail extends StatelessWidget {
  static DrVisitAppointmentController controller = Get.find<DrVisitAppointmentController>();

  const ItemPatientDetail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Container(
        width: double.maxFinite,
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
              width: double.maxFinite,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: AppColors.colorDarkBlue,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.strAppointmentSchdule.tr,
                        style: BaseStyle.textStyleNunitoSansRegular(
                          12,
                          AppColors.white,
                        ),
                      ),
                      const SpaceVertical(5),
                      Row(
                        children: [
                          Image.asset(
                            AppImages.calender,
                            width: 20,
                            height: 20,
                          ),
                          const SpaceHorizontal(5),
                          Text(
                            controller.schedule,
                            style: BaseStyle.textStyleNunitoSansBold(
                              14,
                              AppColors.white,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 3),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: AppColors.colorDarkOrange,
                    ),
                    child: Center(
                      child: Text(
                        AppStringKey.visit.tr,
                        style: BaseStyle.textStyleDomineSemiBold(
                          10,
                          AppColors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SpaceVertical(14),
            Row(
              children: [
                Container(
                  height: 55,
                  width: 55,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    image: const DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage(
                        AppImages.dummyProfile,
                      ),
                    ),
                  ),
                ),
                const SpaceHorizontal(10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Obx(
                      () => Text(
                        //controller.soapDetails.personalInformation!.name!,
                        controller.soapName.toString(),
                        style: BaseStyle.textStyleDomineBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    ),
                    Obx(
                      () => Text(
                        '${controller.soapAge} ${AppStringKey.yearsOld.tr}, ${controller.soapGender}',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    ),
                    Obx(
                      () => Text(
                        '${controller.soapDistrict}, ${controller.soapCountry}',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SpaceVertical(10),
            Row(
              children: [
                Expanded(
                  child: CustomCardWidget(
                    shadowOpacity: shadow,
                    widget: RippleEffectWidget(
                      callBack: () {
                        Get.put(CallController());
                        Get.find<CallController>().initilize();
                        controller.getAccessToken();
                      },
                      borderRadius: 5,
                      widget: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.videocam_outlined,
                            color: AppColors.colorOrange,
                            size: 20,
                          ),
                          const SpaceHorizontal(8),
                          Text(
                            AppStringKey.labelInAppVideoCall.tr,
                            style: BaseStyle.textStyleNunitoSansBold(10, AppColors.colorDarkBlue),
                          ),
                        ],
                      ).paddingSymmetric(vertical: 8),
                    ),
                  ),
                ),
                Expanded(
                  child: CustomCardWidget(
                    shadowOpacity: shadow,
                    widget: RippleEffectWidget(
                      callBack: () {
                        Utilities.commonDialog(
                          context,
                          AppStringKey.labelWarning.tr,
                          AppStringKey.cancelSingleAppointmentDesc.tr,
                          AppStringKey.deleteYes.tr,
                          AppStringKey.deleteNo.tr,
                          () {
                            Get.back();
                          },
                          () {
                            Get.back();
                          },
                        );
                      },
                      borderRadius: 5,
                      widget: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.cancel,
                            color: AppColors.colorPink,
                            size: 20,
                          ),
                          const SpaceHorizontal(10),
                          Text(
                            AppStringKey.labelCancelAppointment.tr,
                            style: BaseStyle.textStyleNunitoSansBold(10, AppColors.colorDarkBlue),
                          ),
                        ],
                      ).paddingSymmetric(vertical: 8),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class ItemComplaintsSymptoms extends StatelessWidget {
  static DrVisitAppointmentController controller = Get.find<DrVisitAppointmentController>();

  const ItemComplaintsSymptoms({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomHeadingVisitFollowUp(
                heading: AppStringKey.strComplaintsSymptoms.tr,
              ),
              RippleEffectWidget(
                borderRadius: 5,
                callBack: () {
                  Get.toNamed(RoutePaths.DR_VISIT_UPDATE_SYMPTOMS);
                },
                widget: Image.asset(
                  AppImages.edit,
                  height: 20,
                  width: 20,
                ).paddingAll(5),
              ),
            ],
          ),
          const SpaceVertical(10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 5),
                height: 5,
                width: 5,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppColors.colorDarkBlue,
                ),
              ),
              const SpaceHorizontal(10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.labelChiefComplaint.tr,
                    style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
                  ),
                  const SpaceVertical(3),
                  Obx(
                    () => Text(
                      CustomFunctions.setStringFromListString(controller.chiefComplaints),
                      style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
                    ),
                  ),
                ],
              )
            ],
          ),
          const SpaceVertical(20),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 5),
                height: 5,
                width: 5,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppColors.colorDarkBlue,
                ),
              ),
              const SpaceHorizontal(10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.labelSymptoms.tr,
                    style: BaseStyle.textStyleNunitoSansBold(12, AppColors.colorDarkBlue),
                  ),
                  const SpaceVertical(3),
                  Obx(
                    () => Text(
                      CustomFunctions.setStringFromListString(controller.symptoms),
                      style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
                    ),
                  ),
                ],
              )
            ],
          )
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}

class ItemMedicalHistory extends StatelessWidget {
  static DrVisitAppointmentController controller = Get.find<DrVisitAppointmentController>();

  const ItemMedicalHistory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomHeadingVisitFollowUp(
            heading: AppStringKey.strMedicalHistory.tr,
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.strMedicalCondition.tr,
              value: CustomFunctions.setStringFromListString(controller.medicalHistory),
            ),
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.strAllergies.tr,
              value: controller.allergy.toString(),
            ),
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.strFamilyHistory.tr,
              value: CustomFunctions.setStringFromListString(controller.familyHistory),
            ),
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.strPersonalHistory.tr,
              value: CustomFunctions.setStringFromListString(controller.personalHistory),
            ),
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.strMedicalCondition.tr,
              value: controller.medicalCondition.toString(),
            ),
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.strMedication.tr,
              value: controller.medication.toString(),
            ),
          ),
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}

class ItemVitalSigns extends StatelessWidget {
  static DrVisitAppointmentController controller = Get.find<DrVisitAppointmentController>();

  const ItemVitalSigns({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomHeadingVisitFollowUp(
                heading: AppStringKey.strVitalSigns.tr,
              ),
              RippleEffectWidget(
                borderRadius: 5,
                callBack: () {
                  Get.toNamed(RoutePaths.DR_UPDATE_VITAL_SIGNS);
                },
                widget: Image.asset(
                  AppImages.edit,
                  height: 20,
                  width: 20,
                ).paddingAll(5),
              ),
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.strBP.tr,
              value: controller.bp.toString(),
            ),
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.strTemp.tr,
              value: controller.temp.toString(),
            ),
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.strPulseRate.tr,
              value: controller.pulse.toString(),
            ),
          ),
          const SpaceVertical(10),
          Obx(
            () => CustomContentVisitFollowUpWidget(
              title: AppStringKey.strBreathingsRate.tr,
              value: controller.breathingRate.toString(),
            ),
          ),
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}

class ItemRecommendations extends StatelessWidget {
  static DrVisitAppointmentController controller = Get.find<DrVisitAppointmentController>();

  const ItemRecommendations({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomHeadingVisitFollowUp(
                heading: AppStringKey.strRecommendations.tr,
              ),
              RippleEffectWidget(
                borderRadius: 5,
                callBack: () {},
                widget: Image.asset(
                  AppImages.edit,
                  height: 20,
                  width: 20,
                ).paddingAll(5),
              )
            ],
          ),
          const SpaceVertical(15),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strProvisionalDiagnosis.tr,
            value: 'Chronic obstructive pulmonary disease ',
          ),
          const SpaceVertical(10),
          CustomContentVisitFollowUpWidget(
            title: AppStringKey.strRecommendedMedicines.tr,
            value: 'Unknown',
          ),
        ],
      ).paddingSymmetric(horizontal: 8, vertical: 15),
    );
  }
}
