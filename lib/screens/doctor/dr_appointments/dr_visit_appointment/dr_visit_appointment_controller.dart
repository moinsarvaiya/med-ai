import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_endpoints.dart';
import 'package:med_ai/api/api_interface/api_interface.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/models/dr_visit_soap_model.dart';
import 'package:med_ai/utils/app_preference/visit_preferences.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../../api_patient/patient_api_end_point.dart';
import '../../../../api_patient/patient_api_interface.dart';
import '../../../../api_patient/patient_api_presenter.dart';
import '../../../../constant/app_strings.dart';
import '../../../../utils/functions/custom_functions.dart';
import '../../../../utils/functions/permission_check.dart';
import '../../../../utils/video_call.dart';

class DrVisitAppointmentController extends BaseController implements PatientApiCallBacks {
  ApiCallObject api = ApiCallObject();
  var consultationID;
  String schedule = "";
  late DrVisitSoapModel soapDetails = DrVisitSoapModel();

  var soapName = "".obs;
  RxString soapAge = "".obs;
  RxString soapGender = "".obs;
  RxString soapDistrict = "".obs;
  RxString soapCountry = "".obs;

  RxList<String> chiefComplaints = <String>[].obs;
  RxList<String> symptoms = <String>[].obs;

  RxList<String> medicalHistory = <String>[].obs;
  RxString allergy = "".obs;
  RxList<String> familyHistory = <String>[].obs;
  RxList<String> personalHistory = <String>[].obs;

  RxString medicalCondition = "".obs;
  RxString medication = "".obs;

  RxString bp = "".obs;
  RxString temp = "".obs;
  RxString pulse = "".obs;
  RxString breathingRate = "".obs;
  String roomName = '';

  var declarationText =
      'Please note that the information provided by MedAi is provided solely for guideline purposes and is not a qualified medical opinion.'
      ' This information should not be considered advice or an opinion. of doctor or other health professional about your actual medical state and you should see a doctor for any symptoms you may have. '
      'If you are experiencing a health emergency, you should call your local emergency number immediately to request emergency medical assistance.';

  @override
  void onInit() {
    super.onInit();
    setData();
    getVisitSoap();
  }

  void setData() {
    try {
      var receivedIntent = jsonDecode(Get.arguments[AppStrings.forwardArgument]);
      consultationID = receivedIntent['consultationId'];
      schedule = receivedIntent['schedule'];
      developer.log("consultationID: $consultationID");

      VisitPreferences().sharedPrefWrite(StorageKeys.visitID, consultationID);
      developer.log("VisitPref ${VisitPreferences().sharedPrefRead(StorageKeys.visitID)}");
    } catch (e) {
      developer.log(e.toString());
    }
  }

  Future<void> getVisitSoap() async {
    http.Response serverResponse;
    Object requestBody = {"consultation_id": consultationID};

    serverResponse = await api.postData(ApiEndpoints.urlDoctorGetVisitSoap, requestBody);
    if (serverResponse.statusCode == 200) {
      Map<String, dynamic> responseBody = jsonDecode(serverResponse.body);
      print('object $responseBody');
      var code = responseBody["code"];
      if (code == "200") {
        var soapData = responseBody["soap_data"];
        //soapDetails = DrVisitSoapModel.fromJson(soapData);
        var details = DrVisitSoapModel.fromJson(soapData);

        soapName.value = details.personalInformation!.name!;
        soapAge.value = details.personalInformation!.age.toString();
        soapGender.value = details.personalInformation!.gender!;
        soapDistrict.value = details.personalInformation!.district!;
        soapCountry.value = details.personalInformation!.country!;

        chiefComplaints.addAll(details.complaintsSymptoms!.chiefComplaints!);
        symptoms.addAll(details.complaintsSymptoms!.symptoms!);

        medicalHistory.addAll(details.medicalHistory!.medicalCondition!);
        allergy.value = details.medicalHistory?.allergy ?? "";
        familyHistory.addAll(details.medicalHistory!.familyHistory!);
        personalHistory.addAll(details.medicalHistory!.personalHistory!);

        medicalCondition.value = details.preExistingCond?.medicalCondition ?? "";
        medication.value = details.preExistingCond?.medication ?? "";

        bp.value = details.vitalSigns?.bp ?? "";
        temp.value = details.vitalSigns?.temp ?? "";
        pulse.value = details.vitalSigns?.pulse ?? "";
        breathingRate.value = details.vitalSigns?.breathingRate ?? "";

        details.personalInformation!.name ?? "";
      }
    }
  }

  void getAccessToken() {
    CheckPermission.check([Permission.camera, Permission.microphone], "storage");
    roomName = CustomFunctions.generateRoomNameWithTimestamp();
    // PatientApiPresenter(this).getAccessToken(roomName);
    onJoin(Get.context!, '');
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    // TODO: implement onConnectionError
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    // TODO: implement onError
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    // TODO: implement onLoading
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.getAgoraAccessToken:
        print('object $object');
        String accessToken = object['access_token'];
        onJoin(Get.context!, accessToken);
        break;
    }
  }

  Future<void> onJoin(BuildContext context, String accessToken) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => VideoCall(accessToken: accessToken),
      ),
    );
  }
}
