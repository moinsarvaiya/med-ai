import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/common/select_user/select_user_controller.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/select_user_widget.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../constant/ui_constant.dart';

class SelectUserScreen extends GetView<SelectUserController> {
  const SelectUserScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SpaceVertical(BaseSize.height(3)),
            Text(
              AppStringKey.strWelcome.tr,
              style: BaseStyle.textStyleDomineBold(
                32,
                AppColors.colorDarkBlue,
              ),
            ),
            SpaceVertical(BaseSize.height(3)),
            Image.asset(
              AppImages.splashLogo,
              height: MediaQuery.sizeOf(context).height / 4,
            ),
            Text(
              AppStringKey.selectUserDescription.tr,
              style: BaseStyle.textStyleNunitoSansMedium(
                16,
                AppColors.colorTextDarkGray,
              ),
              textAlign: TextAlign.center,
            ),
            SpaceVertical(BaseSize.height(6)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                UserSelectWidget(
                  image: AppImages.patient,
                  title: AppStringKey.strPatient.tr,
                  onClick: () {
                    isDoctor = false;
                    userTypeId = 0;
                    LoginPreferences().sharedPrefWrite(StorageKeys.userTypeId, 'patient');
                    Get.toNamed(
                      RoutePaths.LOGIN,
                      arguments: {
                        'isDoctor': false,
                      },
                    );
                  },
                ),
                UserSelectWidget(
                  image: AppImages.doctor,
                  title: AppStringKey.strDoctor.tr,
                  onClick: () {
                    isDoctor = true;
                    userTypeId = 1;
                    LoginPreferences().sharedPrefWrite(StorageKeys.userTypeId, 'doctor');
                    Get.toNamed(
                      RoutePaths.LOGIN,
                      arguments: {
                        'isDoctor': true,
                      },
                    );
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
