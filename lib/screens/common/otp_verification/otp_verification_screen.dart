import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_endpoints.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/common/otp_verification/otp_verification_controller.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/space_vertical.dart';
import 'package:otp_pin_field/otp_pin_field.dart';

import '../../../api/api_interface/api_interface.dart';
import '../../../constant/app_strings.dart';

class OtpVerificationScreen extends GetView<OtpVerificationController> {
  const OtpVerificationScreen({super.key});

  Future<void> checkOTPToServer(String endpoint, Object object) async {
    ApiCallObject api = ApiCallObject();
    http.Response response;

    try {
      response = await api.postData(endpoint, object, false);
      if (response.statusCode == 200) {
        var body = jsonDecode(response.body.toString());
        if (body['code'] == '200') {
          String message = body['success_msg'];
          // message.showSuccessSnackBar();
          LoginPreferences().sharedPrefWrite(StorageKeys.token, body['access']);
          LoginPreferences().sharedPrefWrite(StorageKeys.username, body['username']);
          LoginPreferences().sharedPrefWrite(StorageKeys.personId, body['person_id']);
          LoginPreferences().sharedPrefWrite(StorageKeys.name, body['name']);
          LoginPreferences().sharedPrefWrite(StorageKeys.approvalStatus, body['approval_status']);
          LoginPreferences().sharedPrefWrite(StorageKeys.referralCode, body['referral_code']);

          if (body["user_type"] == "doctor") {
            LoginPreferences().sharedPrefWrite(StorageKeys.userTypeId, "doctor");
            Get.offNamedUntil(RoutePaths.DR_BOTTOM_TAB, (route) => false);
          } else if (body["user_type"] == "patient") {
            LoginPreferences().sharedPrefWrite(StorageKeys.userTypeId, "patient");
            Get.offNamedUntil(RoutePaths.PT_BOTTOM_TAB, (route) => false);
          }
        } else {
          String message = body['error_msg'];
          message.showErrorSnackBar();
        }
      } else {
        String message = jsonDecode(response.body.toString())['detail'] ?? "No Data Found!";
        message.showErrorSnackBar();
      }
    } on Exception catch (e) {
      AppStrings.somethingWentWrong.showErrorSnackBar();
      await Future.delayed(const Duration(seconds: 3));
    }
  }

  Future<void> resendOTPToServer(String endpoint, Object object) async {
    ApiCallObject api = ApiCallObject();
    http.Response response;

    try {
      response = await api.postData(endpoint, object, false);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body)["code"] == "200") {
        } else {
          String message = AppStrings.somethingWentWrong;
          message.showErrorSnackBar();
        }
      }
    } on Exception catch (e) {
      AppStrings.somethingWentWrong.showErrorSnackBar();
      await Future.delayed(const Duration(seconds: 3));
    }
  }

  void checkOTP(String text) async {
    Object object;
    if (text.length == 6) {
      object = {"otp": text};
      checkOTPToServer(ApiEndpoints.urlVerifyOtp, object);
    } else {
      var error = "Error";
      error.showErrorSnackBar;
    }
  }

  void resendOtp() {
    String data = controller.argumentData['userMobileOrEmail'];
    Object object;
    object = {"username": data};

    resendOTPToServer(ApiEndpoints.urlResendOtp, object);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
          bottom: MediaQuery.viewInsetsOf(context).bottom + 15,
          top: 10,
        ),
        child: SubmitButton(
          title: AppStringKey.labelVerify.tr,
          onClick: () {
            controller.stopTimer();
            if (LoginPreferences().sharedPrefRead(StorageKeys.userTypeId) == 'doctor') {
              checkOTP(controller.verificationCode.value);
              // Get.toNamed(RoutePaths.DR_BOTTOM_TAB);
            }
            //Get.find<OtpVerificationController>().startTimer();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SpaceVertical(MediaQuery.sizeOf(context).height / 20),
              Text(
                AppStringKey.titleOtpScreen.tr,
                style: BaseStyle.textStyleDomineBold(22, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(3),
              Text(
                AppStringKey.subTitleOtpScreen.tr,
                style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
              ),
              SpaceVertical(MediaQuery.sizeOf(context).height / 16),
              Center(
                child: Text(
                  AppStringKey.hintOtp.tr,
                  style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorTextDarkGray),
                ),
              ),
              SpaceVertical(MediaQuery.sizeOf(context).height / 25),
              OtpPinField(
                key: controller.otpPinFieldController,
                onSubmit: (text) {
                  controller.verificationCode.value = text;
                },
                onChange: (text) {
                  if (text.length == 6) {
                    checkOTP(text);
                  }
                },
                autoFillEnable: true,
                textInputAction: TextInputAction.done,
                onCodeChanged: (code) {},
                maxLength: 6,
                otpPinFieldStyle: OtpPinFieldStyle(
                  activeFieldBorderColor: AppColors.colorDarkBlue,
                  defaultFieldBorderColor: AppColors.colorDarkBlue,
                  fieldPadding: 15,
                  textStyle: BaseStyle.textStyleNunitoSansRegular(20, AppColors.colorDarkBlue),
                ),
                showCursor: true,
                cursorColor: AppColors.colorDarkBlue,
                showCustomKeyboard: false,
                cursorWidth: 1.5,
                mainAxisAlignment: MainAxisAlignment.center,
                fieldWidth: 25,
                fieldHeight: 40,
                otpPinFieldDecoration: OtpPinFieldDecoration.underlinedPinBoxDecoration,
              ),
              SpaceVertical(MediaQuery.sizeOf(context).height / 25),
              Obx(
                () => controller.duration.value == 0
                    ? GestureDetector(
                        onTap: () {
                          controller.duration.value = controller.startingDuration;
                          controller.startTimer();
                          resendOtp();
                        },
                        child: Center(
                          child: Text(
                            AppStringKey.labelResendOtp.tr,
                            style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorTextDarkGray),
                          ),
                        ),
                      )
                    : Center(
                        child: Text(
                          '(${Utilities.formatHHMMSSTimer(controller.duration.value)})',
                          style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorTextDarkGray),
                        ),
                      ),
              ),
              SpaceVertical(MediaQuery.sizeOf(context).height / 20),
              Center(
                child: Text(
                  AppStringKey.labelDoNotShareOTP.tr,
                  style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorTextDarkGray),
                ),
              ),
            ],
          ).paddingSymmetric(horizontal: 20),
        ),
      ),
    );
  }
}
