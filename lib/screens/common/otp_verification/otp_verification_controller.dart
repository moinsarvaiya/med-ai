import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:otp_pin_field/otp_pin_field.dart';

class OtpVerificationController extends BaseController {
  late Timer timer;
  int startingDuration = 20;
  var duration = 0.obs;
  var verificationCode = ''.obs;
  final otpPinFieldController = GlobalKey<OtpPinFieldState>();
  dynamic argumentData = Get.arguments;

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (duration.value == 0) {
          timer.cancel();
        } else {
          duration.value--;
        }
      },
    );
  }

  void stopTimer() {
    timer.cancel();
  }

  @override
  void onInit() {
    super.onInit();

    duration.value = startingDuration;
    startTimer();
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }
}
