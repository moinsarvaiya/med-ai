import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/common/splash/splash_screen_api.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: AppColors.white,
        child: Center(
          child: SizedBox(
            height: MediaQuery.sizeOf(context).height / 2,
            width: MediaQuery.sizeOf(context).width / 2,
            child: Image.asset(
              AppImages.splashLogo,
            ),
          ),
        ),
      ),
    );
  }

  startTime() async {
    var duration = const Duration(seconds: 2);
    return Timer(duration, route);
  }

  route() async {
    var onBoarding = LoginPreferences().sharedPrefRead(StorageKeys.isOnBoarding) ?? "";
    var token = LoginPreferences().sharedPrefRead(StorageKeys.token) ?? "";
    var userType = LoginPreferences().sharedPrefRead(StorageKeys.userTypeId) ?? "";

    if (onBoarding.toString().isEmpty) {
      print("isOnBoarding = null");
    } else {
      print("isOnBoarding = " + onBoarding);
    }
    if (token.toString().isEmpty) {
      print("token = null");
    } else {
      print("token = " + token);
    }
    if (userType.toString().isEmpty) {
      print("userType = null");
    } else {
      print("userType = " + userType);
    }

    if (token.toString().isNotEmpty && await APISplashScreen().verifyToken(token)) {
      if (userType.toString() == "doctor") {
        print("Doctor Logging in");
        Get.offAllNamed(RoutePaths.DR_BOTTOM_TAB);
      } else if (userType.toString() == "patient") {
        print("Patient Logging in");
        Get.offAllNamed(RoutePaths.PT_BOTTOM_TAB);
      } else {
        print("data missing");
        LoginPreferences().sharedPrefEraseAllData();
        Get.offAllNamed(RoutePaths.SELECT_USER);
      }
    } else if (onBoarding == "" || onBoarding == "0") {
      LoginPreferences().sharedPrefEraseAllData();
      Get.offAllNamed(RoutePaths.ONBOARDING);
    } else {
      LoginPreferences().sharedPrefEraseAllData();
      Get.offAllNamed(RoutePaths.SELECT_USER);
    }
  }
}
