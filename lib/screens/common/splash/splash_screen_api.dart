import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:med_ai/constant/base_extension.dart';

import '../../../api/api_endpoints.dart';
import '../../../api/api_interface/api_interface.dart';

class APISplashScreen {
  Future<bool> verifyToken(String token) async {
    ApiCallObject api = ApiCallObject();
    http.Response response;
    Object object = {"token": token};
    var endpoint = ApiEndpoints.urlVerifyToken;

    // print("requestBody: " + object.toString());

    try {
      response = await api.postData(endpoint, object, false);
      if (response.statusCode == 200) {
        // print(response.body.toString());
        return true;
      } else {
        String message = jsonDecode(response.body.toString())['detail'];
        message.showErrorSnackBar();
        return false;
      }
    } on Exception catch (e) {
      print(e);
      return false;
    }
  }
}
