import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/app_strings.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/utils/validators/validators.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../api/api_endpoints.dart';
import '../../../api/api_interface/api_interface.dart';
import '../../../constant/storage_keys.dart';
import '../../../routes/route_paths.dart';
import '../../../utils/app_preference/login_preferences.dart';
import '../../../widgets/custom_round_text_field.dart';
import '../../../widgets/select_user_widget.dart';
import 'login_controller.dart';

class LoginScreen extends GetView<LoginController> {
  const LoginScreen({super.key});

  Future<void> sendToServer(String endpoint, Object object) async {
    ApiCallObject api = ApiCallObject();
    http.Response response;
    print("requestBody: $object");

    try {
      response = await api.postData(endpoint, object, false);
      if (response.statusCode == 200) {
        print("received");
        print(response.body.toString());
        if (jsonDecode(response.body)["code"] == "200") {
          Get.toNamed(
            RoutePaths.OTP_VERIFICATION,
            arguments: {
              'userMobileOrEmail': controller.emailOrMobileController.value.text,
              "requestBody": object,
            },
          );
        } else {
          // String message = AppStrings.somethingWentWrong;
          String message = jsonDecode(response.body)["error_msg"];
          message.showErrorSnackBar();
        }
      }
    } on Exception catch (e) {
      print(e);
    }
  }

  void check() {
    var data = controller.emailOrMobileController.value.text;
    Object object;
    String userType;

    if (LoginPreferences().sharedPrefRead(StorageKeys.userTypeId) == "doctor") {
      userType = "doctor";
    } else {
      userType = "patient";
    }

    if (data == ApiEndpoints.mobileNo) {
      Get.toNamed(
        RoutePaths.OTP_VERIFICATION,
        arguments: {
          'userMobileOrEmail': controller.emailOrMobileController.value.text,
        },
      );
    } else if (data.contains("@")) {
      object = {"email": data, "user_type": userType};
      sendToServer(ApiEndpoints.urlContinueEmail, object);
    } else if (data.isNotEmpty) {
      object = {"phone": data, "user_type": userType};
      sendToServer(ApiEndpoints.urlContinuePhone, object);
    } else {
      AppStrings.hintEmailOrMobile.showSnackBar;
    }
  }

  @override
  Widget build(BuildContext context) {
    controller.isDoctor = controller.argumentData['isDoctor'];
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          displayLogo: true,
          onClick: () {
            if (WidgetsBinding.instance.window.viewInsets.bottom > 0.0) {
              FocusScope.of(context).requestFocus(FocusNode());
            } else {
              Get.back();
            }
          },
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(bottom: MediaQuery.viewInsetsOf(context).bottom + 16, top: 16, left: 16, right: 16),
        child: SubmitButton(
          title: AppStringKey.labelGetOtp.tr,
          onClick: () {
            check();
            // Get.toNamed(RoutePaths.OTP_VERIFICATION);
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: SizedBox(
            width: double.infinity,
            child: Form(
              key: controller.form,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SpaceVertical(MediaQuery.sizeOf(context).height / 9),
                  Center(
                    child: UserSelectWidget(
                      title: controller.isDoctor! ? AppStringKey.strDoctor.tr : AppStringKey.strPatient.tr,
                      image: controller.isDoctor! ? AppImages.doctor : AppImages.patient,
                    ),
                  ),
                  SpaceVertical(MediaQuery.sizeOf(context).height / 10),
                  Text(
                    AppStringKey.titleLogin.tr,
                    style: BaseStyle.textStyleDomineBold(
                      22,
                      AppColors.colorDarkBlue,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  const SpaceVertical(30),
                  CustomRoundTextField(
                    labelText: '',
                    labelVisible: false,
                    isRequireField: true,
                    textEditingController: controller.emailOrMobileController.value,
                    hintText: AppStringKey.hintEmailOrMobile.tr,
                    backgroundColor: AppColors.transparent,
                    validator: Validators.phoneNumberValidator,
                  ),
                  const SpaceVertical(24),
                  Wrap(
                    children: [
                      Text(
                        AppStringKey.labelTermsAndPrivacyStartPoint.tr,
                        style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorTextDarkGray),
                      ),
                      const SpaceHorizontal(3),
                      GestureDetector(
                        onTap: () {},
                        child: Text(
                          AppStringKey.labelTermsOfService.tr,
                          style: BaseStyle.underLineTextStyle,
                        ),
                      ),
                      const SpaceHorizontal(3),
                      Text(
                        AppStringKey.strAnd.tr,
                        style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorTextDarkGray),
                      ),
                      const SpaceHorizontal(3),
                      GestureDetector(
                        onTap: () {},
                        child: Text(
                          AppStringKey.labelPrivacyAndPolicy.tr,
                          style: BaseStyle.underLineTextStyle,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ).paddingAll(20),
        ),
      ),
    );
  }
}
