import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';

class LoginController extends BaseController {
  var emailOrMobileController = TextEditingController().obs;
  GlobalKey<FormState> form = GlobalKey();
  dynamic argumentData = Get.arguments;
  bool? isDoctor;
}
