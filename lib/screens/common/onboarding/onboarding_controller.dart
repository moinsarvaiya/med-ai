import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';

import '../../../utils/local_notification_service.dart';

class OnBoardingController extends BaseController {
  var pageIndex = 0.obs;
  PageController pageController = PageController(initialPage: 0);

  Future<void> notificationInitialization(BuildContext context) async {
    Future.wait([
      LocalNotificationService.initialize(context),
    ]);
  }

  @override
  void onInit() {
    super.onInit();
  }
}
