import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/common/onboarding/onboarding_controller.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/space_vertical.dart';

class OnBoardingScreen extends GetView<OnBoardingController> {
  const OnBoardingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<void>(
        future: controller.notificationInitialization(context),
        builder: (_, __) {
          return WillPopScope(
            onWillPop: () {
              if (controller.pageIndex.value == 0) {
                return Future.value(true);
              } else {
                var index = controller.pageIndex.value - 1;
                controller.pageController.jumpToPage(index);
                return Future.value(false);
              }
            },
            child: Scaffold(
              backgroundColor: AppColors.white,
              bottomNavigationBar: Container(
                padding: const EdgeInsets.all(16),
                child: SubmitButton(
                  title: AppStringKey.btnGetStarted.tr,
                  onClick: () {
                    if (controller.pageIndex.value == 2) {
                      LoginPreferences().sharedPrefWrite(StorageKeys.isOnBoarding, "1");
                      // print("onBoarding: " + AppPreferences().sharedPrefRead(StorageKeys.isOnBoarding).toString());
                      Get.toNamed(RoutePaths.SELECT_USER);
                    } else {
                      var index = controller.pageIndex.value + 1;
                      controller.pageController.jumpToPage(index);
                    }
                  },
                ),
              ),
              body: ScrollConfiguration(
                behavior: MyBehavior(),
                child: Stack(
                  children: [
                    PageView(
                      onPageChanged: (index) {
                        controller.pageIndex.value = index;
                      },
                      scrollDirection: Axis.horizontal,
                      controller: controller.pageController,
                      children: [
                        Pages(
                          title: AppStringKey.titleOnBoarding1.tr,
                          image: AppImages.onBoarding1,
                          content: AppStringKey.contentOnBoarding1.tr,
                        ),
                        Pages(
                          title: AppStringKey.titleOnBoarding2.tr,
                          image: AppImages.onBoarding2,
                          content: AppStringKey.contentOnBoarding2.tr,
                        ),
                        Pages(
                          title: AppStringKey.titleOnBoarding3.tr,
                          image: AppImages.onBoarding3,
                          content: AppStringKey.contentOnBoarding3.tr,
                        ),
                      ],
                    ),
                    Positioned(
                      left: 0,
                      right: 0,
                      bottom: MediaQuery.sizeOf(context).height / 5,
                      child: Obx(
                        () => PageIndicator(
                          pageIndex: controller.pageIndex.value,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}

class PageIndicator extends StatelessWidget {
  const PageIndicator({Key? key, this.pageIndex = 0}) : super(key: key);

  final int pageIndex;
  final double indicatorHeight = 5;
  final double indicatorWidth = 14;
  final double spacing = 4;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: indicatorHeight,
      child: Container(
        alignment: Alignment.center,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 3,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            final isActive = index == pageIndex;
            return Padding(
              padding: EdgeInsets.only(left: index != 0 ? spacing : 0),
              child: Container(
                width: indicatorWidth,
                color: isActive ? AppColors.colorDarkBlue : AppColors.colorLightSkyBlue,
              ),
            );
          },
        ),
      ),
    );
  }
}

class Pages extends StatelessWidget {
  const Pages({
    Key? key,
    required this.title,
    required this.image,
    required this.content,
  }) : super(key: key);

  final String title;
  final String content;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SpaceVertical(MediaQuery.sizeOf(context).height / 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            title,
            style: BaseStyle.textStyleDomineBold(
              22,
              AppColors.colorDarkBlue,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        SpaceVertical(MediaQuery.sizeOf(context).height / 12),
        Image.asset(
          image,
          height: MediaQuery.sizeOf(context).height / 3,
          fit: BoxFit.cover,
        ),
        SpaceVertical(MediaQuery.sizeOf(context).height / 5),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: Text(
            content,
            style: BaseStyle.textStyleNunitoSansMedium(
              16,
              AppColors.colorTextDarkGray,
            ),
            maxLines: 2,
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
