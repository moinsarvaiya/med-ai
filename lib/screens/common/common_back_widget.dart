import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constant/ui_constant.dart';
import '../../widgets/custom_appbar.dart';

class CommonBackWidget extends StatelessWidget {
  final Widget widget;

  const CommonBackWidget({
    Key? key,
    required this.widget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: widget,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          onClick: () {
            Get.back();
          },
        ),
      ),
    );
  }
}
