import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/space_vertical.dart';
import '../search_hospital_list/search_hospital_list_screen.dart';
import 'search_filter_service_controller.dart';

class SearchFilterServiceScreen extends GetView<SearchFilterServiceController> {
  const SearchFilterServiceScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: controller.service,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${AppStringKey.labelChooseServicesFor.tr} ${controller.service}.',
                style: BaseStyle.textStyleNunitoSansBold(
                  18,
                  AppColors.colorDarkBlue,
                ),
              ),
              const SpaceVertical(10),
              ItemAvailableServices(
                controller: controller,
              ),
              const SpaceVertical(25),
              Obx(
                () => Visibility(
                  visible: controller.listService.any((item) => item.isSelected == true),
                  child: ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: 5,
                    itemBuilder: (context, i) {
                      return const ItemHospitalSearch();
                    },
                  ),
                ),
              )
              /*SubmitButton(
                title: AppStringKey.btnSearch,
                onClick: () {
                  Get.toNamed(RoutePaths.SEARCH_HOSPITAL_LIST);
                },
              )*/
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemAvailableServices extends StatelessWidget {
  const ItemAvailableServices({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final SearchFilterServiceController controller;

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Container(
        width: double.infinity,
        padding: const EdgeInsets.only(
          left: 15,
          right: 15,
          top: 20,
          bottom: 10,
        ),
        child: Obx(
          () => Wrap(
            children: controller.listService
                .map(
                  (element) => GestureDetector(
                    onTap: () {
                      element.isSelected = !element.isSelected!;
                      controller.listService.refresh();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: 8,
                        horizontal: 15,
                      ),
                      decoration: BoxDecoration(
                        color: AppColors.colorBackground,
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            element.serviceName!,
                            style: BaseStyle.textStyleNunitoSansMedium(
                              14,
                              AppColors.colorDarkBlue,
                            ),
                          ),
                          Visibility(
                              visible: element.isSelected!,
                              child: Image.asset(
                                AppImages.checkCircle,
                                height: 16,
                                width: 16,
                              ).paddingOnly(left: 5)),
                        ],
                      ),
                    ).marginOnly(right: 10, bottom: 20),
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }
}
