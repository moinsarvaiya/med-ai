import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings_key.dart';

import '../../../../models/dr_available_service_model.dart';

class SearchFilterServiceController extends BaseController {
  RxList<DrAvailableServiceModel> listService = RxList();
  var service = '';

  @override
  void onInit() {
    super.onInit();
    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceCCU.tr,
        isSelected: false,
      ),
    );
    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceICU.tr,
        isSelected: false,
      ),
    );
    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceAmbulanceService.tr,
        isSelected: false,
      ),
    );
    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceDialysisCenter.tr,
        isSelected: false,
      ),
    );
    service = Get.arguments['service'];
  }
}
