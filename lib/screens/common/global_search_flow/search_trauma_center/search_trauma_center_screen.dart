import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_sub_item_search.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';
import '../search_hospital_list/search_hospital_list_screen.dart';
import 'search_trauma_center_controller.dart';

class SearchTraumaCenterScreen extends GetView<SearchTraumaCenterController> {
  const SearchTraumaCenterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: AppStringKey.strServiceTraumaCenter.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /*Text(
                AppStringKey.titleSearchHospital,
                style: BaseStyle.textStyleNunitoSansBold(18, AppColors.colorDarkBlue),
              ),*/
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: ItemSearchView(
                  onChange: (String val) {
                    controller.searchTraumaCenterController.text = val;
                  },
                  hintString: AppStringKey.labelSearchTraumaCenter.tr,
                ),
              ),
              const SpaceVertical(15),
              ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: controller.traumaCenterList.length,
                  itemBuilder: (context, i) {
                    return ItemTraumaCenterSearch(
                      index: i,
                      controller: controller,
                    );
                  }),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemTraumaCenterSearch extends StatelessWidget {
  final int index;
  final SearchTraumaCenterController controller;

  const ItemTraumaCenterSearch({
    Key? key,
    required this.index,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: GestureDetector(
        onTap: () {},
        /*splashColor: AppColors.colorDarkBlue.withOpacity(0.3),
        borderRadius: BorderRadius.circular(5),*/
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                    AppImages.traumaCenter,
                    height: 25,
                    width: 25,
                  ),
                  const SpaceHorizontal(5),
                  Expanded(
                    child: Text(
                      controller.traumaCenterList[index],
                      style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
                    ),
                  ),
                ],
              ),
              const SpaceVertical(20),
              SubItemSearch(
                title: AppStringKey.labelType.tr,
                value: AppStringKey.strServiceTraumaCenter.tr,
              ),
              const SubItemSearch(
                image: AppImages.call,
                imageVisible: true,
                value: '047484751785',
              ),
              const SubItemSearch(
                image: AppImages.email,
                imageVisible: true,
                value: 'sahidnagar@ugho.gob.bd',
              ),
            ],
          ),
        ),
      ),
    ).marginOnly(bottom: 5);
  }
}
