import 'package:flutter/cupertino.dart';
import 'package:med_ai/bindings/base_controller.dart';

class SearchTraumaCenterController extends BaseController {
  var traumaCenterList = [
    'Sahidnagar 20 bed Trauma Center',
    'Feni Trauma Center',
    'Sahidnagar rauma Center',
  ];
  var searchTraumaCenterController = TextEditingController();
}
