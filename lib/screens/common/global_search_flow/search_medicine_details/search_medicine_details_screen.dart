import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../search_medicine_list/search_medicine_list_screen.dart';
import 'search_medicine_details_controller.dart';

class SearchMedicineDetailsScreen extends GetView<SearchMedicineDetailsController> {
  const SearchMedicineDetailsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: AppStringKey.labelMedicineDetails.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: CustomCardWidget(
            shadowOpacity: shadow,
            widget: ScrollConfiguration(
              behavior: MyBehavior(),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SubItemMedicineSearch(
                      title: AppStringKey.labelBrand.tr,
                      value: 'Napa, 500mg',
                      color: AppColors.colorSkyBlue,
                    ),
                    SubItemMedicineSearch(
                      title: AppStringKey.labelGeneric.tr,
                      value: 'paracetamol',
                    ),
                    SubItemMedicineSearch(
                      title: AppStringKey.labelDesignAdministration.tr,
                      value: 'Tablet',
                    ),
                    const SpaceVertical(20),
                    Text(
                      '${controller.strDummyTextMedicineDetails}\n\n${controller.strDummyTextMedicineDetails} ',
                      style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorTextDarkGray),
                    ),
                    const SpaceVertical(15),
                    Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        width: BaseSize.width(25),
                        height: BaseSize.height(4.5),
                        child: SubmitButton(
                          title: AppStringKey.back.tr,
                          isBottomMargin: false,
                          onClick: () {
                            Get.back();
                          },
                        ),
                      ),
                    ),
                    const SpaceVertical(5),
                  ],
                ).paddingAll(10),
              ),
            ),
          ).marginAll(15),
        ),
      ),
    );
  }
}
