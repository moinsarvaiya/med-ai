import 'package:flutter/cupertino.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/ui_constant.dart';

import '../../../../models/dr_available_service_model.dart';

class GlobalSearchController extends BaseController {
  final TextEditingController searchEditingController = TextEditingController();
  final FocusNode searchFocusNode = FocusNode();
  GlobalKey<TextFieldAutoCompleteState<String>> searchFieldAutoCompleteKey = GlobalKey();
  RxList<DrAvailableServiceModel> listService = RxList();
  var isMedicineSelected = false.obs;

  final List<String> searchOptions = <String>[
    'Paracetamol tablet-1',
    'Paracetamol tablet-2',
    'Paracetamol tablet-3',
    'Paracetamol tablet-4',
    'Paracetamol tablet-5',
    'Paracetamol tablet-6',
  ];

  @override
  void onInit() {
    super.onInit();

    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceAmbulance.tr,
        isSelected: false,
        enums: Services.AMBULANCE,
      ),
    );
    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceBloodBank.tr,
        isSelected: false,
        enums: Services.BLOOD_BANK,
      ),
    );
    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceDiagnosticCenter.tr,
        isSelected: false,
        enums: Services.DIAGNOSTIC_CENTER,
      ),
    );
    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceGeneralHospital.tr,
        isSelected: false,
        enums: Services.GENERAL_HOSPITAL,
      ),
    );
    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceMedicine.tr,
        isSelected: false,
        enums: Services.MEDICINE,
      ),
    );
    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceSpecializedClinic.tr,
        isSelected: false,
        enums: Services.SPECIALIZED_CLINIC,
      ),
    );
    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceSpecializedHospital.tr,
        isSelected: false,
        enums: Services.SPECIALIZED_HOSPITAL,
      ),
    );
    listService.add(
      DrAvailableServiceModel(
        serviceName: AppStringKey.strServiceTraumaCenter.tr,
        isSelected: false,
        enums: Services.TRAUMA_CENTER,
      ),
    );
  }
}
