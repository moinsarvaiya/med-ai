import 'package:flutter/material.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../widgets/space_vertical.dart';
import 'global_search_controller.dart';

class GlobalSearchScreen extends GetView<GlobalSearchController> {
  const GlobalSearchScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Utilities.removeFocus();
      },
      child: Scaffold(
        backgroundColor: AppColors.colorGrayBackground,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(defaultAppBarHeight),
          child: CustomAppBar(
            isDividerVisible: true,
            onClick: () {
              Get.back();
            },
          ),
        ),
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ItemSearchView(
                  searchController: controller,
                ),
                const SpaceVertical(15),
                Text(
                  AppStringKey.labelServiceNeed.tr,
                  style: BaseStyle.textStyleNunitoSansBold(18, AppColors.colorDarkBlue),
                ),
                const SpaceVertical(10),
                ItemAvailableServices(
                  controller: controller,
                )
              ],
            ).paddingAll(20),
          ),
        ),
      ),
    );
  }
}

class ItemSearchView extends StatelessWidget {
  const ItemSearchView({
    Key? key,
    required this.searchController,
  }) : super(key: key);

  final GlobalSearchController searchController;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextFieldAutoComplete(
          decoration: InputDecoration(
            prefixIcon: const Icon(
              Icons.search,
              color: AppColors.colorGreen,
              size: 20,
            ),
            suffixIcon: GestureDetector(
                onTap: () {
                  searchController.searchEditingController.clear();
                },
                child: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.close,
                    size: 18,
                  ),
                )),
            hintText: AppStringKey.labelSearchGlobal.tr,
            fillColor: AppColors.transparent,
            hintStyle: BaseStyle.textStyleNunitoSansRegular(
              12,
              AppColors.colorDarkBlue.withOpacity(0.5),
            ),
          ),
          focusNode: searchController.searchFocusNode,
          clearOnSubmit: false,
          style: BaseStyle.textStyleNunitoSansSemiBold(
            12,
            AppColors.colorDarkBlue,
          ),
          controller: searchController.searchEditingController,
          itemSubmitted: (String item) {
            searchController.searchEditingController.text = item;
            Get.toNamed(RoutePaths.SEARCH_MEDICINE_LIST);
          },
          key: searchController.searchFieldAutoCompleteKey,
          suggestions: searchController.searchOptions,
          itemBuilder: (context, String item) {
            return Container(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 10),
              child: Row(
                children: [
                  Text(
                    item,
                    style: BaseStyle.textStyleNunitoSansSemiBold(
                      12,
                      AppColors.colorDarkBlue,
                    ),
                  )
                ],
              ),
            );
          },
          itemSorter: (String a, String b) {
            return a.compareTo(b);
          },
          itemFilter: (String item, query) {
            if (query.length > 2) {
              return item.toLowerCase().startsWith(query.toLowerCase());
            } else {
              return false;
            }
          }),
    );
  }
}

class ItemAvailableServices extends StatelessWidget {
  const ItemAvailableServices({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final GlobalSearchController controller;

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: Container(
        width: double.infinity,
        padding: const EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 10),
        child: Obx(
          () => Wrap(
            children: controller.listService
                .map(
                  (element) => GestureDetector(
                    onTap: () {
                      if (element.enums == Services.SPECIALIZED_HOSPITAL ||
                          element.enums == Services.SPECIALIZED_CLINIC ||
                          element.enums == Services.GENERAL_HOSPITAL) {
                        Get.toNamed(
                          RoutePaths.SEARCH_FILTER_SERVICE,
                          arguments: {'service': element.serviceName},
                        );
                      } else if (element.enums == Services.AMBULANCE) {
                        Get.toNamed(RoutePaths.SEARCH_HOSPITAL_LIST);
                      } else if (element.enums == Services.BLOOD_BANK) {
                        Get.toNamed(RoutePaths.SEARCH_BLOOD_BANK);
                      } else if (element.enums == Services.DIAGNOSTIC_CENTER) {
                        Get.toNamed(RoutePaths.SEARCH_DIAGNOSTIC_CENTER);
                      } else if (element.enums == Services.TRAUMA_CENTER) {
                        Get.toNamed(RoutePaths.SEARCH_TRAUMA_CENTER);
                      } else {
                        controller.isMedicineSelected.value = !controller.isMedicineSelected.value;
                        controller.listService.refresh();
                        if (controller.isMedicineSelected.value) {
                          controller.searchFocusNode.requestFocus();
                        } else {
                          Utilities.removeFocus();
                        }
                      }
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                      decoration: BoxDecoration(
                        color: AppColors.colorBackground,
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            element.serviceName!,
                            style: BaseStyle.textStyleNunitoSansMedium(14, AppColors.colorDarkBlue),
                          ),
                          Visibility(
                              visible: controller.isMedicineSelected.value && element.enums == Services.MEDICINE,
                              child: const Icon(
                                Icons.check_circle,
                                color: AppColors.colorDarkBlue,
                                size: 20,
                              ).paddingOnly(left: 5)),
                        ],
                      ),
                    ).marginOnly(right: 10, bottom: 20),
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }
}
