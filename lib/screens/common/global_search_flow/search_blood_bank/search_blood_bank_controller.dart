import 'package:flutter/cupertino.dart';
import 'package:med_ai/bindings/base_controller.dart';

class SearchBloodBankController extends BaseController {
  var bloodBankList = [
    'Landsteiner Blood Bank & Transfusion Medicine Center',
    'Enam Medical Hospital (Pvt.) Ltd.',
    'Japan East West Medical Collage Hospital',
    'Imperial Hospital Limited',
  ];
  var searchBloodBankController = TextEditingController();
}
