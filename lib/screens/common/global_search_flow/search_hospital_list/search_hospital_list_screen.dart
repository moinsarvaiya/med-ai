import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/space_horizontal.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_sub_item_search.dart';
import '../../../../widgets/space_vertical.dart';
import 'search_hospital_list_controller.dart';

class SearchHospitalListScreen extends GetView<SearchHospitalListController> {
  const SearchHospitalListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: AppStringKey.strServiceAmbulance.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /*Text(
                AppStringKey.titleSearchHospital,
                style: BaseStyle.textStyleNunitoSansBold(18, AppColors.colorDarkBlue),
              ),*/
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: ItemSearchView(
                  onChange: (String val) {
                    controller.searchHospitalController.text = val;
                  },
                  hintString: AppStringKey.labelSearchHospital.tr,
                ),
              ),
              const SpaceVertical(25),
              ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: 5,
                  itemBuilder: (context, i) {
                    return const ItemHospitalSearch();
                  }),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemSearchView extends StatelessWidget {
  const ItemSearchView({
    Key? key,
    required this.onChange,
    required this.hintString,
  }) : super(key: key);

  final Function(String val) onChange;
  final String hintString;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextField(
        decoration: InputDecoration(
          prefixIcon: const Icon(
            Icons.search,
            color: AppColors.colorGreen,
            size: 20,
          ),
          hintText: hintString,
          fillColor: AppColors.transparent,
          hintStyle: BaseStyle.textStyleNunitoSansRegular(
            12,
            AppColors.colorDarkBlue.withOpacity(0.5),
          ),
        ),
        style: BaseStyle.textStyleNunitoSansRegular(
          12,
          AppColors.colorDarkBlue,
        ),
        onChanged: (val) {
          onChange(val);
        },
      ),
    );
  }
}

class ItemHospitalSearch extends StatelessWidget {
  const ItemHospitalSearch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: GestureDetector(
        onTap: () {},
        /*splashColor: AppColors.colorDarkBlue.withOpacity(0.3),
        borderRadius: BorderRadius.circular(5),*/
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                    AppImages.building,
                    height: 25,
                    width: 25,
                  ),
                  const SpaceHorizontal(5),
                  Expanded(
                    child: Text(
                      'National Institute of Diseases of the Chest & Hospital (NIDCH)',
                      style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
                    ),
                  ),
                ],
              ),
              const SpaceVertical(20),
              // const SubItemSearch(
              //   image: AppImages.location,
              //   imageVisible: true,
              //   value: 'Plot 15 Rd No 71, Dhaka 1212, Bangladesh ',
              // ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    AppImages.location,
                    height: 18,
                    width: 18,
                    color: AppColors.colorDarkBlue,
                  ),
                  const SpaceHorizontal(5),
                  Expanded(
                    child: Text(
                      'Plot 15 Rd No 71, Dhaka 1212, Bangladesh ',
                      style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
                    ),
                  ),
                ],
              ).marginOnly(bottom: 10),
              SubItemSearch(
                title: AppStringKey.labelType.tr,
                value: 'Specialized Hospital',
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2.5,
                    child: SubItemSearch(
                      title: AppStringKey.strServiceICU.tr,
                      value: '8',
                    ),
                  ),
                  Expanded(
                    child: SubItemSearch(
                      title: AppStringKey.strServiceCCU.tr,
                      value: '8',
                    ),
                  ),
                ],
              ),
              SubItemSearch(
                title: AppStringKey.strServiceDialysis.tr,
                value: 'Available',
              ),
              SubItemSearch(
                title: AppStringKey.strServiceAmbulance.tr,
                value: 'Available',
              ),
              SubItemSearch(
                image: AppImages.call,
                imageVisible: true,
                title: AppStringKey.labelContact.tr,
                value: '55067146, 0171564489',
              ),
              SubItemSearch(
                image: AppImages.email,
                imageVisible: true,
                title: AppStringKey.labelEmail.tr,
                value: 'nidch@hopi.dghs.gov.bd',
              ),
              const SpaceVertical(5),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      // controller.launchInBrowser(23.0225, 72.5714);
                    },
                    child: Container(
                      width: MediaQuery.sizeOf(context).width / 1.8,
                      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                      decoration: const BoxDecoration(
                        color: AppColors.colorDarkBlue,
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            AppImages.googleMap,
                            height: 24,
                            width: 24,
                          ),
                          const SpaceHorizontal(10),
                          Expanded(
                            child: Text(
                              'Open in Google Map',
                              style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.white),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ).marginOnly(bottom: 5);
  }
}
