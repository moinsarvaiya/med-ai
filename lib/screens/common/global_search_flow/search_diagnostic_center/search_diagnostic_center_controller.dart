import 'package:flutter/material.dart';
import 'package:med_ai/bindings/base_controller.dart';

class SearchDiagnosticCenterController extends BaseController {
  var diagnosticCenterList = [
    'Kidney care and Diagnostic Center',
    '1 Digital Diagnostic Center',
    'Meghna Lab & Diagnostic Center',
    'Subha Diagnostic Center',
  ];
  var searchDiagnosticCenterController = TextEditingController();
}
