import 'dart:convert';
import 'dart:developer' as developer;

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api/api_endpoints.dart';
import 'package:med_ai/api/api_interface/api_interface.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings.dart';
import 'package:med_ai/models/single_drug_type_model.dart';

class SearchMedicineListController extends BaseController {
  final String tag = "AddNewMedVisit";
  RxList<SingleDrugTypeListModel> medicineList = <SingleDrugTypeListModel>[].obs;
  var api = ApiCallObject();

  var previousRoute = Get.previousRoute.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    setData();
    print("Type of Previoust route: ${previousRoute.runtimeType}");
  }

  void selectMedicine() {}

  Future<void> getDrugListFromApi(String searchBy) async {
    http.Response response;

    Object requestBody = {"search_by": searchBy, "search_type": "brand"};

    try {
      response = await api.postData(ApiEndpoints.urlGetSingleDrugTypeList, requestBody);
      if (response.statusCode == 200) {
        medicineList.value = [];
        var responseBody = jsonDecode(response.body);
        var medicineListFromApi = responseBody['data']['list'];
        //developer.log("DataDetails: $medicineListFromApi",name: tag);
        for (var list in medicineListFromApi) {
          //developer.log("DataDetails: $list",name: tag);
          medicineList.add(SingleDrugTypeListModel.fromJson(list));
        }
      }
      developer.log("MedicineCount: ${medicineList.length}");
    } catch (e) {
      developer.log('SetDataException: $e', name: tag);
    }
  }

  void setData() {
    try {
      var receivedIntent = jsonDecode(Get.arguments[AppStrings.forwardArgument]);
      String selectedName = receivedIntent['selectedName'];
      getDrugListFromApi(selectedName);
    } catch (e) {
      developer.log("SetDataException: $e", name: tag);
    }
  }
}
