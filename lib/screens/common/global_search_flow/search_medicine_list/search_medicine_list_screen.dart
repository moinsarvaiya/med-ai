import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../models/single_drug_type_model.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/space_vertical.dart';
import '../../../doctor/dr_appointments/dr_add_new_medicine/dr_add_new_medicine_controller.dart';
import 'search_medicine_list_controller.dart';

class SearchMedicineListScreen extends GetView<SearchMedicineListController> {
  const SearchMedicineListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          titleName: AppStringKey.strServiceMedicine.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStringKey.titleSearchMedicine.tr,
                style: BaseStyle.textStyleNunitoSansBold(18, AppColors.colorDarkBlue),
              ),
              const SpaceVertical(15),
              Obx(
                () => ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: controller.medicineList.length,
                    itemBuilder: (context, i) {
                      return ItemMedicineSearch(
                        controller: controller,
                        index: i,
                        medicineList: controller.medicineList,
                      );
                    }),
              ),
            ],
          ).paddingAll(20),
        ),
      ),
    );
  }
}

class ItemMedicineSearch extends StatelessWidget {
  final SearchMedicineListController controller;
  final int index;
  final RxList<SingleDrugTypeListModel> medicineList;

  const ItemMedicineSearch({
    Key? key,
    required this.controller,
    required this.index,
    required this.medicineList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      widget: InkWell(
        onTap: () {
          print(controller.previousRoute.toString());
          if (controller.previousRoute.toString() == "/drAddNewMedicine") {
            String brandName = '${medicineList[index].brandName}, ${medicineList[index].strength}';
            print("Selected MedicineName: $brandName}");
            Get.find<DrAddNewMedicineController>().setDataToBrandName(brandName);
            Get.back();
          } else {
            Get.toNamed(RoutePaths.SEARCH_MEDICINE_DETAILS);
          }
        },
        splashColor: AppColors.colorDarkBlue.withOpacity(0.3),
        borderRadius: BorderRadius.circular(5),
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              SubItemMedicineSearch(
                title: AppStringKey.labelBrand.tr,
                value: '${medicineList[index].brandName}, ${medicineList[index].strength}',
                color: AppColors.colorSkyBlue,
              ),
              SubItemMedicineSearch(
                title: AppStringKey.labelGeneric.tr,
                value: '${medicineList[index].genericName}',
              ),
              SubItemMedicineSearch(
                title: AppStringKey.labelCompany.tr,
                value: '${medicineList[index].company}',
              ),
            ],
          ),
        ),
      ),
    ).marginOnly(bottom: 5);
  }
}

class SubItemMedicineSearch extends StatelessWidget {
  final String title;
  final String value;
  final Color color;

  const SubItemMedicineSearch({Key? key, required this.title, this.color = AppColors.colorDarkBlue, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          '$title : ',
          style: BaseStyle.textStyleNunitoSansBold(14, color),
        ),
        Text(
          value,
          style: BaseStyle.textStyleNunitoSansBold(14, color),
        ),
      ],
    ).marginOnly(bottom: 10);
  }
}
