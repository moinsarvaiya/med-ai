import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/screens/common/no_internet/no_internet_controller.dart';

import '../../../constant/app_colors.dart';
import '../../../constant/app_images.dart';
import '../../../constant/app_strings_key.dart';
import '../../../constant/base_style.dart';
import '../../../widgets/custom_buttons.dart';
import '../../../widgets/space_vertical.dart';

class NoInternetScreen extends GetView<NoInternetController> {
  const NoInternetScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              AppImages.noInternet,
              height: 120,
              width: 120,
            ),
            const SpaceVertical(20),
            Text(
              AppStringKey.labelNoInternetConnection.tr,
              style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
              textAlign: TextAlign.center,
            ),
            const SpaceVertical(20),
            Text(
              AppStringKey.labelCheckInternet.tr,
              style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorDarkBlue),
              textAlign: TextAlign.center,
            ),
            const SpaceVertical(50),
            SubmitButton(
              title: AppStringKey.labelTryAgain.tr,
              onClick: () {},
              titleFontSize: 14,
            ),
          ],
        ).paddingAll(20),
      ),
    );
  }
}
