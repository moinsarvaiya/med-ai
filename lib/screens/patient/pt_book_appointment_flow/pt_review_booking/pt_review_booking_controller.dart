import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/api_patient/patient_api_end_point.dart';
import 'package:med_ai/api_patient/patient_api_interface.dart';
import 'package:med_ai/api_patient/patient_api_presenter.dart';
import 'package:med_ai/api_patient/patient_response_key.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_doctor_time_slot_selection/pt_doctor_time_slot_selection_controller.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';
import 'package:med_ai/utils/app_preference/patient_preferences.dart';
import 'package:med_ai/utils/utilities.dart';

import '../../../../constant/app_strings_key.dart';
import '../../../../routes/route_paths.dart';

class PtReviewBookingController extends BaseController implements PatientApiCallBacks {
  var searchPromoCodeController = TextEditingController();
  var checkBoxSelected = false.obs;
  RxList<String> selectedPromoCode = RxList();
  RxBool isPromoCodeApply = false.obs;
  String appointmentId = '';

  String maximumDiscount = '';
  String discountType = '';
  double discountAmount = 0;

  @override
  void onInit() {
    super.onInit();
  }

  void applyPromoCode() {
    if (searchPromoCodeController.text.isNotEmpty) {
      PatientApiPresenter(this).applyPromoCode(
        searchPromoCodeController.text.toUpperCase(),
        LoginPreferences().sharedPrefRead(StorageKeys.personId),
        PatientPreferences().sharedPrefRead(StorageKeys.doctorId),
      );
    } else {
      AppStringKey.strPleaseEnterPromoCode.showErrorSnackBar(title: AppStringKey.error);
    }
  }

  void bookAppointment() {
    PatientApiPresenter(this).bookAppointment(
      PatientPreferences().sharedPrefRead(StorageKeys.doctorId),
      PatientPreferences().sharedPrefRead(StorageKeys.vid),
      'visit',
      Utilities.availableDayFormat_yyyyMMdd(PatientPreferences().sharedPrefRead(StorageKeys.availableDaySlot)),
      PatientPreferences().sharedPrefRead(StorageKeys.availableTimeSlot),
      PatientPreferences().sharedPrefRead(StorageKeys.patientMobile),
      Get.find<PtDoctorTimeSlotSelectionController>().callingMethodList.firstWhere((method) => method.isSelected!).methodName ?? '',
      'Yes',
      PatientPreferences().sharedPrefRead(StorageKeys.visitFee),
    );
  }

  void createPayment() {
    PatientApiPresenter(this).createPayment(
      appointmentId,
      PatientPreferences().sharedPrefRead(StorageKeys.visitFee),
      'AABBCCDDEE',
      'AABBCCDDEE',
      'BDT',
      searchPromoCodeController.text.toUpperCase(),
      calculateDiscountAmount().toString(),
    );
  }

  void updatePromoCode() {
    PatientApiPresenter(this).updatePromoCode(
      searchPromoCodeController.text.toUpperCase(),
      PatientPreferences().sharedPrefRead(StorageKeys.profileSelectionPersonId),
    );
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.checkValidity:
        if (object[PatientResponseKey.doctorCode] == '200') {
          selectedPromoCode.add(searchPromoCodeController.text.toUpperCase());
          selectedPromoCode.refresh();
          maximumDiscount = object[PatientResponseKey.maximumDiscount];
          discountType = object[PatientResponseKey.discountType];
          discountAmount = double.parse(object[PatientResponseKey.discountAmount]);
          isPromoCodeApply.value = true;
        }
        break;
      case PatientApiEndPoints.bookAppointment:
        appointmentId = object[PatientResponseKey.appointmentId];
        createPayment();
        break;
      case PatientApiEndPoints.createPayment:
        if (isPromoCodeApply.value) {
          updatePromoCode();
        } else {
          Get.toNamed(RoutePaths.PT_THANK_YOU_BOOKING);
          if (object.containsKey(PatientResponseKey.successMsg)) {
            object[PatientResponseKey.successMsg].toString().showSuccessSnackBar(title: AppStringKey.success);
          }
        }
        break;
      case PatientApiEndPoints.updatePromoUsage:
        Get.toNamed(RoutePaths.PT_THANK_YOU_BOOKING);
        break;
    }
  }

  double calculateDiscountAmount() {
    if (discountType == 'percentage') {
      return (discountAmount / 100) * int.parse(PatientPreferences().sharedPrefRead(StorageKeys.visitFee));
    } else {
      return int.parse(PatientPreferences().sharedPrefRead(StorageKeys.visitFee)) - discountAmount;
    }
  }
}
