import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_doctor_time_slot_selection/pt_doctor_time_slot_selection_controller.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/app_preference/patient_preferences.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_round_text_field.dart';
import 'pt_review_booking_controller.dart';

class PtReviewBookingScreen extends GetView<PtReviewBookingController> {
  const PtReviewBookingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          titleName: AppStringKey.titleReviewBooking.tr,
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(
          bottom: MediaQuery.viewInsetsOf(context).bottom + 16,
          top: 16,
          left: 20,
          right: 20,
        ),
        child: Obx(
          () => SubmitButton(
            title: AppStringKey.strPayBook.tr,
            backgroundColor: !controller.checkBoxSelected.value /*controller.privacyList.any((element) => element.selected! == false)*/
                ? AppColors.colorGrayBackground2
                : AppColors.colorDarkBlue,
            borderColor: Colors.transparent,
            onClick: !controller.checkBoxSelected.value
                ? null
                : () {
                    controller.bookAppointment();
                  },
          ),
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                PatientProfileSection(controller: controller),
                const SpaceVertical(15),
                const DoctorProfileSection(),
                const SpaceVertical(20),
                PromoCodeSection(controller: controller),
                const SpaceVertical(20),
                FreeDetailSection(controller: controller),
                const SpaceVertical(20),
                PrivacySection(controller: controller)
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PrivacySection extends StatelessWidget {
  final PtReviewBookingController controller;

  const PrivacySection({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCheckBoxWithRichWidget(
      controller: controller,
      index: 0,
      callback: () {
        controller.checkBoxSelected.value = !controller.checkBoxSelected.value;
      },
    );
  }
}

class CustomCheckBoxWithRichWidget extends StatelessWidget {
  final int index;
  final PtReviewBookingController controller;
  final Function()? callback;

  const CustomCheckBoxWithRichWidget({
    Key? key,
    required this.index,
    required this.controller,
    this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Obx(
            () => Container(
              width: 24.0,
              height: 24.0,
              decoration: BoxDecoration(
                color: controller.checkBoxSelected.value ? AppColors.colorDarkBlue : Colors.transparent,
                border: Border.all(
                  color: AppColors.colorDarkBlue,
                  width: 1.0,
                ),
              ),
              child: Icon(
                Icons.check,
                size: 16.0,
                color: controller.checkBoxSelected.value ? AppColors.colorGreen : Colors.white,
              ),
            ),
          ),
          const SpaceHorizontal(10),
          Expanded(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: AppStringKey.strIHaveRead.tr,
                    style: BaseStyle.textStyleNunitoSansRegular(
                      14,
                      AppColors.colorDarkBlue.withOpacity(0.5),
                    ),
                  ),
                  const WidgetSpan(
                    child: SpaceHorizontal(5),
                  ),
                  TextSpan(
                    recognizer: TapGestureRecognizer()..onTap = () {},
                    text: AppStringKey.strRefundPolicy.tr,
                    style: BaseStyle.textStyleNunitoSansRegular(
                      14,
                      AppColors.colorSkyBlue,
                    ),
                  ),
                  TextSpan(
                    recognizer: TapGestureRecognizer()..onTap = () {},
                    text: ', ${AppStringKey.strTermsCondition.tr}',
                    style: BaseStyle.textStyleNunitoSansRegular(
                      14,
                      AppColors.colorSkyBlue,
                    ),
                  ),
                  TextSpan(
                    recognizer: TapGestureRecognizer()..onTap = () {},
                    text: ', ${AppStringKey.strPrivacyPolicy.tr}',
                    style: BaseStyle.textStyleNunitoSansRegular(
                      14,
                      AppColors.colorSkyBlue,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FreeDetailSection extends StatelessWidget {
  final PtReviewBookingController controller;

  const FreeDetailSection({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: AppColors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 1,
            blurRadius: 10,
            offset: const Offset(0, 5), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: double.maxFinite,
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 7,
            ),
            decoration: const BoxDecoration(
              color: AppColors.colorBackground,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              ),
            ),
            child: Text(
              AppStringKey.strFeeDetails.tr,
              style: BaseStyle.textStyleNunitoSansBold(
                12,
                AppColors.colorDarkBlue,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(17),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      AppStringKey.strAppointmentFee.tr,
                      style: BaseStyle.textStyleNunitoSansBold(
                        12,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    Text(
                      PatientPreferences().sharedPrefRead(StorageKeys.visitFee),
                      style: BaseStyle.textStyleNunitoSansBold(
                        12,
                        AppColors.colorDarkBlue,
                      ),
                    )
                  ],
                ),
                const SpaceVertical(10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      AppStringKey.strDiscountAmount.tr,
                      style: BaseStyle.textStyleNunitoSansBold(
                        12,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    Obx(
                      () => Text(
                        controller.isPromoCodeApply.value ? '${controller.discountAmount}' : '00',
                        style: BaseStyle.textStyleNunitoSansBold(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    )
                  ],
                ),
                const SpaceVertical(10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      AppStringKey.strFinalAmount.tr,
                      style: BaseStyle.textStyleNunitoSansBold(
                        12,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    Obx(
                      () => Text(
                        controller.isPromoCodeApply.value
                            ? controller.calculateDiscountAmount().toString()
                            : PatientPreferences().sharedPrefRead(StorageKeys.visitFee),
                        style: BaseStyle.textStyleNunitoSansBold(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class PromoCodeSection extends StatelessWidget {
  final PtReviewBookingController controller;

  const PromoCodeSection({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: AppColors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 1,
            blurRadius: 10,
            offset: const Offset(0, 5), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: double.maxFinite,
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 7,
            ),
            decoration: const BoxDecoration(
              color: AppColors.colorBackground,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              ),
            ),
            child: Text(
              AppStringKey.strPromoCode.tr,
              style: BaseStyle.textStyleNunitoSansBold(
                12,
                AppColors.colorDarkBlue,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 15,
              right: 15,
              top: 17,
              bottom: 10,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Obx(() => Visibility(
                            visible: controller.selectedPromoCode.isEmpty,
                            child: CustomRoundTextField(
                              labelText: '',
                              hintText: AppStringKey.hintEnterPromoCode.tr,
                              labelVisible: false,
                              isRequireField: false,
                              textEditingController: controller.searchPromoCodeController,
                            ),
                          )),
                      Obx(
                        () => Visibility(
                          visible: controller.selectedPromoCode.isNotEmpty,
                          child: Container(
                            width: double.maxFinite,
                            height: 50,
                            decoration: BoxDecoration(
                              color: AppColors.white,
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(
                                color: AppColors.colorLightSkyBlue,
                                width: 1,
                              ),
                            ),
                            child: GestureDetector(
                              onTap: () {
                                controller.selectedPromoCode.remove(controller.selectedPromoCode[0]);
                                controller.selectedPromoCode.refresh();
                                controller.isPromoCodeApply.value = false;
                                controller.searchPromoCodeController.clear();
                              },
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: FittedBox(
                                  child: Container(
                                    height: 50,
                                    margin: const EdgeInsets.all(3),
                                    padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                                    decoration: BoxDecoration(
                                      color: AppColors.colorBackground,
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text(
                                          controller.selectedPromoCode.isNotEmpty ? controller.selectedPromoCode[0] : '',
                                          style: BaseStyle.textStyleNunitoSansMedium(
                                            14,
                                            AppColors.colorDarkBlue,
                                          ),
                                        ),
                                        const Icon(
                                          Icons.cancel,
                                          color: Colors.black,
                                          size: 16,
                                        ).paddingOnly(left: 5)
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SpaceHorizontal(10),
                Obx(
                  () => SizedBox(
                    width: 80,
                    child: SubmitButton(
                      titleColor: controller.selectedPromoCode.isEmpty ? AppColors.white : AppColors.black.withOpacity(0.5),
                      borderColor: controller.selectedPromoCode.isEmpty ? AppColors.colorDarkBlue : AppColors.colorLightSkyBlue,
                      backgroundColor: controller.selectedPromoCode.isEmpty ? AppColors.colorDarkBlue : AppColors.colorLightSkyBlue,
                      title: AppStringKey.strApply.tr,
                      isBottomMargin: false,
                      onClick: controller.selectedPromoCode.isEmpty
                          ? () {
                              controller.applyPromoCode();
                            }
                          : null,
                    ),
                  ),
                )
              ],
            ),
          ),
          // Obx(
          //   () => Visibility(
          //     visible: controller.selectedPromoCode.isEmpty,
          //     child: Padding(
          //       padding: const EdgeInsets.symmetric(horizontal: 15),
          //       child: Wrap(
          //         children: controller.promoCodeList
          //             .map(
          //               (element) => GestureDetector(
          //                 onTap: () {
          //                   controller.selectedPromoCode.add(element);
          //                   controller.selectedPromoCode.refresh();
          //                 },
          //                 child: Container(
          //                   padding: const EdgeInsets.symmetric(
          //                     vertical: 8,
          //                     horizontal: 15,
          //                   ),
          //                   decoration: BoxDecoration(
          //                     color: AppColors.colorBackground,
          //                     borderRadius: BorderRadius.circular(30),
          //                   ),
          //                   child: Row(
          //                     mainAxisSize: MainAxisSize.min,
          //                     crossAxisAlignment: CrossAxisAlignment.center,
          //                     children: [
          //                       Text(
          //                         element,
          //                         style: BaseStyle.textStyleNunitoSansMedium(
          //                           14,
          //                           AppColors.colorDarkBlue,
          //                         ),
          //                       ),
          //                       const Icon(
          //                         Icons.cancel,
          //                         color: Colors.black,
          //                         size: 16,
          //                       ).paddingOnly(left: 5)
          //                     ],
          //                   ),
          //                 ).marginOnly(right: 5, bottom: 10),
          //               ),
          //             )
          //             .toList(),
          //       ),
          //     ),
          //   ),
          // ),
          Obx(
            () => Padding(
              padding: const EdgeInsets.only(left: 17, bottom: 20),
              child: Visibility(
                visible: controller.selectedPromoCode.isNotEmpty,
                child: Text(
                  AppStringKey.strPromoCodeApplySuccess.tr,
                  style: BaseStyle.textStyleNunitoSansRegular(
                    12,
                    AppColors.colorDarkGreen,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class DoctorProfileSection extends StatelessWidget {
  const DoctorProfileSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: AppColors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 1,
            blurRadius: 10,
            offset: const Offset(0, 5), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                height: 60,
                width: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(
                      PatientPreferences().sharedPrefRead(StorageKeys.doctorSelectionProfileImage),
                    ),
                  ),
                ),
              ),
              const SpaceHorizontal(10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          PatientPreferences().sharedPrefRead(StorageKeys.doctorSelectionName),
                          style: BaseStyle.textStyleNunitoSansBold(
                            16,
                            AppColors.colorDarkBlue,
                          ),
                        ),
                        Utilities.userVerifiedWidget()
                      ],
                    ),
                    Text(
                      PatientPreferences().sharedPrefRead(StorageKeys.doctorSelectionQualification),
                      style: BaseStyle.textStyleNunitoSansRegular(
                        12,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    Text(
                      PatientPreferences().sharedPrefRead(StorageKeys.doctorSelectionSpecialization),
                      style: BaseStyle.textStyleNunitoSansRegular(
                        10,
                        AppColors.colorDarkBlue,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          const SpaceVertical(15),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.maxFinite,
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                decoration: const BoxDecoration(
                  color: AppColors.colorGrayBackground2,
                ),
                child: Text(
                  AppStringKey.strBookingTime.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    12,
                    AppColors.colorDarkBlue,
                  ),
                ),
              ),
              const SpaceVertical(5),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  Utilities.reviewScreenDateFormat_ddMMMyyyy(PatientPreferences().sharedPrefRead(StorageKeys.availableDaySlot)),
                  style: BaseStyle.textStyleNunitoSansBold(
                    14,
                    AppColors.colorDarkBlue,
                  ),
                ),
              ),
              const SpaceVertical(5),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  children: [
                    Text(
                      PatientPreferences().sharedPrefRead(StorageKeys.availableTimeSlot),
                      style: BaseStyle.textStyleNunitoSansBold(
                        14,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                    const SpaceHorizontal(5),
                    Text(
                      '(${Utilities.getDifferenceTime(PatientPreferences().sharedPrefRead(StorageKeys.availableTimeSlot))})',
                      style: BaseStyle.textStyleNunitoSansRegular(
                        14,
                        AppColors.colorDarkBlue,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SpaceVertical(15),
          Container(
            width: double.maxFinite,
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
            decoration: const BoxDecoration(
              color: AppColors.colorGrayBackground2,
            ),
            child: Text(
              AppStringKey.strCallingMethodHeading.tr,
              style: BaseStyle.textStyleNunitoSansBold(
                12,
                AppColors.colorDarkBlue,
              ),
            ),
          ),
          const SpaceVertical(15),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
            ),
            child: Row(
              children: [
                Text(
                  setCallingMethod(Get.find<PtDoctorTimeSlotSelectionController>()
                      .callingMethodList
                      .firstWhere((method) => method.isSelected!)
                      .methodName!),
                  style: BaseStyle.textStyleNunitoSansRegular(
                    14,
                    AppColors.colorDarkBlue,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  String setCallingMethod(String method) {
    if (method.contains('In App Video')) {
      return AppStringKey.strVideoCallTxt.tr;
    } else if (method.contains('In App Voice')) {
      return AppStringKey.strVoiceCallTxt.tr;
    } else {
      return '${AppStringKey.strDirectCall.tr} : ${PatientPreferences().sharedPrefRead(StorageKeys.patientMobile)}';
    }
  }
}

class PatientProfileSection extends StatelessWidget {
  final PtReviewBookingController controller;

  const PatientProfileSection({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: AppColors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 1,
            blurRadius: 10,
            offset: const Offset(0, 5), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        children: [
          Container(
            height: 44,
            width: 44,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage(
                  PatientPreferences().sharedPrefRead(StorageKeys.profileSelectionProfileImg),
                ),
              ),
            ),
          ),
          const SpaceHorizontal(10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                PatientPreferences().sharedPrefRead(StorageKeys.profileSelectionName) ?? '',
                style: BaseStyle.textStyleNunitoSansBold(
                  16,
                  AppColors.colorDarkBlue,
                ),
              ),
              Text(
                '${PatientPreferences().sharedPrefRead(StorageKeys.age)} year old, B+, ${PatientPreferences().sharedPrefRead(StorageKeys.gender)}} ',
                style: BaseStyle.textStyleNunitoSansRegular(
                  12,
                  AppColors.colorDarkBlue,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
