import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/widgets/ripple_effect_widget.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/app_preference/patient_preferences.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import 'pt_doctor_available_days_controller.dart';

class PtDoctorAvailableDaysScreen extends GetView<PtDoctorAvailableDaysController> {
  const PtDoctorAvailableDaysScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.maxFinite,
                padding: const EdgeInsets.symmetric(vertical: 5),
                decoration: const BoxDecoration(color: AppColors.colorBackground),
                child: Center(
                  child: Text(
                    AppStringKey.labelAvailabilityDays.tr.toUpperCase(),
                    style: BaseStyle.textStyleNunitoSansBold(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                ),
              ),
              const SpaceVertical(15),
              Container(
                width: double.maxFinite,
                padding: const EdgeInsets.symmetric(vertical: 8),
                decoration: const BoxDecoration(color: AppColors.colorDarkBlue),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Obx(
                      () => Visibility.maintain(
                        visible: !controller.isInCurrentWeek(controller.startDate!.value.add(const Duration(days: 1))),
                        child: GestureDetector(
                          onTap: !controller.isInCurrentWeek(controller.startDate!.value.add(const Duration(days: 1)))
                              ? () {
                                  controller.getPreviousWeekDates();
                                }
                              : null,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Text(
                              '<<${AppStringKey.strPrev.tr}',
                              style: BaseStyle.textStyleNunitoSansBold(
                                14,
                                AppColors.colorYellow,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Obx(
                      () => Text(
                        '${controller.firstDayOfWeek()} - ${controller.lastDayOfWeek()}',
                        style: BaseStyle.textStyleNunitoSansBold(
                          14,
                          AppColors.white,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        controller.getNextWeekDates();
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(right: 15),
                        child: Text(
                          '${AppStringKey.strNext.tr}>>',
                          style: BaseStyle.textStyleNunitoSansBold(
                            14,
                            AppColors.colorYellow,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              const SpaceVertical(20),
              Obx(
                () => Column(
                  children: [
                    ...controller.doctorDateList.map(
                      (date) => Container(
                        margin: const EdgeInsets.only(bottom: 15),
                        decoration: BoxDecoration(
                          color: AppColors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.3),
                              spreadRadius: 1,
                              blurRadius: 5,
                              offset: const Offset(0, 5), // changes position of shadow
                            ),
                          ],
                        ),
                        child: RippleEffectWidget(
                          borderRadius: 0,
                          callBack: () {
                            PatientPreferences().sharedPrefWrite(StorageKeys.availableDaySlot, date);
                            Get.toNamed(RoutePaths.PT_DOCTOR_TIME_SLOT_SELECTION, arguments: {
                              'doctor_id': Get.arguments['doctor_id'],
                              'selected_date': date,
                            });
                          },
                          widget: Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 12,
                              horizontal: 10,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Image.asset(
                                      AppImages.docAvailabledaysCalender,
                                      width: 24,
                                      height: 24,
                                    ),
                                    const SpaceHorizontal(10),
                                    Text(
                                      controller.getDateTimeFromString(date),
                                      style: BaseStyle.textStyleNunitoSansBold(
                                        14,
                                        AppColors.colorDarkBlue,
                                      ),
                                    )
                                  ],
                                ),
                                const Icon(
                                  Icons.arrow_forward_ios,
                                  size: 15,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
