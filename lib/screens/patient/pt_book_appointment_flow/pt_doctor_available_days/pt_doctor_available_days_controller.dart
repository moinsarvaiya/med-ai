import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';

import '../../../../api_patient/patient_api_end_point.dart';
import '../../../../api_patient/patient_api_interface.dart';
import '../../../../api_patient/patient_api_presenter.dart';
import '../../../../api_patient/patient_response_key.dart';
import '../../../../constant/app_strings_key.dart';

class PtDoctorAvailableDaysController extends BaseController implements PatientApiCallBacks {
  Rx<DateTime>? startDate = DateTime.now().obs;
  Rx<DateTime>? firstDay = DateTime.now().obs;
  Rx<DateTime>? lastDay = DateTime.now().obs;
  String selectedDate = "";

  final RxBool _isLoading = false.obs;
  var doctorCode = "0";

  bool get isLoading => _isLoading.value;

  set isLoading(bool value) => _isLoading.value = value;
  RxList doctorDateList = RxList();

  @override
  void onInit() {
    super.onInit();
    startDate!.value = getStartOfCurrentWeek();
    getDoctorDates();
  }

  void getDoctorDates() {
    PatientApiPresenter(this).getPatientDoctorAvailableDays(
        Get.arguments['doctor_id'], DateFormat('y-MM-d').format(startDate!.value), DateFormat('y-MM-d').format(getLastDateOfCurrentWeek()));
  }

  List<String> getWeekDates(DateTime startDate) {
    final dates = <String>[];
    for (int i = 0; i < 6; i++) {
      final date = startDate.add(Duration(days: i));
      final formattedDate = DateFormat('EEEE, d MMMM y').format(date);
      dates.add(formattedDate);
    }
    return dates;
  }

  DateTime getStartOfCurrentWeek() {
    final now = DateTime.now();
    return now;
  }

  DateTime getLastDateOfCurrentWeek() {
    return startDate!.value.add(const Duration(days: 6));
  }

  String getDateTimeFromString(String date) {
    return DateFormat('EEEE, d MMMM y').format(DateFormat("EEE, y-MM-d").parse(date));
  }

  void getNextWeekDates() {
    startDate!.value = startDate!.value.add(const Duration(days: 7));
    getDoctorDates();
  }

  void getPreviousWeekDates() {
    startDate!.value = startDate!.value.subtract(const Duration(days: 7));
    getDoctorDates();
  }

  String firstDayOfWeek() {
    firstDay!.value = startDate!.value;
    final formattedDate = DateFormat('d MMM').format(firstDay!.value);
    return formattedDate;
  }

  String lastDayOfWeek() {
    lastDay!.value = firstDay!.value.add(const Duration(days: 6));
    final formattedDate = DateFormat('d MMM').format(lastDay!.value);
    return formattedDate;
  }

  bool isInCurrentWeek(DateTime givenDate) {
    DateTime now = DateTime.now();
    DateTime startOfWeek = now.subtract(Duration(days: now.weekday - 1));
    DateTime endOfWeek = startOfWeek.add(const Duration(days: 6));
    return givenDate.isAfter(startOfWeek) && givenDate.isBefore(endOfWeek);
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    this.isLoading = isLoading;
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.patientDoctorAvailableDays:
        doctorCode = object[PatientResponseKey.doctorCode];
        doctorDateList.value = (object[PatientResponseKey.doctorDates]);
        break;
    }
  }
}
