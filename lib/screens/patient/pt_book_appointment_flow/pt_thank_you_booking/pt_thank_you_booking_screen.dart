import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/app_preference/patient_preferences.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../pt_doctor_time_slot_selection/pt_doctor_time_slot_selection_controller.dart';
import 'pt_thank_you_booking_controller.dart';

class PtThankYouBookingScreen extends GetView<PtThankYouBookingController> {
  const PtThankYouBookingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String setCallingMethod(String method) {
      if (method.contains('In App Video')) {
        return AppStringKey.strVideoCallTxt.tr;
      } else if (method.contains('In App Voice')) {
        return AppStringKey.strVoiceCallTxt.tr;
      } else {
        return '${AppStringKey.strDirectCall.tr} : ${PatientPreferences().sharedPrefRead(StorageKeys.patientMobile)}';
      }
    }

    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: false,
          isBackButtonVisible: false,
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(left: 35, right: 35, bottom: MediaQuery.viewInsetsOf(context).bottom + 15, top: 10),
        child: SubmitButton(
          title: AppStringKey.labelMyAppointments.tr,
          onClick: () {
            Get.until((route) => Get.currentRoute == RoutePaths.PT_BOTTOM_TAB);
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 20),
            child: SizedBox(
              width: double.maxFinite,
              child: Column(
                children: [
                  const SpaceVertical(30),
                  Image.asset(
                    AppImages.success,
                    width: 84,
                    height: 84,
                  ),
                  const SpaceVertical(25),
                  Text(
                    AppStringKey.strThanksBooking.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      22,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(10),
                  Text(
                    AppStringKey.strConfirmSoon.tr,
                    textAlign: TextAlign.center,
                    style: BaseStyle.textStyleNunitoSansRegular(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(20),
                  Container(
                    height: 2,
                    width: double.maxFinite,
                    color: AppColors.colorGrayBackground,
                  ),
                  const SpaceVertical(20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        AppStringKey.strBookingID.tr,
                        textAlign: TextAlign.center,
                        style: BaseStyle.textStyleNunitoSansRegular(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceHorizontal(5),
                      Text(
                        PatientPreferences().sharedPrefRead(StorageKeys.vid),
                        style: BaseStyle.textStyleNunitoSansBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    ],
                  ),
                  const SpaceVertical(20),
                  Container(
                    height: 2,
                    width: double.maxFinite,
                    color: AppColors.colorGrayBackground,
                  ),
                  const SpaceVertical(20),
                  Text(
                    Utilities.reviewScreenDateFormat_ddMMMyyyy(PatientPreferences().sharedPrefRead(StorageKeys.availableDaySlot)),
                    style: BaseStyle.textStyleNunitoSansBold(
                      14,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        PatientPreferences().sharedPrefRead(StorageKeys.availableTimeSlot),
                        textAlign: TextAlign.center,
                        style: BaseStyle.textStyleNunitoSansBold(
                          14,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceHorizontal(5),
                      Text(
                        '(${Utilities.getDifferenceTime(PatientPreferences().sharedPrefRead(StorageKeys.availableTimeSlot))})',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          14,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    ],
                  ),
                  const SpaceVertical(20),
                  Container(
                    height: 2,
                    width: double.maxFinite,
                    color: AppColors.colorGrayBackground,
                  ),
                  const SpaceVertical(20),
                  Text(
                    PatientPreferences().sharedPrefRead(StorageKeys.doctorSelectionName),
                    style: BaseStyle.textStyleNunitoSansBold(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(20),
                  Container(
                    height: 2,
                    width: double.maxFinite,
                    color: AppColors.colorGrayBackground,
                  ),
                  const SpaceVertical(20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        setCallingMethod(Get.find<PtDoctorTimeSlotSelectionController>()
                            .callingMethodList
                            .firstWhere((method) => method.isSelected!)
                            .methodName!),
                        textAlign: TextAlign.center,
                        style: BaseStyle.textStyleNunitoSansRegular(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
