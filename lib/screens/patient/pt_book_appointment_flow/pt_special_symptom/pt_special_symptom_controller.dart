import 'package:get/get.dart';
import 'package:med_ai/api_patient/patient_api_interface.dart';
import 'package:med_ai/api_patient/patient_api_presenter.dart';
import 'package:med_ai/api_patient/patient_response_key.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/utils/app_preference/patient_preferences.dart';

import '../../../../api_patient/patient_api_end_point.dart';
import '../../../../bindings/base_controller.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../models/dr_patient_temperture_model.dart';
import '../../../../models/patient_models/pt_submit_symtpoms_requestbody_model.dart';
import '../../../../routes/route_paths.dart';

class PtSpecialSymptomController extends BaseController implements PatientApiCallBacks {
  final RxBool _isLoading = false.obs;

  bool get isLoading => _isLoading.value;

  set isLoading(bool value) => _isLoading.value = value;
  RxList<Rx<DrPatientTemperatureModel>> temperatures = List.generate(
    11,
    (index) => DrPatientTemperatureModel(temperature: '${96 + index}°F', isSelected: false).obs,
  ).obs;
  var fromMyAppointment = false.obs;

  RxList<String> durationList = RxList();
  RxList<String> oftenItComesList = RxList();
  RxList<String> traveledRecentlyList = [
    'Yes',
    'No',
  ].obs;

  var isTemperatureSelected = false.obs;
  var temperatureValue = ''.obs;
  var isDurationExpand = false.obs;

  var isDurationSelected = false.obs;
  var durationValue = ''.obs;
  var oftenItComesExpand = false.obs;

  var isOftenItComesSelected = false.obs;
  var oftenIsComesValue = ''.obs;
  var traveledRecentlyExpand = false.obs;

  var isTraveledRecentlySelected = false.obs;
  var traveledRecentlyValue = ''.obs;
  var completedButtonExpand = false.obs;

  @override
  void onInit() {
    super.onInit();
    getData();
    getFeverData();
  }

  void getData() {
    if (Get.arguments != null) {
      fromMyAppointment.value = Get.arguments['from'] == EnumProfileSectionFrom.MY_APPOINTMENT;
    }
  }

  void getFeverData() {
    PatientApiPresenter(this).getFeverOptionData(PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng');
  }

  void submitSpecialSymptomData() {
    final specialSymptomsRequestBody = createSpecialSymptomsRequestBody();
    final submitSymptomRequestBodyModel = createSubmitSymptomRequestBodyModel(specialSymptomsRequestBody);
    PatientApiPresenter(this).submitSpecialSymptomData(submitSymptomRequestBodyModel.toJson());
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    this.isLoading = isLoading;
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.getSpecialSymptomFeverData:
        durationList.addAll((object[PatientResponseKey.duration] as List).map((e) => e as String).toList());
        oftenItComesList.addAll((object[PatientResponseKey.frequency] as List).map((e) => e as String).toList());
        break;
      case PatientApiEndPoints.submitSpecialSymptoms:
        if (fromMyAppointment.value) {
          Get.toNamed(RoutePaths.PT_FOLLOWUP_REVIEW);
        } else {
          if (traveledRecentlyValue.toLowerCase() == 'yes') {
            Get.back(result: {
              'static_symptom': 'lived or travelled recently to areas prone to viral fevers like dengue, malaria and chikungunya',
              'api_symptom': object[PatientResponseKey.mappedSymptom].toString()
            });
          } else {
            Get.back(result: object[PatientResponseKey.mappedSymptom].toString());
          }
        }
        break;
    }
  }

  SpecialSymptomsRequestBody createSpecialSymptomsRequestBody() {
    return SpecialSymptomsRequestBody(
      fever: true,
      cough: false,
      pain: false,
      symptomLevel: int.parse(temperatureValue.value.replaceAll('°F', '')),
      site: 'None',
      onset: 'None',
      frequency: oftenIsComesValue.toString(),
      duration: durationValue.toString(),
      painCharacter: 'None',
      painExacerbating: 'None',
      coughType: 'None',
      coughTrigger: 'None',
    );
  }

  SubmitSymptomRequestBodyModel createSubmitSymptomRequestBodyModel(
    SpecialSymptomsRequestBody specialSymptomsRequestBody,
  ) {
    return SubmitSymptomRequestBodyModel(
      specialSymptoms: specialSymptomsRequestBody,
      lang: PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng',
    );
  }
}
