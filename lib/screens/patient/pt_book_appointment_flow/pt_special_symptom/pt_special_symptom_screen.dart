import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/widgets/custom_chip_widget.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/space_vertical.dart';
import 'pt_special_symptom_controller.dart';

class PtSpecialSymptomScreen extends GetView<PtSpecialSymptomController> {
  const PtSpecialSymptomScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Get.back(result: 'fever');
        return Future.value(false);
      },
      child: Scaffold(
        backgroundColor: AppColors.white,
        bottomNavigationBar: SizedBox(
          height: BaseSize.height(20),
          child: Column(
            children: [
              SubmitButton(
                title: AppStringKey.strSkip.tr,
                backgroundColor: AppColors.white,
                borderColor: AppColors.colorDarkBlue,
                titleColor: AppColors.colorDarkBlue,
                titleFontSize: 14,
                isBottomMargin: false,
                onClick: () {
                  Get.back(result: 'fever');
                },
              ),
              SpaceVertical(BaseSize.height(2)),
              Obx(
                () => controller.completedButtonExpand.value &&
                        controller.isTemperatureSelected.value &&
                        controller.isDurationSelected.value &&
                        controller.isOftenItComesSelected.value &&
                        controller.isTraveledRecentlySelected.value
                    ? SubmitButton(
                        title: AppStringKey.strComplete.tr,
                        titleFontSize: 14,
                        isBottomMargin: false,
                        onClick: () {
                          controller.submitSpecialSymptomData();
                        },
                      )
                    : const SizedBox.shrink(),
              ),
            ],
          ).paddingAll(20),
        ),
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(defaultAppBarHeight),
          child: CustomAppBar(
            isDividerVisible: true,
            onClick: () {
              Get.back(result: 'fever');
            },
          ),
        ),
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.titleFever.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      20,
                      AppColors.colorDarkOrange,
                    ),
                  ),
                  const SpaceVertical(10),
                  Text(
                    AppStringKey.titleBreakSymptom.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(20),
                  // Temperature widget
                  TemperatureWidget(controller: controller),
                  Obx(() => !controller.isTemperatureSelected.value ? const SpaceVertical(10) : const SizedBox.shrink()),
                  Obx(() => controller.isDurationExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(10),
                  // Duration widget
                  Obx(() => controller.isDurationExpand.value ? DurationListWidget(controller: controller) : const SizedBox.shrink()),
                  Obx(() => controller.isDurationSelected.value ? const SpaceVertical(10) : const SizedBox.shrink()),
                  Obx(() => controller.oftenItComesExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(10),
                  // Often It Comes widget
                  Obx(() => controller.oftenItComesExpand.value ? OftenItComesWidget(controller: controller) : const SizedBox.shrink()),
                  Obx(() => controller.isOftenItComesSelected.value ? const SpaceVertical(10) : const SizedBox.shrink()),
                  Obx(() => controller.traveledRecentlyExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(10),
                  // Traveled Recently widget
                  Obx(() =>
                      controller.traveledRecentlyExpand.value ? TraveledRecentlyWidget(controller: controller) : const SizedBox.shrink()),
                  const SpaceVertical(40),
                  // const SpaceVertical(20),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class TemperatureWidget extends StatelessWidget {
  final PtSpecialSymptomController controller;

  const TemperatureWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              AppStringKey.labelTemperatureWithOptional.tr,
              style: BaseStyle.textStyleNunitoSansBold(
                14,
                AppColors.colorDarkBlue,
              ),
            ).marginOnly(bottom: 0),
            Obx(
              () => controller.isTemperatureSelected.value
                  ? CustomChipWidget(
                      label: controller.temperatureValue.value,
                      callback: () {
                        controller.isTemperatureSelected.value = false;
                        for (var element in controller.temperatures) {
                          element.value.isSelected = false;
                        }
                        controller.temperatures.refresh();
                      },
                    ).marginOnly(right: 20)
                  : const SizedBox.shrink(),
            )
          ],
        ),
        const SpaceVertical(20),
        Obx(() => !controller.isTemperatureSelected.value
            ? Stack(
                children: [
                  Positioned(
                    left: 10,
                    right: 15,
                    top: 7,
                    child: Container(
                      height: 1,
                      width: double.maxFinite,
                      color: Colors.green.withOpacity(0.5),
                    ),
                  ),
                  Obx(
                    () => Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ...controller.temperatures.asMap().entries.map(
                          (entry) {
                            var index = entry.key;
                            var value = entry.value.value.temperature;
                            var evenIndex = index % 2 == 0;
                            return Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          for (var element in controller.temperatures) {
                                            element.value.isSelected = false;
                                          }
                                          entry.value.value.isSelected = true;
                                          controller.temperatures.refresh();
                                          controller.isTemperatureSelected.value = true;
                                          controller.temperatureValue.value = value!;
                                          controller.isDurationExpand.value = true;
                                        },
                                        child: Container(
                                          margin: EdgeInsets.zero,
                                          width: 15,
                                          height: 15,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: AppColors.white,
                                            border: Border.all(
                                              color: AppColors.colorDarkBlue,
                                            ),
                                          ),
                                          child: entry.value.value.isSelected!
                                              ? Container(
                                                  margin: const EdgeInsets.all(2),
                                                  width: 10,
                                                  height: 10,
                                                  decoration: const BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: AppColors.colorGreen,
                                                  ),
                                                )
                                              : Container(),
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SpaceVertical(10),
                                  evenIndex
                                      ? Text(
                                          value.toString(),
                                          style: BaseStyle.textStyleNunitoSansBold(
                                            10,
                                            AppColors.colorDarkBlue,
                                          ),
                                        )
                                      : const SizedBox.shrink()
                                ],
                              ),
                            );
                          },
                        )
                      ],
                    ),
                  ),
                ],
              )
            : const SizedBox.shrink())
      ],
    );
  }
}

class DurationListWidget extends StatelessWidget {
  final PtSpecialSymptomController controller;

  const DurationListWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                AppStringKey.strDuration.tr,
                style: BaseStyle.textStyleNunitoSansBold(
                  14,
                  AppColors.colorDarkBlue,
                ),
              ).marginOnly(bottom: 0),
              Obx(
                () => controller.isDurationSelected.value
                    ? CustomChipWidget(
                        label: controller.durationValue.value,
                        callback: () {
                          controller.isDurationSelected.value = false;
                        },
                      ).marginOnly(right: 10)
                    : const SizedBox.shrink(),
              )
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isDurationSelected.value
                ? Wrap(
                    children: controller.durationList
                        .map(
                          (element) => CustomChipWidget(
                            label: element,
                            cancelVisible: false,
                            viewCallback: () {
                              controller.isDurationSelected.value = true;
                              controller.durationValue.value = element;
                              controller.oftenItComesExpand.value = true;
                            },
                          ).marginOnly(right: 10, bottom: 20),
                        )
                        .toList(),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}

class OftenItComesWidget extends StatelessWidget {
  final PtSpecialSymptomController controller;

  const OftenItComesWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  AppStringKey.strHowOftenItComes.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    14,
                    AppColors.colorDarkBlue,
                  ),
                ),
              ),
              Obx(
                () => controller.isOftenItComesSelected.value
                    ? CustomChipWidget(
                        label: controller.oftenIsComesValue.value,
                        callback: () {
                          controller.isOftenItComesSelected.value = false;
                        },
                      ).marginOnly(right: 10)
                    : const SizedBox.shrink(),
              ),
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isOftenItComesSelected.value
                ? Wrap(
                    children: controller.oftenItComesList
                        .map(
                          (element) => CustomChipWidget(
                            label: element,
                            cancelVisible: false,
                            viewCallback: () {
                              controller.isOftenItComesSelected.value = true;
                              controller.oftenIsComesValue.value = element;
                              controller.traveledRecentlyExpand.value = true;
                            },
                          ).marginOnly(right: 10, bottom: 20),
                        )
                        .toList(),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}

class TraveledRecentlyWidget extends StatelessWidget {
  final PtSpecialSymptomController controller;

  const TraveledRecentlyWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: double.maxFinite,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  maxLines: 2,
                  AppStringKey.strTraveledRecentlyDesc.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    14,
                    AppColors.colorDarkBlue,
                  ),
                ).marginOnly(bottom: 0),
                Obx(
                  () => controller.isTraveledRecentlySelected.value
                      ? Align(
                          alignment: Alignment.centerRight,
                          child: CustomChipWidget(
                            label: controller.traveledRecentlyValue.value,
                            callback: () {
                              controller.isTraveledRecentlySelected.value = false;
                            },
                          ),
                        ).marginOnly(right: 10)
                      : const SizedBox.shrink(),
                ),
              ],
            ),
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isTraveledRecentlySelected.value
                ? Wrap(
                    children: controller.traveledRecentlyList
                        .map(
                          (element) => CustomChipWidget(
                            label: element,
                            cancelVisible: false,
                            viewCallback: () {
                              controller.isTraveledRecentlySelected.value = true;
                              controller.traveledRecentlyValue.value = element;
                              controller.completedButtonExpand.value = true;
                            },
                          ).marginOnly(right: 10),
                        )
                        .toList(),
                  )
                : const SizedBox.shrink(),
          ),
          Obx(
            () => !controller.isTraveledRecentlySelected.value ? Image.asset(AppImages.worldMap) : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}
