import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/api_patient/patient_api_end_point.dart';
import 'package:med_ai/api_patient/patient_api_interface.dart';
import 'package:med_ai/api_patient/patient_api_presenter.dart';
import 'package:med_ai/api_patient/patient_response_key.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/utils/app_preference/patient_preferences.dart';

import '../../../../constant/app_strings_key.dart';
import '../../../../models/dr_patient_temperture_model.dart';

class PtVitalHealthMetricsController extends BaseController implements PatientApiCallBacks {
  final RxBool _isLoading = false.obs;

  bool get isLoading => _isLoading.value;

  set isLoading(bool value) => _isLoading.value = value;
  var weightController = TextEditingController().obs;
  var heightFeetController = TextEditingController().obs;
  var heightInchController = TextEditingController().obs;
  var lowestBloodPressureController = TextEditingController().obs;
  var highestBloodPressureController = TextEditingController().obs;
  var breathingController = TextEditingController().obs;
  var pulseController = TextEditingController().obs;
  List<String> breathing = ['Low', 'High'];

  var breathingSelectedValue = '';
  var temperatureSelectedValue = '';
  RxList<Rx<DrPatientTemperatureModel>> temperatures = List.generate(
    11,
    (index) => DrPatientTemperatureModel(temperature: '${96 + index}°F', isSelected: false).obs,
  ).obs;

  RxList<String> breathingList = [''].obs;

  @override
  void onInit() {
    super.onInit();
    getBreathingData();
  }

  void getBreathingData() async {
    PatientApiPresenter(this).getBreathingData(PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng');
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    this.isLoading = isLoading;
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.vitalHealthDropdownValue:
        breathingList.clear();
        breathingList.addAll((object[PatientResponseKey.breathing] as List).map((e) => e as String).toList());
        break;
    }
  }
}
