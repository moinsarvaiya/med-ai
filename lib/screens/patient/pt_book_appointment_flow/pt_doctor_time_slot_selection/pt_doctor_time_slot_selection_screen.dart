import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/app_preference/patient_preferences.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/ripple_effect_widget.dart';
import '../../../../widgets/space_horizontal.dart';
import 'pt_doctor_time_slot_selection_controller.dart';

class PtDoctorTimeSlotSelectionScreen extends GetView<PtDoctorTimeSlotSelectionController> {
  const PtDoctorTimeSlotSelectionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                HeadingWidget(heading: AppStringKey.strCallingMethod.tr),
                const SpaceVertical(20),
                CallingOptionWidget(controller: controller),
                const SpaceVertical(30),
                HeadingWidget(heading: AppStringKey.strTimeSlotSelection.tr),
                const SpaceVertical(20),
                IntrinsicHeight(
                  child: Obx(
                    () => controller.timeSlotList.isNotEmpty
                        ? Column(
                            children: [
                              ...controller.timeSlotList.map(
                                (element) => Container(
                                  margin: const EdgeInsets.only(bottom: 15),
                                  decoration: BoxDecoration(
                                    color: AppColors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.3),
                                        spreadRadius: 1,
                                        blurRadius: 5,
                                        offset: const Offset(0, 5), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  child: RippleEffectWidget(
                                    borderRadius: 0,
                                    callBack: () {
                                      PatientPreferences()
                                          .sharedPrefWrite(StorageKeys.availableTimeSlot, '${element.startTime}-${element.endTime}');
                                      Get.toNamed(RoutePaths.PT_REVIEW_BOOKING);
                                    },
                                    widget: Padding(
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 12,
                                        horizontal: 10,
                                      ),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Row(
                                            children: [
                                              Image.asset(
                                                AppImages.docAvailabledaysCalender,
                                                width: 24,
                                                height: 24,
                                              ),
                                              const SpaceHorizontal(10),
                                              Text(
                                                '${element.startTime} - ${element.endTime}',
                                                style: BaseStyle.textStyleNunitoSansBold(
                                                  14,
                                                  AppColors.colorDarkBlue,
                                                ),
                                              )
                                            ],
                                          ),
                                          const Icon(
                                            Icons.arrow_forward_ios,
                                            size: 15,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          )
                        : IntrinsicHeight(
                            child: Text(
                              AppStringKey.strTimeSlotNotAvailable.tr,
                              style: BaseStyle.textStyleNunitoSansBold(
                                16,
                                AppColors.colorDarkBlue,
                              ),
                            ),
                          ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CallingOptionWidget extends StatelessWidget {
  final PtDoctorTimeSlotSelectionController controller;

  const CallingOptionWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...controller.callingMethodList.map(
            (element) => Expanded(
              child: GestureDetector(
                onTap: () {
                  for (var item in controller.callingMethodList) {
                    item.isSelected = false;
                  }
                  controller.callingMethodList[controller.callingMethodList.indexOf(element)].isSelected = true;
                  controller.callingMethodList.refresh();
                },
                child: Column(
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(
                          width: 2,
                          color: element.isSelected! ? AppColors.colorGreen : AppColors.white,
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            spreadRadius: 1,
                            blurRadius: 5,
                            offset: const Offset(0, 5), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Center(
                        child: Image.asset(element.methodIcon!),
                      ),
                    ),
                    const SpaceVertical(10),
                    Text(
                      element.methodName!,
                      textAlign: TextAlign.center,
                      style: BaseStyle.textStyleNunitoSansBold(
                        10,
                        AppColors.colorTextDarkGray,
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class HeadingWidget extends StatelessWidget {
  final String heading;

  const HeadingWidget({Key? key, required this.heading}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      decoration: const BoxDecoration(color: AppColors.colorBackground),
      child: Text(
        heading.toUpperCase(),
        style: BaseStyle.textStyleNunitoSansBold(
          16,
          AppColors.colorDarkBlue,
        ),
      ),
    );
  }
}
