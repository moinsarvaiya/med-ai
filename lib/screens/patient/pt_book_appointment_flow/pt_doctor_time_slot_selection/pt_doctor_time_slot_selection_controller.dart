import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';

import '../../../../api_patient/patient_api_end_point.dart';
import '../../../../api_patient/patient_api_interface.dart';
import '../../../../api_patient/patient_api_presenter.dart';
import '../../../../api_patient/patient_response_key.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../models/pt_doctor_calling_method_model.dart';
import '../../../../models/pt_doctor_time_slot_model.dart';

class PtDoctorTimeSlotSelectionController extends BaseController implements PatientApiCallBacks {
  RxList<PtDoctorCallingMethodModel> callingMethodList = RxList();
  RxList<PtDoctorTimeSlotModel> timeSlotList = RxList();

  var doctorCode = "0";
  final RxBool _isLoading = false.obs;

  bool get isLoading => _isLoading.value;

  set isLoading(bool value) => _isLoading.value = value;
  RxList<String> doctorAvailableTimeSlot = RxList();

  @override
  void onInit() {
    super.onInit();
    callingMethodList.add(
      PtDoctorCallingMethodModel(
        methodIcon: AppImages.videoCall,
        methodName: AppStringKey.strVideoCall.tr,
        isSelected: true,
      ),
    );
    callingMethodList.add(
      PtDoctorCallingMethodModel(
        methodIcon: AppImages.audioCall,
        methodName: AppStringKey.strAudioCall.tr,
        isSelected: false,
      ),
    );
    callingMethodList.add(
      PtDoctorCallingMethodModel(
        methodIcon: AppImages.directCall,
        methodName: AppStringKey.strDirectCall.tr,
        isSelected: false,
      ),
    );

    getDoctorAvailableSlot();
  }

  void getDoctorAvailableSlot() {
    PatientApiPresenter(this).getDoctorAvailableSlot(Get.arguments['doctor_id'], Get.arguments['selected_date']);
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    this.isLoading = isLoading;
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.patientDoctorTimeSlotSelection:
        doctorAvailableTimeSlot.addAll((object[PatientResponseKey.doctorAvailableSlot] as List).map((e) => e as String).toList());
        for (var element in doctorAvailableTimeSlot) {
          timeSlotList.add(PtDoctorTimeSlotModel(
            startTime: element.split("-")[0],
            endTime: element.split("-").length > 1 ? element.split("-")[1] : "",
            isAvailable: true,
          ));
        }
        break;
    }
  }
}
