import 'package:another_xlider/another_xlider.dart';
import 'package:another_xlider/enums/tooltip_direction_enum.dart';
import 'package:another_xlider/models/handler.dart';
import 'package:another_xlider/models/tooltip/tooltip.dart';
import 'package:another_xlider/models/tooltip/tooltip_box.dart';
import 'package:another_xlider/models/tooltip/tooltip_position_offset.dart';
import 'package:another_xlider/models/trackbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/models/patient_models/pt_available_doctor_list_model.dart';
import 'package:med_ai/utils/app_preference/patient_preferences.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/ripple_effect_widget.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_size.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../models/pt_speciality_item_model.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_round_text_field.dart';
import '../../pt_health_record_flow/pt_health_record2/pt_health_record2_screen.dart';
import 'pt_doctor_selection_controller.dart';

class PtDoctorSelectionScreen extends GetView<PtDoctorSelectionController> {
  const PtDoctorSelectionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          titleName: AppStringKey.titleAvailableAppDoctor.tr,
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomRoundTextField(
                labelText: AppStringKey.labelTime.tr,
                labelVisible: false,
                isRequireField: true,
                hintText: AppStringKey.hintSearchDoctor.tr,
                isReadOnly: false,
                textInputType: TextInputType.text,
                suffixIcon: const Icon(
                  Icons.mic,
                  size: 20,
                  color: AppColors.colorDarkBlue,
                ),
                backgroundColor: AppColors.transparent,
                textEditingController: controller.searchDoctorController,
              ),
              const SpaceVertical(10),
              FilterSortOptionSection(controller: controller),
              const SpaceVertical(10),
              DoctorSelectionSection(controller: controller)
            ],
          ),
        ),
      ),
    );
  }
}

class DoctorSelectionSection extends StatelessWidget {
  final PtDoctorSelectionController controller;

  const DoctorSelectionSection({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: Obx(
          () => ListView.builder(
            itemCount: controller.availableDoctorList.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return DoctorSelectionItem(availableDoctorList: controller.availableDoctorList[index]);
            },
          ),
        ),
      ),
    );
  }
}

class DoctorSelectionItem extends StatelessWidget {
  final PtAvailableDoctorListModel availableDoctorList;

  const DoctorSelectionItem({Key? key, required this.availableDoctorList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      width: double.maxFinite,
      decoration: BoxDecoration(color: AppColors.colorGrayBackground, borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 94,
              width: 94,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                    availableDoctorList.profile_img!,
                  ),
                ),
              ),
            ),
            const SpaceHorizontal(5),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            availableDoctorList.name!,
                            style: BaseStyle.textStyleNunitoSansBold(
                              14,
                              AppColors.colorDarkBlue,
                            ),
                          ),
                          Utilities.userVerifiedWidget(),
                        ],
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: AppColors.colorGrayBackground2,
                        ),
                        child: Text(
                          '${availableDoctorList.visit_fee!} BDT',
                          style: BaseStyle.textStyleNunitoSansBold(
                            12,
                            AppColors.colorDarkBlue,
                          ),
                        ),
                      )
                    ],
                  ),
                  const SpaceVertical(5),
                  Text(
                    availableDoctorList.qualification!,
                    style: BaseStyle.textStyleNunitoSansRegular(
                      10,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(5),
                  Text(
                    availableDoctorList.specialization!,
                    style: BaseStyle.textStyleNunitoSansRegular(
                      10,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(5),
                  Row(
                    children: [
                      Image.asset(
                        AppImages.calendar,
                        width: 16,
                        height: 16,
                      ),
                      const SpaceHorizontal(5),
                      Text(
                        '${availableDoctorList.patient_treated} consultation completed',
                        style: BaseStyle.textStyleNunitoSansBold(
                          10,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    ],
                  ),
                  const SpaceVertical(10),
                  Row(
                    children: [
                      RippleEffectWidget(
                        borderRadius: 20,
                        callBack: () {
                          Get.toNamed(RoutePaths.PT_AVAILABLE_DOCTOR_DETAILS, arguments: {
                            'doctor_detail': availableDoctorList,
                            'from': 'book_appointment',
                          });
                        },
                        widget: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(
                              color: AppColors.colorTextDarkGray,
                            ),
                          ),
                          child: Text(
                            AppStringKey.titleViewProfile.tr,
                            style: BaseStyle.textStyleNunitoSansBold(
                              12,
                              AppColors.colorTextDarkGray,
                            ),
                          ),
                        ),
                      ),
                      const SpaceHorizontal(10),
                      RippleEffectWidget(
                        borderRadius: 20,
                        callBack: () {
                          PatientPreferences().sharedPrefWrite(StorageKeys.visitFee, availableDoctorList.visit_fee);
                          PatientPreferences().sharedPrefWrite(StorageKeys.doctorId, availableDoctorList.doctor_id);
                          PatientPreferences().sharedPrefWrite(StorageKeys.doctorSelectionName, availableDoctorList.name);
                          PatientPreferences()
                              .sharedPrefWrite(StorageKeys.doctorSelectionSpecialization, availableDoctorList.specialization);
                          PatientPreferences().sharedPrefWrite(StorageKeys.doctorSelectionQualification, availableDoctorList.qualification);
                          PatientPreferences().sharedPrefWrite(StorageKeys.doctorSelectionProfileImage, availableDoctorList.profile_img);
                          Get.toNamed(RoutePaths.PT_DOCTOR_AVAILABLE_DAYS, arguments: {
                            'doctor_id': availableDoctorList.doctor_id,
                          });
                        },
                        widget: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                          decoration: BoxDecoration(
                            color: AppColors.colorTextDarkGray,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Text(
                            AppStringKey.strBookNow.tr,
                            style: BaseStyle.textStyleNunitoSansBold(
                              12,
                              AppColors.white,
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FilterSortOptionSection extends StatelessWidget {
  final PtDoctorSelectionController controller;

  const FilterSortOptionSection({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.maxFinite,
          height: 1,
          color: AppColors.colorBackground,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              onTap: () => filterBottomSheet(context, controller),
              child: Row(
                children: [
                  Image.asset(
                    AppImages.filter,
                    width: 24,
                    height: 24,
                  ),
                  const SpaceHorizontal(10),
                  Text(
                    AppStringKey.strFilter.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      14,
                      AppColors.colorDarkBlue,
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: 1,
              height: 35,
              color: AppColors.colorBackground,
            ),
            GestureDetector(
              onTap: () {
                sortBottomSheet(context, controller);
              },
              child: Row(
                children: [
                  Image.asset(
                    AppImages.sorting,
                    width: 24,
                    height: 24,
                  ),
                  const SpaceHorizontal(10),
                  Text(
                    AppStringKey.strSort.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      14,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceHorizontal(5),
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    width: 7,
                    height: 7,
                    decoration: const BoxDecoration(
                      color: AppColors.colorGreen,
                      shape: BoxShape.circle,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        Container(
          width: double.maxFinite,
          height: 1,
          color: AppColors.colorBackground,
        ),
      ],
    );
  }
}

void sortBottomSheet(BuildContext context, PtDoctorSelectionController controller) {
  showModalBottomSheet(
    context: context,
    builder: (builder) {
      return Container(
        padding: EdgeInsets.zero,
        color: const Color(0xFF737373),
        child: Container(
          padding: const EdgeInsets.only(
            bottom: 30,
            left: 35,
            right: 35,
            top: 18,
          ),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.0),
              topRight: Radius.circular(30.0),
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: const Align(
                  alignment: Alignment.topRight,
                  child: Icon(
                    Icons.close,
                    size: 24,
                    color: AppColors.black,
                  ),
                ),
              ),
              const SpaceVertical(20),
              Text(
                AppStringKey.strSortBy.tr,
                style: BaseStyle.textStyleNunitoSansBold(
                  16,
                  AppColors.colorDarkBlue,
                ),
              ),
              const SpaceVertical(10),
              Text(
                AppStringKey.strSortByPrice.tr,
                style: BaseStyle.textStyleNunitoSansSemiBold(
                  16,
                  AppColors.colorDarkBlue,
                ),
              ),
              const SpaceVertical(10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      controller.isPriceLowToHighSelected.value = true;
                      controller.isPriceHighToLowSelected.value = false;
                    },
                    child: Row(
                      children: [
                        Obx(
                          () => CustomRadioButtonWidget(
                            isSelected: controller.isPriceLowToHighSelected.value,
                          ),
                        ),
                        const SpaceHorizontal(10),
                        Text(
                          AppStringKey.strLowToHigh.tr,
                          style: BaseStyle.textStyleNunitoSansRegular(
                            16,
                            AppColors.colorDarkBlue,
                          ),
                        )
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      controller.isPriceLowToHighSelected.value = false;
                      controller.isPriceHighToLowSelected.value = true;
                    },
                    child: Row(
                      children: [
                        Obx(
                          () => CustomRadioButtonWidget(
                            isSelected: controller.isPriceHighToLowSelected.value,
                          ),
                        ),
                        const SpaceHorizontal(10),
                        Text(
                          AppStringKey.strHighToLow.tr,
                          style: BaseStyle.textStyleNunitoSansRegular(
                            16,
                            AppColors.colorDarkBlue,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              const SpaceVertical(30),
              Text(
                AppStringKey.strSortByPatient.tr,
                style: BaseStyle.textStyleNunitoSansSemiBold(
                  16,
                  AppColors.colorDarkBlue,
                ),
              ),
              const SpaceVertical(10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      controller.isPatientLowToHighSelected.value = true;
                      controller.isPatientHighToLowSelected.value = false;
                    },
                    child: Row(
                      children: [
                        Obx(
                          () => CustomRadioButtonWidget(
                            isSelected: controller.isPatientLowToHighSelected.value,
                          ),
                        ),
                        const SpaceHorizontal(10),
                        Text(
                          AppStringKey.strLowToHigh.tr,
                          style: BaseStyle.textStyleNunitoSansRegular(
                            16,
                            AppColors.colorDarkBlue,
                          ),
                        )
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      controller.isPatientLowToHighSelected.value = false;
                      controller.isPatientHighToLowSelected.value = true;
                    },
                    child: Row(
                      children: [
                        Obx(
                          () => CustomRadioButtonWidget(
                            isSelected: controller.isPatientHighToLowSelected.value,
                          ),
                        ),
                        const SpaceHorizontal(10),
                        Text(
                          AppStringKey.strHighToLow.tr,
                          style: BaseStyle.textStyleNunitoSansRegular(
                            16,
                            AppColors.colorDarkBlue,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              const SpaceVertical(40),
              Row(
                children: [
                  Expanded(
                    child: SubmitButton(
                      borderColor: AppColors.colorDarkBlue,
                      backgroundColor: AppColors.white,
                      titleColor: AppColors.colorDarkBlue,
                      title: AppStringKey.strClearAll.tr,
                      onClick: () {
                        Get.back();
                      },
                    ),
                  ),
                  const SpaceHorizontal(20),
                  Expanded(
                    child: SubmitButton(
                      title: AppStringKey.strApply.tr,
                      onClick: () {
                        // controller.sortListByPrice();
                        Get.back();
                      },
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      );
    },
  );
}

void filterBottomSheet(BuildContext context, PtDoctorSelectionController controller) {
  showModalBottomSheet(
    context: context,
    builder: (builder) {
      return Container(
        padding: EdgeInsets.zero,
        color: const Color(0xFF737373),
        child: Container(
          padding: const EdgeInsets.only(
            bottom: 20,
            left: 35,
            right: 35,
            top: 18,
          ),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.0),
              topRight: Radius.circular(30.0),
            ),
          ),
          child: ScrollConfiguration(
            behavior: MyBehavior(),
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                    child: const Align(
                      alignment: Alignment.topRight,
                      child: Icon(
                        Icons.close,
                        size: 24,
                        color: AppColors.black,
                      ),
                    ),
                  ),
                  const SpaceVertical(20),
                  Text(
                    AppStringKey.strFilterBy.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(10),
                  Text(
                    AppStringKey.strGender.tr,
                    style: BaseStyle.textStyleNunitoSansSemiBold(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  GridView.builder(
                      padding: EdgeInsets.zero,
                      itemCount: controller.genderList.length,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 0,
                        crossAxisSpacing: 0,
                        childAspectRatio: 1 / .34,
                      ),
                      itemBuilder: (context, i) {
                        return CustomCheckBox(
                          listCheckBox: controller.genderList,
                          index: i,
                          labelFontSize: 16,
                          callback: () {
                            controller.genderList[i].selected = !controller.genderList[i].selected!;
                            controller.genderList.refresh();
                          },
                        );
                      }),
                  Text(
                    AppStringKey.strSpecialty.tr,
                    style: BaseStyle.textStyleNunitoSansSemiBold(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(10),
                  GridView.builder(
                      padding: EdgeInsets.zero,
                      itemCount: controller.specialityList.length,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 0,
                        crossAxisSpacing: 0,
                        childAspectRatio: 1 / .25,
                      ),
                      itemBuilder: (context, i) {
                        return ItemSpecialityWidget(
                          controller: controller,
                          index: i,
                        );
                      }),
                  Text(
                    AppStringKey.strPriceRange.tr,
                    style: BaseStyle.textStyleNunitoSansSemiBold(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  SpaceVertical(BaseSize.height(1)),
                  FlutterSlider(
                    values: controller.minPrice != 0 && controller.maxPrice != 500
                        ? [controller.minPrice, controller.maxPrice]
                        : const [10, 499],
                    rangeSlider: true,
                    max: controller.maxPriceRange,
                    min: controller.minPriceRange,
                    handlerHeight: 15,
                    selectByTap: false,
                    tooltip: FlutterSliderTooltip(
                      textStyle: BaseStyle.textStyleNunitoSansBold(
                        12,
                        AppColors.colorDarkBlue,
                      ),
                      alwaysShowTooltip: true,
                      direction: FlutterSliderTooltipDirection.top,
                      positionOffset: FlutterSliderTooltipPositionOffset(
                        top: BaseSize.height(2.5),
                      ),
                      boxStyle: const FlutterSliderTooltipBox(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                    handler: FlutterSliderHandler(
                      child: const SizedBox.shrink(),
                      decoration: BoxDecoration(
                        border: Border.all(color: AppColors.colorDarkBlue),
                        color: Colors.white,
                        shape: BoxShape.circle,
                      ),
                    ),
                    rightHandler: FlutterSliderHandler(
                      child: const SizedBox.shrink(),
                      decoration: BoxDecoration(
                        border: Border.all(color: AppColors.colorDarkBlue),
                        color: Colors.white,
                        shape: BoxShape.circle,
                      ),
                    ),
                    trackBar: const FlutterSliderTrackBar(
                      activeTrackBarHeight: 2,
                      inactiveTrackBarHeight: 2,
                      activeTrackBar: BoxDecoration(
                        color: AppColors.colorGreen,
                      ),
                      inactiveTrackBar: BoxDecoration(
                        color: AppColors.colorGrayBackground2,
                      ),
                    ),
                    onDragCompleted: (handlerIndex, lowerValue, upperValue) {
                      controller.minPrice = lowerValue;
                      controller.maxPrice = upperValue;
                    },
                  ),
                  SpaceVertical(BaseSize.height(3)),
                  Row(
                    children: [
                      Expanded(
                        child: SubmitButton(
                          borderColor: AppColors.colorDarkBlue,
                          backgroundColor: AppColors.white,
                          titleColor: AppColors.colorDarkBlue,
                          title: AppStringKey.strClearAll.tr,
                          onClick: () {
                            Get.back();
                          },
                        ),
                      ),
                      const SpaceHorizontal(10),
                      Expanded(
                        child: SubmitButton(
                          title: AppStringKey.strApply.tr,
                          onClick: () {
                            Get.back();
                          },
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      );
    },
  );
}

class CustomRadioButtonWidget extends StatelessWidget {
  final bool isSelected;

  const CustomRadioButtonWidget({
    Key? key,
    required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 24,
      height: 24,
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          width: 1,
          color: AppColors.colorDarkBlue,
        ),
      ),
      child: isSelected
          ? Container(
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: AppColors.colorGreen,
              ),
            )
          : const SizedBox.shrink(),
    );
  }
}

class ItemSpecialityWidget extends StatelessWidget {
  final PtDoctorSelectionController controller;
  final int index;

  const ItemSpecialityWidget({
    Key? key,
    required this.controller,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomRadioButton(
            label: controller.specialityList[index].question!,
            itemIndex: index,
            listIndex: index,
            callback: () {
              for (var element in controller.specialityList) {
                element.option = false;
              }
              controller.specialityList[index].option = true;
              controller.specialityList.refresh();
            },
          ),
        ],
      ),
    );
  }
}

class CustomRadioButton extends StatelessWidget {
  final String label;
  final int itemIndex;
  final int listIndex;
  final Function()? callback;
  final List<PtSpecialityItemModel> specialityList = Get.find<PtDoctorSelectionController>().specialityList;

  CustomRadioButton({
    Key? key,
    required this.itemIndex,
    required this.listIndex,
    required this.label,
    this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => GestureDetector(
        onTap: callback,
        child: Row(
          children: [
            Container(
              height: 24,
              width: 24,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: AppColors.colorDarkBlue, width: 1),
              ),
              child: Container(
                margin: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: specialityList[listIndex].option! ? AppColors.colorGreen : AppColors.transparent,
                  shape: BoxShape.circle,
                ),
              ),
            ),
            const SpaceHorizontal(5),
            Text(
              label,
              style: BaseStyle.textStyleNunitoSansRegular(16, AppColors.colorDarkBlue),
            ),
          ],
        ),
      ),
    );
  }
}
