import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';

import '../../../../api_patient/patient_api_end_point.dart';
import '../../../../api_patient/patient_api_interface.dart';
import '../../../../api_patient/patient_api_presenter.dart';
import '../../../../api_patient/patient_response_key.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../models/model_check_box.dart';
import '../../../../models/patient_models/pt_available_doctor_list_model.dart';
import '../../../../models/pt_speciality_item_model.dart';

class PtDoctorSelectionController extends BaseController implements PatientApiCallBacks {
  var searchDoctorController = TextEditingController();
  Rx<bool> isPriceLowToHighSelected = true.obs;
  Rx<bool> isPriceHighToLowSelected = false.obs;
  Rx<bool> isPatientLowToHighSelected = true.obs;
  Rx<bool> isPatientHighToLowSelected = false.obs;

  final RxBool _isLoading = false.obs;
  var doctorCode = "0";

  bool get isLoading => _isLoading.value;

  set isLoading(bool value) => _isLoading.value = value;
  RxList<PtAvailableDoctorListModel> availableDoctorList = RxList();

  RxList<ModelCheckBox> genderList = RxList();
  RxList<PtSpecialityItemModel> specialityList = RxList();
  double minPriceRange = 0;
  double maxPriceRange = 500;
  double minPrice = 0;
  double maxPrice = 500;

  @override
  void onInit() {
    super.onInit();
    genderList.add(
      ModelCheckBox(
        name: 'Male',
        selected: false,
      ),
    );
    genderList.add(
      ModelCheckBox(
        name: 'Female',
        selected: false,
      ),
    );

    specialityList.add(
      PtSpecialityItemModel(
        question: 'Specialty-1',
        option: false,
      ),
    );
    specialityList.add(
      PtSpecialityItemModel(
        question: 'Specialty-2',
        option: false,
      ),
    );
    specialityList.add(
      PtSpecialityItemModel(
        question: 'Specialty-3',
        option: false,
      ),
    );
    specialityList.add(
      PtSpecialityItemModel(
        question: 'Specialty-4',
        option: false,
      ),
    );
    getAvailableDoctorList();
  }

  void getAvailableDoctorList() {
    PatientApiPresenter(this).getAvailableDoctorList();
  }

  void sortListByPrice() {
    if (isPriceLowToHighSelected.value) {
      availableDoctorList.sort((a, b) => a.visit_fee!.compareTo(b.visit_fee!));
    } else {
      availableDoctorList.sort((b, a) => a.visit_fee!.compareTo(b.visit_fee!));
    }
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    this.isLoading = isLoading;
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.patientDoctorSelection:
        doctorCode = object[PatientResponseKey.doctorCode];
        availableDoctorList.value =
            (object[PatientResponseKey.availableDoctorList] as List).map((e) => PtAvailableDoctorListModel.fromJson(e)).toList();
        break;
    }
  }
}
