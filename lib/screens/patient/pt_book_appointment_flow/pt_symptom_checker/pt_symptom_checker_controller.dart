import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api_patient/patient_api_interface.dart';
import 'package:med_ai/api_patient/patient_api_presenter.dart';
import 'package:med_ai/api_patient/patient_response_key.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/models/patient_models/pt_symptoms_model.dart';
import 'package:med_ai/utils/app_preference/patient_preferences.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';

import '../../../../api/api_endpoints.dart';
import '../../../../api_patient/patient_api_end_point.dart';
import '../../../../api_patient/patient_web_fields_key.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../models/patient_models/pt_upload_images_model.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/app_preference/login_preferences.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_loader.dart';

class PtSymptomCheckerController extends BaseController implements PatientApiCallBacks {
  final RxBool _isLoading = false.obs;

  bool get isLoading => _isLoading.value;

  set isLoading(bool value) => _isLoading.value = value;
  var speakController = TextEditingController();
  RxList<PtSymptomsModel> listSymptoms = RxList(); // for set symptom list value in model with selected variable
  var selectedSymptomsList = [].obs; // for store selected symptom

  RxList<String> symptomsSearchOptions = RxList();
  GlobalKey<TextFieldAutoCompleteState<String>> medicineDosageAutoCompleteKey = GlobalKey();
  var medicineDosageFocusNode = FocusNode();
  List<String> enteredSymptomsList = [];
  var symptomName = '';

  Rx<SpeechToText> speechToText = SpeechToText().obs;
  bool speechEnabled = false;
  RxString lastWords = ''.obs;
  FlutterTts flutterTts = FlutterTts();
  String wavFileName = '';
  RxList<PtUploadedImagesModel> imageUrls = RxList();
  var additionalCommentController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
    getData();
    getSymptomsSearchOption();
    initSpeech();
  }

  void initSpeech() async {
    speechEnabled = await speechToText.value.initialize(
      onError: (errorNotification) {
        // Handle errors here
        print('Speech recognition error: $errorNotification');
        Get.back();
      },
      onStatus: (status) {
        // Handle status changes here
        print('Speech recognition status: $status');
      },
    );
  }

  void startListening() async {
    if (speechEnabled) {
      await speechToText.value.listen(
        onResult: _onSpeechResult,

        cancelOnError: true, // This stops listening on error
      );
    }
  }

  void stopListening() async {
    await speechToText.value.stop();
  }

  void _onSpeechResult(SpeechRecognitionResult result) {
    lastWords.value = result.recognizedWords;
  }

  void getData() {
    symptomName = Get.arguments['symptom_name'];
  }

  void getSymptomsSearchOption() {
    PatientApiPresenter(this).getSymptomsSearchOption(PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng');
  }

  void getSuggestionSymptomsData() {
    PatientApiPresenter(this).getSuggestionSymptomsData(
      List.from(selectedSymptomsList),
      PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng',
    );
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    this.isLoading = isLoading;
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.getSuggestionSymptoms:
        listSymptoms.clear();
        for (var element in object[PatientResponseKey.suggestedSymptoms]) {
          listSymptoms.add(PtSymptomsModel(symptomName: element, isSelected: false, isSpecialSymptom: false));
        }
        break;
      case PatientApiEndPoints.getSymptomsSearchOption:
        symptomsSearchOptions.addAll((object[PatientResponseKey.symptoms] as List).map((e) => e['value'] as String).toList());
        getSuggestionSymptomsData();
        break;
    }
  }

  // Function to handle symptom selection
  void handleSymptomSelection(String val) {
    if (selectedSymptomsList.contains(val)) {
      speakController.clear();
      Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
      return;
    }
    // Check if the symptom needs special handling
    handleRedirection(val);
  }

  // Function to handle symptom submission
  void handleSymptomSubmission(String val) {
    if (selectedSymptomsList.contains(val)) {
      speakController.clear();
      Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
    } else {
      // Check if the symptom needs special handling
      handleRedirection(val);

      // Check if the symptom is not in the existing list, add it to enteredSymptomsList
      if (!symptomsSearchOptions.contains(val)) {
        enteredSymptomsList.add(val);
      }
    }
  }

  // Function to handle symptom selection and submission
  void handleSymptom(String val) {
    if (selectedSymptomsList.contains(val)) {
      speakController.clear();
      Utilities.showErrorMessage(AppStringKey.labelAlreadyAdded.tr);
    } else {
      // Check if the symptom needs special handling
      handleRedirection(val);
    }
  }

  // Function to handle redirection
  void handleRedirection(String val) async {
    // Create a map of special symptoms and their corresponding routes
    final specialSymptoms = {
      AppStringKey.strSymptomFever.tr.toLowerCase(): RoutePaths.PT_SPECIAL_SYMPTOM,
      AppStringKey.strSymptomCough.tr.toLowerCase(): RoutePaths.PT_COUGH_SYMPTOM,
      AppStringKey.strSymptomPain.tr.toLowerCase(): RoutePaths.PT_PAIN_SYMPTOM,
    };
    // Check if the symptom needs special handling and clear the speakController
    if (specialSymptoms.containsKey(val.toLowerCase())) {
      speakController.clear();
      var symptomName = await Get.toNamed(specialSymptoms[val.toLowerCase()]!);
      if (symptomName != null) {
        if (symptomName is Map<String, String>) {
          addToSelectedSymptomsList(symptomName['static_symptom']!, isFeverTravelYes: true);
          addToSelectedSymptomsList(symptomName['api_symptom']!);
        } else {
          addToSelectedSymptomsList(symptomName);
        }
      }
    } else {
      addToSelectedSymptomsList(val);
    }
  }

  // Function to add a symptom to the selectedSymptomsList
  void addToSelectedSymptomsList(String val, {bool isFeverTravelYes = false}) {
    selectedSymptomsList.add(val);
    selectedSymptomsList.refresh();
    speakController.clear();
    if (!isFeverTravelYes) {
      getSuggestionSymptomsData();
    }
  }

  Future<void> textToWav(String text) async {
    bool isInternet = await Utilities.isConnectedNetwork();

    if (!isInternet) {
      AppStringKey.labelNoInternetConnection.showErrorSnackBar(title: AppStringKey.error);
      return;
    }

    try {
      LoadingDialog.fullScreenLoader();
      await configureTts();

      wavFileName = generateWavFileName();
      String filePath = '/storage/emulated/0/Download/$wavFileName';

      await synthesizeAndSaveWav(text, filePath);

      File? wavFile = await getFileFromDownloadDirectory(wavFileName);

      if (wavFile != null) {
        await uploadWavFile(wavFile);
      } else {
        LoadingDialog.closeFullScreenDialog();
        AppStringKey.somethingWentWrong.showErrorSnackBar(title: AppStringKey.error);
      }
    } catch (e) {
      // Handle specific exceptions here
      LoadingDialog.closeFullScreenDialog();
      AppStringKey.somethingWentWrong.showErrorSnackBar(title: AppStringKey.error);
    }
  }

  Future<void> configureTts() async {
    await flutterTts.setLanguage("en-US");
    await flutterTts.setPitch(1.0);
    await flutterTts.setSpeechRate(0.5);
    await flutterTts.setVolume(1.0);
    await flutterTts.setVoice({"name": "en-gb-x-gba-local"});
  }

  Future<void> synthesizeAndSaveWav(String text, String filePath) async {
    await flutterTts.speak(text);
    await Future.delayed(const Duration(milliseconds: 2000));
    final directory = await getApplicationDocumentsDirectory();
    final downloadPath = directory.path;
    Directory directory1 = Directory('$downloadPath/your_folder_name');
    directory1.createSync(recursive: true);
    await flutterTts.stop();
    await flutterTts.synthesizeToFile(
      text,
      filePath,
    );
  }

  Future<void> uploadWavFile(File wavFile) async {
    var request = http.MultipartRequest(
      PatientWebFieldKey.strPostMethod,
      Uri.parse('${ApiEndpoints.baseUrl}${PatientApiEndPoints.getSpeechToText}'),
    );

    var headers = {
      PatientWebFieldKey.strContentType: PatientWebFieldKey.strMultipartFormData,
      PatientWebFieldKey.strAuthorization: 'Bearer ${LoginPreferences().sharedPrefRead(StorageKeys.token)}',
    };

    request.headers.addAll(headers);

    var stream = http.ByteStream(wavFile.openRead());
    stream.cast();
    var length = await wavFile.length();

    request.files.add(
      http.MultipartFile(
        PatientWebFieldKey.file,
        stream,
        length,
        filename: path.basename(wavFile.path),
      ),
    );

    request.fields[PatientWebFieldKey.lang] = LoginPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng';

    http.StreamedResponse response = await request.send();
    final responseBody = await response.stream.bytesToString();
    final parsedJson = jsonDecode(responseBody);

    LoadingDialog.closeFullScreenDialog();
    addToSelectedSymptomsList(parsedJson['speech_to_rext']);
  }

  Future<File?> getFileFromDownloadDirectory(String fileName) async {
    try {
      final file = File('/storage/emulated/0/Download/$fileName');
      if (await file.exists()) {
        return file;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  String generateWavFileName() {
    final now = DateTime.now();
    final timestamp = now.toIso8601String();
    final formattedTimestamp = timestamp.replaceAll(":", "_");
    return 'voice_$formattedTimestamp.wav';
  }

  // Function for getting different voice from plugin
  void listAvailableVoices() async {
    List<dynamic> voices = await flutterTts.getVoices;
    for (dynamic voice in voices) {
      print("Voice name: ${voice["name"]}");
      print("Language: ${voice["language"]}");
    }
  }
}
