import 'package:avatar_glow/avatar_glow.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/utils/extension_functions.dart';
import 'package:med_ai/widgets/custom_chip_widget.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/functions/permission_check.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_auto_complete_textfield.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_round_text_field.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';
import 'pt_symptom_checker_controller.dart';

class PtSymptomCheckerScreen extends GetView<PtSymptomCheckerController> {
  const PtSymptomCheckerScreen({Key? key}) : super(key: key);

  static voiceSymptomDialog(BuildContext context, PtSymptomCheckerController controller) {
    controller.lastWords.value = '';
    controller.startListening();
    showDialog(
      context: context,
      builder: (BuildContext context) => Center(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              DefaultTextStyle(
                style: BaseStyle.textStyleNunitoSansBold(
                  16,
                  AppColors.colorDarkBlue,
                ),
                child: Text(
                  AppStringKey.strPleaseRecordYourVoice.tr,
                ),
              ),
              const Divider(
                color: AppColors.colorLightSkyBlue,
              ),
              const SpaceVertical(15),
              AvatarGlow(
                glowColor: Colors.blue,
                endRadius: 90.0,
                duration: const Duration(milliseconds: 2000),
                repeat: true,
                showTwoGlows: true,
                repeatPauseDuration: const Duration(milliseconds: 100),
                child: Material(
                  // Replace this child with your own
                  elevation: 8.0,
                  shape: const CircleBorder(),
                  child: CircleAvatar(
                    backgroundColor: Colors.grey[100],
                    radius: 40.0,
                    child: const Icon(
                      Icons.keyboard_voice,
                      color: AppColors.colorDarkBlue,
                    ),
                  ),
                ),
              ),
              const SpaceVertical(15),
              Align(
                alignment: Alignment.center,
                child: DefaultTextStyle(
                  style: BaseStyle.textStyleNunitoSansRegular(
                    16,
                    AppColors.colorDarkBlue,
                  ),
                  child: Obx(
                    () => Text(
                      controller.lastWords.value,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
              const SpaceVertical(15),
              Material(
                child: Row(
                  children: [
                    Expanded(
                      child: SubmitButton(
                        title: AppStringKey.strSubmit.tr.toUpperCase(),
                        onClick: () {
                          Get.back();
                          controller.textToWav(controller.lastWords.value);
                          // controller.handleSymptom(controller.lastWords.value);
                        },
                      ),
                    ),
                    const SpaceHorizontal(10),
                    Expanded(
                      child: SubmitButton(
                        title: AppStringKey.strLabelCancel.tr[0].toUpperCase() + AppStringKey.strLabelCancel.tr.substring(1),
                        displayBorder: true,
                        borderColor: AppColors.colorDarkBlue,
                        backgroundColor: Colors.white,
                        titleColor: AppColors.colorDarkBlue,
                        onClick: () {
                          controller.stopListening();
                          Get.back();
                        },
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(bottom: MediaQuery.viewInsetsOf(context).bottom + 16, top: 16, left: 16, right: 16),
        child: SizedBox(
          height: BaseSize.height(15),
          child: Column(
            children: [
              SubmitButton(
                backgroundColor: AppColors.white,
                titleColor: AppColors.colorDarkBlue,
                title: AppStringKey.strSkip.tr,
                isBottomMargin: false,
                onClick: () {
                  Get.toNamed(RoutePaths.PT_DYNAMIC_SYMPTOM);
                },
              ),
              const SpaceVertical(10),
              SubmitButton(
                title: AppStringKey.strNext.tr,
                isBottomMargin: false,
                onClick: () {
                  Get.toNamed(RoutePaths.PT_DYNAMIC_SYMPTOM);
                },
              ),
            ],
          ),
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppStringKey.titleDescSymptomChecker.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    18,
                    AppColors.colorDarkBlue,
                  ),
                ),
                const SpaceVertical(20),
                Obx(
                  () => Wrap(
                    children: controller.selectedSymptomsList
                        .map(
                          (e) => CustomChipWidget(
                              label: e,
                              callback: () {
                                for (var element in controller.listSymptoms) {
                                  if (element == e) {
                                    element.isSelected = false;
                                    controller.listSymptoms.refresh();
                                  }
                                }
                                controller.selectedSymptomsList.remove(e);
                                controller.enteredSymptomsList.remove(e);
                                controller.selectedSymptomsList.refresh();
                                controller.getSuggestionSymptomsData();
                              }).marginOnly(bottom: 15, right: 15),
                        )
                        .toList(),
                  ),
                ),
                const SpaceVertical(10),
                Obx(
                  () => controller.symptomsSearchOptions.isEmpty
                      ? CustomRoundTextField(
                          labelVisible: false,
                          labelText: AppStringKey.labelTime.tr,
                          isRequireField: true,
                          hintText: AppStringKey.hintSymptomChecker.tr,
                          isReadOnly: false,
                          textInputType: TextInputType.text,
                          suffixIcon: GestureDetector(
                            onTap: () {},
                            child: const Icon(
                              Icons.keyboard_voice,
                              size: 20,
                              color: AppColors.colorDarkBlue,
                            ),
                          ),
                          backgroundColor: AppColors.transparent,
                          textEditingController: controller.speakController,
                        )
                      : CustomAutoCompleteTextField(
                          labelVisible: false,
                          suffixIcon: GestureDetector(
                            onTap: () async {
                              DeviceInfoPlugin plugin = DeviceInfoPlugin();
                              AndroidDeviceInfo android = await plugin.androidInfo;
                              if (android.version.sdkInt >= 33) {
                                CheckPermission.check([Permission.manageExternalStorage], "storage");
                              } else {
                                CheckPermission.check([Permission.storage], "storage");
                              }
                              voiceSymptomDialog(context, controller);
                            },
                            child: const Icon(
                              Icons.keyboard_voice,
                              size: 20,
                              color: AppColors.colorDarkBlue,
                            ),
                          ),
                          label: AppStringKey.labelTime.tr,
                          hint: AppStringKey.hintSymptomChecker.tr,
                          focusNode: controller.medicineDosageFocusNode,
                          textEditingController: controller.speakController,
                          onSelectedValue: (val) {
                            controller.handleSymptomSelection(val);
                          },
                          onSubmit: (val) {
                            controller.handleSymptomSubmission(val);
                          },
                          searchOptions: controller.symptomsSearchOptions.toNormalList(),
                          searchFieldAutoCompleteKey: controller.medicineDosageAutoCompleteKey,
                        ),
                ),
                const SpaceVertical(30),
                Text(
                  AppStringKey.titleSelectSymptom.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    18,
                    AppColors.colorDarkBlue,
                  ),
                ),
                const SpaceVertical(30),
                ItemAvailableSymptoms(
                  controller: controller,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ItemAvailableSymptoms extends StatelessWidget {
  const ItemAvailableSymptoms({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final PtSymptomCheckerController controller;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Obx(
        () => Wrap(
          children: controller.listSymptoms
              .map(
                (element) => CustomChipWidget(
                  label: element.symptomName!,
                  cancelVisible: false,
                  viewCallback: () {
                    controller.handleSymptom(element.symptomName!);
                  },
                ).marginOnly(right: 15, bottom: 15),
              )
              .toList(),
        ),
      ),
    );
  }
}
