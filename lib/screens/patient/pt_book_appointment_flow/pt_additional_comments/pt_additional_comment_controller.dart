import 'dart:convert';
import 'dart:io';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api_patient/patient_response_key.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/models/patient_models/pt_submit_additional_comment_model.dart';
import 'package:med_ai/models/patient_models/pt_upload_images_model.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_dynamic_symptom/pt_dynamic_symptom_controller.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_symptom_checker/pt_symptom_checker_controller.dart';
import 'package:med_ai/utils/app_preference/patient_preferences.dart';
import 'package:med_ai/utils/extension_functions.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';
import 'package:path/path.dart' as path;

import '../../../../api/api_endpoints.dart';
import '../../../../api_patient/patient_api_end_point.dart';
import '../../../../api_patient/patient_api_interface.dart';
import '../../../../api_patient/patient_api_presenter.dart';
import '../../../../api_patient/patient_web_fields_key.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/app_preference/login_preferences.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_loader.dart';
import '../pt_vital_health_metrics/pt_vital_health_metrics_controller.dart';

class PtAdditionalCommentController extends BaseController implements PatientApiCallBacks {
  int imageIndex = 0;
  List<String> urls = [];

  void submitPatientProfile() {
    for (var element in Get.find<PtSymptomCheckerController>().imageUrls) {
      urls.add(element.url!);
    }
    int lowestBloodPressure = int.tryParse(Get.find<PtVitalHealthMetricsController>().lowestBloodPressureController.value.text) ?? 0;
    int highestBloodPressure = int.tryParse(Get.find<PtVitalHealthMetricsController>().highestBloodPressureController.value.text) ?? 0;
    var bpCount = lowestBloodPressure / highestBloodPressure;
    PatientApiPresenter(this).submitPatientProfile(ptSubmitAdditionalCommentModel(bpCount).toJson());
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.createPatientProfile:
        PatientPreferences().sharedPrefWrite(StorageKeys.vid, object[PatientResponseKey.vid]);
        Get.toNamed(RoutePaths.PT_BOOKING_REVIEW);
        break;
    }
  }

  PtSubmitAdditionalCommentModel ptSubmitAdditionalCommentModel(num bpCount) => PtSubmitAdditionalCommentModel(
        pid: int.parse(PatientPreferences().sharedPrefRead(StorageKeys.profileSelectionPersonId)),
        prevRecordId: '',
        age: PatientPreferences().sharedPrefRead(StorageKeys.age),
        gender: PatientPreferences().sharedPrefRead(StorageKeys.gender),
        height:
            '${Get.find<PtVitalHealthMetricsController>().heightFeetController.value.text} ${Get.find<PtVitalHealthMetricsController>().heightInchController.value.text}',
        weight: Get.find<PtVitalHealthMetricsController>().weightController.value.text,
        operatorId: '',
        gps: '',
        symptoms: Get.isRegistered<PtSymptomCheckerController>()
            ? Get.find<PtSymptomCheckerController>().selectedSymptomsList.toListOfStrings()
            : [],
        userTypedSymptom: Get.isRegistered<PtDynamicSymptomController>()
            ? Get.find<PtDynamicSymptomController>()
                .ptDynamicSymptomsResponseBodyModel
                .value
                .responseBodytempInfo!
                .userTypedSymptom!
                .toListOfStrings()
            : [],
        answeredNoSymptoms: Get.isRegistered<PtDynamicSymptomController>()
            ? Get.find<PtDynamicSymptomController>()
                .ptDynamicSymptomsResponseBodyModel
                .value
                .responseBodytempInfo!
                .answeredNoSymptoms!
                .toListOfStrings()
            : [],
        temperature:
            Get.isRegistered<PtVitalHealthMetricsController>() ? Get.find<PtVitalHealthMetricsController>().temperatureSelectedValue : '',
        bp: bpCount.toString(),
        pulseRate:
            Get.isRegistered<PtVitalHealthMetricsController>() ? Get.find<PtVitalHealthMetricsController>().pulseController.value.text : '',
        breathingRate: Get.isRegistered<PtVitalHealthMetricsController>()
            ? Get.find<PtVitalHealthMetricsController>().breathingController.value.text
            : '',
        lastMenstrualCycle: '',
        patientComment: Get.find<PtSymptomCheckerController>().additionalCommentController.text,
        additionalImage: urls,
      );

  void uploadImage(File? imagePath) async {
    if (imagePath != null) {
      bool isInternet = await Utilities.isConnectedNetwork();
      if (isInternet) {
        try {
          LoadingDialog.fullScreenLoader();
          var request = http.MultipartRequest(
            PatientWebFieldKey.strPostMethod,
            Uri.parse(
              '${ApiEndpoints.baseUrl}${PatientApiEndPoints.uploadImages}',
            ),
          );

          var headers = {
            PatientWebFieldKey.strContentType: PatientWebFieldKey.strMultipartFormData,
            PatientWebFieldKey.strAuthorization: 'Bearer ${LoginPreferences().sharedPrefRead(StorageKeys.token)}',
          };
          request.headers.addAll(headers);

          var stream = http.ByteStream(imagePath.openRead());
          stream.cast();
          var length = await imagePath.length();
          request.files.add(
            http.MultipartFile(
              PatientWebFieldKey.strFile,
              stream,
              length,
              filename: path.basename(imagePath.path),
            ),
          );

          request.fields[PatientWebFieldKey.strFilenameWithExtension] =
              CustomFunctions.generateName(Get.find<PtSymptomCheckerController>().imageUrls.length + 1);
          request.fields[PatientWebFieldKey.username] = LoginPreferences().sharedPrefRead(StorageKeys.username);
          http.StreamedResponse response = await request.send();
          final responseBody = await response.stream.bytesToString();
          final parsedJson = jsonDecode(responseBody);
          if (parsedJson['code'] == '200') {
            LoadingDialog.closeFullScreenDialog();
            Get.find<PtSymptomCheckerController>()
                .imageUrls
                .add(PtUploadedImagesModel(filePath: parsedJson['file_path'], url: parsedJson['url']));
            Get.find<PtSymptomCheckerController>().imageUrls.refresh();
            AppStringKey.strImageUploadSuccessfully.showSuccessSnackBar(title: AppStringKey.success);
          } else {
            LoadingDialog.closeFullScreenDialog();
            AppStringKey.somethingWentWrong.showErrorSnackBar(title: AppStringKey.error);
          }
        } catch (e) {
          LoadingDialog.closeFullScreenDialog();
          e.toString().showErrorSnackBar(title: AppStringKey.error);
        }
      } else {
        AppStringKey.labelNoInternetConnection.showErrorSnackBar(title: AppStringKey.error);
      }
    }
  }

  void deleteImage(String filePath) async {
    bool isInternet = await Utilities.isConnectedNetwork();
    if (isInternet) {
      try {
        LoadingDialog.fullScreenLoader();
        var request = http.MultipartRequest(
          PatientWebFieldKey.strDeleteMethod,
          Uri.parse(
            '${ApiEndpoints.baseUrl}${PatientApiEndPoints.uploadImages}',
          ),
        );

        var headers = {
          PatientWebFieldKey.strContentType: PatientWebFieldKey.strMultipartFormData,
          PatientWebFieldKey.strAuthorization: 'Bearer ${LoginPreferences().sharedPrefRead(StorageKeys.token)}',
        };
        request.headers.addAll(headers);
        request.fields[PatientWebFieldKey.imagePath] = filePath;
        request.fields[PatientWebFieldKey.username] = LoginPreferences().sharedPrefRead(StorageKeys.username);

        http.StreamedResponse response = await request.send();
        final responseBody = await response.stream.bytesToString();
        final parsedJson = jsonDecode(responseBody);
        if (parsedJson['code'] == '200') {
          LoadingDialog.closeFullScreenDialog();
          Get.find<PtSymptomCheckerController>().imageUrls.removeAt(imageIndex);
          Get.find<PtSymptomCheckerController>().imageUrls.refresh();
          AppStringKey.strImageDeleteSuccessfully.showSuccessSnackBar(title: AppStringKey.success);
        } else {
          LoadingDialog.closeFullScreenDialog();
          AppStringKey.somethingWentWrong.showErrorSnackBar(title: AppStringKey.error);
        }
      } catch (e) {
        LoadingDialog.closeFullScreenDialog();
        e.toString().showErrorSnackBar(title: AppStringKey.error);
      }
    } else {
      AppStringKey.labelNoInternetConnection.showErrorSnackBar(title: AppStringKey.error);
    }
  }

  String getSymptomScreen() {
    if (Get.isRegistered<PtDynamicSymptomController>()) {
      return Get.find<PtDynamicSymptomController>().ptDynamicSymptomsResponseBodyModel.value.userTypedSymptom != null
          ? Get.find<PtDynamicSymptomController>().ptDynamicSymptomsResponseBodyModel.value.userTypedSymptom!.join(',')
          : '';
    }
    return '';
  }
}
