import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../pt_symptom_checker/pt_symptom_checker_controller.dart';
import 'pt_additional_comment_controller.dart';

class PtAdditionalCommentScreen extends GetView<PtAdditionalCommentController> {
  const PtAdditionalCommentScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.strSaveNext.tr,
        onClick: () {
          controller.submitPatientProfile();
        },
      ).paddingOnly(left: 20, right: 20, top: 20, bottom: MediaQuery.viewInsetsOf(context).bottom + 20),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              children: [
                Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 15,
                    vertical: 20,
                  ),
                  decoration: BoxDecoration(
                    color: AppColors.colorBackground,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.strSymptoms.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(10),
                      Text(
                        controller.getSymptomScreen(),
                        style: BaseStyle.textStyleNunitoSansRegular(
                          14,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(15),
                      Text(
                        AppStringKey.strAdditionalCommentOrNotes.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(15),
                      TextFormField(
                        textCapitalization: TextCapitalization.sentences,
                        cursorColor: AppColors.colorDarkBlue,
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.symmetric(
                            vertical: 8,
                            horizontal: 10,
                          ),
                          fillColor: AppColors.white,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25),
                            borderSide: const BorderSide(
                              color: AppColors.colorLightSkyBlue,
                              width: 1,
                            ),
                          ),
                          counterText: '',
                          hintText: '',
                          hintStyle: BaseStyle.textStyleNunitoSansRegular(
                            14,
                            AppColors.hintColor,
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(00),
                            borderSide: const BorderSide(
                              color: AppColors.colorLightSkyBlue,
                              width: 1,
                            ),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(00),
                            borderSide: const BorderSide(
                              color: AppColors.colorRed,
                              width: 1,
                            ),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(00),
                            borderSide: const BorderSide(
                              color: AppColors.colorLightSkyBlue,
                              width: 1,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(00),
                            borderSide: const BorderSide(
                              color: AppColors.colorLightSkyBlue,
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(00),
                            borderSide: const BorderSide(
                              color: AppColors.colorLightSkyBlue,
                              width: 1,
                            ),
                          ),
                        ),
                        keyboardType: TextInputType.text,
                        obscuringCharacter: "*",
                        controller: Get.find<PtSymptomCheckerController>().additionalCommentController,
                        textInputAction: TextInputAction.done,
                        maxLines: 5,
                        style: BaseStyle.textStyleDomineRegular(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(20),
                      Text(
                        AppStringKey.strAdditionalCommentImages.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(10),
                      SizedBox(
                        height: 100,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: () async {
                                if (Get.find<PtSymptomCheckerController>().imageUrls.length < 5) {
                                  var imagePath = await CustomFunctions.showImagePickBottomSheet(context);
                                  controller.uploadImage(imagePath);
                                } else {
                                  Utilities.showErrorMessage(AppStringKey.only5ImagesAdded.tr);
                                }
                              },
                              child: Container(
                                padding: const EdgeInsets.all(15),
                                width: 58,
                                height: 58,
                                decoration: BoxDecoration(
                                  color: AppColors.colorGrayBackground2,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Image.asset(AppImages.camera),
                              ),
                            ),
                            Obx(
                              () => Expanded(
                                child: ListView.builder(
                                  itemCount: Get.find<PtSymptomCheckerController>().imageUrls.value.length,
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, i) {
                                    return ItemUploadedImage(
                                      index: i,
                                      controller: controller,
                                      onRemoveClick: () {
                                        controller.deleteImage(Get.find<PtSymptomCheckerController>().imageUrls[i].filePath!);
                                        controller.imageIndex = i;
                                      },
                                    );
                                  },
                                ).paddingOnly(left: 10),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                const SpaceVertical(30),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ItemUploadedImage extends StatelessWidget {
  final int index;
  final PtAdditionalCommentController controller;
  final Function()? onRemoveClick;

  const ItemUploadedImage({
    Key? key,
    required this.index,
    required this.controller,
    this.onRemoveClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          // padding: const EdgeInsets.all(15),
          width: 58,
          height: 58,
          decoration: BoxDecoration(
            color: AppColors.colorGrayBackground2,
            borderRadius: BorderRadius.circular(5),
          ),
          child: Image.network(
            Get.find<PtSymptomCheckerController>().imageUrls[index].url ?? '',
            fit: BoxFit.cover,
          ),
        ),
        const SpaceVertical(10),
        Get.find<PtSymptomCheckerController>().imageUrls.length == index
            ? Container()
            : GestureDetector(
                onTap: onRemoveClick,
                child: Text(
                  AppStringKey.strRemove.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    10,
                    AppColors.colorRed,
                  ),
                ),
              ),
      ],
    ).marginOnly(right: 10);
  }
}
