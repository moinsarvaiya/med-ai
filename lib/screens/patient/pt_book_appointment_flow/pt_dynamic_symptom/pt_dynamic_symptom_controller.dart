import 'package:get/get.dart';
import 'package:med_ai/api_patient/patient_api_interface.dart';
import 'package:med_ai/api_patient/patient_response_key.dart';
import 'package:med_ai/constant/base_extension.dart';

import '../../../../api_patient/patient_api_end_point.dart';
import '../../../../api_patient/patient_api_presenter.dart';
import '../../../../bindings/base_controller.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../models/patient_models/pt_dynamic_symptoms_models/pt_dynamic_symptom_firstque_requestparams_model.dart';
import '../../../../models/patient_models/pt_dynamic_symptoms_models/pt_dynamic_symptom_requestparams_model.dart';
import '../../../../models/patient_models/pt_dynamic_symptoms_models/pt_dynamic_symptoms_responsebody_model.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/app_preference/patient_preferences.dart';
import '../pt_symptom_checker/pt_symptom_checker_controller.dart';

class PtDynamicSymptomController extends BaseController implements PatientApiCallBacks {
  Rx<int> queIndex = 0.obs;
  Rx<int> queListSize = 6.obs;
  var isYesSelected = false.obs;
  var isNoSelected = false.obs;
  var isDoNotKnowSelected = false.obs;

  Rx<PtDynamicSymptomsResponseBodyModel> ptDynamicSymptomsResponseBodyModel = PtDynamicSymptomsResponseBodyModel().obs;
  Map<String, dynamic> answers = {};

  @override
  void onInit() {
    super.onInit();
    getDynamicSymptoms();
  }

  void getDynamicSymptoms() {
    var firstQueRequestParams = createObjectForFirstQueInfo();
    var ptDynamicSymptomsFirstQueRequestParamsModel = createObjectForDynamicSymptomFirstQueRequestParams(firstQueRequestParams);
    PatientApiPresenter(this).getDynamicSymptoms(ptDynamicSymptomsFirstQueRequestParamsModel.toJson());
  }

  FirstQueTempInfo createObjectForFirstQueInfo() => FirstQueTempInfo(
        res: {},
        allSym: [],
        disCount: [],
        alreadyAsked: [],
        allSymBen: [],
        bigListBen: [],
        bigList: [],
        userTypedSymptom: [],
      );

  PtDynamicSymptomFirstQueRequestParamsModel createObjectForDynamicSymptomFirstQueRequestParams(
    FirstQueTempInfo firstQueTempInfo,
  ) =>
      PtDynamicSymptomFirstQueRequestParamsModel(
        answer: false,
        lang: PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng',
        gender: PatientPreferences().sharedPrefRead(StorageKeys.gender) ?? '',
        firstQueTempInfo: firstQueTempInfo,
        radioObject: {},
        allSymptoms: Get.find<PtSymptomCheckerController>().selectedSymptomsList.map((element) => element as String).toList(),
        userTypedSymptoms: [],
      );

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.getDynamicSymptoms:
        isYesSelected.value = isNoSelected.value = isDoNotKnowSelected.value = false;
        if (!object[PatientResponseKey.lastQuestion]) {
          queIndex.value = queIndex.value + 1;
          ptDynamicSymptomsResponseBodyModel.value = PtDynamicSymptomsResponseBodyModel.fromJson(object);
        } else {
          List<dynamic> userTypedSymptoms = object['user_typed_symptom'];
          if (userTypedSymptoms.isNotEmpty) {
            Get.toNamed(RoutePaths.PT_ADDITIONAL_COMMENT);
          } else {
            Get.offNamed(RoutePaths.PT_ADDITIONAL_COMMENT);
          }
        }
        break;
    }
  }

  void goToNextQuestion() {
    if (isYesSelected.value || isNoSelected.value || isDoNotKnowSelected.value) {
      List<String> symptomList = [];
      Set<String> uniqueSymptoms = Set<String>.from(Get.find<PtSymptomCheckerController>().selectedSymptomsList.map((e) => e as String));
      if (isYesSelected.value) {
        uniqueSymptoms.addAll(ptDynamicSymptomsResponseBodyModel.value.userTypedSymptom!);
      }
      symptomList.addAll(uniqueSymptoms.toList());

      var requestParamsTempInfo = createParamObjectForRequestBody();
      var ptDynamicSymptomRequestParamsModel = createObjectForDynamicSymptomRequestParams(
        requestParamsTempInfo,
        symptomList,
      );
      PatientApiPresenter(this).getDynamicSymptoms(ptDynamicSymptomRequestParamsModel.toJson());
    } else {
      AppStringKey.strPleaseSelectOption.showSnackBar;
    }
  }

  RequestParamsTempInfo createParamObjectForRequestBody() => RequestParamsTempInfo(
        allSymMatched: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.allSymMatched!,
        allGivenSymptoms: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.allGivenSymptoms!,
        disCount: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.disCount!,
        alreadyAsked: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.alreadyAsked!,
        res: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.res!,
        count: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.count!,
        bigList: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.bigList!,
        ans: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.ans!,
        allSymBen: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.allSymBen!,
        bigListBen: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.bigListBen!,
        userTypedSymptom: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.userTypedSymptom!,
        gender: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.gender!,
        answeredNoSymptoms: ptDynamicSymptomsResponseBodyModel.value.responseBodytempInfo!.answeredNoSymptoms!,
      );

  PtDynamicSymptomRequestParamsModel createObjectForDynamicSymptomRequestParams(
          RequestParamsTempInfo requestParamsTempInfo, List<String> symptomList) =>
      PtDynamicSymptomRequestParamsModel(
        answer: true,
        lang: PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng',
        gender: PatientPreferences().sharedPrefRead(StorageKeys.gender) ?? '',
        requestParamsTempInfo: requestParamsTempInfo,
        radioObject: answers,
        allSymptoms: symptomList,
        userTypedSymptoms: [],
      );
}
