import 'package:get/get.dart';
import 'package:med_ai/api_patient/patient_api_end_point.dart';
import 'package:med_ai/api_patient/patient_api_interface.dart';
import 'package:med_ai/api_patient/patient_api_presenter.dart';
import 'package:med_ai/api_patient/patient_response_key.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/storage_keys.dart';

import '../../../../constant/app_strings_key.dart';
import '../../../../models/patient_models/pt_disease_list_model.dart';
import '../../../../utils/app_preference/patient_preferences.dart';

class PtBookingReviewController extends BaseController implements PatientApiCallBacks {
  var dummyText =
      'Please not that the information provided by MedAi is provided solely for guideline purposes and is not a qualified medical opinion.'
      ' This information should not be considered advice or an opinion. of doctor or other health professional about your actual medical state and you should see a doctor for any symptoms you may have. '
      'If you are experiencing a health emergency, you should call your local emergency number immediately to request emergency medical assistance.';

  RxList<String> symptomsList = RxList();
  RxList<PtDiseaseListModel> diseaseList = RxList();
  RxString specialization = ''.obs;

  @override
  void onInit() {
    super.onInit();
    getSummaryViewData();
  }

  void getSummaryViewData() {
    PatientApiPresenter(this).getSummaryViewData(PatientPreferences().sharedPrefRead(StorageKeys.vid));
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.getSummaryView:
        if (object[PatientResponseKey.doctorCode] == '200') {
          symptomsList.value = (object[PatientResponseKey.symptoms] as List).map((e) => e as String).toList();
          diseaseList.value = (object[PatientResponseKey.diseaseList] as List).map((e) => PtDiseaseListModel.fromJson(e)).toList();
          specialization.value = object[PatientResponseKey.specialization];
        }
        break;
    }
  }
}
