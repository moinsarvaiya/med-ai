import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_booking_reviews/pt_booking_review_controller.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_symptom_checker/pt_symptom_checker_controller.dart';
import 'package:med_ai/widgets/space_horizontal.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/space_vertical.dart';
import '../../../doctor/dr_home/dr_home_screen.dart';

class PtBookingReviewScreen extends GetView<PtBookingReviewController> {
  const PtBookingReviewScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    AppStringKey.titleReview.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      18,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                ),
                const SpaceVertical(20),
                Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                  decoration: BoxDecoration(
                    color: AppColors.colorGrayBackground,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.strSymptoms.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(10),
                      Obx(
                        () => Text(
                          controller.symptomsList.join(', ').tr,
                          style: BaseStyle.textStyleNunitoSansRegular(
                            14,
                            AppColors.colorDarkBlue,
                          ),
                        ),
                      ),
                      const SpaceVertical(24),
                      GestureDetector(
                        onTap: () {
                          Get.find<PtSymptomCheckerController>().selectedSymptomsList.clear();
                          for (var i = 0; i < controller.symptomsList.length; i++) {
                            final isLastItem = i == controller.symptomsList.length - 1;
                            Get.find<PtSymptomCheckerController>().addToSelectedSymptomsList(
                              controller.symptomsList[i],
                              isFeverTravelYes: !isLastItem,
                            );
                          }
                          Get.until((route) => Get.currentRoute == RoutePaths.PT_SYMPTOM_CHECKER);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: 24,
                              height: 24,
                              padding: const EdgeInsets.all(0),
                              child: Image.asset(
                                AppImages.editQuestions,
                              ),
                            ),
                            const SpaceHorizontal(5),
                            Text(
                              AppStringKey.strReviewEditQuestions.tr,
                              style: BaseStyle.textStyleNunitoSansBold(
                                14,
                                AppColors.colorDarkBlue,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                const SpaceVertical(20),
                Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                  decoration: BoxDecoration(
                    color: AppColors.colorGrayBackground,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.strPredication.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(15),
                      Obx(
                        () => Column(
                          children: [
                            ...controller.diseaseList.map(
                              (element) => Column(
                                children: [
                                  const SpaceVertical(7),
                                  PredicationWidget(
                                    title: element.disease_name!,
                                    image: AppImages.symptomFever,
                                    percentage: (element.weight! * 100).toStringAsFixed(2).toString(),
                                    progress: element.weight!,
                                  ),
                                  const SpaceVertical(7),
                                  Container(
                                    width: double.maxFinite,
                                    height: 0.5,
                                    color: Colors.grey,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      const SpaceVertical(3),
                    ],
                  ),
                ),
                const SpaceVertical(20),
                Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                  decoration: BoxDecoration(
                    color: AppColors.colorGrayBackground,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.strRecommendation.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(5),
                      Text(
                        'Consult a Doctor',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          14,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(5),
                      Text(
                        'Based on the symptoms provided media believes that you \nneed to consult a doctor from the following specialty',
                        style: BaseStyle.textStyleNunitoSansRegular(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(20),
                      Text(
                        AppStringKey.strRecommendationSpecialist.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(5),
                      Obx(
                        () => Text(
                          controller.specialization.value.tr,
                          style: BaseStyle.textStyleNunitoSansRegular(
                            14,
                            AppColors.colorDarkBlue,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SpaceVertical(20),
                SubmitButton(
                  title: AppStringKey.strBookAnAppointment.tr,
                  isBottomMargin: false,
                  onClick: () {
                    Get.toNamed(RoutePaths.PT_DOCTOR_SELECTION);
                  },
                ),
                const SpaceVertical(10),
                SubmitButton(
                  title: AppStringKey.strSaveExit.tr,
                  isBottomMargin: false,
                  onClick: () {
                    Utilities.commonDialog(
                      context,
                      AppStringKey.labelWarning.tr,
                      AppStringKey.strReturnHomePage.tr,
                      AppStringKey.deleteYes.tr,
                      AppStringKey.deleteNo.tr,
                      () {
                        Get.back();
                        Get.until((route) => Get.currentRoute == RoutePaths.PT_BOTTOM_TAB);
                      },
                      () {
                        Get.back();
                      },
                    );
                  },
                ),
                const SpaceVertical(20),
                Container(
                  width: double.maxFinite,
                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                  decoration: BoxDecoration(
                    color: AppColors.colorGrayBackground,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    children: [
                      Center(
                        child: Text(
                          AppStringKey.strDisclaimer.tr,
                          style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
                        ),
                      ),
                      const SpaceVertical(10),
                      Text(
                        controller.dummyText,
                        style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PredicationWidget extends StatelessWidget {
  final String title;
  final String image;
  final String percentage;
  final double progress;

  const PredicationWidget({
    Key? key,
    required this.title,
    required this.image,
    required this.percentage,
    required this.progress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('object $progress');
    return GestureDetector(
      onTap: () {
        Get.toNamed(RoutePaths.PT_HOME_SYMPTOMS, arguments: {
          'image': image,
          'symptom': title,
          'fromHome': 'no',
          'from': 'book_appointment',
        });
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 150,
            child: Text(
              title,
              maxLines: 2,
              style: BaseStyle.textStyleNunitoSansRegular(
                14,
                AppColors.colorDarkBlue,
              ),
            ),
          ),
          Row(
            children: [
              SizedBox(
                width: 100,
                child: ItemProgressBar(
                  percentage: progress,
                  progressColor: AppColors.colorGreen,
                ),
              ),
              Text(
                formatPercentage(double.parse(percentage)),
                style: BaseStyle.textStyleNunitoSansRegular(
                  14,
                  AppColors.colorDarkBlue,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  String formatPercentage(double value) {
    String formattedValue = value.toStringAsFixed(2);
    if (value < 10.00) {
      formattedValue = '0$formattedValue';
    }
    return '$formattedValue%';
  }
}
