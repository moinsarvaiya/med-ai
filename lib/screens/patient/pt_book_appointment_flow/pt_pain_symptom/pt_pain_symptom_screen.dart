import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/utils/extension_functions.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_chip_widget.dart';

import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_size.dart';
import '../../../../constant/base_style.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_dropdown_field.dart';
import '../../../../widgets/space_vertical.dart';
import 'pt_pain_symptom_controller.dart';

class PtPainSymptomScreen extends GetView<PtPainSymptomController> {
  const PtPainSymptomScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Get.back(result: 'pain');
        return Future.value(false);
      },
      child: Scaffold(
        backgroundColor: AppColors.white,
        bottomNavigationBar: SizedBox(
          height: BaseSize.height(20),
          child: Column(
            children: [
              Obx(
                () => controller.isLevelOfPainSelected.value &&
                        controller.isPainFromRecentInjurySelected.value &&
                        controller.isWhereSelected.value &&
                        controller.isTypeSelected.value &&
                        controller.isDurationSelected.value &&
                        controller.isOftenItComeSelected.value &&
                        controller.isPainBecomeExtremeSelected.value
                    ? SubmitButton(
                        title: AppStringKey.strComplete.tr,
                        titleFontSize: 14,
                        isBottomMargin: false,
                        onClick: () {
                          controller.submitSpecialSymptomData();
                        },
                      )
                    : const SizedBox.shrink(),
              ),
              SpaceVertical(BaseSize.height(2)),
              SubmitButton(
                title: AppStringKey.strSkip.tr,
                backgroundColor: AppColors.white,
                borderColor: AppColors.colorDarkBlue,
                titleColor: AppColors.colorDarkBlue,
                titleFontSize: 14,
                onClick: () {
                  Get.back(result: 'pain');
                },
              ),
            ],
          ).paddingAll(20),
        ),
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(defaultAppBarHeight),
          child: CustomAppBar(
            isDividerVisible: true,
            onClick: () {
              Get.back(result: 'pain');
            },
          ),
        ),
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.titlePain.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      20,
                      AppColors.colorDarkOrange,
                    ),
                  ),
                  const SpaceVertical(10),
                  Text(
                    AppStringKey.titleBreakSymptom.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(20),
                  // Type widget
                  TemperatureWidget(controller: controller),
                  Obx(() => !controller.isLevelOfPainSelected.value ? const SpaceVertical(20) : const SizedBox.shrink()),
                  Obx(() => controller.isPainFromRecentInjuryExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(10),
                  // Duration widget
                  Obx(() => controller.isPainFromRecentInjuryExpand.value
                      ? PainFromRecentInjuryWidget(controller: controller)
                      : const SizedBox.shrink()),
                  Obx(() => controller.isPainFromRecentInjurySelected.value ? const SpaceVertical(20) : const SizedBox.shrink()),
                  Obx(() => controller.isWhereExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(10),
                  // How It Started widget
                  Obx(() => controller.isWhereExpand.value ? WhereListWidget(controller: controller) : const SizedBox.shrink()),
                  Obx(() => controller.isWhereSelected.value ? const SpaceVertical(20) : const SizedBox.shrink()),
                  Obx(() => controller.isTypeExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(10),
                  // Type widget
                  Obx(() => controller.isTypeExpand.value ? TypeListWidget(controller: controller) : const SizedBox.shrink()),
                  Obx(() => controller.isTypeSelected.value ? const SpaceVertical(20) : const SizedBox.shrink()),
                  Obx(() => controller.isDurationExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(10),
                  // Duration widget
                  Obx(() => controller.isDurationExpand.value ? DurationListWidget(controller: controller) : const SizedBox.shrink()),
                  Obx(() => controller.isDurationSelected.value ? const SpaceVertical(20) : const SizedBox.shrink()),
                  Obx(() => controller.isOftenItComeExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(10),
                  // Often it comes widget
                  Obx(() => controller.isOftenItComeExpand.value ? OftenItComeListWidget(controller: controller) : const SizedBox.shrink()),
                  Obx(() => controller.isOftenItComeSelected.value ? const SpaceVertical(20) : const SizedBox.shrink()),
                  Obx(() => controller.isPainBecomeExtremeExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(10),
                  // Pain become extreme widget
                  Obx(() => controller.isPainBecomeExtremeExpand.value
                      ? PainBecomeExtremeListWidget(controller: controller)
                      : const SizedBox.shrink()),
                  Obx(() => controller.isPainBecomeExtremeSelected.value ? const SpaceVertical(20) : const SizedBox.shrink()),
                  // Obx(() => controller.isPainBecomeExtremeExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(40),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class TemperatureWidget extends StatelessWidget {
  final PtPainSymptomController controller;

  const TemperatureWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                AppStringKey.labelLevelOfPain.tr,
                style: BaseStyle.textStyleNunitoSansBold(
                  14,
                  AppColors.colorDarkBlue,
                ),
              ).marginOnly(bottom: 0),
              Obx(
                () => controller.isLevelOfPainSelected.value
                    ? CustomChipWidget(
                        label: controller.levelOfPainValue.value,
                        callback: () {
                          controller.isLevelOfPainSelected.value = false;
                          for (var element in controller.temperatures) {
                            element.value.isSelected = false;
                          }
                          controller.temperatures.refresh();
                        },
                      ).marginOnly(right: 20)
                    : const SizedBox.shrink(),
              )
            ],
          ),
          const SpaceVertical(20),
          Obx(
            () => !controller.isLevelOfPainSelected.value
                ? Stack(
                    children: [
                      Positioned(
                        left: MediaQuery.sizeOf(context).width / 6,
                        right: MediaQuery.sizeOf(context).width / 6,
                        top: 15,
                        child: Container(
                          height: 1,
                          width: double.maxFinite,
                          color: Colors.grey.withOpacity(0.5),
                        ),
                      ),
                      Obx(
                        () => Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ...controller.temperatures.asMap().entries.map(
                              (entry) {
                                var value = entry.value.value.temperature;
                                var temperatureValue = entry.value.value.temperatureValue;
                                return Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              for (var element in controller.temperatures) {
                                                element.value.isSelected = false;
                                              }
                                              entry.value.value.isSelected = true;
                                              controller.temperatures.refresh();
                                              controller.isLevelOfPainSelected.value = true;
                                              controller.levelOfPainValue.value = value!;
                                              controller.temperatureValue.value = temperatureValue!;
                                              controller.isPainFromRecentInjuryExpand.value = true;
                                            },
                                            child: Container(
                                              padding: const EdgeInsets.all(4),
                                              width: 25,
                                              height: 30,
                                              child: Container(
                                                margin: EdgeInsets.zero,
                                                width: 15,
                                                height: 15,
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: AppColors.white,
                                                  border: Border.all(
                                                    color: AppColors.colorDarkBlue,
                                                  ),
                                                ),
                                                child: entry.value.value.isSelected!
                                                    ? Container(
                                                        margin: const EdgeInsets.all(3),
                                                        width: 10,
                                                        height: 10,
                                                        decoration: const BoxDecoration(
                                                          shape: BoxShape.circle,
                                                          color: AppColors.colorGreen,
                                                        ),
                                                      )
                                                    : Container(),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SpaceVertical(10),
                                      Text(
                                        value.toString(),
                                        style: BaseStyle.textStyleNunitoSansBold(
                                          10,
                                          AppColors.colorDarkBlue,
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              },
                            )
                          ],
                        ),
                      ),
                    ],
                  )
                : const SizedBox.shrink(),
          )
        ],
      ),
    );
  }
}

class PainFromRecentInjuryWidget extends StatelessWidget {
  final PtPainSymptomController controller;

  const PainFromRecentInjuryWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                AppStringKey.labelPainRecent.tr,
                style: BaseStyle.textStyleNunitoSansBold(
                  14,
                  AppColors.colorDarkBlue,
                ),
              ).marginOnly(bottom: 0),
              Obx(
                () => controller.isPainFromRecentInjurySelected.value
                    ? CustomChipWidget(
                        label: controller.painFromRecentInjuryValue.value,
                        callback: () {
                          controller.isPainFromRecentInjurySelected.value = false;
                        },
                      ).marginOnly(right: 20)
                    : const SizedBox.shrink(),
              )
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isPainFromRecentInjurySelected.value
                ? Wrap(
                    children: controller.painFromRecentInjury
                        .map(
                          (element) => GestureDetector(
                            onTap: () {
                              if (element == 'No') {
                                controller.isPainFromRecentInjurySelected.value = true;
                                controller.painFromRecentInjuryValue.value = element;
                                controller.isWhereExpand.value = true;
                              } else {
                                yesPainDialog(
                                  context,
                                  () {
                                    Get.back(result: 'pain from a recent injury');
                                  },
                                );
                              }
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                                horizontal: 15,
                              ),
                              decoration: BoxDecoration(
                                color: AppColors.colorBackground,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    element,
                                    style: BaseStyle.textStyleNunitoSansMedium(
                                      14,
                                      AppColors.colorDarkBlue,
                                    ),
                                  ),
                                ],
                              ),
                            ).marginOnly(right: 10, bottom: 10),
                          ),
                        )
                        .toList(),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}

class WhereListWidget extends StatelessWidget {
  final PtPainSymptomController controller;

  const WhereListWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                AppStringKey.labelWhere.tr,
                style: BaseStyle.textStyleNunitoSansBold(
                  14,
                  AppColors.colorDarkBlue,
                ),
              ).marginOnly(bottom: 0),
              Obx(
                () => controller.isWhereSelected.value
                    ? CustomChipWidget(
                        label: controller.whereValue.value,
                        callback: () {
                          controller.isWhereSelected.value = false;
                        },
                      ).marginOnly(right: 20)
                    : const SizedBox.shrink(),
              )
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isWhereSelected.value
                ? CustomDropDownField(
                    hintTitle: AppStringKey.hintSelectWhere.tr,
                    items: controller.whereList.toNormalList(),
                    validationText: '',
                    onChanged: (String? val) {
                      controller.isWhereSelected.value = true;
                      controller.whereValue.value = val!;
                      controller.isTypeExpand.value = true;
                    },
                    labelText: '',
                    lableVisibility: false,
                    isRequireField: true,
                    selectedValue: controller.whereValue.value,
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}

class TypeListWidget extends StatelessWidget {
  final PtPainSymptomController controller;

  const TypeListWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  AppStringKey.strTypeWithoutOptional.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    14,
                    AppColors.colorDarkBlue,
                  ),
                ).marginOnly(bottom: 0),
              ),
              Obx(() => controller.isTypeSelected.value
                  ? CustomChipWidget(
                      label: controller.typeValue.value,
                      callback: () {
                        controller.isTypeSelected.value = false;
                      },
                    ).marginOnly(right: 20)
                  : const SizedBox.shrink())
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isTypeSelected.value
                ? Wrap(
                    children: controller.typeList
                        .toNormalList()
                        .map(
                          (element) => GestureDetector(
                            onTap: () {
                              controller.isTypeSelected.value = true;
                              controller.typeValue.value = element;
                              controller.isDurationExpand.value = true;
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                                horizontal: 15,
                              ),
                              decoration: BoxDecoration(
                                color: AppColors.colorBackground,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    element,
                                    style: BaseStyle.textStyleNunitoSansMedium(
                                      14,
                                      AppColors.colorDarkBlue,
                                    ),
                                  ),
                                ],
                              ),
                            ).marginOnly(right: 10, bottom: 10),
                          ),
                        )
                        .toList(),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}

class DurationListWidget extends StatelessWidget {
  final PtPainSymptomController controller;

  const DurationListWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  AppStringKey.strDuration.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    14,
                    AppColors.colorDarkBlue,
                  ),
                ).marginOnly(bottom: 0),
              ),
              Obx(
                () => controller.isDurationSelected.value
                    ? CustomChipWidget(
                        label: controller.durationValue.value,
                        callback: () {
                          controller.isDurationSelected.value = false;
                        },
                      ).marginOnly(right: 20)
                    : const SizedBox.shrink(),
              ),
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isDurationSelected.value
                ? Wrap(
                    children: controller.durationList
                        .toNormalList()
                        .map(
                          (element) => GestureDetector(
                            onTap: () {
                              controller.isDurationSelected.value = true;
                              controller.durationValue.value = element;
                              controller.isOftenItComeExpand.value = true;
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                                horizontal: 15,
                              ),
                              decoration: BoxDecoration(
                                color: AppColors.colorBackground,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    element,
                                    style: BaseStyle.textStyleNunitoSansMedium(
                                      14,
                                      AppColors.colorDarkBlue,
                                    ),
                                  ),
                                ],
                              ),
                            ).marginOnly(right: 10, bottom: 10),
                          ),
                        )
                        .toList(),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}

class OftenItComeListWidget extends StatelessWidget {
  final PtPainSymptomController controller;

  const OftenItComeListWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  AppStringKey.strHowOftenItComes.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    14,
                    AppColors.colorDarkBlue,
                  ),
                ).marginOnly(bottom: 0),
              ),
              Obx(() => controller.isOftenItComeSelected.value
                  ? CustomChipWidget(
                      label: controller.oftenItComeValue.value,
                      callback: () {
                        controller.isOftenItComeSelected.value = false;
                      },
                    ).marginOnly(right: 20)
                  : const SizedBox.shrink())
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isOftenItComeSelected.value
                ? Wrap(
                    children: controller.oftenItComeList
                        .toNormalList()
                        .map(
                          (element) => GestureDetector(
                            onTap: () {
                              controller.isOftenItComeSelected.value = true;
                              controller.oftenItComeValue.value = element;
                              controller.isPainBecomeExtremeExpand.value = true;
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                                horizontal: 15,
                              ),
                              decoration: BoxDecoration(
                                color: AppColors.colorBackground,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    element,
                                    style: BaseStyle.textStyleNunitoSansMedium(
                                      14,
                                      AppColors.colorDarkBlue,
                                    ),
                                  ),
                                ],
                              ),
                            ).marginOnly(right: 10, bottom: 10),
                          ),
                        )
                        .toList(),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}

class PainBecomeExtremeListWidget extends StatelessWidget {
  final PtPainSymptomController controller;

  const PainBecomeExtremeListWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  AppStringKey.labelPainBecomeExtreme.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    14,
                    AppColors.colorDarkBlue,
                  ),
                ).marginOnly(bottom: 0, right: 15),
              ),
              Obx(() => controller.isPainBecomeExtremeSelected.value
                  ? CustomChipWidget(
                      label: controller.painBecomeExtremeValue.value,
                      callback: () {
                        controller.isPainBecomeExtremeSelected.value = false;
                      },
                    ).marginOnly(right: 20)
                  : const SizedBox.shrink())
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isPainBecomeExtremeSelected.value
                ? Wrap(
                    children: controller.painBecomeExtremeList
                        .toNormalList()
                        .map(
                          (element) => GestureDetector(
                            onTap: () {
                              controller.isPainBecomeExtremeSelected.value = true;
                              controller.painBecomeExtremeValue.value = element;
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                                horizontal: 15,
                              ),
                              decoration: BoxDecoration(
                                color: AppColors.colorBackground,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    element,
                                    style: BaseStyle.textStyleNunitoSansMedium(
                                      14,
                                      AppColors.colorDarkBlue,
                                    ),
                                  ),
                                ],
                              ),
                            ).marginOnly(right: 10, bottom: 10),
                          ),
                        )
                        .toList(),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}

void yesPainDialog(BuildContext context, Function callBack) {
  showDialog(
    context: context,
    builder: (BuildContext context) => Center(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20),
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              AppStringKey.deleteWarning.tr,
              style: BaseStyle.textStyleNunitoSansBold(
                16,
                AppColors.colorDarkBlue,
              ),
            ),
            const Divider(
              color: AppColors.colorLightSkyBlue,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                AppStringKey.strPainNO.tr,
                style: BaseStyle.textStyleNunitoSansRegular(
                  12,
                  AppColors.colorDarkBlue,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            const SpaceVertical(15),
            Material(
              child: SubmitButton(
                title: AppStringKey.strOkay.tr,
                onClick: () {
                  Get.back();
                  callBack();
                },
              ),
            )
          ],
        ),
      ),
    ),
  );
}

void noPainDialog(BuildContext context, PtPainSymptomController controller, Function callBack) {
  showDialog(
    context: context,
    builder: (BuildContext context) => Center(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20),
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: Material(
          color: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Image.asset(
                    AppImages.closePainYes,
                    width: 24,
                    height: 24,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  AppStringKey.strEnterThePain.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    12,
                    AppColors.colorDarkBlue,
                  ),
                ),
              ),
              const SpaceVertical(15),
              TextFormField(
                textCapitalization: TextCapitalization.sentences,
                cursorColor: AppColors.colorDarkBlue,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(
                    vertical: 8,
                    horizontal: 10,
                  ),
                  fillColor: AppColors.white,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25),
                    borderSide: const BorderSide(
                      color: AppColors.colorLightSkyBlue,
                      width: 1,
                    ),
                  ),
                  counterText: '',
                  hintText: '',
                  hintStyle: BaseStyle.textStyleNunitoSansRegular(
                    14,
                    AppColors.hintColor,
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(00),
                    borderSide: const BorderSide(
                      color: AppColors.colorLightSkyBlue,
                      width: 1,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(00),
                    borderSide: const BorderSide(
                      color: AppColors.colorRed,
                      width: 1,
                    ),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(00),
                    borderSide: const BorderSide(
                      color: AppColors.colorLightSkyBlue,
                      width: 1,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(00),
                    borderSide: const BorderSide(
                      color: AppColors.colorLightSkyBlue,
                      width: 1,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(00),
                    borderSide: const BorderSide(
                      color: AppColors.colorLightSkyBlue,
                      width: 1,
                    ),
                  ),
                ),
                keyboardType: TextInputType.text,
                obscuringCharacter: "*",
                controller: controller.painFromRecentInjuryController,
                textInputAction: TextInputAction.done,
                maxLines: 5,
                style: BaseStyle.textStyleDomineRegular(
                  12,
                  AppColors.colorDarkBlue,
                ),
              ),
              const SpaceVertical(15),
              Material(
                child: SubmitButton(
                  title: AppStringKey.strSubmit.tr,
                  onClick: () {
                    callBack();
                  },
                ),
              )
            ],
          ),
        ),
      ),
    ),
  );
}
