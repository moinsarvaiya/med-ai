import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/ui_constant.dart';

import '../../../../api_patient/patient_api_end_point.dart';
import '../../../../api_patient/patient_api_interface.dart';
import '../../../../api_patient/patient_api_presenter.dart';
import '../../../../api_patient/patient_response_key.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../models/dr_patient_temperture_model.dart';
import '../../../../models/patient_models/pt_submit_symtpoms_requestbody_model.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/app_preference/patient_preferences.dart';

class PtPainSymptomController extends BaseController implements PatientApiCallBacks {
  final RxBool _isLoading = false.obs;

  bool get isLoading => _isLoading.value;

  set isLoading(bool value) => _isLoading.value = value;
  var painFromRecentInjuryController = TextEditingController();
  RxList<Rx<DrPatientTemperatureModel>> temperatures = [
    DrPatientTemperatureModel(temperature: 'Mild', temperatureValue: 3, isSelected: false).obs,
    DrPatientTemperatureModel(temperature: 'Moderate', temperatureValue: 6, isSelected: false).obs,
    DrPatientTemperatureModel(temperature: 'Extreme', temperatureValue: 9, isSelected: false).obs,
  ].obs;
  var fromMyAppointment = false.obs;
  RxList<String> painFromRecentInjury = [
    'Yes',
    'No',
  ].obs;
  RxList<String> whereList = RxList();
  RxList<String> typeList = RxList();
  RxList<String> durationList = RxList();
  RxList<String> oftenItComeList = RxList();
  RxList<String> painBecomeExtremeList = RxList();

  var isLevelOfPainSelected = false.obs;
  var levelOfPainValue = ''.obs;
  var temperatureValue = 0.obs;
  var isPainFromRecentInjuryExpand = false.obs;

  var isPainFromRecentInjurySelected = false.obs;
  var painFromRecentInjuryValue = ''.obs;
  var isWhereExpand = false.obs;

  var isWhereSelected = false.obs;
  var whereValue = ''.obs;
  var isTypeExpand = false.obs;

  var isTypeSelected = false.obs;
  var typeValue = ''.obs;
  var isDurationExpand = false.obs;

  var isDurationSelected = false.obs;
  var durationValue = ''.obs;
  var isOftenItComeExpand = false.obs;

  var isOftenItComeSelected = false.obs;
  var oftenItComeValue = ''.obs;
  var isPainBecomeExtremeExpand = false.obs;

  var isPainBecomeExtremeSelected = false.obs;
  var painBecomeExtremeValue = ''.obs;

  @override
  void onInit() {
    super.onInit();
    getData();
    getPainData();
  }

  void getData() {
    if (Get.arguments != null) {
      fromMyAppointment.value = Get.arguments['from'] == EnumProfileSectionFrom.MY_APPOINTMENT;
    }
  }

  void getPainData() {
    PatientApiPresenter(this).getPainOptionData(PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng');
  }

  void submitSpecialSymptomData() {
    final specialSymptomsRequestBody = createSpecialSymptomsRequestBody();
    final submitSymptomRequestBodyModel = createSubmitSymptomRequestBodyModel(specialSymptomsRequestBody);
    PatientApiPresenter(this).submitSpecialSymptomData(submitSymptomRequestBodyModel.toJson());
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    this.isLoading = isLoading;
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.getSpecialSymptomPainData:
        whereList.addAll((object[PatientResponseKey.painLocation] as List).map((e) => e as String).toList());
        typeList.addAll((object[PatientResponseKey.painCharacter] as List).map((e) => e as String).toList());
        durationList.addAll((object[PatientResponseKey.painOnset] as List).map((e) => e as String).toList());
        oftenItComeList.addAll((object[PatientResponseKey.frequency] as List).map((e) => e as String).toList());
        painBecomeExtremeList.addAll((object[PatientResponseKey.painExacerbates] as List).map((e) => e as String).toList());
        break;
      case PatientApiEndPoints.submitSpecialSymptoms:
        if (fromMyAppointment.value) {
          Get.toNamed(RoutePaths.PT_FOLLOWUP_REVIEW);
        } else {
          Get.back(result: object[PatientResponseKey.mappedSymptom].toString());
        }
        break;
    }
  }

  SpecialSymptomsRequestBody createSpecialSymptomsRequestBody() {
    return SpecialSymptomsRequestBody(
      fever: false,
      cough: false,
      pain: true,
      symptomLevel: temperatureValue.value,
      site: whereValue.toString(),
      onset: durationValue.toString(),
      frequency: oftenItComeValue.toString(),
      duration: 'None',
      painCharacter: typeValue.toString(),
      painExacerbating: painBecomeExtremeValue.toString(),
      coughType: 'None',
      coughTrigger: 'None',
    );
  }

  SubmitSymptomRequestBodyModel createSubmitSymptomRequestBodyModel(
    SpecialSymptomsRequestBody specialSymptomsRequestBody,
  ) {
    return SubmitSymptomRequestBodyModel(
      specialSymptoms: specialSymptomsRequestBody,
      lang: PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng',
    );
  }
}
