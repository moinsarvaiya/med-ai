import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/space_vertical.dart';
import 'pt_cough_symptom_controller.dart';

class PtCoughSymptomScreen extends GetView<PtCoughSymptomController> {
  const PtCoughSymptomScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Get.back(result: 'cough');
        return Future.value(false);
      },
      child: Scaffold(
        backgroundColor: AppColors.white,
        bottomNavigationBar: SizedBox(
          height: BaseSize.height(20),
          child: Column(
            children: [
              Obx(
                () => controller.isTypeSelected.value && controller.isDurationSelected.value && controller.isHowItStartedSelected.value
                    ? SubmitButton(
                        title: AppStringKey.strComplete.tr,
                        titleFontSize: 14,
                        isBottomMargin: false,
                        onClick: () {
                          controller.submitSpecialSymptomData();
                        },
                      )
                    : const SizedBox.shrink(),
              ),
              SpaceVertical(
                BaseSize.height(2),
              ),
              SubmitButton(
                title: AppStringKey.strSkip.tr,
                backgroundColor: AppColors.white,
                borderColor: AppColors.colorDarkBlue,
                titleColor: AppColors.colorDarkBlue,
                titleFontSize: 14,
                onClick: () {
                  Get.back(result: 'cough');
                },
              ),
            ],
          ).paddingAll(20),
        ),
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(defaultAppBarHeight),
          child: CustomAppBar(
            isDividerVisible: true,
            onClick: () {
              Get.back(result: 'cough');
            },
          ),
        ),
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.titleCough.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      20,
                      AppColors.colorDarkOrange,
                    ),
                  ),
                  const SpaceVertical(10),
                  Text(
                    AppStringKey.titleBreakSymptom.tr,
                    style: BaseStyle.textStyleNunitoSansBold(
                      16,
                      AppColors.colorDarkBlue,
                    ),
                  ),
                  const SpaceVertical(20),
                  // Type widget
                  TypeListWidget(controller: controller),
                  Obx(() => !controller.isTypeSelected.value ? const SpaceVertical(20) : const SizedBox.shrink()),
                  Obx(() => controller.isDurationExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(10),
                  // Duration widget
                  Obx(() => controller.isDurationExpand.value ? DurationListWidget(controller: controller) : const SizedBox.shrink()),
                  Obx(() => controller.isDurationSelected.value ? const SpaceVertical(20) : const SizedBox.shrink()),
                  Obx(() => controller.howItStartedExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(10),
                  // How It Started widget
                  Obx(() => controller.howItStartedExpand.value ? OftenItComesWidget(controller: controller) : const SizedBox.shrink()),
                  Obx(() => controller.isHowItStartedSelected.value ? const SpaceVertical(20) : const SizedBox.shrink()),
                  // Obx(() => controller.traveledRecentlyExpand.value ? const Divider(color: Colors.grey) : const SizedBox.shrink()),
                  const SpaceVertical(40),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class TypeListWidget extends StatelessWidget {
  final PtCoughSymptomController controller;

  const TypeListWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                AppStringKey.strType.tr,
                style: BaseStyle.textStyleNunitoSansBold(
                  14,
                  AppColors.colorDarkBlue,
                ),
              ).marginOnly(bottom: 0),
              Obx(
                () => controller.isTypeSelected.value
                    ? SelectedValueItemWidget(
                        itemValue: controller.typeValue.value,
                        callBack: () {
                          controller.isTypeSelected.value = false;
                        },
                      ).marginOnly(right: 10)
                    : const SizedBox.shrink(),
              )
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isTypeSelected.value
                ? Wrap(
                    children: controller.typeList
                        .map(
                          (element) => GestureDetector(
                            onTap: () {
                              controller.isTypeSelected.value = true;
                              controller.typeValue.value = element;
                              controller.isDurationExpand.value = true;
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                                horizontal: 15,
                              ),
                              decoration: BoxDecoration(
                                color: AppColors.colorBackground,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    element,
                                    style: BaseStyle.textStyleNunitoSansMedium(
                                      14,
                                      AppColors.colorDarkBlue,
                                    ),
                                  ),
                                ],
                              ),
                            ).marginOnly(right: 10, bottom: 10),
                          ),
                        )
                        .toList(),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}

class DurationListWidget extends StatelessWidget {
  final PtCoughSymptomController controller;

  const DurationListWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                AppStringKey.strDuration.tr,
                style: BaseStyle.textStyleNunitoSansBold(
                  14,
                  AppColors.colorDarkBlue,
                ),
              ).marginOnly(bottom: 0),
              Obx(
                () => controller.isDurationSelected.value
                    ? SelectedValueItemWidget(
                        itemValue: controller.durationValue.value,
                        callBack: () {
                          controller.isDurationSelected.value = false;
                        },
                      ).marginOnly(right: 10)
                    : const SizedBox.shrink(),
              )
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isDurationSelected.value
                ? Wrap(
                    children: controller.durationList
                        .map(
                          (element) => GestureDetector(
                            onTap: () {
                              controller.isDurationSelected.value = true;
                              controller.durationValue.value = element;
                              controller.howItStartedExpand.value = true;
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                                horizontal: 15,
                              ),
                              decoration: BoxDecoration(
                                color: AppColors.colorBackground,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    element,
                                    style: BaseStyle.textStyleNunitoSansMedium(
                                      14,
                                      AppColors.colorDarkBlue,
                                    ),
                                  ),
                                ],
                              ),
                            ).marginOnly(right: 10, bottom: 10),
                          ),
                        )
                        .toList(),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}

class OftenItComesWidget extends StatelessWidget {
  final PtCoughSymptomController controller;

  const OftenItComesWidget({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  AppStringKey.strHowItStarted.tr,
                  style: BaseStyle.textStyleNunitoSansBold(
                    14,
                    AppColors.colorDarkBlue,
                  ),
                ).marginOnly(bottom: 0),
              ),
              Obx(() => controller.isHowItStartedSelected.value
                  ? SelectedValueItemWidget(
                      itemValue: controller.howItStartedValue.value,
                      callBack: () {
                        controller.isHowItStartedSelected.value = false;
                      },
                    ).marginOnly(right: 10)
                  : const SizedBox.shrink())
            ],
          ),
          const SpaceVertical(10),
          Obx(
            () => !controller.isHowItStartedSelected.value
                ? Wrap(
                    children: controller.howItStartedList
                        .map(
                          (element) => GestureDetector(
                            onTap: () {
                              controller.isHowItStartedSelected.value = true;
                              controller.howItStartedValue.value = element;
                              controller.traveledRecentlyExpand.value = true;
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                                horizontal: 15,
                              ),
                              decoration: BoxDecoration(
                                color: AppColors.colorBackground,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    element,
                                    style: BaseStyle.textStyleNunitoSansMedium(
                                      14,
                                      AppColors.colorDarkBlue,
                                    ),
                                  ),
                                ],
                              ),
                            ).marginOnly(right: 10, bottom: 10),
                          ),
                        )
                        .toList(),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}

class SelectedValueItemWidget extends StatelessWidget {
  final String itemValue;
  final Function()? callBack;

  const SelectedValueItemWidget({Key? key, required this.itemValue, required this.callBack}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callBack,
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 15,
        ),
        decoration: BoxDecoration(
          color: AppColors.colorBackground,
          borderRadius: BorderRadius.circular(30),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              itemValue,
              style: BaseStyle.textStyleNunitoSansMedium(
                14,
                AppColors.colorDarkBlue,
              ),
            ),
            const Icon(
              Icons.cancel,
              color: Colors.black,
              size: 16,
            ).paddingOnly(left: 5),
          ],
        ),
      ).marginOnly(right: 10, bottom: 00),
    );
  }
}
