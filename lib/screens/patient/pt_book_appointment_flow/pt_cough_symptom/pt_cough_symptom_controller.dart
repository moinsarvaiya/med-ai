import 'package:get/get.dart';
import 'package:med_ai/constant/base_extension.dart';

import '../../../../api_patient/patient_api_end_point.dart';
import '../../../../api_patient/patient_api_interface.dart';
import '../../../../api_patient/patient_api_presenter.dart';
import '../../../../api_patient/patient_response_key.dart';
import '../../../../bindings/base_controller.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../models/dr_patient_temperture_model.dart';
import '../../../../models/patient_models/pt_submit_symtpoms_requestbody_model.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/app_preference/patient_preferences.dart';

class PtCoughSymptomController extends BaseController implements PatientApiCallBacks {
  final RxBool _isLoading = false.obs;

  bool get isLoading => _isLoading.value;

  set isLoading(bool value) => _isLoading.value = value;
  RxList<Rx<DrPatientTemperatureModel>> temperatures = List.generate(
    11,
    (index) => DrPatientTemperatureModel(temperature: '${96 + index}°F', isSelected: false).obs,
  ).obs;
  var fromMyAppointment = false.obs;
  RxList<String> typeList = RxList();
  RxList<String> durationList = RxList();
  RxList<String> howItStartedList = RxList();

  var isTypeSelected = false.obs;
  var typeValue = ''.obs;
  var isDurationExpand = false.obs;

  var isDurationSelected = false.obs;
  var durationValue = ''.obs;
  var howItStartedExpand = false.obs;

  var isHowItStartedSelected = false.obs;
  var howItStartedValue = ''.obs;
  var traveledRecentlyExpand = false.obs;

  @override
  void onInit() {
    super.onInit();
    getData();
    getCoughData();
  }

  void getData() {
    if (Get.arguments != null) {
      fromMyAppointment.value = Get.arguments['from'] == EnumProfileSectionFrom.MY_APPOINTMENT;
    }
  }

  void getCoughData() {
    PatientApiPresenter(this).getCoughOptionData(PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng');
  }

  void submitSpecialSymptomData() {
    final specialSymptomsRequestBody = createSpecialSymptomsRequestBody();
    final submitSymptomRequestBodyModel = createSubmitSymptomRequestBodyModel(specialSymptomsRequestBody);
    PatientApiPresenter(this).submitSpecialSymptomData(submitSymptomRequestBodyModel.toJson());
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    this.isLoading = isLoading;
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.getSpecialSymptomCoughData:
        typeList.addAll((object[PatientResponseKey.coughType] as List).map((e) => e as String).toList());
        durationList.addAll((object[PatientResponseKey.coughDuration] as List).map((e) => e as String).toList());
        howItStartedList.addAll((object[PatientResponseKey.cougTrigger] as List).map((e) => e as String).toList());
        break;
      case PatientApiEndPoints.submitSpecialSymptoms:
        if (fromMyAppointment.value) {
          Get.toNamed(RoutePaths.PT_FOLLOWUP_REVIEW);
        } else {
          Get.back(result: object[PatientResponseKey.mappedSymptom].toString());
        }
        break;
    }
  }

  SpecialSymptomsRequestBody createSpecialSymptomsRequestBody() {
    return SpecialSymptomsRequestBody(
      fever: false,
      cough: true,
      pain: false,
      symptomLevel: 0,
      site: 'None',
      onset: 'None',
      frequency: 'None',
      duration: durationValue.toString(),
      painCharacter: 'None',
      painExacerbating: 'None',
      coughType: typeValue.toString(),
      coughTrigger: howItStartedValue.toString(),
    );
  }

  SubmitSymptomRequestBodyModel createSubmitSymptomRequestBodyModel(
    SpecialSymptomsRequestBody specialSymptomsRequestBody,
  ) {
    return SubmitSymptomRequestBodyModel(
      specialSymptoms: specialSymptomsRequestBody,
      lang: PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng',
    );
  }
}
