import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/screens/patient/pt_select_patient_profile/pt_select_patient_profile_controller.dart';
import 'package:med_ai/screens/patient/pt_select_patient_profile/pt_select_patient_profile_screen.dart';

import '../pt_home/pt_home_screen.dart';
import '../pt_more_services_flow/pt_more_services/pt_more_services_screen.dart';
import '../pt_setting_flow/pt_settings/pt_setting_screen.dart';

class PtBottomNavigationController extends BaseController {
  var tabIndex = 0.obs;
  double iconSize = 20;
  var backPressDetected = false.obs;
  EdgeInsetsGeometry margin = const EdgeInsets.only(bottom: 5);

  void changeTabIndexPt(int index) {
    tabIndex.value = index;
    if (tabIndex.value == 1) {
      profileSelectionFrom = EnumProfileSectionFrom.MY_APPOINTMENT;
      Get.find<PtSelectPatientProfileController>().getBookAppointmentProfileDetails();
    }
  }

  var pages = [
    const PtHomeScreen(),
    const PtSelectPatientProfileScreen(),
    const PtSettingScreen(),
    const PtMoreServicesScreen(),
  ];

  timer() async {
    var duration = const Duration(seconds: 2);
    return Timer(duration, data);
  }

  data() {
    backPressDetected.value = false;
  }
}
