import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/patient/pt_payment_history_flow/pt_payment_history/pt_payment_history_controller.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/ripple_effect_widget.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/base_style.dart';

class PtPaymentHistoryScreen extends GetView<PtPaymentHistoryController> {
  const PtPaymentHistoryScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          onClick: () {
            Get.back();
          },
          isDividerVisible: true,
          titleName: AppStringKey.titlePaymentHistory.tr,
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: controller.paymentStatus.length,
            itemBuilder: (context, i) {
              return ItemPaymentHistory(
                paymentStatus: controller.paymentStatus[i],
              );
            }).paddingOnly(left: 16, right: 16, top: 20),
      ),
    );
  }
}

class ItemPaymentHistory extends StatelessWidget {
  final int paymentStatus;

  const ItemPaymentHistory({
    Key? key,
    required this.paymentStatus,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      widget: RippleEffectWidget(
        borderRadius: 5,
        callBack: () {
          Get.toNamed(RoutePaths.PT_PAYMENT_DETAILS);
        },
        widget: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '29 Apr, 2023',
                  style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                ),
                const SpaceVertical(2),
                Text(
                  '10:54 AM',
                  style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  '700BDT',
                  style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                ),
                const SpaceVertical(2),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: paymentStatus == 0
                          ? AppColors.colorPending
                          : paymentStatus == 1
                              ? AppColors.colorMint
                              : AppColors.colorRefunded),
                  child: Text(
                    paymentStatus == 0
                        ? AppStringKey.labelPending.tr
                        : paymentStatus == 1
                            ? AppStringKey.labelPaid.tr
                            : AppStringKey.labelRefunded.tr,
                    style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                  ),
                ),
              ],
            ),
          ],
        ).paddingAll(15),
      ),
    ).marginOnly(bottom: 10);
  }
}
