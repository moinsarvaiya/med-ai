import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/screens/patient/pt_payment_history_flow/pt_payment_details/pt_payment_details_controller.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../widgets/space_vertical.dart';

class PtPaymentDetailsScreen extends GetView<PtPaymentDetailsController> {
  const PtPaymentDetailsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          onClick: () {
            Get.back();
          },
          isDividerVisible: true,
        ),
      ),
      body: CustomCardWidget(
        widget: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '29 Apr, 2023',
                      style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                    ),
                    const SpaceVertical(2),
                    Text(
                      '10:54 AM',
                      style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      '700BDT',
                      style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                    ),
                    const SpaceVertical(2),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: controller.paymentStatus == 0
                              ? AppColors.colorPending
                              : controller.paymentStatus == 1
                                  ? AppColors.colorMint
                                  : AppColors.colorRefunded),
                      child: Text(
                        controller.paymentStatus == 0
                            ? AppStringKey.labelPending.tr
                            : controller.paymentStatus == 1
                                ? AppStringKey.labelPaid.tr
                                : AppStringKey.labelRefunded.tr,
                        style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SpaceVertical(25),
            ItemPaymentDetails(label: AppStringKey.labelReferenceID.tr, value: 'I8P38145278'),
            ItemPaymentDetails(label: AppStringKey.labelTrxID.tr, value: '98452417454BLPK'),
            ItemPaymentDetails(label: AppStringKey.labelAppointmentID.tr, value: '5484574252011210147'),
            ItemPaymentDetails(label: AppStringKey.labelDoctorName.tr, value: 'Dr.Fazle Elahi'),
            ItemPaymentDetails(label: AppStringKey.labelAmount.tr, value: '200  BDT'),
          ],
        ).paddingAll(15),
      ).paddingAll(16),
    );
  }
}

class ItemPaymentDetails extends StatelessWidget {
  final String label;
  final String value;

  const ItemPaymentDetails({
    Key? key,
    required this.label,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorTextDarkGray),
        ),
        const SpaceVertical(2),
        Text(
          value,
          style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
        ),
        const SpaceVertical(3),
        const Divider(
          color: AppColors.colorLightSkyBlue,
        ),
      ],
    ).marginOnly(bottom: 6);
  }
}
