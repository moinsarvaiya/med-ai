import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/utils/utilities.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../routes/route_paths.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/space_vertical.dart';
import 'pt_profile_controller.dart';

class PtProfileScreen extends GetView<PtProfileController> {
  const PtProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          displayLogo: false,
          isBackButtonVisible: true,
          isDividerVisible: true,
          titleName: AppStringKey.titleMyProfile.tr,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 10,
        ),
        child: ScrollConfiguration(
          behavior: MyBehavior(),
          child: const SingleChildScrollView(
            child: Column(
              children: [
                PatientProfileDetail(
                  patientProfileImage: AppImages.dummyProfile,
                  patientName: "Ricky J",
                  patientState: 'Noakhali',
                  patientCountry: 'Bangladesh',
                ),
                SpaceVertical(15),
                ProfileDetailSection(),
                SpaceVertical(10),
                VerifiedAccountSection(
                  emailValue: 'test@gmail.com',
                  mobileValue: '-',
                  isEmailVerified: true,
                  isMobileVerified: false,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class VerifiedAccountSection extends StatelessWidget {
  final String emailValue;
  final String mobileValue;
  final bool isEmailVerified;
  final bool isMobileVerified;

  const VerifiedAccountSection({
    Key? key,
    required this.emailValue,
    required this.mobileValue,
    required this.isEmailVerified,
    required this.isMobileVerified,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 10,
      ),
      width: double.maxFinite,
      padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 3,
            blurRadius: 3,
            offset: const Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Email',
                style: BaseStyle.textStyleNunitoSansRegular(
                  12,
                  AppColors.colorDarkBlue,
                ),
              ),
              Text(
                emailValue,
                style: BaseStyle.textStyleNunitoSansRegular(
                  12,
                  Colors.grey,
                ),
              ),
              isEmailVerified
                  ? Text(
                      AppStringKey.strVerified.tr,
                      style: BaseStyle.textStyleNunitoSansSemiBold(
                        12,
                        AppColors.colorGreen,
                      ),
                    )
                  : GestureDetector(
                      onTap: () {
                        Get.toNamed(RoutePaths.PT_VERIFY_EMAIL_NUMBER, arguments: {'isFromNumber': false});
                      },
                      child: Container(
                        width: 45,
                        height: 20,
                        decoration: BoxDecoration(
                          color: AppColors.colorDarkBlue,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Center(
                          child: Text(
                            AppStringKey.strAdd.tr.toUpperCase(),
                            style: BaseStyle.textStyleNunitoSansSemiBold(
                              12,
                              AppColors.white,
                            ),
                          ),
                        ),
                      ),
                    )
            ],
          ),
          const SpaceVertical(20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppStringKey.strMobile.tr,
                style: BaseStyle.textStyleNunitoSansRegular(
                  12,
                  AppColors.colorDarkBlue,
                ),
              ),
              Text(
                mobileValue,
                style: BaseStyle.textStyleNunitoSansRegular(
                  12,
                  Colors.grey,
                ),
              ),
              isMobileVerified
                  ? Text(
                      AppStringKey.strVerified.tr,
                      style: BaseStyle.textStyleNunitoSansSemiBold(
                        12,
                        AppColors.colorGreen,
                      ),
                    )
                  : GestureDetector(
                      onTap: () {
                        Get.toNamed(RoutePaths.PT_VERIFY_EMAIL_NUMBER, arguments: {'isFromNumber': true});
                      },
                      child: Container(
                        width: 45,
                        height: 20,
                        decoration: BoxDecoration(
                          color: AppColors.colorDarkBlue,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Center(
                          child: Text(
                            AppStringKey.strAdd.tr.toUpperCase(),
                            style: BaseStyle.textStyleNunitoSansSemiBold(
                              12,
                              AppColors.white,
                            ),
                          ),
                        ),
                      ),
                    )
            ],
          ),
        ],
      ),
    );
  }
}

class PatientProfileDetail extends StatelessWidget {
  final String patientProfileImage;
  final String patientName;
  final String patientState;
  final String patientCountry;

  const PatientProfileDetail({
    Key? key,
    required this.patientProfileImage,
    required this.patientName,
    required this.patientState,
    required this.patientCountry,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 10,
      ),
      width: double.maxFinite,
      padding: const EdgeInsets.symmetric(vertical: 14),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 3,
            blurRadius: 3,
            offset: const Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 94,
            width: 94,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey.withOpacity(0.5), width: 0.5),
              borderRadius: BorderRadius.circular(50),
              image: const DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(
                  AppImages.dummyProfile,
                ),
              ),
            ),
          ),
          const SpaceVertical(10),
          Text(
            patientName,
            style: BaseStyle.textStyleDomineBold(
              16,
              AppColors.colorDarkBlue,
            ),
          ),
          const SpaceVertical(4),
          Text(
            '32 Years Old, B+ve',
            style: BaseStyle.textStyleNunitoSansBold(
              10,
              AppColors.colorDarkBlue,
            ),
          ),
          const SpaceVertical(4),
          Text(
            '$patientState, $patientCountry',
            style: BaseStyle.textStyleNunitoSansBold(
              10,
              AppColors.colorDarkBlue,
            ),
          ),
          const SpaceVertical(4),
        ],
      ),
    );
  }
}

class ProfileDetailSection extends StatelessWidget {
  const ProfileDetailSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      width: double.maxFinite,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 3,
            blurRadius: 3,
            offset: const Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 20,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelProfileAge.tr,
              labelValue: '32 Years Old',
            ),
            const SpaceVertical(15),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelMonthlyEarning.tr,
              labelValue: '15000-20000 Expense/Month',
            ),
            const SpaceVertical(15),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelEducation.tr,
              labelValue: 'Highest Education HSC',
            ),
            const SpaceVertical(15),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelOccupation.tr,
              labelValue: 'Unknow in Profession',
            ),
            const SpaceVertical(15),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: MediaQuery.sizeOf(context).width / 3.5,
                      child: Text(
                        AppStringKey.labelReferralCodeWithoutValue.tr,
                        style: BaseStyle.textStyleNunitoSansRegular(
                          12,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                    ),
                    Text(
                      'PTPPL8',
                      style: BaseStyle.textStyleNunitoSansRegular(
                        12,
                        AppColors.colorDarkBlue,
                      ),
                    )
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    Utilities.addTextToClipBoard(context, "PTPPL8");
                  },
                  child: Image.asset(
                    AppImages.setCopy,
                    width: 24,
                    height: 24,
                  ),
                )
              ],
            ),
            /*  const SpaceVertical(15),
            const ProfileDetailItemWidget(
              labelName: AppStringKey.labelArea.tr,
              labelValue: 'Dhaka',
            ),*/
            const SpaceVertical(10),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelArea.tr,
              labelValue: 'Dhaka',
            ),
            const SpaceVertical(10),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelAddress.tr,
              labelValue: 'ZakirHussain Road',
            ),
            const SpaceVertical(15),
            ProfileDetailItemWidget(
              labelName: AppStringKey.labelCountry.tr,
              labelValue: 'Bangladesh',
            ),
            const SpaceVertical(15),
            Padding(
              padding: const EdgeInsets.only(
                bottom: 0,
                top: 15,
                left: 20,
                right: 20,
              ),
              child: SubmitButton(
                title: AppStringKey.labelEditProfile.tr,
                onClick: () {
                  Get.toNamed(
                    RoutePaths.PT_EDIT_PROFILE,
                    arguments: {
                      'from_screen': 'profile',
                      'from_option': 'profile_edit',
                      'person_id': '',
                    },
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ProfileDetailItemWidget extends StatelessWidget {
  final String labelName;
  final String labelValue;

  const ProfileDetailItemWidget({
    Key? key,
    required this.labelName,
    required this.labelValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          width: MediaQuery.sizeOf(context).width / 3.5,
          child: Text(
            labelName,
            style: BaseStyle.textStyleNunitoSansRegular(
              12,
              AppColors.colorDarkBlue,
            ),
          ),
        ),
        Text(
          labelValue,
          style: BaseStyle.textStyleNunitoSansRegular(
            12,
            AppColors.colorDarkBlue,
          ),
        )
      ],
    );
  }
}
