import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_settings/pt_setting_controller.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';
import 'package:med_ai/utils/app_preference/patient_preferences.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_images.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';
import '../../../doctor/dr_setting_flow/dr_settings/dr_settings_screen.dart';

class PtSettingScreen extends GetView<PtSettingController> {
  const PtSettingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isBackButtonVisible: false,
          displayLogo: false,
          isDividerVisible: true,
          titleName: AppStringKey.titleSettings,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const PatientProfileDetail(
                  patientProfileImage: AppImages.dummyProfile,
                  patientName: "Ricky J",
                ),
                const SpaceVertical(20),
                SettingOptionSection(controller: controller),
                const SpaceVertical(30),
                Text(
                  AppStringKey.appVersion.tr,
                  style: BaseStyle.textStyleNunitoSansMedium(
                    12,
                    AppColors.colorDarkBlue,
                  ),
                ),
                const SpaceVertical(20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PatientProfileDetail extends StatelessWidget {
  final String patientProfileImage;
  final String patientName;

  const PatientProfileDetail({
    Key? key,
    required this.patientProfileImage,
    required this.patientName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      padding: const EdgeInsets.symmetric(vertical: 14),
      width: double.maxFinite,
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 94,
            width: 94,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey.withOpacity(0.5), width: 0.5),
              borderRadius: BorderRadius.circular(50),
              image: const DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(AppImages.dummyProfile),
              ),
            ),
          ),
          const SpaceVertical(10),
          Text(
            patientName,
            style: BaseStyle.textStyleDomineBold(
              16,
              AppColors.colorDarkBlue,
            ),
          ),
          const SpaceVertical(4),
        ],
      ),
    );
  }
}

class SettingOptionSection extends StatelessWidget {
  final PtSettingController controller;

  const SettingOptionSection({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      width: double.maxFinite,
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            color: Colors.white,
            child: InkWell(
              splashColor: AppColors.colorDarkBlue,
              onTap: () {
                Get.toNamed(RoutePaths.PT_PROFILE);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SettingOptionItemWidget(
                    icon: AppImages.setProfile,
                    optionName: AppStringKey.labelMyProfile.tr,
                  ),
                  const Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                  )
                ],
              ),
            ),
          ),
          const CustomDivider(),
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            color: Colors.white,
            child: InkWell(
              onTap: () {
                Get.toNamed(RoutePaths.PT_MULTI_PROFILE);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SettingOptionItemWidget(
                    icon: AppImages.otherProfile,
                    optionName: AppStringKey.labelOtherProfile.tr,
                  ),
                  const Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                  )
                ],
              ),
            ),
          ),
          const CustomDivider(),
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            color: Colors.white,
            child: InkWell(
              onTap: () {
                Get.toNamed(RoutePaths.PT_PAYMENT_HISTORY);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SettingOptionItemWidget(
                    icon: AppImages.paymentHistory,
                    optionName: AppStringKey.labelPaymentHistory.tr,
                  ),
                  const Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                  )
                ],
              ),
            ),
          ),
          const CustomDivider(),
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SettingOptionItemWidget(
                  icon: AppImages.setChangeLanguage,
                  optionName: AppStringKey.labelChangeLanguage.tr,
                ),
                Row(
                  children: [
                    Text(
                      "বন।",
                      style: BaseStyle.textStyleNunitoSansSemiBold(14, AppColors.colorDarkBlue),
                    ),
                    const SpaceHorizontal(5),
                    Obx(() => FlutterSwitch(
                          width: 30,
                          height: 20,
                          valueFontSize: 0.0,
                          toggleSize: 15.0,
                          value: controller.switchChangeLanguage.value,
                          borderRadius: 30.0,
                          padding: 2.0,
                          activeColor: AppColors.colorGreen,
                          showOnOff: false,
                          onToggle: (val) {
                            if (val) {
                              Get.updateLocale(const Locale('bn', 'BN'));
                              PatientPreferences().sharedPrefWrite(StorageKeys.languageCode, 'ben');
                            } else {
                              Get.updateLocale(const Locale('en', 'EN'));
                              PatientPreferences().sharedPrefWrite(StorageKeys.languageCode, 'eng');
                            }
                            controller.switchChangeLanguage.value = !controller.switchChangeLanguage.value;
                          },
                        ))
                  ],
                )
              ],
            ),
          ),
          const CustomDivider(),
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SettingOptionItemWidget(
                  icon: AppImages.setReferralCode,
                  optionName: AppStringKey.labelReferralCode.tr,
                ),
                GestureDetector(
                    onTap: () {
                      Utilities.addTextToClipBoard(context, "DRS12S");
                    },
                    child: Image.asset(AppImages.setCopy, width: 24, height: 24)),
              ],
            ),
          ),
          const CustomDivider(),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SettingOptionItemWidget(
                  icon: AppImages.setShare,
                  optionName: AppStringKey.labelShareApp.tr,
                ),
              ],
            ),
          ),
          const CustomDivider(),
          InkWell(
            onTap: () {
              Utilities.commonDialog(
                context,
                AppStringKey.labelWarning.tr,
                AppStringKey.deleteDesc.tr,
                AppStringKey.deleteYes.tr,
                AppStringKey.deleteNo.tr,
                () {},
                // positive button
                () {
                  Get.back();
                }, // negative button
              );
            },
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SettingOptionItemWidget(
                    icon: AppImages.setDeleteAccount,
                    optionName: AppStringKey.labelDeleteAcc.tr,
                    labelColor: AppColors.colorPink,
                  ),
                ],
              ),
            ),
          ),
          const CustomDivider(),
          InkWell(
            onTap: () {
              Utilities.commonDialog(
                context,
                AppStringKey.logoutTitle.tr,
                AppStringKey.logoutDesc.tr,
                AppStringKey.deleteYes.tr,
                AppStringKey.deleteNo.tr,
                () {
                  LoginPreferences().sharedPrefEraseAllData();
                  Get.offNamedUntil(RoutePaths.SELECT_USER, (route) => false);
                },
                // positive button
                () {
                  Get.back();
                }, // negative button
              );
            },
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SettingOptionItemWidget(
                    icon: AppImages.setLogout,
                    optionName: AppStringKey.labelLogout.tr,
                  ),
                ],
              ),
            ),
          ),
          const CustomDivider(),
        ],
      ),
    );
  }
}

class SettingOptionItemWidget extends StatelessWidget {
  final String icon;
  final String optionName;
  final Color labelColor;

  const SettingOptionItemWidget({
    Key? key,
    required this.icon,
    required this.optionName,
    this.labelColor = AppColors.colorDarkBlue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image.asset(icon, width: 24, height: 24),
        const SpaceHorizontal(16),
        Text(
          optionName,
          style: BaseStyle.textStyleNunitoSansMedium(
            14,
            labelColor,
          ),
        )
      ],
    );
  }
}
