import 'package:get/get.dart';
import 'package:med_ai/api_patient/patient_api_end_point.dart';
import 'package:med_ai/api_patient/patient_api_interface.dart';
import 'package:med_ai/api_patient/patient_api_presenter.dart';
import 'package:med_ai/api_patient/patient_response_key.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/models/patient_models/pt_multi_profile_model.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';

import '../../../../routes/route_paths.dart';

class PtMultiProfileController extends BaseController implements PatientApiCallBacks {
  final RxBool _isLoading = true.obs;

  bool get isLoading => _isLoading.value;

  set isLoading(bool value) => _isLoading.value = value;

  RxList<ProfileList> multiProfileList = RxList();
  RxList<ProfileList> otherProfileBookAppointmentProfileList = RxList();
  RxList<ProfileList> mainProfileBookAppointmentProfileList = RxList();
  var patientCount = 0;

  @override
  void onInit() {
    super.onInit();
    getMultiProfile();
  }

  void getMultiProfile() {
    PatientApiPresenter(this).getMultiProfiles(
      LoginPreferences().sharedPrefRead(StorageKeys.username),
    );
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    this.isLoading = isLoading;
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.getPatientsProfile:
        patientCount = object[PatientResponseKey.profileCount];
        if (object[PatientResponseKey.profileCount] == 0) {
          Get.offNamed(RoutePaths.PT_EDIT_PROFILE, arguments: {
            'from_screen': 'multi_profile',
            'from_option': 'profile_add',
          });
        }
        multiProfileList.clear();
        multiProfileList.value = (object[PatientResponseKey.profileList] as List).map((e) => ProfileList.fromJson(e)).toList();
        manageResponse(multiProfileList);
        break;
    }
  }

  void manageResponse(List<ProfileList> profileList) {
    mainProfileBookAppointmentProfileList.clear();
    otherProfileBookAppointmentProfileList.clear();
    for (var element in profileList) {
      if (element.is_primary_user!.toLowerCase() == 'yes') {
        print('elementName ${element.name}');
        mainProfileBookAppointmentProfileList.add(element);
        mainProfileBookAppointmentProfileList.refresh();
      } else {
        print('elementName1 ${element.name}');
        otherProfileBookAppointmentProfileList.add(element);
        otherProfileBookAppointmentProfileList.refresh();
      }
    }
  }
}
