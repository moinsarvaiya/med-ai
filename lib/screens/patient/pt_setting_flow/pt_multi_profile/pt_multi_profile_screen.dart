import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/models/patient_models/pt_multi_profile_model.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_multi_profile/pt_multi_profile_controller.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_size.dart';
import '../../../../routes/route_paths.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/subtitle_widget.dart';

class PtMultiProfileScreen extends GetView<PtMultiProfileController> {
  const PtMultiProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      bottomNavigationBar: SubmitButton(
        title: AppStringKey.labelAddNewProfile.tr,
        onClick: () {
          Get.toNamed(RoutePaths.PT_EDIT_PROFILE, arguments: {
            'from_screen': 'multi_profile',
            'from_option': 'profile_add',
            'person_id': '0',
          })?.then((value) {
            controller.getMultiProfile();
          });
        },
      ).paddingAll(20),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: Obx(
          () => !controller.isLoading
              ? SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SubTitleWidget(
                        subTitle: AppStringKey.labelMyProfile.tr,
                      ),
                      const SpaceVertical(20),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            ...controller.mainProfileBookAppointmentProfileList.map((element) {
                              return SizedBox(
                                width: BaseSize.width(45),
                                child: ItemProfile(profileList: element),
                              );
                            })
                          ],
                        ),
                      ),
                      const SpaceVertical(40),
                      Visibility(
                        visible: controller.otherProfileBookAppointmentProfileList.isNotEmpty,
                        child: SubTitleWidget(
                          subTitle: AppStringKey.titleOtherFamilyProfile.tr,
                        ),
                      ),
                      const SpaceVertical(25),
                      GridView.builder(
                        itemCount: controller.otherProfileBookAppointmentProfileList.length,
                        physics: const NeverScrollableScrollPhysics(),
                        primary: false,
                        shrinkWrap: true,
                        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 5.0,
                          mainAxisSpacing: 10.0,
                          mainAxisExtent: 230,
                        ),
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {},
                            child: ItemProfile(profileList: controller.otherProfileBookAppointmentProfileList[index]),
                          );
                        },
                      )
                    ],
                  ).paddingAll(20),
                )
              : const SizedBox.shrink(),
        ),
      ),
    );
  }
}

class ItemProfile extends StatelessWidget {
  final ProfileList profileList;

  const ItemProfile({
    Key? key,
    required this.profileList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      elevation: 3,
      borderRadius: 10,
      backgroundColor: AppColors.colorGrayBackground,
      widget: Column(
        children: [
          ClipOval(
            child: Image.network(
              profileList.profile_img!,
              width: 94,
              height: 94,
              fit: BoxFit.cover,
              errorBuilder: (context, error, stackTrace) {
                return Utilities.getPlaceHolder(width: 94, height: 94);
              },
            ),
          ),
          const SpaceVertical(10),
          Text(
            profileList.name ?? '',
            style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
          ),
          const SpaceVertical(4),
          Text(
            '${profileList.gender}, ${profileList.age}',
            style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorTextDarkGray),
          ),
          const SpaceVertical(15),
          IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Get.toNamed(RoutePaths.PT_EDIT_PROFILE, arguments: {
                      'from_screen': 'multi_profile',
                      'from_option': 'profile_view',
                      'person_id': profileList.person_id,
                    });
                  },
                  child: Image.asset(
                    AppImages.eye,
                    height: 24,
                    width: 24,
                  ),
                ),
                const VerticalDivider(
                  color: AppColors.colorLightSkyBlue,
                ),
                GestureDetector(
                  onTap: () {
                    Get.toNamed(RoutePaths.PT_EDIT_PROFILE, arguments: {
                      'from_screen': 'multi_profile',
                      'from_option': 'profile_edit',
                      'person_id': profileList.person_id,
                    });
                  },
                  child: Image.asset(
                    AppImages.edit,
                    height: 24,
                    width: 24,
                  ),
                ),
                const VerticalDivider(
                  color: AppColors.colorLightSkyBlue,
                ),
                GestureDetector(
                  onTap: () {
                    Utilities.commonDialog(
                      context,
                      AppStringKey.deleteWarning.tr,
                      AppStringKey.deleteProfileDesc.tr,
                      AppStringKey.deleteYes.tr,
                      AppStringKey.deleteNo.tr,
                      () {
                        Get.back();
                      },
                      () {
                        Get.back();
                      },
                    );
                  },
                  child: Image.asset(
                    AppImages.delete,
                    height: 24,
                    width: 24,
                  ),
                ),
              ],
            ),
          )
        ],
      ).paddingAll(15),
    );
  }
}
