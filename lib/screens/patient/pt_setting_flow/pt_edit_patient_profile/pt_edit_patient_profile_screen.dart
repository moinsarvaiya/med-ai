import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/utils/extension_functions.dart';

import '../../../../constant/app_colors.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/base_style.dart';
import '../../../../constant/ui_constant.dart';
import '../../../../utils/functions/custom_functions.dart';
import '../../../../utils/utilities.dart';
import '../../../../widgets/custom_appbar.dart';
import '../../../../widgets/custom_buttons.dart';
import '../../../../widgets/custom_dropdown_field.dart';
import '../../../../widgets/custom_round_text_field.dart';
import '../../../../widgets/space_horizontal.dart';
import '../../../../widgets/space_vertical.dart';
import 'pt_edit_patient_profile_controller.dart';

class PtEditPatientProfileScreen extends GetView<PtEditPatientProfileController> {
  const PtEditPatientProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (controller.stepperIndex.value == 1) {
          controller.stepperIndex.value = 0;
          controller.scrollToTop();
          return Future.value(false);
        } else {
          return Future.value(true);
        }
      },
      child: Scaffold(
        backgroundColor: AppColors.white,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(defaultAppBarHeight),
          child: CustomAppBar(
            titleName: controller.isFromView
                ? AppStringKey.titleViewProfile.tr
                : controller.isFromAdd
                    ? AppStringKey.titleAddProfile.tr
                    : AppStringKey.titleEditProfile.tr,
            isDividerVisible: true,
            onClick: () {
              if (controller.stepperIndex.value == 1) {
                controller.stepperIndex.value = 0;
                controller.scrollToTop();
              } else {
                Get.back();
              }
            },
          ),
        ),
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            controller: controller.scrollController,
            child: Column(
              children: [
                const SpaceVertical(20),
                StepperSection(controller: controller),
                const SpaceVertical(20),
                Obx(
                  () => controller.stepperIndex.value == 1
                      ? SecondStep(
                          controller: controller,
                        )
                      : FirstStep(
                          controller: controller,
                        ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class StepperSection extends StatelessWidget {
  final PtEditPatientProfileController controller;

  const StepperSection({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 40,
          width: 40,
          decoration: fillBoxDecoration(),
          child: Center(
            child: Text(
              "1",
              style: BaseStyle.textStyleNunitoSansMedium(
                16,
                AppColors.white,
              ),
            ),
          ),
        ),
        const SpaceHorizontal(10),
        Container(
          width: 50,
          height: 2,
          color: AppColors.colorLightSkyBlue,
        ),
        const SpaceHorizontal(10),
        Obx(
          () => Container(
            height: 40,
            width: 40,
            decoration: controller.stepperIndex.value == 0 ? borderBoxDecoration() : fillBoxDecoration(),
            child: Center(
              child: Text(
                "2",
                style: BaseStyle.textStyleNunitoSansMedium(
                  16,
                  controller.stepperIndex.value == 0 ? AppColors.colorDarkBlue : AppColors.white,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}

BoxDecoration borderBoxDecoration() {
  return BoxDecoration(
    border: Border.all(color: AppColors.colorDarkBlue),
    shape: BoxShape.circle,
    color: AppColors.white,
  );
}

BoxDecoration fillBoxDecoration() {
  return const BoxDecoration(
    shape: BoxShape.circle,
    color: AppColors.colorDarkBlue,
  );
}

class FirstStep extends StatelessWidget {
  final PtEditPatientProfileController controller;

  const FirstStep({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            Center(
              child: Stack(
                children: [
                  Container(
                    height: 90,
                    width: 90,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.grey.withOpacity(0.5), width: 0.5),
                    ),
                    child: ClipOval(
                      child: Obx(
                        () => controller.isFromAdd
                            ? Image.file(
                                controller.fileProfile.value ?? File(""),
                                errorBuilder: (context, error, stackTrace) {
                                  return Utilities.getPlaceHolder();
                                },
                              )
                            : Image.network(
                                controller.profileImage.value,
                                fit: BoxFit.cover,
                                errorBuilder: (context, error, stackTrace) {
                                  return Utilities.getPlaceHolder();
                                },
                              ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    right: 0,
                    child: GestureDetector(
                      onTap: controller.isFromAdd
                          ? () {
                              CustomFunctions.showImagePickBottomSheet(context).then(
                                (value) => controller.fileProfile.value = value,
                              );
                            }
                          : null,
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        decoration: const BoxDecoration(shape: BoxShape.circle, color: AppColors.colorDarkBlue),
                        child: const Icon(
                          Icons.camera_alt_outlined,
                          color: AppColors.white,
                          size: 18,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomRoundTextField(
                labelText: AppStringKey.labelFullName.tr,
                isRequireField: true,
                hintText: AppStringKey.hintFullName.tr,
                textInputType: TextInputType.text,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                textEditingController: controller.fullNameController.value,
                validatorText: AppStringKey.validationEnterFullName,
                keyBoardAction: TextInputAction.done,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomDropDownField(
                isRequireField: true,
                labelText: AppStringKey.labelCountry.tr,
                validationText: AppStringKey.validationEnterYourCountry.tr,
                hintTitle: AppStringKey.hintSelectCountry.tr,
                onChanged: (countryValue) {
                  controller.dropdownDistrictList.clear();
                  controller.countryController.value.text = countryValue!;
                  controller.countryName.value = countryValue;
                  if (controller.countryName.toLowerCase() != 'united kingdom') {
                    controller.getDropDownDivision();
                  } else {
                    controller.getDropDownDistrict();
                  }
                },
                items: controller.dropdownCountriesList.toNormalList(),
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                selectedValue: controller.countryController.value.text,
              ),
            ),
            Obx(
              () => Visibility(
                visible: controller.countryName.value.toLowerCase() != 'united kingdom',
                child: const SpaceVertical(20),
              ),
            ),
            Obx(
              () => Visibility(
                visible: controller.countryName.value.toLowerCase() != 'united kingdom',
                child: CustomDropDownField(
                  isRequireField: true,
                  labelText: AppStringKey.labelDivision.tr,
                  validationText: AppStringKey.validationEnterYourCountry.tr,
                  hintTitle: AppStringKey.hintSelectDivision.tr,
                  onChanged: (countryValue) {
                    controller.divisionController.value.text = countryValue!;
                    controller.getDropDownDistrict();
                  },
                  items: controller.dropdownDivisionList.toNormalList(),
                  backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                  isReadOnly: controller.isFromView ? true : false,
                  selectedValue: controller.divisionController.value.text,
                ),
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomDropDownField(
                isRequireField: true,
                labelText: AppStringKey.labelDistrict.tr,
                validationText: AppStringKey.validationEnterYourCountry.tr,
                hintTitle: AppStringKey.hintSelectDistrict.tr,
                onChanged: (countryValue) {
                  controller.districtController.value.text = countryValue!;
                },
                items: controller.dropdownDistrictList.toNormalList(),
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                selectedValue: controller.districtController.value.text,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomRoundTextField(
                labelText: AppStringKey.labelAddress.tr,
                isRequireField: true,
                hintText: AppStringKey.hintAddress.tr,
                textInputType: TextInputType.text,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                textEditingController: controller.addressController.value,
                validatorText: AppStringKey.validationEnterAddress,
                keyBoardAction: TextInputAction.next,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomRoundTextField(
                labelText: AppStringKey.labelPostCode.tr,
                isRequireField: true,
                hintText: AppStringKey.hintPostCode.tr,
                textInputType: TextInputType.number,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                textEditingController: controller.postCodeController.value,
                validatorText: AppStringKey.validationEnterPostCode,
                keyBoardAction: TextInputAction.next,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomRoundTextField(
                labelText: AppStringKey.labelMobile.tr,
                isRequireField: true,
                hintText: AppStringKey.hintMobile.tr,
                textInputType: TextInputType.number,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                textEditingController: controller.mobileController.value,
                validatorText: AppStringKey.validationEnterMobile,
                keyBoardAction: TextInputAction.done,
              ),
            ),
            const SpaceVertical(20),
            SubmitButton(
              title: AppStringKey.strNext.tr,
              onClick: () {
                if (controller.isFromView) {
                  controller.scrollToTop();
                  controller.stepperIndex.value = 1;
                } else {
                  if (formKey.currentState!.validate()) {
                    controller.scrollToTop();
                    controller.stepperIndex.value = 1;
                    controller.getDropDownValuesPage2();
                  }
                }
              },
            ).paddingOnly(
              left: 0,
              right: 0,
              top: 20,
              bottom: MediaQuery.viewInsetsOf(context).bottom + 20,
            ),
          ],
        ),
      ),
    );
  }
}

class SecondStep extends StatelessWidget {
  final PtEditPatientProfileController controller;

  const SecondStep({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            Visibility(
              visible: !controller.isFromView,
              child: Center(
                child: Text(
                  textAlign: TextAlign.center,
                  AppStringKey.strCreatePatientDesc.tr,
                  style: BaseStyle.textStyleDomineBold(
                    16,
                    AppColors.colorDarkBlue,
                  ),
                ),
              ),
            ),
            const SpaceVertical(30),
            Obx(
              () => CustomDropDownField(
                isRequireField: true,
                hintTitle: AppStringKey.labelProfileGender.tr,
                items: controller.dropdownGender.toNormalList(),
                validationText: AppStringKey.validationSelectGender.tr,
                onChanged: (String? val) {
                  controller.genderController.value.text = val!;
                },
                labelText: AppStringKey.labelProfileGender.tr,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                selectedValue: controller.genderController.value.text,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomRoundTextField(
                labelText: AppStringKey.labelProfileAge.tr,
                isRequireField: true,
                keyBoardAction: TextInputAction.next,
                hintText: AppStringKey.hintAge.tr,
                textInputType: TextInputType.number,
                inputFormatter: inputFormatterDigit,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                textEditingController: controller.ageController.value,
                validatorText: AppStringKey.validationEnterAge,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomDropDownField(
                isRequireField: true,
                hintTitle: AppStringKey.labelBloodGroup.tr,
                items: controller.bloodGroup,
                validationText: AppStringKey.validationSelectBloodGroop.tr,
                onChanged: (String? val) {
                  controller.bloodGroupController.value.text = val!;
                },
                labelText: AppStringKey.labelBloodGroup.tr,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                selectedValue: controller.bloodGroupController.value.text,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomDropDownField(
                isRequireField: true,
                hintTitle: AppStringKey.hintSelectArea.tr,
                items: controller.dropdownArea.toNormalList(),
                validationText: AppStringKey.validationSelectArea.tr,
                onChanged: (String? val) {
                  controller.areaController.value.text = val!;
                },
                labelText: AppStringKey.labelArea.tr,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                selectedValue: controller.areaController.value.text,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomDropDownField(
                isRequireField: false,
                hintTitle: AppStringKey.hintSelectLanguages.tr,
                items: controller.arrLanguages.toNormalList(),
                validationText: AppStringKey.validationSelectLanguage.tr,
                onChanged: (String? val) {
                  if (!controller.selectedLanguages.contains(val!)) {
                    controller.selectedLanguages.add(val);
                  }
                },
                labelText: AppStringKey.labelLanguages.tr,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                selectedValue: controller.isFromView ? controller.selectedLanguages[0] : '',
              ),
            ),
            const SpaceVertical(10),
            Obx(
              () => Align(
                alignment: Alignment.centerLeft,
                child: Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: controller.selectedLanguages
                      .map(
                        (element) => ItemLanguage(
                          language: element,
                          controller: controller,
                        ),
                      )
                      .toList(),
                ),
              ),
            ),
            const SpaceVertical(10),
            Obx(
              () => CustomDropDownField(
                isRequireField: false,
                hintTitle: AppStringKey.hintSelectMaritalStatus.tr,
                items: controller.dropdownMaritalStatus.toNormalList(),
                validationText: AppStringKey.validationSelectMaritalStatus.tr,
                onChanged: (String? val) {
                  controller.maritalStatusController.value.text = val!;
                },
                labelText: AppStringKey.labelMaritalStatusWithOptional.tr,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                selectedValue: controller.maritalStatusController.value.text,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomDropDownField(
                isRequireField: false,
                hintTitle: AppStringKey.hintSelectEducation.tr,
                items: controller.dropdownEducation.toNormalList(),
                validationText: AppStringKey.validationSelectEducation.tr,
                onChanged: (String? val) {
                  controller.educationController.value.text = val!;
                },
                labelText: AppStringKey.labelEducationWithOptional.tr,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                selectedValue: controller.educationController.value.text,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomDropDownField(
                isRequireField: false,
                hintTitle: AppStringKey.hintSelectOccupation.tr,
                items: controller.dropdownOccupation.toNormalList(),
                validationText: AppStringKey.validationSelectOccupation.tr,
                onChanged: (String? val) {
                  controller.occupationController.value.text = val!;
                },
                labelText: AppStringKey.labelOccupationWithOptional.tr,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                selectedValue: controller.occupationController.value.text,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomDropDownField(
                isRequireField: false,
                hintTitle: AppStringKey.hintSelectEthnicity.tr,
                items: controller.dropdownEthnicity.toNormalList(),
                validationText: AppStringKey.validationSelectEthnicity.tr,
                onChanged: (String? val) {
                  controller.ethnicityController.value.text = val!;
                },
                labelText: AppStringKey.labelEthnicityWithOptional.tr,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                selectedValue: controller.ethnicityController.value.text,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomDropDownField(
                isRequireField: false,
                hintTitle: AppStringKey.hintSelectMonthlyEarning.tr,
                items: controller.dropdownMonthlyEarning.toNormalList(),
                validationText: AppStringKey.validationSelectMonthlyEarning.tr,
                onChanged: (String? val) {
                  controller.monthlyEarningController.value.text = val!;
                },
                labelText: AppStringKey.labelMonthlyEarningWithOptional.tr,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                selectedValue: controller.monthlyEarningController.value.text,
              ),
            ),
            const SpaceVertical(20),
            Obx(
              () => CustomRoundTextField(
                labelText: AppStringKey.labelReferralCodeCreatePatient.tr,
                isRequireField: false,
                keyBoardAction: TextInputAction.done,
                hintText: AppStringKey.hintReferralCode.tr,
                backgroundColor: controller.isFromView ? AppColors.colorGrayBackground : AppColors.transparent,
                isReadOnly: controller.isFromView ? true : false,
                textEditingController: controller.referralCodeController.value,
                validatorText: AppStringKey.validationEnterReferralCode,
              ),
            ),
            const SpaceVertical(20),
            SubmitButton(
              title: controller.isFromView ? AppStringKey.back.tr : AppStringKey.strSave.tr,
              onClick: () {
                if (controller.isFromView) {
                  if (controller.fromScreen == 'multi_profile') {
                    Get.until((route) => Get.currentRoute == RoutePaths.PT_MULTI_PROFILE);
                  } else {
                    Get.until((route) => Get.currentRoute == RoutePaths.PT_PROFILE);
                  }
                } else {
                  if (formKey.currentState!.validate()) {
                    controller.submitPatientProfile();
                  }
                }
              },
            ).paddingOnly(
              left: 0,
              right: 0,
              top: 0,
              bottom: MediaQuery.viewInsetsOf(context).bottom + 20,
            ),
          ],
        ),
      ),
    );
  }
}

class ItemLanguage extends StatelessWidget {
  final String language;
  final int index;
  final PtEditPatientProfileController controller;

  const ItemLanguage({
    Key? key,
    required this.language,
    this.index = 0,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 8, bottom: 8, left: 15, right: 10),
      decoration: const BoxDecoration(
        color: AppColors.colorGrayBackground,
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            language,
            style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
          ),
          InkWell(
            onTap: !controller.isFromView
                ? () {
                    Get.find<PtEditPatientProfileController>().selectedLanguages.remove(language);
                  }
                : null,
            child: const Icon(
              Icons.cancel,
              color: AppColors.colorDarkBlue,
              size: 15,
            ).paddingAll(5),
          ),
        ],
      ),
    );
  }
}
