import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/api_patient/patient_api_end_point.dart';
import 'package:med_ai/api_patient/patient_api_interface.dart';
import 'package:med_ai/api_patient/patient_response_key.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/models/patient_models/pt_multi_user_details_model.dart';
import 'package:med_ai/screens/patient/pt_home/pt_home_controller.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_multi_profile/pt_multi_profile_controller.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';
import 'package:med_ai/utils/app_preference/patient_preferences.dart';
import 'package:med_ai/utils/extension_functions.dart';
import 'package:med_ai/utils/functions/custom_functions.dart';

import '../../../../api_patient/patient_api_presenter.dart';
import '../../../../bindings/base_controller.dart';
import '../../../../constant/app_strings_key.dart';
import '../../../../constant/storage_keys.dart';
import '../../../../routes/route_paths.dart';

class PtEditPatientProfileController extends BaseController implements PatientApiCallBacks {
  Rx<int> stepperIndex = 0.obs;
  late ScrollController scrollController;
  final _showBackToTopButton = false.obs;
  var fullNameController = TextEditingController().obs;
  var countryController = TextEditingController().obs;
  var divisionController = TextEditingController().obs;
  var districtController = TextEditingController().obs;
  var addressController = TextEditingController().obs;
  var postCodeController = TextEditingController().obs;
  var mobileController = TextEditingController().obs;
  var genderController = TextEditingController().obs;
  var ageController = TextEditingController().obs;
  var areaController = TextEditingController().obs;
  var referralCodeController = TextEditingController().obs;
  var bloodGroupController = TextEditingController().obs;
  var maritalStatusController = TextEditingController().obs;
  var educationController = TextEditingController().obs;
  var occupationController = TextEditingController().obs;
  var ethnicityController = TextEditingController().obs;
  var monthlyEarningController = TextEditingController().obs;
  var selectedLanguages = [].obs;
  RxString countryName = ''.obs;

  final RxBool _isLoading = false.obs;

  bool get isLoading => _isLoading.value;

  set isLoading(bool value) => _isLoading.value = value;
  RxList<String> dropdownCountriesList = [''].obs;
  RxList<String> dropdownDivisionList = [''].obs;
  RxList<String> dropdownDistrictList = [''].obs;
  RxList<String> dropdownGender = [''].obs;
  RxList<String> dropdownArea = [''].obs;
  RxList<String> dropdownMaritalStatus = [''].obs;
  RxList<String> dropdownEducation = [''].obs;
  RxList<String> dropdownOccupation = [''].obs;
  RxList<String> dropdownEthnicity = [''].obs;
  RxList<String> dropdownMonthlyEarning = [''].obs;
  Rx<PtMultiUserDetailsModel> userDetail = PtMultiUserDetailsModel().obs;
  var profileImage = ''.obs;
  Rx<File?> fileProfile = Rx<File?>(null);
  var bloodGroup = [
    'A+',
    'A-',
    'B+',
    'A-',
    'AB+',
    'AB-',
    'O+',
    'O-',
  ].obs;
  var arrLanguages = [
    'English',
    'Hindi',
    'Arabic',
    'Spanish',
  ].obs;
  String fromScreen = '';
  String fromOption = '';
  bool isFromView = false;
  bool isFromAdd = false;
  bool isFromEdit = false;
  String personId = '';

  @override
  void onInit() {
    scrollController = ScrollController()
      ..addListener(() {
        if (scrollController.offset >= 400) {
          _showBackToTopButton.value = true; // show the back-to-top button
        } else {
          _showBackToTopButton.value = false; // hide the back-to-top button
        }
      });

    // Values of passing from previous screen
    fromScreen = Get.arguments['from_screen'] ?? '';
    fromOption = Get.arguments['from_option'] ?? '';
    personId = Get.arguments['person_id'] ?? '';

    // Bool variables for easily handle different conditions
    isFromView = fromOption == 'profile_view';
    isFromAdd = fromOption == 'profile_add';
    isFromEdit = fromOption == 'profile_edit';

    if ((isFromAdd && fromScreen == 'multi_profile') || fromScreen == 'profile') {
      getDropDownCountries();
    }
    if (isFromView) {
      getUserDetails();
    }
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
    scrollController.dispose();
  }

  void scrollToTop() {
    scrollController.animateTo(0, duration: const Duration(milliseconds: 500), curve: Curves.linear);
  }

  void getUserDetails() {
    PatientApiPresenter(this).getUserDetails(personId);
  }

  void getDropDownCountries() {
    PatientApiPresenter(this).dropDownCountries(PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng');
  }

  void getDropDownValuesPage2() {
    PatientApiPresenter(this).getDropDownValuesPage2(PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng');
  }

  void getDropDownDivision() {
    PatientApiPresenter(this)
        .getDropDownDivision(PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng', countryController.value.text);
  }

  void getDropDownDistrict() {
    PatientApiPresenter(this).getDropDownDistrict(PatientPreferences().sharedPrefRead(StorageKeys.languageCode) ?? 'eng',
        countryController.value.text, divisionController.value.text);
  }

  void submitPatientProfile() {
    PatientApiPresenter(this).submitPatientRegistration(
      LoginPreferences().sharedPrefRead(StorageKeys.username),
      Get.find<PtMultiProfileController>().patientCount == 0 ? 'Yes' : 'No',
      mobileController.value.text,
      fullNameController.value.text.split(' ')[0],
      fullNameController.value.text.split(' ').length > 1 ? fullNameController.value.text.split(' ')[1] : ' ',
      '',
      selectedLanguages.map((element) => element.toString()).toList(),
      int.parse(ageController.value.text),
      CustomFunctions.getPatientBirthDate(ageController.value.text),
      genderController.value.text,
      occupationController.value.text.defaultIfEmpty(AppStringKey.strUnknown),
      bloodGroupController.value.text,
      addressController.value.text,
      postCodeController.value.text,
      divisionController.value.text,
      districtController.value.text,
      countryController.value.text,
      educationController.value.text.defaultIfEmpty(AppStringKey.strUnknown),
      monthlyEarningController.value.text.defaultIfEmpty(AppStringKey.strUnknown),
      ethnicityController.value.text.defaultIfEmpty(AppStringKey.strUnknown),
      areaController.value.text,
      maritalStatusController.value.text.defaultIfEmpty(AppStringKey.strUnknown),
      'Test',
      'Yes',
      'No Image!',
      referralCodeController.value.text.defaultIfEmpty(''),
    );
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    this.isLoading = isLoading;
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.getPatientDetails:
        userDetail.value = PtMultiUserDetailsModel.fromJson(object[PatientResponseKey.patientInfo]);
        manageUserResponse(userDetail.value);
        break;
      case PatientApiEndPoints.dropdownCountries:
        dropdownCountriesList.clear();
        dropdownCountriesList.addAll((object[PatientResponseKey.countries] as List).map((e) => e as String).toList());
        break;
      case PatientApiEndPoints.dropdownDivisions:
        dropdownDivisionList.clear();
        dropdownDivisionList.addAll((object[PatientResponseKey.divisions] as List).map((e) => e as String).toList());
        break;
      case PatientApiEndPoints.dropdownDistrictOrCountry:
        dropdownDistrictList.clear();
        dropdownDistrictList.addAll((object[PatientResponseKey.district] as List).map((e) => e as String).toList());
        break;
      case PatientApiEndPoints.dropdownValuesPage2:
        manageAllDropdownValuesOfPage2(object);
        break;
      case PatientApiEndPoints.patientRegistration:
        Get.find<PtHomeController>().isProfileAdded.value = true;
        if (fromScreen == 'multi_profile') {
          Get.until((route) => Get.currentRoute == RoutePaths.PT_MULTI_PROFILE);
        } else {
          Get.until((route) => Get.currentRoute == RoutePaths.PT_PROFILE);
        }
        break;
    }
  }

  void manageAllDropdownValuesOfPage2(dropDownListObject) {
    final dropdownsToUpdate = {
      dropdownGender: PatientResponseKey.gender,
      dropdownArea: PatientResponseKey.area,
      dropdownMaritalStatus: PatientResponseKey.maritalStatus,
      dropdownEducation: PatientResponseKey.education,
      dropdownOccupation: PatientResponseKey.occupation,
      dropdownEthnicity: PatientResponseKey.ethnicity,
      dropdownMonthlyEarning: PatientResponseKey.monthlyExpense,
    };

    dropdownsToUpdate.forEach((dropdown, responseKey) {
      final responseList = dropDownListObject[responseKey] as List;
      if (responseList.isNotEmpty) {
        dropdown.clear();
        dropdown.addAll(responseList.map((e) => e as String).toList());
      }
    });
  }

  void manageUserResponse(PtMultiUserDetailsModel user) {
    fullNameController.value.text = '${user.first_name} ${user.last_name}';

    dropdownCountriesList.clear();
    dropdownCountriesList.add(user.country!);
    countryController.value.text = user.country!;

    dropdownDivisionList.clear();
    dropdownDivisionList.add(user.region!);
    divisionController.value.text = user.region!;

    dropdownDistrictList.clear();
    dropdownDistrictList.add(user.city_vill!);
    districtController.value.text = user.city_vill!;

    addressController.value.text = user.address_line!;
    postCodeController.value.text = user.zip_post_code!;
    mobileController.value.text = user.phone!;
    genderController.value.text = user.gender!;

    arrLanguages.add("English");
    selectedLanguages.add("English");

    ageController.value.text = user.age!.toString();
    bloodGroupController.value.text = user.blood_group!;
    areaController.value.text = user.person_location!;
    maritalStatusController.value.text = user.marital_status!;
    educationController.value.text = user.education!;
    occupationController.value.text = user.occupation!;
    ethnicityController.value.text = user.ethnicity!;
    monthlyEarningController.value.text = user.monthly_expenditure!;
    referralCodeController.value.text = user.referral_code!;
    profileImage.value = user.profile_pic!;
  }
}
