import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_home_symptoms/pt_home_symptoms_controller.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_buttons.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:med_ai/widgets/space_vertical.dart';

class PtHomeSymptomsScreen extends GetView<PtHomeSymptomsController> {
  const PtHomeSymptomsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorGrayBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          isDividerVisible: true,
          onClick: () {
            Get.back();
          },
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              CustomCardWidget(
                widget: Column(
                  children: [
                    Row(
                      children: [
                        Obx(
                          () => Image.asset(
                            controller.symptomImage.value,
                            height: 40,
                            width: 40,
                          ),
                        ),
                        const SpaceHorizontal(10),
                        Obx(
                          () => Text(
                            controller.symptomName.value,
                            style: BaseStyle.textStyleNunitoSansSemiBold(18, AppColors.colorDarkBlue),
                          ),
                        ),
                      ],
                    ),
                    const Divider(
                      height: 25,
                    ),
                    Text(
                      controller.dummyText.value,
                      style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorTextDarkGray),
                    ),
                    const SpaceVertical(30),
                    SizedBox(
                      height: BaseSize.height(5),
                      width: BaseSize.width(60),
                      child: SubmitButton(
                        title: controller.isFromBookAppointment ? AppStringKey.back : AppStringKey.btnCheckUsingAIAssistant.tr,
                        titleFontSize: 14,
                        isBottomMargin: false,
                        onClick: () {
                          if (controller.isFromBookAppointment) {
                            profileSelectionFrom = EnumProfileSectionFrom.BOOK_APPOINTMENT;
                            Get.back();
                          } else {
                            if (!controller.fromHome.value) {
                              Get.until((route) => Get.currentRoute == RoutePaths.PT_SELECT_PATIENT_PROFILE);
                            } else {
                              Get.toNamed(
                                RoutePaths.PT_SELECT_PATIENT_PROFILE,
                              );
                            }
                          }
                        },
                      ),
                    ),
                    const SpaceVertical(10),
                  ],
                ).paddingAll(15),
              ).marginAll(15),
            ],
          ),
        ),
      ),
    );
  }
}
