import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/base_extension.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';

import '../../../api_patient/patient_api_end_point.dart';
import '../../../api_patient/patient_api_interface.dart';
import '../../../api_patient/patient_api_presenter.dart';
import '../../../api_patient/patient_response_key.dart';
import '../../../constant/app_strings_key.dart';
import '../../../constant/ui_constant.dart';
import '../../../models/patient_models/pt_book_appointment_model.dart';
import '../../../routes/route_paths.dart';

class PtSelectPatientProfileController extends BaseController implements PatientApiCallBacks {
  final RxBool _isLoading = false.obs;
  var patientCount = 0;

  bool get isLoading => _isLoading.value;

  set isLoading(bool value) => _isLoading.value = value;
  RxList<BookAppointmentProfileList> bookAppointmentProfileList = RxList();
  RxList<BookAppointmentProfileList> otherProfileBookAppointmentProfileList = RxList();
  RxList<BookAppointmentProfileList> mainProfileBookAppointmentProfileList = RxList();

  @override
  void onInit() {
    super.onInit();
    getBookAppointmentProfileDetails();
  }

  void getBookAppointmentProfileDetails() {
    PatientApiPresenter(this).getBookAppointmentProfileDetails(LoginPreferences().sharedPrefRead(StorageKeys.username));
  }

  String titleName(BuildContext context) {
    if (ModalRoute.of(context)!.isFirst) {
      return AppStringKey.labelMyAppointments;
    } else if (profileSelectionFrom == EnumProfileSectionFrom.BOOK_APPOINTMENT) {
      return AppStringKey.titlePatientProfile;
    } else if (profileSelectionFrom == EnumProfileSectionFrom.MY_APPOINTMENT) {
      return AppStringKey.labelMyAppointments;
    } else {
      return '';
    }
  }

  void profileRedirections() {
    if (profileSelectionFrom == EnumProfileSectionFrom.BOOK_APPOINTMENT) {
      Get.toNamed(RoutePaths.PT_VITAL_HEALTH_METRICS);
    } else if (profileSelectionFrom == EnumProfileSectionFrom.MY_APPOINTMENT) {
      Get.toNamed(RoutePaths.PT_APPOINTMENT_LIST);
    } else if (profileSelectionFrom == EnumProfileSectionFrom.HEALTH_RECORD) {
      Get.toNamed(RoutePaths.PT_HEALTH_RECORD1);
    }
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: AppStringKey.strTitleInternetConnection);
  }

  @override
  void onError(String errorMsg, String apiEndPoint) {
    errorMsg.showErrorSnackBar();
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    this.isLoading = isLoading;
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case PatientApiEndPoints.bookAppointmentProfile:
        patientCount = object[PatientResponseKey.profileCount];
        bookAppointmentProfileList.clear();
        bookAppointmentProfileList.value = (object[PatientResponseKey.profileList] as List)
            .map(
              (e) => BookAppointmentProfileList.fromJson(e),
            )
            .toList();
        manageResponse(bookAppointmentProfileList.value);
        break;
    }
  }

  void manageResponse(List<BookAppointmentProfileList> profileList) {
    mainProfileBookAppointmentProfileList.clear();
    otherProfileBookAppointmentProfileList.clear();
    for (var element in profileList) {
      if (element.is_primary_user!.toLowerCase() == 'yes') {
        mainProfileBookAppointmentProfileList.add(element);
        mainProfileBookAppointmentProfileList.refresh();
      } else {
        otherProfileBookAppointmentProfileList.add(element);
        otherProfileBookAppointmentProfileList.refresh();
      }
    }
  }
}
