import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/utils/utilities.dart';
import 'package:med_ai/widgets/custom_appbar.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/ripple_effect_widget.dart';
import 'package:med_ai/widgets/space_vertical.dart';
import 'package:med_ai/widgets/subtitle_widget.dart';

import '../../../constant/app_colors.dart';
import '../../../utils/app_preference/patient_preferences.dart';
import 'pt_select_patient_profile_controller.dart';

class PtSelectPatientProfileScreen extends GetView<PtSelectPatientProfileController> {
  const PtSelectPatientProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(defaultAppBarHeight),
        child: CustomAppBar(
          titleName: controller.titleName(context),
          onClick: () {
            Get.back();
          },
          isBackButtonVisible: !ModalRoute.of(context)!.isFirst,
          isDividerVisible: true,
        ),
      ),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: Obx(
          () => !controller.isLoading
              ? SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.titleSelectProfile.tr,
                        style: BaseStyle.textStyleNunitoSansBold(
                          16,
                          AppColors.colorDarkBlue,
                        ),
                      ),
                      const SpaceVertical(15),
                      SubTitleWidget(
                        subTitle: AppStringKey.labelMyProfile.tr,
                      ),
                      const SpaceVertical(20),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            ...controller.mainProfileBookAppointmentProfileList.map((element) {
                              return ItemSingleProfile(
                                name: element.name!,
                                email: element.mobile!,
                                imageUrl: element.profile_img!,
                                callback: () {
                                  PatientPreferences().sharedPrefWrite(StorageKeys.patientMobile, element.mobile!);
                                  PatientPreferences().sharedPrefWrite(StorageKeys.profileSelectionPersonId, element.person_id!);
                                  PatientPreferences().sharedPrefWrite(StorageKeys.age, element.age!);
                                  PatientPreferences().sharedPrefWrite(StorageKeys.gender, element.gender!);
                                  PatientPreferences().sharedPrefWrite(StorageKeys.profileSelectionName, element.name!);
                                  PatientPreferences().sharedPrefWrite(StorageKeys.profileSelectionProfileImg, element.profile_img!);
                                  controller.profileRedirections();
                                },
                              );
                            })
                          ],
                        ),
                      ),
                      const SpaceVertical(40),
                      Visibility(
                        visible: controller.otherProfileBookAppointmentProfileList.isNotEmpty,
                        child: SubTitleWidget(
                          subTitle: AppStringKey.titleOtherFamilyProfile.tr,
                        ),
                      ),
                      const SpaceVertical(20),
                      GridView.builder(
                        itemCount: controller.otherProfileBookAppointmentProfileList.value.length,
                        physics: const NeverScrollableScrollPhysics(),
                        primary: false,
                        shrinkWrap: true,
                        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 10.0,
                          mainAxisSpacing: 12.0,
                          mainAxisExtent: 175,
                        ),
                        itemBuilder: (BuildContext context, int index) {
                          return ItemProfile(
                            name: controller.otherProfileBookAppointmentProfileList[index].name!,
                            email: controller.otherProfileBookAppointmentProfileList[index].mobile!,
                            imageUrl: controller.otherProfileBookAppointmentProfileList[index].profile_img!,
                            callback: () {
                              PatientPreferences().sharedPrefWrite(
                                  StorageKeys.patientMobile, controller.otherProfileBookAppointmentProfileList[index].mobile!);
                              PatientPreferences().sharedPrefWrite(StorageKeys.profileSelectionPersonId,
                                  controller.otherProfileBookAppointmentProfileList[index].person_id!);
                              PatientPreferences()
                                  .sharedPrefWrite(StorageKeys.age, controller.otherProfileBookAppointmentProfileList[index].age!);
                              PatientPreferences()
                                  .sharedPrefWrite(StorageKeys.gender, controller.otherProfileBookAppointmentProfileList[index].gender!);
                              PatientPreferences().sharedPrefWrite(
                                  StorageKeys.profileSelectionName, controller.otherProfileBookAppointmentProfileList[index].name!);
                              PatientPreferences().sharedPrefWrite(StorageKeys.profileSelectionProfileImg,
                                  controller.otherProfileBookAppointmentProfileList[index].profile_img!);
                              controller.profileRedirections();
                            },
                          );
                        },
                      )
                    ],
                  ).paddingAll(20),
                )
              : const SizedBox.shrink(),
        ),
      ),
    );
  }
}

class ItemSingleProfile extends StatelessWidget {
  final String name;
  final String email;
  final String imageUrl;
  final Function() callback;

  ItemSingleProfile({
    Key? key,
    required this.name,
    required this.email,
    required this.imageUrl,
    required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: BaseSize.width(44),
      child: CustomCardWidget(
        elevation: 3,
        borderRadius: 10,
        backgroundColor: AppColors.colorGrayBackground,
        widget: RippleEffectWidget(
          borderRadius: 10,
          callBack: () {
            callback();
          },
          widget: Column(
            children: [
              const SpaceVertical(8),
              ClipOval(
                child: Image.network(
                  imageUrl.isEmpty ? dummyImageUrl : imageUrl,
                  width: 94,
                  height: 94,
                  fit: BoxFit.cover,
                  errorBuilder: (context, error, stackTrace) {
                    return Utilities.getPlaceHolder(
                      height: 94,
                      width: 94,
                    );
                  },
                ),
              ),
              const SpaceVertical(10),
              Text(
                name,
                style: BaseStyle.textStyleNunitoSansBold(
                  16,
                  AppColors.colorDarkBlue,
                ),
              ),
              const SpaceVertical(4),
              Text(
                email,
                style: BaseStyle.textStyleNunitoSansRegular(
                  14,
                  AppColors.colorTextDarkGray,
                ),
              ),
              const SpaceVertical(8),
            ],
          ),
        ),
      ),
    );
  }
}

class ItemProfile extends StatelessWidget {
  final String name;
  final String email;
  final String imageUrl;
  final Function() callback;

  const ItemProfile({
    Key? key,
    required this.name,
    required this.email,
    required this.imageUrl,
    required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      elevation: 3,
      borderRadius: 10,
      backgroundColor: AppColors.colorGrayBackground,
      widget: RippleEffectWidget(
        borderRadius: 10,
        callBack: () {
          callback();
        },
        widget: Column(
          children: [
            const SpaceVertical(8),
            ClipOval(
              child: Image.network(
                imageUrl.isEmpty ? dummyImageUrl : imageUrl,
                width: 94,
                height: 94,
                fit: BoxFit.cover,
                errorBuilder: (context, error, stackTrace) {
                  return Utilities.getPlaceHolder(
                    height: 94,
                    width: 94,
                  );
                },
              ),
            ),
            const SpaceVertical(10),
            Text(
              name,
              style: BaseStyle.textStyleNunitoSansBold(
                16,
                AppColors.colorDarkBlue,
              ),
            ),
            const SpaceVertical(4),
            Text(
              email,
              style: BaseStyle.textStyleNunitoSansRegular(
                14,
                AppColors.colorTextDarkGray,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
