import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/base_controller.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../models/pt_home_data_model.dart';
import '../../../utils/functions/permission_check.dart';
import '../../../utils/video_call.dart';

class PtHomeController extends BaseController {
  RxList<PtHomeDataModel> listFacilities = RxList();
  var listBannerImages = [
    AppImages.ptHomeBanner,
    AppImages.ptHomeBanner,
    AppImages.ptHomeBanner,
  ].obs;

  var listSymptoms = [
    'Fever',
    'Cough',
    'Pain',
    'Runny',
    'Fever',
    'Cough',
    'Pain',
    'Runny',
  ].obs;

  var listImages = [
    AppImages.symptomFever,
    AppImages.symptomCough,
    AppImages.symptomPain,
    AppImages.symptomRunny,
    AppImages.symptomFever,
    AppImages.symptomCough,
    AppImages.symptomPain,
    AppImages.symptomRunny,
  ].obs;
  RxBool isProfileAdded = false.obs;

  @override
  void onInit() {
    super.onInit();

    listFacilities.add(
      PtHomeDataModel(
        name: 'My\nAppointments',
        image: AppImages.ptHomeMyAppointment,
        navigation: RoutePaths.PT_SELECT_PATIENT_PROFILE,
        enumProfileSelection: EnumProfileSectionFrom.MY_APPOINTMENT,
      ),
    );
    listFacilities.add(
      PtHomeDataModel(
        name: 'Patient\nProfile',
        image: AppImages.ptHomePatientProfile,
        navigation: RoutePaths.PT_MULTI_PROFILE,
        enumProfileSelection: EnumProfileSectionFrom.MULTI_PROFILE,
      ),
    );
    listFacilities.add(
      PtHomeDataModel(
        name: 'Health\nRecord',
        image: AppImages.ptHomeHealthRecord,
        navigation: RoutePaths.PT_SELECT_PATIENT_PROFILE,
        enumProfileSelection: EnumProfileSectionFrom.HEALTH_RECORD,
      ),
    );
    listFacilities.add(
      PtHomeDataModel(
        name: 'Past Medical\nHistory',
        image: AppImages.ptHomePastMedicalHistory,
        navigation: RoutePaths.PT_PREVIOUS_MEDICATION_RECORDS,
        enumProfileSelection: EnumProfileSectionFrom.PREVIOUS_MEDICATION_RECORDS,
      ),
    );
    listFacilities.add(
      PtHomeDataModel(
        name: 'Payment\nHistory',
        image: AppImages.ptHomePaymentHistory,
        navigation: RoutePaths.PT_PAYMENT_HISTORY,
      ),
    );
    listFacilities.add(
      PtHomeDataModel(
        name: 'Add previous\nmedical Record',
        image: AppImages.ptHomePreviousRecord,
        navigation: RoutePaths.PT_ADD_PREVIOUS_MEDICATION_RECORD,
        enumProfileSelection: EnumProfileSectionFrom.ADD_PREVIOUS_MEDICATION,
      ),
    );
  }

  Future<void> onJoin(BuildContext context) async {
    CheckPermission.check([Permission.camera, Permission.microphone], "storage");
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const VideoCall(),
      ),
    );
  }
}
