import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/ui_constant.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/screens/patient/pt_home/pt_home_controller.dart';
import 'package:med_ai/utils/call_controller.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';
import 'package:med_ai/widgets/ripple_effect_widget.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../constant/app_colors.dart';
import '../../../constant/app_images.dart';
import '../../../constant/app_strings_key.dart';
import '../../../constant/base_size.dart';
import '../../../constant/base_style.dart';
import '../../../utils/utilities.dart';
import '../../../widgets/space_vertical.dart';

class PtHomeScreen extends GetView<PtHomeController> {
  const PtHomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(65),
          child: Column(
            children: [
              const SpaceVertical(10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 32,
                    height: 32,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.grey.withOpacity(0.5), width: 0.5),
                    ),
                    child: ClipOval(
                      child: Image.network(
                        dummyImageUrl,
                        width: 32,
                        height: 32,
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return Utilities.getPlaceHolder(width: 32, height: 32);
                        },
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      // Get.toNamed(RoutePaths.DR_NOTIFICATION);
                      Get.put(CallController());
                      Get.find<CallController>().initilize();
                      controller.onJoin(context);
                    },
                    child: Stack(
                      children: [
                        Image.asset(
                          AppImages.notification,
                          height: 24,
                          width: 24,
                        ),
                        Positioned(
                          right: 2,
                          top: 0,
                          child: Image.asset(
                            AppImages.notificationDot,
                            height: 8,
                            width: 8,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ).paddingSymmetric(horizontal: 20),
              const Divider(
                color: AppColors.colorLightSkyBlue,
              ),
            ],
          ),
        ),
        backgroundColor: AppColors.white,
        body: ScrollConfiguration(
          behavior: MyBehavior(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const PatientGreeting(
                  patientName: 'Ricky J',
                ).marginSymmetric(horizontal: 15),
                const SpaceVertical(18),
                const SearchBar().marginSymmetric(horizontal: 15),
                const SpaceVertical(20),
                PtBanner(
                  listBanner: controller.listBannerImages,
                ),
                const SpaceVertical(20),
                PtMyHealthAndAppointmentBanner().marginSymmetric(horizontal: 18),
                const SpaceVertical(20),
                PtSymptomsIndicator().marginSymmetric(horizontal: 15),
                const SpaceVertical(20),
                PtFacilities(),
                const SpaceVertical(20),
                const PtFindDoctor(),
              ],
            ).paddingOnly(
              bottom: 20,
            ),
          ),
        ),
      ),
    );
  }
}

class PatientGreeting extends StatelessWidget {
  final String patientName;

  const PatientGreeting({
    Key? key,
    this.patientName = 'New User',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Good ${Utilities.greeting()}',
              style: BaseStyle.textStyleNunitoSansBold(
                14,
                AppColors.colorDarkBlue,
              ),
            ),
            const SpaceVertical(5),
            Text(
              patientName,
              style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              Utilities.getDay(),
              style: BaseStyle.textStyleNunitoSansSemiBold(
                14,
                AppColors.colorTextDarkGray,
              ),
            ),
            const SpaceVertical(5),
            Text(
              Utilities.getTodayDate(),
              style: BaseStyle.textStyleNunitoSansBold(
                14,
                AppColors.colorTextDarkGray,
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class SearchBar extends StatelessWidget {
  const SearchBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      elevation: 3,
      borderRadius: 10,
      widget: TextField(
        showCursor: false,
        onTap: () {
          Get.toNamed(RoutePaths.GLOBAL_SEARCH);
        },
        cursorColor: AppColors.colorDarkBlue,
        decoration: InputDecoration(
          prefixIcon: const Icon(
            Icons.search,
            color: AppColors.colorGreen,
            size: 20,
          ),
          hintText: AppStringKey.hintSearchDoctorHospital.tr.toUpperCase(),
          fillColor: AppColors.transparent,
          hintStyle: BaseStyle.textStyleNunitoSansRegular(
            12,
            AppColors.hintColor,
          ),
        ),
        readOnly: true,
        autocorrect: false,
        enableSuggestions: false,
        style: BaseStyle.textStyleNunitoSansSemiBold(
          14,
          AppColors.colorDarkBlue,
        ),
        maxLines: 1,
      ),
    );
  }
}

class PtBanner extends StatelessWidget {
  final List listBanner;
  final controller = PageController(viewportFraction: 0.91, keepPage: true);

  PtBanner({
    Key? key,
    required this.listBanner,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pages = List.generate(
      listBanner.length,
      (index) => ItemBannerPager(
        banner: listBanner[index],
      ),
    );

    return Column(
      children: [
        SizedBox(
          height: 100,
          child: PageView.builder(
            controller: controller,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            itemCount: pages.length,
            itemBuilder: (_, index) {
              return pages[index];
            },
          ),
        ),
        const SpaceVertical(10),
        SmoothPageIndicator(
          controller: controller,
          count: pages.length,
          effect: const SlideEffect(
            dotHeight: 8,
            dotWidth: 8,
            activeDotColor: AppColors.colorIndicatorSkyBlue,
            dotColor: AppColors.colorLightSkyBlue,
            type: SlideType.normal,
          ),
        ),
      ],
    );
  }
}

class ItemBannerPager extends StatelessWidget {
  final String banner;

  const ItemBannerPager({
    Key? key,
    required this.banner,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Utilities.openBrowser('https://www.google.com');
      },
      child: CustomCardWidget(
        elevation: 1,
        widget: Image.asset(
          banner,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class PtMyHealthAndAppointmentBanner extends StatelessWidget {
  final controller = PageController(viewportFraction: 1, keepPage: true);

  PtMyHealthAndAppointmentBanner({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pages = List.generate(
      2,
      (index) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: SubItemHealthAndAppointment(
              title: AppStringKey.titleBookAppointment.tr,
              image: AppImages.ptBookAppointment,
              callback: () {
                profileSelectionFrom = EnumProfileSectionFrom.BOOK_APPOINTMENT;
                Get.toNamed(RoutePaths.PT_SELECT_PATIENT_PROFILE);
              },
            ),
          ),
          Expanded(
            child: SubItemHealthAndAppointment(
              title: AppStringKey.titleInstantVideoConsult.tr,
              image: AppImages.ptVideoConsultant,
              callback: () {
                profileSelectionFrom = EnumProfileSectionFrom.BOOK_APPOINTMENT;
                Get.toNamed(RoutePaths.PT_SELECT_PATIENT_PROFILE);
              },
            ),
          ),
          Expanded(
            child: SubItemHealthAndAppointment(
              title: AppStringKey.titlePatientExperience.tr,
              image: AppImages.ptPatientExperience,
            ),
          ),
        ],
      ),
    );

    return Column(
      children: [
        SizedBox(
          height: BaseSize.height(20),
          child: PageView.builder(
            controller: controller,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            itemCount: pages.length,
            itemBuilder: (_, index) {
              return pages[index];
            },
          ),
        ),
        // const SpaceVertical(10),
        SmoothPageIndicator(
          controller: controller,
          count: pages.length,
          effect: const SlideEffect(
            dotHeight: 8,
            dotWidth: 8,
            activeDotColor: AppColors.colorIndicatorSkyBlue,
            dotColor: AppColors.colorLightSkyBlue,
            type: SlideType.normal,
          ),
        ),
      ],
    );
  }
}

class SubItemHealthAndAppointment extends StatelessWidget {
  final String title;
  final String image;
  final Function()? callback;

  const SubItemHealthAndAppointment({
    Key? key,
    required this.title,
    required this.image,
    this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      widget: RippleEffectWidget(
        borderRadius: 10,
        callBack: callback,
        widget: Column(
          children: [
            Expanded(
              flex: 5,
              child: Image.asset(
                image,
                fit: BoxFit.cover,
              ),
            ),
            Expanded(
              flex: 2,
              child: Center(
                child: Text(
                  title,
                  style: BaseStyle.textStyleNunitoSansBold(
                    10,
                    AppColors.colorDarkBlue,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ),
    ).paddingOnly(bottom: 10);
  }
}

class PtSymptomsIndicator extends StatelessWidget {
  PtSymptomsIndicator({Key? key}) : super(key: key);
  final pageController = PageController(viewportFraction: 1, keepPage: true);

  @override
  Widget build(BuildContext context) {
    final pages = List.generate(
      2,
      (index) => grid(crossAxisCount: 4, mainAxisCount: 2),
    );
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppColors.colorBackground,
                ),
                child: const Icon(Icons.videocam_outlined).paddingAll(5)),
            const SpaceHorizontal(10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppStringKey.labelNotFeelingTooWell.tr,
                  style: BaseStyle.textStyleDomineBold(12, AppColors.colorDarkBlue),
                ),
                const SpaceVertical(2),
                Text(
                  AppStringKey.subLabelNotFeelingTooWell.tr,
                  style: BaseStyle.textStyleNunitoSansRegular(9, AppColors.colorTextDarkGray),
                ),
              ],
            ),
          ],
        ).marginSymmetric(horizontal: 10),
        const SpaceVertical(15),
        SizedBox(
          height: 170,
          child: PageView.builder(
            controller: pageController,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            itemCount: pages.length,
            itemBuilder: (_, index) {
              return pages[index];
            },
          ),
        ),
        SmoothPageIndicator(
          controller: pageController,
          count: pages.length,
          effect: const SlideEffect(
            dotHeight: 8,
            dotWidth: 8,
            activeDotColor: AppColors.colorIndicatorSkyBlue,
            dotColor: AppColors.colorLightSkyBlue,
            type: SlideType.normal,
          ),
        ),
      ],
    );
  }

  Widget grid({required int crossAxisCount, required int mainAxisCount}) {
    return Column(
      children: List.generate(mainAxisCount, (index1) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: List.generate(crossAxisCount, (index2) {
            int count = index2 + (index1 * crossAxisCount);
            return SubItemSymptoms(
              image: Get.find<PtHomeController>().listImages[count],
              symptom: Get.find<PtHomeController>().listSymptoms[count],
            ).marginOnly(bottom: 15);
          }),
        );
      }),
    ).marginSymmetric(horizontal: 15);
  }
}

class SubItemSymptoms extends StatelessWidget {
  final String image;
  final String symptom;

  const SubItemSymptoms({
    Key? key,
    required this.image,
    required this.symptom,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(RoutePaths.PT_HOME_SYMPTOMS, arguments: {
          'image': image,
          'symptom': symptom,
          'fromHome': 'yes',
        });
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 50,
            width: 50,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: AppColors.colorLightSkyBlue, width: 1)),
            child: Image.asset(image).paddingAll(5),
          ),
          const SpaceVertical(5),
          Text(
            symptom,
            style: BaseStyle.textStyleNunitoSansRegular(10, AppColors.colorDarkBlue),
          ),
        ],
      ),
    );
  }
}

class PtFacilities extends StatelessWidget {
  PtFacilities({Key? key}) : super(key: key);
  final pageController = PageController(viewportFraction: 1, keepPage: true);

  @override
  Widget build(BuildContext context) {
    final pages = List.generate(
      2,
      (index) => grid(crossAxisCount: 3, mainAxisCount: 1, index: index),
    );

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 15),
      color: AppColors.colorDarkBlue,
      child: Column(
        children: [
          SizedBox(
            height: 120,
            child: PageView.builder(
              controller: pageController,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              itemCount: pages.length,
              itemBuilder: (_, index) {
                return pages[index];
              },
            ),
          ),
          SmoothPageIndicator(
            controller: pageController,
            count: pages.length,
            effect: const SlideEffect(
              dotHeight: 8,
              dotWidth: 8,
              activeDotColor: AppColors.colorIndicatorSkyBlue,
              dotColor: AppColors.colorLightSkyBlue,
              type: SlideType.normal,
            ),
          ),
        ],
      ),
    );
  }

  Widget grid({
    required int crossAxisCount,
    required int mainAxisCount,
    required int index,
  }) {
    return Column(
      children: List.generate(
        mainAxisCount,
        (index1) {
          var controller = Get.find<PtHomeController>();
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(
              crossAxisCount,
              (index2) {
                int count;
                if (index == 0) {
                  count = index2 + (index1 * crossAxisCount); //0,1,2
                } else {
                  count = (index2 + crossAxisCount) + (index1 * crossAxisCount); //3,4,5
                }
                return Expanded(
                  child: SubItemFacilities(
                    image: controller.listFacilities[count].image!,
                    label: controller.listFacilities[count].name!,
                    navigation: controller.listFacilities[count].navigation!,
                    enumProfile: controller.listFacilities[count].enumProfileSelection,
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}

class SubItemFacilities extends StatelessWidget {
  final String image;
  final String label;
  final String navigation;
  final Enum? enumProfile;

  const SubItemFacilities({
    Key? key,
    required this.image,
    required this.label,
    required this.navigation,
    this.enumProfile,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      elevation: 3,
      widget: RippleEffectWidget(
        borderRadius: 10,
        callBack: () {
          if (enumProfile != null) {
            profileSelectionFrom = enumProfile!;
          }
          Get.toNamed(navigation);
        },
        widget: Column(
          children: [
            const SpaceVertical(5),
            Image.asset(
              image,
              height: 40,
              width: 40,
            ),
            const SpaceVertical(10),
            Text(
              label,
              style: BaseStyle.textStyleNunitoSansSemiBold(10, AppColors.colorDarkBlue),
              textAlign: TextAlign.center,
            ),
          ],
        ).paddingAll(10),
      ),
    );
  }
}

class PtFindDoctor extends StatelessWidget {
  const PtFindDoctor({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: GestureDetector(
        onTap: () {
          profileSelectionFrom = EnumProfileSectionFrom.BOOK_APPOINTMENT;
          Get.toNamed(RoutePaths.PT_SELECT_PATIENT_PROFILE);
        },
        child: Stack(
          children: [
            Container(
              height: 90,
              width: double.infinity,
              color: AppColors.colorTextDarkGray,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.labelConfused.tr,
                    style: BaseStyle.textStyleNunitoSansBold(12, AppColors.white),
                  ),
                  Text(
                    AppStringKey.labelFindDoctor.tr,
                    style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.white),
                  ),
                  const SpaceVertical(5),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 3),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: AppColors.white),
                    child: Text(
                      AppStringKey.labelFindNow.tr,
                      style: BaseStyle.textStyleDomineBold(10, AppColors.colorDarkBlue),
                    ),
                  )
                ],
              ).paddingAll(15).marginOnly(left: 120),
            ),
            Positioned(
              top: 10,
              left: 20,
              child: Image.asset(
                AppImages.ptHomeFindDr,
                height: 105,
                width: 100,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
