// User interface related constant values

import 'package:flutter/services.dart';

const double defaultBorderRadius = 5;
const double defaultPadding = 20;
const double defaultAppBarHeight = 60;
int userTypeId = 0;
var shadow = 0.2;
var isDoctor = false;
var dummyImageUrl = 'https://picsum.photos/200/300';
Enum moreServicesEnum = Services.HOSPITAL;
Enum appointmentType = EnumAppointmentType.VISIT; // select appointment type (visit,follow-up)
Enum consultancyFrom = EnumConsultancyFrom.APPOINTMENT_TAB; // New consultancy page selected from dr_bottom_tab / my_patient_screen
Enum profileSelectionFrom =
    EnumProfileSectionFrom.BOOK_APPOINTMENT; // Enum for handle book appointment and your appointment flow in single screen
List<TextInputFormatter> inputFormatterDotDigit = [FilteringTextInputFormatter.allow(RegExp(r'(^\d*\.?\d*)'))];
List<TextInputFormatter> inputFormatterDigit = [FilteringTextInputFormatter.allow(RegExp(r'(^\d*\d*)'))];

enum Consultancy {
  PHYSICAL,
  VIRTUAL,
}

enum EnumAppointmentType {
  VISIT,
  FOLLOWUP,
}

enum Services {
  AMBULANCE,
  BLOOD_BANK,
  DIAGNOSTIC_CENTER,
  GENERAL_HOSPITAL,
  MEDICINE,
  SPECIALIZED_CLINIC,
  SPECIALIZED_HOSPITAL,
  TRAUMA_CENTER,
  PHYSIOTHERAPIST,
  DOCTOR_CHAMBER,
  HOSPITAL,
}

enum EnumConsultancyFrom {
  MY_PATIENT,
  APPOINTMENT_TAB,
}

enum EnumProfileSectionFrom {
  BOOK_APPOINTMENT,
  MY_APPOINTMENT,
  HEALTH_RECORD,
  ADD_PREVIOUS_MEDICATION,
  PREVIOUS_MEDICATION_RECORDS,
  MULTI_PROFILE,
}
