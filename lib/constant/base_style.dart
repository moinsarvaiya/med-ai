import 'package:flutter/material.dart';

import 'app_colors.dart';
import 'base_fonts.dart';

class BaseStyle {
  static TextStyle textStyleDomineRegular(double size, Color color) {
    return TextStyle(fontSize: size, color: color, fontFamily: BaseFonts.domine, fontWeight: FontWeight.normal);
  }

  static TextStyle textStyleDomineBold(double size, Color color) {
    return TextStyle(fontSize: size, color: color, fontFamily: BaseFonts.domine, fontWeight: FontWeight.bold, height: 1.2);
  }

  static TextStyle textStyleDomineMedium(double size, Color color) {
    return TextStyle(
        fontSize: size, color: color, fontFamily: BaseFonts.domine, fontWeight: FontWeight.w400, overflow: TextOverflow.ellipsis);
  }

  static TextStyle textStyleDomineSemiBold(double size, Color color) {
    return TextStyle(
      fontSize: size,
      color: color,
      fontWeight: FontWeight.w600,
      fontFamily: BaseFonts.domine,
    );
  }

  static TextStyle textStyleNunitoSansRegular(double size, Color color) {
    return TextStyle(
      fontSize: size,
      color: color,
      fontFamily: BaseFonts.nunitosans,
      fontWeight: FontWeight.normal,
    );
  }

  static TextStyle textStyleNunitoSansBold(double size, Color color) {
    return TextStyle(fontSize: size, color: color, fontFamily: BaseFonts.nunitosans, fontWeight: FontWeight.bold);
  }

  static TextStyle textStyleNunitoSansMedium(double size, Color color) {
    return TextStyle(
        fontSize: size, color: color, fontFamily: BaseFonts.nunitosans, fontWeight: FontWeight.w400, overflow: TextOverflow.ellipsis);
  }

  static TextStyle textStyleNunitoSansSemiBold(double size, Color color) {
    return TextStyle(
      fontSize: size,
      color: color,
      fontWeight: FontWeight.w600,
      fontFamily: BaseFonts.nunitosans,
    );
  }

  static TextStyle underLineTextStyle = const TextStyle(
    decoration: TextDecoration.underline,
    color: AppColors.colorDarkBlue,
    fontSize: 14,
    fontFamily: BaseFonts.nunitosans,
    fontWeight: FontWeight.normal,
  );
}
