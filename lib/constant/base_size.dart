import 'package:get/get.dart';

class BaseSize {
  static double height(double percentage) {
    return Get.height * percentage / 100;
  }

  static double width(double percentage) {
    return Get.width * percentage / 100;
  }
}
