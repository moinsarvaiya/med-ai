class StorageKeys {
  static const String isUserLoggedIn = 'isUserLoggedIn';
  static const String userTypeId = 'userTypeId'; // doctor-1 , patient-0
  static const String isOnBoarding = 'isOnBoarding'; // shown-1 , not-0

  static const String token = 'token';
  static const String personId = 'person_id';
  static const String name = 'name';
  static const String username = 'username';
  static const String approvalStatus = 'approval_status';
  static const String referralCode = 'referral_code';
  static const String languageCode = 'language_code';
  static const String age = 'age';
  static const String gender = 'gender';
  static const String doctorId = 'doctor_id';
  static const String availableDaySlot = 'available_day_slot';
  static const String availableTimeSlot = 'available_time_slot';
  static const String profileSelectionPersonId = 'profile_selection_person_id';
  static const String profileSelectionName = 'profile_selection_name';
  static const String profileSelectionProfileImg = 'profile_selection_profile_img';
  static const String profileSelectionAge = 'profile_selection_age';
  static const String profileSelectionGender = 'profile_selection_gender';
  static const String vid = 'vid';
  static const String patientMobile = 'patient_mobile';
  static const String visitFee = 'visit_fee';
  static const String doctorSelectionName = 'doctor_selection_name';
  static const String doctorSelectionSpecialization = 'doctor_selection_specialization';
  static const String doctorSelectionQualification = 'doctor_selection_qualification';
  static const String doctorSelectionProfileImage = 'doctor_selection_profile_image';

  static const String visitID = 'visit_id';
  static const String followupID = 'followup_id';
  static const String consultationID = 'consultation_id';
  static const String consultationType = 'consultation_type';
}
