import 'package:flutter/material.dart';

class AppColors {
  /// Hard color codes
  static const Color blue = Color(0xff354868);
  static const Color black = Color(0xff000000);
  static const Color lightBlack = Color(0xff0E1E2B);
  static const Color colorGrayBackground = Color(0xffF6F8FB);
  static const Color colorGrayBackground2 = Color(0xffDBE9F5);
  static const Color white = Color(0xffffffff);
  static const Color green = Color(0xff3FA598);
  static const Color colorDarkBlue = Color(0xff123258);
  static Color hintColor = const Color(0xff123258).withOpacity(0.5);
  static const Color colorTextDarkGray = Color(0xff4C6780);
  static const Color colorLightSkyBlue = Color(0xffD3E3F1);
  static const Color colorBackground = Color(0xffEAF0F5);
  static const Color colorGreen = Color(0xff18DF80);
  static const Color colorSky = Color(0xff03D9E7);
  static const Color colorRed = Color(0xffff1818);
  static const Color colorYellow = Color(0xffF5B715);
  static const Color colorPink = Color(0xffF84C6B);
  static const Color colorMint = Color(0xffB7FFDD);
  static const Color colorPending = Color(0xffB7D4FF);
  static const Color colorRefunded = Color(0xffFFB7B7);
  static const Color colorOrange = Color(0xffF4B382);
  static const Color colorDarkOrange = Color(0xffFF7A00);
  static const Color colorSkyBlue = Color(0xff399CD3);
  static const Color colorLightRed = Color(0xffffc7c7);
  static const Color colorLightSkyBlueBack = Color(0xffD7FBF0);
  static const Color colorDarkGreen = Color(0xff1C7C4F);
  static const Color colorIndicatorSkyBlue = Color(0xff34B6FF);
  static Color colorShadow = const Color(0xff123258).withOpacity(0.1);
  static const Color transparent = Colors.transparent;
}
