import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/bindings/dr_app_bindings.dart';
import 'package:med_ai/bindings/pt_app_bindings.dart';
import 'package:med_ai/screens/common/no_internet/no_internet_screen.dart';
import 'package:med_ai/screens/common/otp_verification/otp_verification_screen.dart';
import 'package:med_ai/screens/common/select_user/select_user_screen.dart';
import 'package:med_ai/screens/common/splash/splash_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_add_new_medicine/dr_add_new_medicine_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_appointment_medicine_details/dr_appointment_medicine_details_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_appointment_medicine_list/dr_appointment_medicine_list_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_clinical_decision/dr_clinical_decision_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_diagnostics_test_images/dr_diagnostics_test_images_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_follow_up_complete_appointment/dr_follow_up_complete_appointment_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_follow_up_date/dr_follow_up_date_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_followup_appointment/dr_followup_appointment_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_identified_disease/dr_identified_disease_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_prescribe_medicine/dr_prescribe_medicine_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_update_vital_signs/dr_update_vital_signs_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_visit_appointment/dr_visit_appointment_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_visit_complete_appointment/dr_visit_complete_appointment_screen.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_visit_update_diagnostic_test/dr_visit_update_diagnostic_test_screen.dart';
import 'package:med_ai/screens/doctor/dr_create_patient_flow/dr_additional_comments/dr_additional_comment_screen.dart';
import 'package:med_ai/screens/doctor/dr_create_patient_flow/dr_dynamic_symptom/dr_dynamic_symptom_screen.dart';
import 'package:med_ai/screens/doctor/dr_create_patient_flow/dr_patient_reviews/dr_patient_review_screen.dart';
import 'package:med_ai/screens/doctor/dr_existing_consultancy_flow/dr_patient_details/dr_patient_detail_screen.dart';
import 'package:med_ai/screens/doctor/dr_home/dr_home_screen.dart';
import 'package:med_ai/screens/doctor/dr_notification/dr_notification_screen.dart';
import 'package:med_ai/screens/doctor/dr_number_email_verification_flow/dr_otp_verification/dr_otp_verification_screen.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_appointment_list/pt_appointment_list_screen.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_completed_appointment_details/pt_completed_appointment_details_screen.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_current_new_symptoms/pt_current_new_symptoms_screen.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_follow_up_diagnostic_status/pt_follow_up_diagnostic_status_screen.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_follow_up_medicine_status/pt_follow_up_medicine_status_screen.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_follow_up_review/pt_follow_up_review_screen.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_follow_up_vital/pt_follow_up_vital_screen.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_future_appointment_details/pt_future_appointment_details_screen.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_visit_followup_history/pt_visit_followup_history_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_additional_comments/pt_additional_comment_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_booking_reviews/pt_booking_review_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_cough_symptom/pt_cough_symptom_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_doctor_available_days/pt_doctor_available_days_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_doctor_selection/pt_doctor_selection_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_doctor_time_slot_selection/pt_doctor_time_slot_selection_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_pain_symptom/pt_pain_symptom_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_review_booking/pt_review_booking_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_special_symptom/pt_special_symptom_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_symptom_checker/pt_symptom_checker_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_symptom_checker_step_2/pt_symptom_checker_step_2_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_thank_you_booking/pt_thank_you_booking_screen.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_vital_health_metrics/pt_vital_health_metrics_screen.dart';
import 'package:med_ai/screens/patient/pt_bottom_navigation/pt_bottom_navigation_screen.dart';
import 'package:med_ai/screens/patient/pt_more_services_flow/pt_available_doctors/pt_available_doctor_screen.dart';
import 'package:med_ai/screens/patient/pt_more_services_flow/pt_near_by_hospital/pt_near_by_hospital_screen.dart';
import 'package:med_ai/screens/patient/pt_previous_medication/pt_add_previous_medication_record/pt_add_previous_medication_record_screen.dart';
import 'package:med_ai/screens/patient/pt_previous_medication/pt_previous_medication_diagnostics/pt_previous_medication_diagnostics_screen.dart';
import 'package:med_ai/screens/patient/pt_previous_medication/pt_previous_medication_records/pt_previous_medication_records_screen.dart';
import 'package:med_ai/screens/patient/pt_select_patient_profile/pt_select_patient_profile_screen.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_edit_patient_profile/pt_edit_patient_profile_screen.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_home_symptoms/pt_home_symptoms_screen.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_number_email_verification_flow/pt_otp_verification/pt_otp_verification_screen.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_number_email_verification_flow/pt_verify_email_number/pt_verify_email_number_screen.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_profile/pt_profile_screen.dart';

import '../screens/common/global_search_flow/global_search/global_search_screen.dart';
import '../screens/common/global_search_flow/search_blood_bank/search_blood_bank_screen.dart';
import '../screens/common/global_search_flow/search_diagnostic_center/search_diagnostic_center_screen.dart';
import '../screens/common/global_search_flow/search_filter_service/search_filter_service_screen.dart';
import '../screens/common/global_search_flow/search_hospital_list/search_hospital_list_screen.dart';
import '../screens/common/global_search_flow/search_medicine_details/search_medicine_details_screen.dart';
import '../screens/common/global_search_flow/search_medicine_list/search_medicine_list_screen.dart';
import '../screens/common/global_search_flow/search_trauma_center/search_trauma_center_screen.dart';
import '../screens/common/login/login_screen.dart';
import '../screens/common/onboarding/onboarding_screen.dart';
import '../screens/doctor/dr_appointments/dr_appointment_data_submitted/dr_appointment_data_submitted_screen.dart';
import '../screens/doctor/dr_appointments/dr_followup_identified_disease/dr_followup_identified_disease_screen.dart';
import '../screens/doctor/dr_appointments/dr_followup_prescribe_medicine/dr_followup_prescribe_medicine_screen.dart';
import '../screens/doctor/dr_appointments/dr_followup_update_diagnostics_test/dr_followup_update_diagnostics_test_screen.dart';
import '../screens/doctor/dr_appointments/dr_followup_update_symptoms/dr_followup_update_symptoms_screen.dart';
import '../screens/doctor/dr_appointments/dr_my_patient/dr_my_patient_screen.dart';
import '../screens/doctor/dr_appointments/dr_new_consultancy/dr_new_consultancy_screen.dart';
import '../screens/doctor/dr_appointments/dr_visit_next_date/dr_visit_next_date_screen.dart';
import '../screens/doctor/dr_appointments/dr_visit_update_symptoms/dr_visit_update_symptoms_screen.dart';
import '../screens/doctor/dr_bottom_navigation/dr_bottom_navigation_screen.dart';
import '../screens/doctor/dr_consultancy_history/dr_consultancy_history_screen.dart';
import '../screens/doctor/dr_consultancy_history_detail/dr_consultancy_history_detail_screen.dart';
import '../screens/doctor/dr_create_patient_flow/dr_create_patient/dr_create_patient_screen.dart';
import '../screens/doctor/dr_create_patient_flow/dr_special_symptom/dr_special_symptom_screen.dart';
import '../screens/doctor/dr_create_patient_flow/dr_symptom_checker/dr_symptom_checker_screen.dart';
import '../screens/doctor/dr_create_patient_flow/dr_symptom_checker_step_2/dr_symptom_checker_step_2_screen.dart';
import '../screens/doctor/dr_create_patient_flow/dr_vital_health_metrics/dr_vital_health_metrics_screen.dart';
import '../screens/doctor/dr_existing_consultancy_flow/dr_patient_list/dr_patient_list_screen.dart';
import '../screens/doctor/dr_number_email_verification_flow/dr_verify_email_number/dr_verify_email_number_screen.dart';
import '../screens/doctor/dr_schedule_flow/dr_physical_consultancy_detail/dr_physical_consultancy_detail_screen.dart';
import '../screens/doctor/dr_schedule_flow/dr_schedule_submitted/dr_schedule_submitted_screen.dart';
import '../screens/doctor/dr_schedule_flow/dr_set_your_calendar_physical/dr_set_your_calendar_physical_screen.dart';
import '../screens/doctor/dr_schedule_flow/dr_set_your_calendar_virtual/dr_set_your_calendar_virtual_screen.dart';
import '../screens/doctor/dr_schedule_flow/dr_set_your_schedule/dr_set_your_schedule_screen.dart';
import '../screens/doctor/dr_schedule_flow/dr_virtual_consultancy/dr_virtual_consultancy_screen.dart';
import '../screens/doctor/dr_setting_flow/dr_add_degree/dr_add_degree_screen.dart';
import '../screens/doctor/dr_setting_flow/dr_edit_profile/dr_edit_profile_screen.dart';
import '../screens/doctor/dr_setting_flow/dr_profile/dr_profile_screen.dart';
import '../screens/doctor/dr_setting_flow/dr_settings/dr_settings_screen.dart';
import '../screens/doctor/dr_wallet_flow/dr_my_wallet/dr_my_wallet_screen.dart';
import '../screens/doctor/dr_wallet_flow/dr_see_withdrawal_methods/dr_see_withdrawal_methods_screen.dart';
import '../screens/doctor/dr_wallet_flow/dr_set_withdrawal_method/dr_set_withdrawal_method_screen.dart';
import '../screens/doctor/dr_wallet_flow/dr_withdraw_balance/dr_withdraw_balance_screen.dart';
import '../screens/doctor/dr_wallet_flow/dr_withdraw_request_submitted/dr_withdraw_request_submitted_screen.dart';
import '../screens/patient/my_appointment_flow/pt_previous_symptoms/pt_previous_symptoms_screen.dart';
import '../screens/patient/pt_book_appointment_flow/pt_dynamic_symptom/pt_dynamic_symptom_screen.dart';
import '../screens/patient/pt_health_record_flow/pt_health_record1/pt_health_record1_screen.dart';
import '../screens/patient/pt_health_record_flow/pt_health_record2/pt_health_record2_screen.dart';
import '../screens/patient/pt_home/pt_home_screen.dart';
import '../screens/patient/pt_more_services_flow/pt_available_doctor_profile_detail/pt_available_doctor_profile_detail_screen.dart';
import '../screens/patient/pt_more_services_flow/pt_more_services/pt_more_services_screen.dart';
import '../screens/patient/pt_payment_history_flow/pt_payment_details/pt_payment_details_screen.dart';
import '../screens/patient/pt_payment_history_flow/pt_payment_history/pt_payment_history_screen.dart';
import '../screens/patient/pt_setting_flow/pt_multi_profile/pt_multi_profile_screen.dart';
import '../screens/patient/pt_setting_flow/pt_settings/pt_setting_screen.dart';
import 'route_paths.dart';

/// All app navigation and routing  with arguments
class Routes {
  static List<GetPage>? get pages {
    List<GetPage> list = [];

    list.add(
      _buildRoute(
        name: RoutePaths.SPLASH,
        widget: const SplashScreen(),
      ),
    );
    list.add(
      _buildRoute(
        name: RoutePaths.ONBOARDING,
        widget: const OnBoardingScreen(),
        bindings: OnBoardingBindings(),
      ),
    );
    list.add(
      _buildRoute(
        name: RoutePaths.SELECT_USER,
        widget: const SelectUserScreen(),
        bindings: SelectUserBindings(),
      ),
    );
    list.add(
      _buildRoute(
        name: RoutePaths.LOGIN,
        widget: const LoginScreen(),
        bindings: LoginBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.OTP_VERIFICATION,
        widget: const OtpVerificationScreen(),
        bindings: OtpVerificationBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_HOME,
        widget: const DrHomeScreen(),
        bindings: DrHomeBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_BOTTOM_TAB,
        widget: const DrBottomNavigation(),
        bindings: DrBottomNavigationBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_PROFILE,
        widget: const DrProfileScreen(),
        bindings: DrProfileBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_SETTINGS,
        widget: const DrSettingsScreen(),
        bindings: DrSettingsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_MORE_SERVICES,
        widget: const DrMyWalletScreen(),
        bindings: DrMyWalletBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_NOTIFICATION,
        widget: const DrNotificationScreen(),
        bindings: DrNotificationBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_VIRTUAL_CONSULTANCY,
        widget: const DrVirtualConsultancyScreen(),
        bindings: DrVirtualConsultancyBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_SET_YOUR_SCHEDULE,
        widget: const DrSetYourScheduleScreen(),
        bindings: DrSetYourScheduleBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_SET_YOUR_CALENDAR_VIRTUAL,
        widget: const DrSetYourCalendarVirtualScreen(),
        bindings: DrSetYourCalendarVirtualBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_SET_YOUR_CALENDAR_PHYSICAL,
        widget: const DrSetYourCalendarPhysicalScreen(),
        bindings: DrSetYourCalendarPhysicalBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_ADD_DEGREE,
        widget: const DrAddDegreeScreen(),
        bindings: DrAddDegreeBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_EDIT_PROFILE,
        widget: const DrEditProfileScreen(),
        bindings: DrEditProfileBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_CREATE_PATIENT,
        widget: const DrCreatePatientScreen(),
        bindings: DrCreatePatientBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_CONSULTANCY_HISTORY,
        widget: const DrConsultancyHistoryScreen(),
        bindings: DrConsultancyHistoryBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_SEE_WITHDRAWAL_METHODS,
        widget: const DrSeeWithdrawalMethodsScreen(),
        bindings: DrSeeWithdrawalMethodsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_SET_WITHDRAWAL_METHOD,
        widget: const DrSetWithdrawalMethodScreen(),
        bindings: DrSetWithdrawalMethodBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_WITHDRAW_BALANCE,
        widget: const DrWithDrawBalanceScreen(),
        bindings: DrWithdrawBalanceBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_CONSULTANCY_HISTORY_DETAIL,
        widget: const DrConsultancyHistoryDetailScreen(),
        bindings: DrConsultancyHistoryDetailBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_PHYSICAL_CONSULTANCY_DETAIL,
        widget: const DrPhysicalConsultancyDetailScreen(),
        bindings: DrPhysicalConsultancyDetailBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.GLOBAL_SEARCH,
        widget: const GlobalSearchScreen(),
        bindings: GlobalSearchBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.SEARCH_FILTER_SERVICE,
        widget: const SearchFilterServiceScreen(),
        bindings: SearchFilterServiceBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.SEARCH_HOSPITAL_LIST,
        widget: const SearchHospitalListScreen(),
        bindings: SearchHospitalListBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.SEARCH_BLOOD_BANK,
        widget: const SearchBloodBankScreen(),
        bindings: SearchBloodBankBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.SEARCH_DIAGNOSTIC_CENTER,
        widget: const SearchDiagnosticCenterScreen(),
        bindings: SearchDiagnosticCenterBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.SEARCH_TRAUMA_CENTER,
        widget: const SearchTraumaCenterScreen(),
        bindings: SearchTraumaCenterBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.SEARCH_MEDICINE_LIST,
        widget: const SearchMedicineListScreen(),
        bindings: SearchMedicineListBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.SEARCH_MEDICINE_DETAILS,
        widget: const SearchMedicineDetailsScreen(),
        bindings: SearchMedicineDetailsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_VITAL_HEALTH_METRICS,
        widget: const DrVitalHealthMetricsScreen(),
        bindings: DrVitalHealthMetricsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_MY_PATIENT,
        widget: const DrMyPatientScreen(),
        bindings: DrMyPatientBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_NEW_CONSULTANCY,
        widget: const DrNewConsultancyScreen(),
        bindings: DrNewConsultancyBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_SYMPTOM_CHECKER,
        widget: const DrSymptomCheckerScreen(),
        bindings: DrSymptomCheckerBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_VISIT_APPOINTMENT,
        widget: const DrVisitAppointmentScreen(),
        bindings: DrVisitAppointmentBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_FOLLOWUP_APPOINTMENT,
        widget: const DrFollowUpAppointmentScreen(),
        bindings: DrFollowUpAppointmentBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_IDENTIFIED_DISEASE,
        widget: const DrIdentifiedDiseaseScreen(),
        bindings: DrIdentifiedDiseaseBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_FOLLOWUP_IDENTIFIED_DISEASE,
        widget: const DrFollowupIdentifiedDiseaseScreen(),
        bindings: DrFollowupIdentifiedDiseaseBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_PRESCRIBE_MEDICINE,
        widget: const DrPrescribeMedicineScreen(),
        bindings: DrPrescribeMedicineBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_FOLLOWUP_PRESCRIBE_MEDICINE,
        widget: const DrFollowupPrescribeMedicineScreen(),
        bindings: DrFollowupPrescribeMedicineBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_ADD_NEW_MEDICINE,
        widget: const DrAddNewMedicineScreen(),
        bindings: DrAddNewMedicineBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_SYMPTOM_CHECKER_STEP_2,
        widget: const DrSymptomCheckerStep2Screen(),
        bindings: DrSymptomCheckerStep2Bindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_SPECIAL_SYMPTOM,
        widget: const DrSpecialSymptomScreen(),
        bindings: DrSpecialSymptomBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_DYNAMIC_SYMPTOM,
        widget: const DrDynamicSymptomScreen(),
        bindings: DrDynamicSymptomBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_ADDITIONAL_COMMENT,
        widget: const DrAdditionalCommentScreen(),
        bindings: DrAdditionalCommentBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_PATIENT_REVIEW,
        widget: const DrPatientReviewScreen(),
        bindings: DrPatientReviewBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_APPOINTMENT_MEDICINE_LIST,
        widget: const DrAppointmentMedicineListScreen(),
        bindings: DrAppointmentMedicineListBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_APPOINTMENT_MEDICINE_DETAILS,
        widget: const DrAppointmentMedicineDetailsScreen(),
        bindings: DrAppointmentMedicineDetailsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_FOLLOWUP_UPDATE_DIAGNOSTICS_TEST,
        widget: const DrFollowUpUpdateDiagnosticsTestScreen(),
        bindings: DrFollowUpUpdateDiagnosticsTestBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_VISIT_NEXT_DATE,
        widget: const DrVisitNextDateScreen(),
        bindings: DrVisitNextDateBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_FOLLOWUP_DATE,
        widget: const DrFollowUpDateScreen(),
        bindings: DrFollowUpDateBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_VERIFY_EMAIL_NUMBER,
        widget: const DrVerifyEmailNumberScreen(),
        bindings: DrVerifyEmailNumberBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_OTP_VERIFICATION,
        widget: const DrOtpVerificationScreen(),
        bindings: DrOTPVerificationBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_EC_PATIENT_LIST,
        widget: const DrECPatientListScreen(),
        bindings: DrECPatientListBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_APPOINTMENT_DATA_SUBMITTED,
        widget: const DrAppointmentDataSubmittedScreen(),
        bindings: DrAppointmentDataSubmittedBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_PATIENT_DETAIL,
        widget: const DrPatientDetailScreen(),
        bindings: DrPatientDetailBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_WITHDRAW_REQUEST_SUBMITTED,
        widget: const DrWithdrawRequestSubmittedScreen(),
        bindings: DrWithdrawRequestSubmittedBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.NO_INTERNET,
        widget: const NoInternetScreen(),
        bindings: NoInternetBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_VISIT_UPDATE_SYMPTOMS,
        widget: const DrVisitUpdateSymptomsScreen(),
        bindings: DrVisitUpdateSymptomsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_UPDATE_VITAL_SIGNS,
        widget: const DrUpdateVitalSignsScreen(),
        bindings: DrUpdateVitalSignsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_VISIT_COMPLETE_APPOINTMENT,
        widget: const DrVisitCompleteAppointmentScreen(),
        bindings: DrVisitCompleteAppointmentBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_FOLLOWUP_COMPLETE_APPOINTMENT,
        widget: const DrFollowUpCompleteAppointmentScreen(),
        bindings: DrFollowUpCompleteAppointmentBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_FOLLOWUP_UPDATE_SYMPTOMS,
        widget: const DrFollowupUpdateSymptomsScreen(),
        bindings: DrFollowupUpdateSymptomsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_DIAGNOSTICS_TEST_IMAGES,
        widget: const DrDiagnosticsTestImagesScreen(),
        bindings: DrDiagnosticsTestImagesBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_SCHEDULE_SUBMIT,
        widget: const DrScheduleSubmittedScreen(),
        bindings: DrScheduleSubmitBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_CLINICAL_DECISION,
        widget: const DrClinicalDecisionScreen(),
        bindings: DrClinicalDecisionBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.DR_VISIT_UPDATE_DIAGNOSTIC_TEST,
        widget: const DrVisitUpdateDiagnosticTestScreen(),
        bindings: DrVisitUpdateDiagnosticTestBindings(),
      ),
    );

    /**
        Patient ROUTE PATHS
     */
    list.add(
      _buildRoute(
        name: RoutePaths.PT_PAYMENT_HISTORY,
        widget: const PtPaymentHistoryScreen(),
        bindings: PtPaymentHistoryBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_BOTTOM_TAB,
        widget: const PtBottomNavigation(),
        bindings: PtBottomNavigationBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_SETTINGS,
        widget: const PtSettingScreen(),
        bindings: PtSettingBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_PROFILE,
        widget: const PtProfileScreen(),
        bindings: PtProfileBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_HOME,
        widget: const PtHomeScreen(),
        bindings: PtHomeBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_MORE_SERVICES,
        widget: const PtMoreServicesScreen(),
        bindings: PtMoreServicesBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_VERIFY_EMAIL_NUMBER,
        widget: const PtVerifyEmailNumberScreen(),
        bindings: PtVerifyEmailNumberBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_OTP_VERIFICATION,
        widget: const PtOtpVerificationScreen(),
        bindings: PtOTPVerificationBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_EDIT_PROFILE,
        widget: const PtEditPatientProfileScreen(),
        bindings: PtEditProfileBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_AVAILABLE_DOCTORS,
        widget: const PtAvailableDoctorScreen(),
        bindings: PtAvailableDoctorBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_AVAILABLE_DOCTOR_DETAILS,
        widget: const PtAvailableDoctorProfileDetailScreen(),
        bindings: PtAvailableDoctorProfileDetailBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_NEAR_BY_HOSPITAL,
        widget: const PtNearByHospitalScreen(),
        bindings: PtNearByHospitalBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_PAYMENT_DETAILS,
        widget: const PtPaymentDetailsScreen(),
        bindings: PtPaymentDetailsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_MULTI_PROFILE,
        widget: const PtMultiProfileScreen(),
        bindings: PtMultiProfileBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_SELECT_PATIENT_PROFILE,
        widget: const PtSelectPatientProfileScreen(),
        bindings: PtSelectPatientProfileBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_HEALTH_RECORD1,
        widget: const PtHealthRecord1Screen(),
        bindings: PtHealthRecord1Bindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_HEALTH_RECORD2,
        widget: const PtHealthRecord2Screen(),
        bindings: PtHealthRecord2Bindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_APPOINTMENT_LIST,
        widget: const PtAppointmentListScreen(),
        bindings: PtAppointmentListBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_FUTURE_APPOINTMENT_DETAILS,
        widget: const PtFutureAppointmentDetailsScreen(),
        bindings: PtFutureAppointmentDetailsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_VISIT_FOLLOWUP_HISTORY,
        widget: const PtVisitFollowupHistoryScreen(),
        bindings: PtVisitFollowupHistoryBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_COMPLETED_APPOINTMENT_DETAILS,
        widget: const PtCompletedAppointmentDetailsScreen(),
        bindings: PtCompletedAppointmentDetailsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_VITAL_HEALTH_METRICS,
        widget: const PtVitalHealthMetricsScreen(),
        bindings: PtVitalHealthMatrixBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_SYMPTOM_CHECKER,
        widget: const PtSymptomCheckerScreen(),
        bindings: PtSymptomCheckerBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_SPECIAL_SYMPTOM,
        widget: const PtSpecialSymptomScreen(),
        bindings: PtSpecialSymptomsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_SYMPTOM_CHECKER_STEP_2,
        widget: const PtSymptomCheckerStep2Screen(),
        bindings: PtSymptomsCheckerStep2Bindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_DYNAMIC_SYMPTOM,
        widget: const PtDynamicSymptomScreen(),
        bindings: PtDynamicSymptomBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_ADDITIONAL_COMMENT,
        widget: const PtAdditionalCommentScreen(),
        bindings: PtAdditionalCommentsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_BOOKING_REVIEW,
        widget: const PtBookingReviewScreen(),
        bindings: PtBookingReviewsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_DOCTOR_SELECTION,
        widget: const PtDoctorSelectionScreen(),
        bindings: PtDoctorSelectionBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_FOLLOWUP_VITAL,
        widget: const PtFollowUpVitalScreen(),
        bindings: PtFollowUpVitalBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_FOLLOWUP_MEDICINE_STATUS,
        widget: const PtFollowUpMedicineStatusScreen(),
        bindings: PtFollowUpMedicineStatusBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_FOLLOWUP_DIAGNOSTIC_STATUS,
        widget: const PtFollowUpDiagnosticStatusScreen(),
        bindings: PtFollowUpDiagnosticStatusBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_DOCTOR_AVAILABLE_DAYS,
        widget: const PtDoctorAvailableDaysScreen(),
        bindings: PtDoctorAvailableDaysBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_DOCTOR_TIME_SLOT_SELECTION,
        widget: const PtDoctorTimeSlotSelectionScreen(),
        bindings: PtDoctorTimeSlotSelectionBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_REVIEW_BOOKING,
        widget: const PtReviewBookingScreen(),
        bindings: PtReviewBookingBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_PREVIOUS_SYMPTOMS,
        widget: const PtPreviousSymptomsScreen(),
        bindings: PtPreviousSymptomsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_COUGH_SYMPTOM,
        widget: const PtCoughSymptomScreen(),
        bindings: PtCoughSymptomBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_CURRENT_NEW_SYMPTOMS,
        widget: const PtCurrentNewSymptomsScreen(),
        bindings: PtCurrentNewSymptomsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_FOLLOWUP_REVIEW,
        widget: const PtFollowUpReviewScreen(),
        bindings: PtFollowUpReviewBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_THANK_YOU_BOOKING,
        widget: const PtThankYouBookingScreen(),
        bindings: PtThankYouBookingBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_PAIN_SYMPTOM,
        widget: const PtPainSymptomScreen(),
        bindings: PtPainSymptomBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_PREVIOUS_MEDICATION_RECORDS,
        widget: const PtPreviousMedicationRecordsScreen(),
        bindings: PtPreviousMedicationBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_ADD_PREVIOUS_MEDICATION_RECORD,
        widget: const PtAddPreviousMedicationRecordScreen(),
        bindings: PtAddPreviousMedicationRecordBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_PREVIOUS_MEDICATION_DIAGNOSTICS,
        widget: const PtPreviousMedicationDiagnosticsScreen(),
        bindings: PtPreviousMedicationDiagnosticsBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.PT_HOME_SYMPTOMS,
        widget: const PtHomeSymptomsScreen(),
        bindings: PtHomeSymptomsBindings(),
      ),
    );

    return list;
  }

  static GetPage _buildRoute({
    required String name,
    required Widget widget,
    Bindings? bindings,
  }) {
    return GetPage(
      name: name,
      page: () => widget,
      binding: bindings,
    );
  }
}
