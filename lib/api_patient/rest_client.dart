import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/api_patient/patient_api_interface.dart';
import 'package:med_ai/api_patient/patient_response_key.dart';
import 'package:med_ai/routes/route_paths.dart';
import 'package:med_ai/widgets/custom_loader.dart';

import '../api/api_endpoints.dart';
import '../constant/app_strings_key.dart';
import '../constant/storage_keys.dart';
import '../constant/ui_constant.dart';
import '../utils/app_preference/login_preferences.dart';
import '../utils/utilities.dart';
import 'http_exception.dart';

enum RequestType {
  post,
  put,
  get,
  delete,
}

class RestClient {
  final PatientApiCallBacks _apiCallBacks;

  late http.Response response;
  late Duration timeOut;
  late Map<String, String> headers;
  late String url;
  late RequestType requestType;
  late Map requestParam;
  late String apiEndPoint;
  int apiCallCount = 0;
  int totalApiCall = 3;

  RestClient(this._apiCallBacks);

  /* Call api using http request
  * @param context
  * @param requestType - GET,POST,DELETE...
  * @param requestParams - Pass api request parameters (Body)
  * @param apiEndPoint - In which form data is return from api (Object or Array)
  * @param responseListener - Callback to handle api response
  * */

  Future<void> apiCall({
    Map requestParam = const {},
    String apiEndPoint = "",
    RequestType requestType = RequestType.get,
    bool isLoading = true,
    String queryParams = '',
    bool needAuthorization = true,
    File? profileImage,
    String? imageName,
    int apiCallCountValue = 0,
    List<File>? imageList,
  }) async {
    bool isInternet = await Utilities.isConnectedNetwork();
    this.requestType = requestType;
    this.requestParam = requestParam;
    this.apiEndPoint = apiEndPoint;
    apiCallCount = apiCallCountValue;

    if (!isInternet) {
      _apiCallBacks.onConnectionError("Check your internet connection and try again", apiEndPoint);
      // Utilities.showNoInternetDialog(Get.context!, () {
      //   Get.back();
      //   RestClient(_apiCallBacks).apiCall(
      //       requestParam: requestParam,
      //       apiEndPoint: apiEndPoint,
      //       requestType: requestType,
      //       isLoading: isLoading,
      //       queryParams: queryParams,
      //       needAuthorazation: needAuthorazation,
      //       profileImage: profileImage,
      //       imageName: imageName,
      //       imageList: imageList);
      // });
    } else {
      _apiCallBacks.onLoading(isLoading, apiEndPoint);

      //time out of the API
      timeOut = const Duration(seconds: 30);
      print('accessToken ${LoginPreferences().sharedPrefRead(StorageKeys.token)}');
      switch (requestType) {
        case RequestType.delete:
          headers = {
            HttpHeaders.contentTypeHeader: 'application/json; charset=UTF-8',
            'Authorization': needAuthorization ? 'Bearer ${LoginPreferences().sharedPrefRead(StorageKeys.token)}' : '',
          };
          break;
        case RequestType.get:
          headers = {
            HttpHeaders.contentTypeHeader: 'application/json',
            'Authorization': needAuthorization ? 'Bearer ${LoginPreferences().sharedPrefRead(StorageKeys.token)}' : '',
            'Accept': 'application/json',
          };
          break;
        case RequestType.post:
          headers = {
            HttpHeaders.contentTypeHeader: 'application/json',
            'Authorization': needAuthorization ? 'Bearer ${LoginPreferences().sharedPrefRead(StorageKeys.token)}' : '',
            'Accept': 'application/json',
          };
          break;
        case RequestType.put:
          headers = {
            HttpHeaders.contentTypeHeader: 'application/json',
            'Authorization': needAuthorization ? 'Bearer ${LoginPreferences().sharedPrefRead(StorageKeys.token)}' : '',
            'Accept': 'application/json',
          };
          break;
      }

      url = ApiEndpoints.baseUrl + apiEndPoint;
      if (queryParams.isNotEmpty) {
        url += queryParams;
      }
      LoadingDialog.fullScreenLoader();
      try {
        callApiMethod();
      } catch (e) {
        if (kDebugMode) {}
        if (e.runtimeType == TimeoutException) {
          if (apiCallCount < totalApiCall) {
            apiCallCount++;
            callApiMethod();
          } else {
            LoadingDialog.closeFullScreenDialog();
            HttpExceptionHandler.onException(e, _apiCallBacks, apiEndPoint);
          }
        } else {
          LoadingDialog.closeFullScreenDialog();
          HttpExceptionHandler.onException(e, _apiCallBacks, apiEndPoint);
        }
      } finally {
        _apiCallBacks.onLoading(false, apiEndPoint);
      }
    }
  }

  void callApiMethod() async {
    switch (requestType) {
      case RequestType.post:
        response = await http
            .post(
              Uri.parse(url),
              headers: headers,
              body: json.encode(requestParam),
            )
            .timeout(timeOut);
        break;

      case RequestType.put:
        response = await http
            .put(
              Uri.parse(url),
              headers: headers,
              body: json.encode(requestParam),
            )
            .timeout(timeOut);
        break;

      case RequestType.get:
        response = await http
            .get(
              Uri.parse(url),
              headers: headers,
            )
            .timeout(timeOut);
        break;

      case RequestType.delete:
        response = await http
            .delete(
              Uri.parse(url),
              headers: headers,
              body: json.encode(requestParam),
            )
            .timeout(timeOut);
        break;
    }
    handleResponse(response, apiEndPoint, response.body);
  }

  void handleResponse(response, String apiEndPoint, String jsonResponse) {
    LoadingDialog.closeFullScreenDialog();
    final responseBody = json.decode(jsonResponse);
    if (response.statusCode == 401) {
      LoginPreferences().sharedPrefEraseAllData();
      Get.offAllNamed(RoutePaths.LOGIN, arguments: {'isDoctor': isDoctor});
      throw (responseBody['detail']);
    }
    if (response.statusCode != 200) {
      throw (AppStringKey.somethingWentWrong.tr);
    } else {
      if (response.statusCode == 200 || response.statusCode == HttpStatus.created) {
        if (responseBody.containsKey(PatientResponseKey.doctorCode)) {
          if (responseBody[PatientResponseKey.doctorCode] == '200') {
            _apiCallBacks.onSuccess(responseBody, apiEndPoint);
          } else {
            if (responseBody.containsKey(PatientResponseKey.errorMsg)) {
              _apiCallBacks.onError(responseBody[PatientResponseKey.errorMsg], apiEndPoint);
            } else {
              _apiCallBacks.onError(AppStringKey.somethingWentWrong.tr, apiEndPoint);
            }
          }
        } else {
          _apiCallBacks.onSuccess(responseBody, apiEndPoint);
        }
      } else {
        throw (AppStringKey.somethingWentWrong.tr);
      }
    }
  }
}
