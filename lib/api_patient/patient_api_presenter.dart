import 'package:med_ai/api_patient/patient_api_end_point.dart';
import 'package:med_ai/api_patient/patient_api_interface.dart';
import 'package:med_ai/api_patient/patient_web_fields_key.dart';
import 'package:med_ai/api_patient/rest_client.dart';

class PatientApiPresenter {
  final PatientApiCallBacks _apiCallBacks;

  PatientApiPresenter(this._apiCallBacks);

  // Get User getUserDetails
  Future<void> getUserDetails(String personId) async {
    Map requestParam = toUserDetailsJson(personId);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: PatientApiEndPoints.getPatientDetails,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toUserDetailsJson(String personId) => {
        PatientWebFieldKey.personId: personId,
      };

  // Get Country List
  Future<void> dropDownCountries(String lang) async {
    Map requestParam = toDropdownCountriesJson(lang);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: PatientApiEndPoints.dropdownCountries,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toDropdownCountriesJson(String lang) => {
        PatientWebFieldKey.lang: lang,
      };

  // Get All Dropdown Values Of Page2
  Future<void> getDropDownValuesPage2(String lang) async {
    Map requestParam = toDropDownValuesPage2Json(lang);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: PatientApiEndPoints.dropdownValuesPage2,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toDropDownValuesPage2Json(String lang) => {
        PatientWebFieldKey.lang: lang,
      };

  // Get Division List Based On Country Name
  Future<void> getDropDownDivision(String lang, String countryName) async {
    Map requestParam = toDropdownDivisionJson(lang, countryName);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: PatientApiEndPoints.dropdownDivisions,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toDropdownDivisionJson(String lang, String countryName) => {
        PatientWebFieldKey.lang: lang,
        PatientWebFieldKey.country: countryName,
      };

  // Get District List Based On Country Name
  Future<void> getDropDownDistrict(String lang, String countryName, String divisionName) async {
    Map requestParam = toDropdownDistrictJson(lang, countryName, divisionName);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: PatientApiEndPoints.dropdownDistrictOrCountry,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toDropdownDistrictJson(String lang, String countryName, String divisionName) => {
        PatientWebFieldKey.lang: lang,
        PatientWebFieldKey.country: countryName,
        PatientWebFieldKey.division: divisionName,
      };

  // Get Multi Profile List
  Future<void> getMultiProfiles(String username) async {
    Map requestParam = toMultiProfileJson(username);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: PatientApiEndPoints.getPatientsProfile,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toMultiProfileJson(String username) => {
        PatientWebFieldKey.username: username,
      };

  // Submit Patinet Profile
  Future<void> submitPatientRegistration(
    String username,
    String isPrimaryUser,
    String phone,
    String firstName,
    String lastName,
    String nhsNumber,
    List<String> languagesSpeaks,
    int age,
    String dob,
    String gender,
    String occupation,
    String bloodGroup,
    String addressLine,
    String zipPostCode,
    String cityVill,
    String region,
    String country,
    String education,
    String monthlyExpenditure,
    String ethnicity,
    String personLocation,
    String maritalStatus,
    String personType,
    String consentGiven,
    String consentImage,
    String referralCode,
  ) async {
    Map requestParam = toSubmitPatientRegistrationToJson(
      username,
      isPrimaryUser,
      phone,
      firstName,
      lastName,
      nhsNumber,
      languagesSpeaks,
      age,
      dob,
      gender,
      occupation,
      bloodGroup,
      addressLine,
      zipPostCode,
      cityVill,
      region,
      country,
      education,
      monthlyExpenditure,
      ethnicity,
      personLocation,
      maritalStatus,
      personType,
      consentGiven,
      consentImage,
      referralCode,
    );
    print('requestParam $requestParam');
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: PatientApiEndPoints.patientRegistration,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toSubmitPatientRegistrationToJson(
    String username,
    String isPrimaryUser,
    String phone,
    String firstName,
    String lastName,
    String nhsNumber,
    List<String> languagesSpeaks,
    int age,
    String dob,
    String gender,
    String occupation,
    String bloodGroup,
    String addressLine,
    String zipPostCode,
    String cityVill,
    String region,
    String country,
    String education,
    String monthlyExpenditure,
    String ethnicity,
    String personLocation,
    String maritalStatus,
    String personType,
    String consentGiven,
    String consentImage,
    String referralCode,
  ) =>
      {
        PatientWebFieldKey.username: username,
        PatientWebFieldKey.isPrimaryUser: isPrimaryUser,
        PatientWebFieldKey.phone: phone,
        PatientWebFieldKey.firstName: firstName,
        PatientWebFieldKey.lastName: lastName,
        PatientWebFieldKey.nhsNumber: nhsNumber,
        PatientWebFieldKey.languagesSpeaks: languagesSpeaks,
        PatientWebFieldKey.age: age,
        PatientWebFieldKey.DOB: dob,
        PatientWebFieldKey.gender: gender,
        PatientWebFieldKey.occupation: occupation,
        PatientWebFieldKey.bloodGroup: bloodGroup,
        PatientWebFieldKey.addressLine: addressLine,
        PatientWebFieldKey.zipPostCode: zipPostCode,
        PatientWebFieldKey.cityVill: cityVill,
        PatientWebFieldKey.region: region,
        PatientWebFieldKey.country: country,
        PatientWebFieldKey.education: education,
        PatientWebFieldKey.monthlyExpenditure: monthlyExpenditure,
        PatientWebFieldKey.ethnicity: ethnicity,
        PatientWebFieldKey.personLocation: personLocation,
        PatientWebFieldKey.maritalStatus: maritalStatus,
        PatientWebFieldKey.personType: personType,
        PatientWebFieldKey.consentGiven: consentGiven,
        PatientWebFieldKey.consentImage: consentImage,
        PatientWebFieldKey.referralCode: referralCode,
      };

  // Book Appointment Profile List
  Future<void> getBookAppointmentProfileDetails(String username) async {
    Map requestParam = toBookAppointmentProfileDetailsJson(username);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: PatientApiEndPoints.bookAppointmentProfile,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toBookAppointmentProfileDetailsJson(String username) => {
        PatientWebFieldKey.username: username,
      };

  // For Get Breathing Dropdown Data
  Future<void> getBreathingData(String lang) async {
    Map requestParam = toBreathingDataToJson(lang);
    await RestClient(_apiCallBacks).apiCall(
        requestParam: requestParam,
        needAuthorization: true,
        requestType: RequestType.post,
        apiEndPoint: PatientApiEndPoints.vitalHealthDropdownValue);
  }

  Map<String, dynamic> toBreathingDataToJson(String lang) => {
        PatientWebFieldKey.lang: lang,
      };

  Future<void> getSuggestionSymptomsData(List<String> symptoms, String land) async {
    Map requestParams = toSuggestionSymptomsDataToJson(symptoms, land);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: PatientApiEndPoints.getSuggestionSymptoms,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toSuggestionSymptomsDataToJson(List<String> symptoms, String land) => {
        PatientWebFieldKey.symptoms: symptoms,
        PatientWebFieldKey.lang: land,
      };

  // Get All Symptom For Dropdown Textfield In Symptom Checker Screen
  Future<void> getSymptomsSearchOption(String lang) async {
    await RestClient(_apiCallBacks).apiCall(
      needAuthorization: true,
      apiEndPoint: PatientApiEndPoints.getSymptomsSearchOption,
      queryParams: lang,
    );
  }

  // Get All Data For Special Symptoms
  Future<void> getFeverOptionData(String lang) async {
    Map requestParams = toFeverOptionDataToJson(lang);
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: PatientApiEndPoints.getSpecialSymptomFeverData,
      needAuthorization: true,
      requestType: RequestType.post,
      requestParam: requestParams,
    );
  }

  Map<String, dynamic> toFeverOptionDataToJson(String land) => {
        PatientWebFieldKey.lang: land,
      };

  // Get All Data For Special Symptoms Pain
  Future<void> getPainOptionData(String lang) async {
    Map requestParams = toPainOptionDataToJson(lang);
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: PatientApiEndPoints.getSpecialSymptomPainData,
      needAuthorization: true,
      requestType: RequestType.post,
      requestParam: requestParams,
    );
  }

  Map<String, dynamic> toPainOptionDataToJson(String land) => {
        PatientWebFieldKey.lang: land,
      };

  // Get All Data For Special Symptoms Cough
  Future<void> getCoughOptionData(String lang) async {
    Map requestParams = toCoughOptionDataToJson(lang);
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: PatientApiEndPoints.getSpecialSymptomCoughData,
      needAuthorization: true,
      requestType: RequestType.post,
      requestParam: requestParams,
    );
  }

  Map<String, dynamic> toCoughOptionDataToJson(String land) => {
        PatientWebFieldKey.lang: land,
      };

  // Submit Special symptoms API
  Future<void> submitSpecialSymptomData(Map requestParams) async {
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: PatientApiEndPoints.submitSpecialSymptoms,
      needAuthorization: true,
      requestType: RequestType.post,
      requestParam: requestParams,
    );
  }

  // Get Dynamic Symptoms
  Future<void> getDynamicSymptoms(Map requestParams) async {
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: PatientApiEndPoints.getDynamicSymptoms,
      needAuthorization: true,
      requestType: RequestType.post,
      requestParam: requestParams,
    );
  }

  // Submit Patient Profile
  Future<void> submitPatientProfile(Map requestParams) async {
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: PatientApiEndPoints.createPatientProfile,
      needAuthorization: true,
      requestType: RequestType.post,
      requestParam: requestParams,
    );
  }

  void deleteImage(String filePath, String username) async {
    Map requestParams = toDeleteImageToJson(filePath, username);
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: PatientApiEndPoints.uploadImages,
      needAuthorization: true,
      requestType: RequestType.delete,
      requestParam: requestParams,
    );
  }

  Map<String, dynamic> toDeleteImageToJson(String filePath, String username) => {
        PatientWebFieldKey.imagePath: filePath,
        PatientWebFieldKey.username: username,
      };

  // Get Available Doctor List
  Future<void> getAvailableDoctorList() async {
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: PatientApiEndPoints.patientDoctorSelection,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  // Get Doctor Available Days List
  Future<void> getPatientDoctorAvailableDays(String doctorId, String firstDate, String lastDate) async {
    Map requestParams = toPatientDoctorAvailableDaysToJson(doctorId, firstDate, lastDate);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: PatientApiEndPoints.patientDoctorAvailableDays,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toPatientDoctorAvailableDaysToJson(String doctorId, String firstDate, String lastDate) => {
        PatientWebFieldKey.doctorId: doctorId,
        PatientWebFieldKey.firstDate: firstDate,
        PatientWebFieldKey.lastDate: lastDate,
      };

  // Get Doctor Available Slot Data
  Future<void> getDoctorAvailableSlot(String doctorId, String selectedDate) async {
    Map requestParams = toDoctorAvailableSlotToJson(doctorId, selectedDate);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: PatientApiEndPoints.patientDoctorTimeSlotSelection,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toDoctorAvailableSlotToJson(String doctorId, String selectedDate) => {
        PatientWebFieldKey.doctorId: doctorId,
        PatientWebFieldKey.selectedDate: selectedDate,
      };

  // Get Summary View Data
  Future<void> getSummaryViewData(String vId) async {
    Map requestParams = toSummaryViewDataToJson(vId);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: PatientApiEndPoints.getSummaryView,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toSummaryViewDataToJson(String vId) => {
        PatientWebFieldKey.strVid: vId,
      };

  // Check Promo Code is Valid or Not
  Future<void> applyPromoCode(String promoCode, String personId, String personIdDoc) async {
    Map requestParams = toApplyPromoCodeToJson(promoCode, personId, personIdDoc);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: PatientApiEndPoints.checkValidity,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toApplyPromoCodeToJson(String promoCode, String personId, String personIdDoc) => {
        PatientWebFieldKey.promoCode: promoCode,
        PatientWebFieldKey.personId: personId,
        PatientWebFieldKey.personIdDoc: personIdDoc,
      };

  // Function For Book Appointment
  Future<void> bookAppointment(
    String doctorId,
    String consultationId,
    String idType,
    String date,
    String timeSlot,
    String contactNumber,
    String contactMethod,
    String isPaid,
    String appointmentFee,
  ) async {
    Map requestParams = toBookAppointmentToJson(
      doctorId,
      consultationId,
      idType,
      date,
      timeSlot,
      contactNumber,
      contactMethod,
      isPaid,
      appointmentFee,
    );
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: PatientApiEndPoints.bookAppointment,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toBookAppointmentToJson(
    String doctorId,
    String consultationId,
    String idType,
    String date,
    String timeSlot,
    String contactNumber,
    String contactMethod,
    String isPaid,
    String appointmentFee,
  ) =>
      {
        PatientWebFieldKey.doctorId: doctorId,
        PatientWebFieldKey.consultationId: consultationId,
        PatientWebFieldKey.idType: idType,
        PatientWebFieldKey.date: date,
        PatientWebFieldKey.timeSlot: timeSlot,
        PatientWebFieldKey.contactNumber: contactNumber,
        PatientWebFieldKey.contactMethod: contactMethod,
        PatientWebFieldKey.isPaid: isPaid,
        PatientWebFieldKey.appointmentFee: appointmentFee,
      };

  // Function For Create Payment
  Future<void> createPayment(
    String appointmentId,
    String totalAmount,
    String referenceId,
    String trxID,
    String currency,
    String promoCode,
    String promoDiscount,
  ) async {
    Map requestParams = toCreatePaymentToJson(
      appointmentId,
      totalAmount,
      referenceId,
      trxID,
      currency,
      promoCode,
      promoDiscount,
    );
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: PatientApiEndPoints.createPayment,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toCreatePaymentToJson(
    String appointmentId,
    String totalAmount,
    String referenceId,
    String trxID,
    String currency,
    String promoCode,
    String promoDiscount,
  ) =>
      {
        PatientWebFieldKey.appointmentId: appointmentId,
        PatientWebFieldKey.totalAmount: totalAmount,
        PatientWebFieldKey.referenceId: referenceId,
        PatientWebFieldKey.trxID: trxID,
        PatientWebFieldKey.currency: currency,
        PatientWebFieldKey.promoCode: promoCode,
        PatientWebFieldKey.promoDiscount: promoDiscount,
      };

  // Function For Update Promo Code
  Future<void> updatePromoCode(
    String promoCode,
    String personId,
  ) async {
    Map requestParams = toUpdatePromoCodeJson(
      promoCode,
      personId,
    );
    print('object1 $requestParams');
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: PatientApiEndPoints.updatePromoUsage,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toUpdatePromoCodeJson(
    String promoCode,
    String personId,
  ) =>
      {
        PatientWebFieldKey.promoCode: promoCode,
        PatientWebFieldKey.personId: personId,
      };

  // Function For Get Agora Access Token
  Future<void> getAccessToken(String roomId) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: roomId,
      apiEndPoint: PatientApiEndPoints.getAgoraAccessToken,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }
}
