class PatientApiEndPoints {
  //define which api you want to call
  static const String login = "api/login";
  static const String getPatientsProfile = "/all_user/get_patients";
  static const String getPatientDetails = "/patients/get_patient_details";
  static const String dropdownCountries = "/dropdown/get_countries";
  static const String dropdownDivisions = "/dropdown/get_divisions";
  static const String dropdownDistrictOrCountry = "/dropdown/get_district_or_county";
  static const String dropdownValuesPage2 = "/dropdown/profile/page_two";
  static const String patientRegistration = "/all_user/patient_registration";
  static const String bookAppointmentProfile = "/all_user/get_patients";
  static const String vitalHealthDropdownValue = "/dropdown/visit/create_patient_profile";
  static const String patientSymptomsChecker = "/all_user/symptoms/get_all_symptoms";
  static const String getSuggestionSymptoms = "/all_user/symptoms/get_symptom_suggestions";
  static const String getSymptomsSearchOption = "/all_user/symptoms/get_all_symptoms?lang=";
  static const String getSpecialSymptomFeverData = "/dropdown/special_symptom_fever";
  static const String getSpecialSymptomPainData = "/dropdown/special_symptom_pain";
  static const String getSpecialSymptomCoughData = "/dropdown/special_symptom_cough";

  static const String submitSpecialSymptoms = "/all_user/symptoms/get_special_symptoms";
  static const String getDynamicSymptoms = "/all_user/symptoms/get_dynamic_symptoms";
  static const String createPatientProfile = "/all_user/visit/create_patient_profile";
  static const String uploadImages = "/all_user/image";
  static const String getSummaryView = "/doctors/visit/get_summary_view";
  static const String checkValidity = "/promo/check_validity";
  static const String bookAppointment = "/doctors/book_appointment";
  static const String createPayment = "/patients/payment/create_payment";
  static const String updatePromoUsage = "/promo/update_promo_usage";

  static const String patientDoctorAvailableDays = "/doctors/get_dates";
  static const String patientDoctorTimeSlotSelection = "/doctors/get_available_slot";
  static const String patientDoctorSelection = "/all_user/get_doctor_list";
  static const String getSpeechToText = "/all_user/speech_to_text";
  static const String getAgoraAccessToken = "/doctors/get_agora_token";
}
