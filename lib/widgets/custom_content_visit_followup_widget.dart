import 'package:flutter/material.dart';
import 'package:med_ai/constant/base_style.dart';

import '../constant/app_colors.dart';

class CustomContentVisitFollowUpWidget extends StatelessWidget {
  final String title;
  final String value;
  final double fontSize;

  const CustomContentVisitFollowUpWidget({
    Key? key,
    required this.title,
    required this.value,
    this.fontSize = 12,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Text(
            title,
            style: BaseStyle.textStyleNunitoSansBold(fontSize, AppColors.colorDarkBlue),
          ),
        ),
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                ' : ',
                softWrap: true,
                style: BaseStyle.textStyleNunitoSansRegular(fontSize, AppColors.colorDarkBlue),
              ),
              Flexible(
                child: Text(
                  value,
                  softWrap: true,
                  style: BaseStyle.textStyleNunitoSansRegular(fontSize, AppColors.colorDarkBlue),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
