import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

import '../constant/app_colors.dart';
import '../constant/base_style.dart';
import '../widgets/space_vertical.dart';

class CustomDropDownFieldSelected extends StatelessWidget {
  final String hintTitle;
  final String labelText;
  final List<String> items;
  final String validationText;
  final Function(String? val) onChanged;
  final String? selectedValue;

  const CustomDropDownFieldSelected(
      {Key? key,
      required this.hintTitle,
      required this.items,
      required this.validationText,
      required this.onChanged,
      required this.labelText,
      this.selectedValue = ""})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          labelText,
          style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
        ),
        const SpaceVertical(5),
        Theme(
            data: Theme.of(context).copyWith(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              hoverColor: Colors.transparent,
            ),
            child: selectedValue!.isNotEmpty && selectedValue != null
                ? DropdownButtonFormField2(
                    value: selectedValue,
                    isDense: false,
                    decoration: InputDecoration(
                      focusColor: Colors.transparent,
                      disabledBorder: InputBorder.none,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25),
                        borderSide: const BorderSide(
                          width: 1,
                          color: AppColors.colorLightSkyBlue,
                        ),
                      ),
                      errorBorder: InputBorder.none,
                      focusedBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(25),
                          topRight: Radius.circular(25),
                          topLeft: Radius.circular(25),
                        ),
                        borderSide: BorderSide(
                          width: 1,
                          color: AppColors.colorLightSkyBlue,
                        ),
                      ),
                      focusedErrorBorder: InputBorder.none,
                      contentPadding: const EdgeInsets.fromLTRB(
                        0,
                        0,
                        0,
                        0,
                      ),
                      isDense: false,
                      fillColor: AppColors.white,
                      border: InputBorder.none,
                    ),
                    hint: Text(
                      hintTitle,
                      style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.hintColor),
                    ),
                    items: items
                        .map(
                          (item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(
                              item,
                              style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
                            ),
                          ),
                        )
                        .toList(),
                    validator: (value) {
                      if (value == null) {
                        return validationText;
                      }
                      return null;
                    },
                    buttonStyleData: const ButtonStyleData(
                      padding: EdgeInsets.only(
                        left: 15,
                      ),
                    ),
                    onChanged: onChanged,
                    iconStyleData: IconStyleData(
                      iconEnabledColor: AppColors.white,
                      icon: Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width / 8,
                        decoration: const BoxDecoration(
                          color: AppColors.colorDarkBlue,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(25),
                            bottomRight: Radius.circular(
                              25,
                            ),
                          ),
                        ),
                        child: const Icon(
                          Icons.keyboard_arrow_down,
                          size: 20,
                        ),
                      ),
                    ),
                    dropdownStyleData: DropdownStyleData(
                      isOverButton: false,
                      useSafeArea: false,
                      padding: EdgeInsets.zero,
                      elevation: 2,
                      maxHeight: 200,
                      width: MediaQuery.of(context).size.width * 0.775,
                      decoration: BoxDecoration(
                        color: AppColors.white,
                        border: Border.all(
                          color: AppColors.colorLightSkyBlue,
                        ),
                        borderRadius: const BorderRadius.only(
                          bottomRight: Radius.circular(25),
                          bottomLeft: Radius.circular(25),
                        ),
                      ),
                    ),
                  )
                : DropdownButtonFormField2(
                    isDense: false,
                    decoration: InputDecoration(
                      focusColor: Colors.transparent,
                      disabledBorder: InputBorder.none,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25),
                        borderSide: const BorderSide(
                          width: 1,
                          color: AppColors.colorLightSkyBlue,
                        ),
                      ),
                      errorBorder: InputBorder.none,
                      focusedBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(25),
                          topRight: Radius.circular(25),
                          topLeft: Radius.circular(25),
                        ),
                        borderSide: BorderSide(
                          width: 1,
                          color: AppColors.colorLightSkyBlue,
                        ),
                      ),
                      focusedErrorBorder: InputBorder.none,
                      contentPadding: const EdgeInsets.fromLTRB(
                        0,
                        0,
                        0,
                        0,
                      ),
                      isDense: false,
                      fillColor: AppColors.white,
                      border: InputBorder.none,
                    ),
                    hint: Text(
                      hintTitle,
                      style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.hintColor),
                    ),
                    items: items
                        .map(
                          (item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(
                              item,
                              style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
                            ),
                          ),
                        )
                        .toList(),
                    validator: (value) {
                      if (value == null) {
                        return validationText;
                      }
                      return null;
                    },
                    buttonStyleData: const ButtonStyleData(
                      padding: EdgeInsets.only(
                        left: 15,
                      ),
                    ),
                    onChanged: onChanged,
                    iconStyleData: IconStyleData(
                      iconEnabledColor: AppColors.white,
                      icon: Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width / 8,
                        decoration: const BoxDecoration(
                          color: AppColors.colorDarkBlue,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(25),
                            bottomRight: Radius.circular(
                              25,
                            ),
                          ),
                        ),
                        child: const Icon(
                          Icons.keyboard_arrow_down,
                          size: 20,
                        ),
                      ),
                    ),
                    dropdownStyleData: DropdownStyleData(
                      isOverButton: false,
                      useSafeArea: false,
                      padding: EdgeInsets.zero,
                      elevation: 2,
                      maxHeight: 200,
                      width: MediaQuery.of(context).size.width * 0.775,
                      decoration: BoxDecoration(
                        color: AppColors.white,
                        border: Border.all(
                          color: AppColors.colorLightSkyBlue,
                        ),
                        borderRadius: const BorderRadius.only(
                          bottomRight: Radius.circular(25),
                          bottomLeft: Radius.circular(25),
                        ),
                      ),
                    ),
                  )),
      ],
    );
  }
}
