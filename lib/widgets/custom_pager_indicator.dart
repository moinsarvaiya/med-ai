import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/widgets/space_vertical.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../constant/app_colors.dart';
import '../constant/app_strings_key.dart';
import '../constant/base_size.dart';
import '../models/dr_prescribe_medicine_model.dart';
import '../screens/doctor/dr_appointments/dr_follow_up_complete_appointment/dr_follow_up_complete_appointment_screen.dart';
import '../screens/doctor/dr_appointments/dr_visit_complete_appointment/dr_visit_complete_appointment_screen.dart';

class CustomPagerIndicator extends StatelessWidget {
  final List<DrPrescribeMedicineModel> listMedicines;
  final Function(int index)? onEditClick;
  final currentIndex = 0.obs;
  final bool visibleEdit;

  CustomPagerIndicator({
    Key? key,
    required this.listMedicines,
    this.onEditClick,
    this.visibleEdit = true,
  }) : super(
          key: key,
        );
  final controller = PageController(viewportFraction: 1, keepPage: true);

  @override
  Widget build(BuildContext context) {
    final pages = List.generate(
      listMedicines.length,
      (index) => ItemPager(
        listMedicines: listMedicines,
        index: index,
      ),
    );
    return CardEditSection(
      title: AppStringKey.strPrescribedMedication.tr,
      visibleEdit: visibleEdit,
      callBack: () {
        onEditClick!(currentIndex.value);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: BaseSize.height(25),
            child: PageView.builder(
              onPageChanged: (index) {
                currentIndex.value = index;
              },
              controller: controller,
              itemCount: pages.length,
              itemBuilder: (_, index) {
                return pages[index];
              },
            ),
          ),
          Center(
            child: SmoothPageIndicator(
              controller: controller,
              count: pages.length,
              effect: const SlideEffect(
                dotHeight: 8,
                dotWidth: 8,
                activeDotColor: AppColors.colorDarkBlue,
                dotColor: AppColors.colorLightSkyBlue,
                type: SlideType.normal,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ItemPager extends StatelessWidget {
  final List<DrPrescribeMedicineModel> listMedicines;
  final int index;

  const ItemPager({
    Key? key,
    required this.listMedicines,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(child: SubItemPreviousDecision(title: AppStringKey.strBrand.tr, value: listMedicines[index].brandName!)),
              Expanded(
                  child: SubItemPreviousDecision(title: AppStringKey.labelMedicineTiming.tr, value: listMedicines[index].threeTimesDay!)),
            ],
          ),
          const SpaceVertical(15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  child:
                      SubItemPreviousDecision(title: AppStringKey.labelMealInstruction.tr, value: listMedicines[index].mealInstruction!)),
              Expanded(child: SubItemPreviousDecision(title: AppStringKey.strDuration.tr, value: listMedicines[index].duration!)),
            ],
          ),
          const SpaceVertical(15),
          SubItemPreviousDecision(title: AppStringKey.labelSpecialInstruction.tr, value: listMedicines[index].specialInstruction!),
        ],
      ).paddingSymmetric(horizontal: 0, vertical: 0),
    );
  }
}
