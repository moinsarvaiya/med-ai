import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/base_style.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({
    Key? key,
    this.displayLogo = false,
    this.onClick,
    this.isBackButtonVisible = true,
    this.isDividerVisible = false,
    this.titleName = '',
  }) : super(key: key);

  final bool displayLogo;
  final Function()? onClick;
  final bool isBackButtonVisible;
  final bool isDividerVisible;
  final String titleName;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      alignment: Alignment.center,
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 40, left: 10, right: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                isBackButtonVisible
                    ? InkWell(
                        splashColor: AppColors.colorDarkBlue.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(10),
                        onTap: onClick,
                        child: const Icon(
                          Icons.arrow_back_ios_new,
                          size: 16,
                          color: AppColors.colorDarkBlue,
                        ).paddingAll(10),
                      )
                    : const SizedBox.shrink(),
                displayLogo
                    ? Image.asset(
                        AppImages.appLogo,
                        height: 28,
                        width: 74,
                        fit: BoxFit.cover,
                      )
                    : Container(),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 40, left: 10, right: 10, bottom: 10),
            child: Center(
              child: Text(
                titleName,
                style: BaseStyle.textStyleDomineBold(20, AppColors.colorDarkBlue),
              ),
            ),
          ),
          isDividerVisible
              ? const Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Divider(
                    height: 1,
                    color: AppColors.colorLightSkyBlue,
                  ),
                )
              : const SizedBox.shrink()
        ],
      ),
    );
  }
}
