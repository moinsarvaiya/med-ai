import 'package:flutter/material.dart';
import 'package:med_ai/constant/app_colors.dart';

class RippleEffectWidget extends StatelessWidget {
  final double borderRadius;
  final Function()? callBack;
  final Widget widget;
  final Color color;

  const RippleEffectWidget({
    Key? key,
    required this.borderRadius,
    this.color = AppColors.transparent,
    required this.callBack,
    required this.widget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: color,
      borderRadius: BorderRadius.circular(borderRadius),
      child: InkWell(
        borderRadius: BorderRadius.circular(borderRadius),
        onTap: callBack,
        child: widget,
      ),
    );
  }
}
