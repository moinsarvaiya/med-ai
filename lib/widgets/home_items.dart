import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/widgets/ripple_effect_widget.dart';
import 'package:med_ai/widgets/space_vertical.dart';

class HomeItems extends StatelessWidget {
  const HomeItems({Key? key, required this.label, required this.image, this.onClick}) : super(key: key);

  final String label;
  final String image;
  final Function()? onClick;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: BaseSize.width(30),
      decoration: BoxDecoration(
          color: AppColors.colorGrayBackground,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: AppColors.colorGrayBackground2, width: 1)),
      child: RippleEffectWidget(
        borderRadius: 5,
        callBack: onClick,
        widget: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              image,
              height: BaseSize.height(8),
              width: BaseSize.width(8),
            ),
            const SpaceVertical(10),
            Text(
              label.tr,
              style: BaseStyle.textStyleNunitoSansMedium(10, AppColors.colorDarkBlue),
            ),
          ],
        ),
      ),
    ).marginOnly(right: 12);
  }
}
