import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

import '../constant/app_colors.dart';
import '../constant/base_style.dart';
import '../widgets/space_vertical.dart';

class CustomDropDownField extends StatelessWidget {
  final String hintTitle;
  final String labelText;
  final List<String> items;
  final String validationText;
  final bool? lableVisibility;
  final Function(String? val) onChanged;
  final Color? backgroundColor;
  final bool? isReadOnly;
  final String? selectedValue;
  final bool? isRequireField;

  const CustomDropDownField({
    Key? key,
    required this.hintTitle,
    required this.items,
    required this.validationText,
    required this.onChanged,
    required this.labelText,
    this.lableVisibility = true,
    this.backgroundColor = Colors.white,
    this.isReadOnly = false,
    this.selectedValue,
    this.isRequireField,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: lableVisibility!,
          child: Text(
            labelText,
            style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
          ),
        ),
        const SpaceVertical(5),
        Theme(
          data: Theme.of(context).copyWith(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            hoverColor: Colors.transparent,
          ),
          child: DropdownButtonFormField2(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            isDense: false,
            decoration: InputDecoration(
              focusColor: Colors.transparent,
              disabledBorder: InputBorder.none,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25),
                borderSide: const BorderSide(
                  width: 1,
                  color: AppColors.colorLightSkyBlue,
                ),
              ),
              errorBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(25),
                    topRight: Radius.circular(25),
                    topLeft: Radius.circular(25),
                    bottomLeft: Radius.circular(25)),
                borderSide: BorderSide(
                  width: 1,
                  color: AppColors.colorLightSkyBlue,
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(25),
                  topRight: Radius.circular(25),
                  topLeft: Radius.circular(25),
                ),
                borderSide: BorderSide(
                  width: 1,
                  color: AppColors.colorLightSkyBlue,
                ),
              ),
              focusedErrorBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(25),
                  topRight: Radius.circular(25),
                  topLeft: Radius.circular(25),
                ),
                borderSide: BorderSide(
                  width: 1,
                  color: AppColors.colorLightSkyBlue,
                ),
              ),
              contentPadding: const EdgeInsets.all(0),
              isDense: false,
              fillColor: backgroundColor,
              border: InputBorder.none,
            ),
            hint: Text(
              selectedValue != '' ? selectedValue! : hintTitle,
              style: BaseStyle.textStyleNunitoSansRegular(
                14,
                selectedValue != '' ? AppColors.colorDarkBlue : AppColors.hintColor,
              ),
            ),
            items: items
                .map(
                  (item) => DropdownMenuItem<String>(
                    value: item,
                    child: Text(
                      item,
                      style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
                    ),
                  ),
                )
                .toList(),
            validator: selectedValue!.isEmpty && isRequireField!
                ? (value) {
                    if (value == null && !isReadOnly!) {
                      return validationText;
                    }
                    return null;
                  }
                : null,
            buttonStyleData: const ButtonStyleData(
              padding: EdgeInsets.only(
                left: 15,
              ),
            ),
            onChanged: isReadOnly! ? null : onChanged,
            iconStyleData: IconStyleData(
              iconEnabledColor: AppColors.white,
              icon: Container(
                height: 50,
                width: MediaQuery.sizeOf(context).width / 8,
                decoration: const BoxDecoration(
                  color: AppColors.colorDarkBlue,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    bottomRight: Radius.circular(
                      25,
                    ),
                  ),
                ),
                child: const Icon(
                  Icons.keyboard_arrow_down,
                  size: 20,
                ),
              ),
            ),
            dropdownStyleData: DropdownStyleData(
              isOverButton: false,
              useSafeArea: false,
              padding: EdgeInsets.zero,
              elevation: 2,
              maxHeight: MediaQuery.sizeOf(context).height / 4.5,
              width: MediaQuery.sizeOf(context).width * 0.775,
              decoration: BoxDecoration(
                color: AppColors.white,
                border: Border.all(
                  color: AppColors.colorLightSkyBlue,
                ),
                borderRadius: const BorderRadius.only(
                  bottomRight: Radius.circular(25),
                  bottomLeft: Radius.circular(25),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
