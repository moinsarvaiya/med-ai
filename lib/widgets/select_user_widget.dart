import 'package:flutter/material.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/widgets/space_vertical.dart';

class UserSelectWidget extends StatelessWidget {
  const UserSelectWidget({Key? key, required this.image, required this.title, this.onClick}) : super(key: key);

  final String image;
  final String title;
  final Function()? onClick;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(
            spreadRadius: 0,
            blurRadius: 5,
            offset: const Offset(0, 5),
            color: AppColors.colorShadow,
          ),
        ],
      ),
      child: Material(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15.0),
        child: InkWell(
          borderRadius: BorderRadius.circular(15.0),
          onTap: onClick,
          child: Padding(
            padding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 9, bottom: 12),
            child: Column(
              children: [
                Image.asset(
                  image,
                  width: MediaQuery.sizeOf(context).width / 3.7,
                  height: MediaQuery.sizeOf(context).width / 3.7,
                ),
                const SpaceVertical(8),
                Text(
                  title,
                  style: BaseStyle.textStyleNunitoSansBold(16, AppColors.colorDarkBlue),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
