import 'package:flutter/material.dart';
import 'package:med_ai/constant/base_style.dart';

import '../constant/app_colors.dart';

class CustomHeadingVisitFollowUp extends StatelessWidget {
  final String heading;

  const CustomHeadingVisitFollowUp({
    Key? key,
    required this.heading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      heading,
      style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorSkyBlue),
    );
  }
}
