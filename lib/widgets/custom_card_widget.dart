import 'package:flutter/material.dart';

import '../constant/app_colors.dart';

class CustomCardWidget extends StatelessWidget {
  final double borderRadius;
  final Widget widget;
  final double elevation;
  final Color backgroundColor;
  final double shadowOpacity;

  const CustomCardWidget({
    Key? key,
    this.borderRadius = 5,
    this.backgroundColor = AppColors.white,
    this.shadowOpacity = 0.1,
    required this.widget,
    this.elevation = 5,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: elevation,
      color: backgroundColor,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shadowColor: AppColors.colorDarkBlue.withOpacity(shadowOpacity),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(borderRadius),
      ),
      child: widget,
    );
  }
}
