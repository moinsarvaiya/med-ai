import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import '../constant/app_colors.dart';

class LoadingDialog {
  static bool isLoading = false;

  static void fullScreenLoader() {
    if (!isLoading) {
      isLoading = true;
      Get.dialog(
        barrierColor: Colors.white.withOpacity(0.6),
        SpinKitChasingDots(
          size: 50,
          itemBuilder: (_, __) {
            return const DecoratedBox(
              decoration: BoxDecoration(
                color: AppColors.blue,
              ),
            );
          },
        ),
        barrierDismissible: false,
      );
    }
  }

  static void closeFullScreenDialog() {
    isLoading = false;
    Get.back();
  }

  static Widget showLoadingWidget(context) {
    return SpinKitWave(
      size: 30,
      itemBuilder: (_, __) {
        return const DecoratedBox(
          decoration: BoxDecoration(
            color: AppColors.blue,
          ),
        );
      },
    );
  }

  Widget showProgressIndicator() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: AppColors.lightBlack.withOpacity(0.1),
      child: Center(
        child: SpinKitWave(
          size: 30,
          itemBuilder: (_, __) {
            return const DecoratedBox(
              decoration: BoxDecoration(
                color: AppColors.blue,
              ),
            );
          },
        ),
      ),
    );
  }

  static void hideDialog() {
    //if (Get.isDialogOpen!) {
    Get.back();
    //}
  }

  static Widget indicator() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: AppColors.colorLightSkyBlue.withOpacity(0.3),
      child: const Center(
        child: CircularProgressIndicator(
          color: AppColors.blue,
        ),
      ),
    );
  }
}
