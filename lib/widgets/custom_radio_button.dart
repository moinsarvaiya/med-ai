import 'package:flutter/material.dart';
import 'package:med_ai/widgets/space_horizontal.dart';

import '../constant/app_colors.dart';
import '../constant/base_style.dart';

class CustomRadioButton extends StatelessWidget {
  final String label;
  final int itemIndex;
  final int listIndex;
  final bool selected;
  final Function()? callback;

  const CustomRadioButton({
    Key? key,
    required this.itemIndex,
    required this.listIndex,
    required this.label,
    required this.selected,
    this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Row(
        children: [
          Container(
            height: 24,
            width: 24,
            decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: AppColors.colorDarkBlue, width: 1)),
            child: Container(
              margin: const EdgeInsets.all(4),
              decoration: BoxDecoration(
                color: selected ? AppColors.colorGreen : AppColors.transparent,
                shape: BoxShape.circle,
              ),
            ),
          ),
          const SpaceHorizontal(5),
          Text(
            label,
            style: BaseStyle.textStyleNunitoSansRegular(12, AppColors.colorDarkBlue),
          ),
        ],
      ),
    );
  }
}
