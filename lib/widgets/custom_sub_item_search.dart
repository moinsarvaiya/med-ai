import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../constant/app_colors.dart';
import '../constant/base_style.dart';

class SubItemSearch extends StatelessWidget {
  final String? title;
  final String value;
  final String? image;
  final bool imageVisible;

  const SubItemSearch({Key? key, this.title, this.image, this.imageVisible = false, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        imageVisible
            ? Image.asset(
                image!,
                height: 18,
                width: 18,
              ).marginOnly(right: 5)
            : Text(
                '${title!} : ',
                style: BaseStyle.textStyleNunitoSansSemiBold(14, AppColors.colorDarkBlue),
              ),
        Expanded(
          child: Text(
            value,
            style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
          ),
        ),
      ],
    ).marginOnly(bottom: 10);
  }
}
