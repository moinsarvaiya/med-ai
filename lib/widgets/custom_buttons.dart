import 'dart:io';

import 'package:flutter/material.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/base_size.dart';
import 'package:med_ai/constant/base_style.dart';

class SubmitButton extends StatefulWidget {
  const SubmitButton({
    Key? key,
    required this.title,
    this.titleColor = AppColors.white,
    this.onClick,
    this.titleFontSize = 16,
    this.displayBorder = false,
    this.backgroundColor = AppColors.colorDarkBlue,
    this.borderColor = AppColors.colorDarkBlue,
    this.isBottomMargin = true,
  }) : super(key: key);

  final String title;
  final Function()? onClick;
  final bool displayBorder;
  final Color backgroundColor;
  final Color borderColor;
  final Color titleColor;
  final double titleFontSize;
  final bool isBottomMargin;

  @override
  State<SubmitButton> createState() => _SubmitButtonState();
}

class _SubmitButtonState extends State<SubmitButton> with SingleTickerProviderStateMixin {
  late final AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(
        milliseconds: 100,
      ),
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  void _tapDown(TapDownDetails details) {
    _controller.forward();
  }

  void _tapUp(TapUpDetails details) {
    _controller.reverse();
  }

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: 1 - _controller.value,
      child: InkWell(
        onTap: widget.onClick,
        onTapDown: widget.onClick != null ? _tapDown : null,
        onTapUp: widget.onClick != null ? _tapUp : null,
        child: Container(
          margin: Platform.isIOS && widget.isBottomMargin ? const EdgeInsets.only(bottom: 20) : const EdgeInsets.only(bottom: 0),
          height: BaseSize.height(6),
          decoration: BoxDecoration(
            border: Border.all(color: widget.borderColor, width: widget.displayBorder ? 1 : 0),
            color: widget.backgroundColor,
          ),
          child: Center(
            child: Text(
              widget.title,
              style: BaseStyle.textStyleNunitoSansBold(
                widget.titleFontSize,
                widget.titleColor,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
