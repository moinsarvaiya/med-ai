import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/base_style.dart';

class CustomChipWidget extends StatelessWidget {
  final bool cancelVisible;
  final String label;
  final Function()? callback;
  final Function()? viewCallback;

  const CustomChipWidget({
    Key? key,
    this.cancelVisible = true,
    required this.label,
    this.callback,
    this.viewCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: viewCallback,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: AppColors.colorBackground,
        ),
        padding: EdgeInsets.only(
          left: 15,
          top: cancelVisible ? 5 : 8,
          bottom: cancelVisible ? 5 : 8,
          right: cancelVisible ? 8 : 15,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: Text(
                label,
                style: BaseStyle.textStyleNunitoSansRegular(14, AppColors.colorDarkBlue),
              ),
            ),
            cancelVisible
                ? GestureDetector(
                    onTap: callback,
                    child: const Icon(
                      Icons.cancel,
                      color: AppColors.colorDarkBlue,
                      size: 15,
                    ).paddingAll(5),
                  )
                : const SizedBox.shrink(),
          ],
        ),
      ),
    );
  }
}
