import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../constant/app_colors.dart';
import '../constant/base_style.dart';
import '../models/dr_patient_temperture_model.dart';

class CustomTemperatureWidget extends StatelessWidget {
  final Function(String selectValue)? onChanged;
  final RxList<Rx<DrPatientTemperatureModel>> temperatures = List.generate(
    11,
    (index) => DrPatientTemperatureModel(temperature: '${96 + index}°F', isSelected: false).obs,
  ).obs;

  CustomTemperatureWidget({Key? key, this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppStringKey.labelTemperatureWithOptional.tr,
            style: BaseStyle.textStyleNunitoSansBold(
              14,
              AppColors.colorDarkBlue,
            ),
          ).marginOnly(bottom: 0),
          const SpaceVertical(20),
          Stack(
            children: [
              Positioned(
                left: 10,
                right: 15,
                top: 15,
                child: Container(
                  height: 1,
                  width: double.maxFinite,
                  color: Colors.grey.withOpacity(0.5),
                ),
              ),
              Obx(
                () => Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ...temperatures.asMap().entries.map(
                      (entry) {
                        var index = entry.key;
                        var value = entry.value.value.temperature;
                        var evenIndex = index % 2 == 0;
                        return Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      for (var element in temperatures) {
                                        element.value.isSelected = false;
                                      }
                                      entry.value.value.isSelected = true;
                                      temperatures.refresh();
                                      onChanged!(entry.value.value.temperature!);
                                    },
                                    child: Container(
                                      padding: const EdgeInsets.all(4),
                                      width: 25,
                                      height: 30,
                                      child: Container(
                                        margin: EdgeInsets.zero,
                                        width: 15,
                                        height: 15,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: AppColors.white,
                                          border: Border.all(
                                            color: AppColors.colorDarkBlue,
                                          ),
                                        ),
                                        child: entry.value.value.isSelected!
                                            ? Container(
                                                margin: const EdgeInsets.all(3),
                                                width: 10,
                                                height: 10,
                                                decoration: const BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: AppColors.colorGreen,
                                                ),
                                              )
                                            : Container(),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              const SpaceVertical(0),
                              evenIndex
                                  ? Text(
                                      value.toString(),
                                      style: BaseStyle.textStyleNunitoSansBold(
                                        10,
                                        AppColors.colorDarkBlue,
                                      ),
                                    )
                                  : const SizedBox.shrink()
                            ],
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
