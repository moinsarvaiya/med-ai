import 'package:flutter/material.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/widgets/space_horizontal.dart';

class SubTitleWidget extends StatelessWidget {
  const SubTitleWidget({
    Key? key,
    required this.subTitle,
  }) : super(key: key);

  final String subTitle;

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        children: [
          Container(
            width: 5,
            color: AppColors.colorGreen,
          ),
          const SpaceHorizontal(5),
          Text(
            subTitle,
            style: BaseStyle.textStyleDomineSemiBold(14, AppColors.colorDarkBlue),
          )
        ],
      ),
    );
  }
}
