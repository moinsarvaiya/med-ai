import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/base_style.dart';

class CustomRoundTextField extends StatelessWidget {
  final String? hintText;
  final String labelText;
  final bool isRequireField;
  final Widget? suffixIcon;
  final int? maxLength;
  final double labelFontSize;
  final TextEditingController textEditingController;
  final bool? isPasswordField;
  final String? errorText;
  final TextInputAction? keyBoardAction;
  final FocusNode? focusScope;
  final TextInputType? textInputType;
  final Function(String?)? onChange;
  final Function(String?)? onFieldSubmitted;
  final Function()? onTap;
  final String? Function(String?)? validator;
  final bool? isReadOnly;
  final bool isAbscureText = false;
  final bool labelVisible;
  final int? maxLines;
  final Color? backgroundColor;
  final List<TextInputFormatter>? inputFormatter;
  final double? borderRadius;
  final String? validatorText;

  const CustomRoundTextField({
    Key? key,
    this.hintText,
    required this.labelText,
    this.labelFontSize = 14,
    required this.isRequireField,
    this.suffixIcon,
    required this.textEditingController,
    this.isPasswordField,
    this.errorText,
    this.keyBoardAction,
    this.focusScope,
    this.textInputType,
    this.onTap,
    this.validator,
    this.onChange,
    this.onFieldSubmitted,
    this.maxLength = 1000,
    this.maxLines = 1,
    this.isReadOnly,
    this.labelVisible = true,
    this.inputFormatter,
    this.backgroundColor,
    this.borderRadius = 25,
    this.validatorText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        labelVisible
            ? Text(
                labelText,
                style: BaseStyle.textStyleNunitoSansBold(
                  labelFontSize,
                  AppColors.colorDarkBlue,
                ),
              ).marginOnly(bottom: 5)
            : Container(),
        TextFormField(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          textCapitalization: TextCapitalization.sentences,
          readOnly: isReadOnly ?? false,
          cursorColor: AppColors.colorDarkBlue,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(
              vertical: 15,
              horizontal: 15,
            ),
            fillColor: backgroundColor,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius!),
              borderSide: const BorderSide(
                color: AppColors.colorLightSkyBlue,
                width: 1,
              ),
            ),
            counterText: '',
            hintText: hintText,
            hintStyle: BaseStyle.textStyleNunitoSansRegular(
              14,
              AppColors.hintColor,
            ),
            suffixIcon: suffixIcon,
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius!),
              borderSide: const BorderSide(
                color: AppColors.colorLightSkyBlue,
                width: 1,
              ),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius!),
              borderSide: const BorderSide(
                color: AppColors.colorRed,
                width: 1,
              ),
            ),
            disabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius!),
              borderSide: const BorderSide(
                color: AppColors.colorLightSkyBlue,
                width: 1,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius!),
              borderSide: const BorderSide(
                color: AppColors.colorLightSkyBlue,
                width: 1,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius!),
              borderSide: const BorderSide(
                color: AppColors.colorLightSkyBlue,
                width: 1,
              ),
            ),
          ),
          keyboardType: textInputType,
          obscureText: isPasswordField ?? false,
          obscuringCharacter: "*",
          controller: textEditingController,
          textInputAction: keyBoardAction,
          focusNode: focusScope,
          maxLength: maxLength,
          maxLines: maxLines,
          onChanged: onChange,
          onTap: onTap,
          onFieldSubmitted: onFieldSubmitted,
          validator: isRequireField
              ? (value) {
                  if (value!.isEmpty) {
                    return validatorText;
                  }
                  return null; // Return null if the input is valid
                }
              : null,
          style: BaseStyle.textStyleNunitoSansRegular(
            14,
            AppColors.colorDarkBlue,
          ),
          inputFormatters: inputFormatter,
        ),
      ],
    );
  }
}
