import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_strings_key.dart';

import '../constant/app_colors.dart';

class NoDataFound extends StatelessWidget {
  final String? text;

  const NoDataFound({Key? key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 150,
      decoration: const BoxDecoration(
        color: Colors.transparent,
      ),
      child: Center(
        child: Text(
          text ?? AppStringKey.noDataFound.tr,
          style: Theme.of(context).textTheme.headlineSmall?.copyWith(color: AppColors.blue),
        ),
      ),
    );
  }
}
