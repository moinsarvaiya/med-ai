import 'package:flutter/material.dart';
import 'package:flutter_textfield_autocomplete/flutter_textfield_autocomplete.dart';
import 'package:med_ai/widgets/space_vertical.dart';

import '../constant/app_colors.dart';
import '../constant/base_style.dart';

class CustomAutoCompleteTextField extends StatelessWidget {
  final String label;
  final String hint;
  final FocusNode focusNode;
  final bool? labelVisible;
  final Widget? suffixIcon;
  final TextEditingController textEditingController;
  final List<String> searchOptions;
  final Function(String val)? onSelectedValue;
  final Function(String val)? onSubmit;
  final GlobalKey<TextFieldAutoCompleteState<String>> searchFieldAutoCompleteKey;

  const CustomAutoCompleteTextField({
    Key? key,
    required this.label,
    required this.hint,
    required this.focusNode,
    required this.textEditingController,
    required this.searchOptions,
    this.onSelectedValue,
    this.onSubmit,
    required this.searchFieldAutoCompleteKey,
    this.labelVisible,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        labelVisible!
            ? Text(
                label,
                style: BaseStyle.textStyleNunitoSansBold(14, AppColors.colorDarkBlue),
              )
            : const SizedBox.shrink(),
        labelVisible! ? const SpaceVertical(10) : const SizedBox.shrink(),
        TextFieldAutoComplete(
            decoration: InputDecoration(
              hintText: hint,
              fillColor: AppColors.transparent,
              hintStyle: BaseStyle.textStyleNunitoSansRegular(
                14,
                AppColors.colorDarkBlue.withOpacity(0.5),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25),
                borderSide: const BorderSide(
                  color: AppColors.colorLightSkyBlue,
                  width: 1,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25),
                borderSide: const BorderSide(
                  color: AppColors.colorLightSkyBlue,
                  width: 1,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25),
                borderSide: const BorderSide(
                  color: AppColors.colorLightSkyBlue,
                  width: 1,
                ),
              ),
              suffixIcon: suffixIcon,
            ),
            focusNode: focusNode,
            clearOnSubmit: false,
            style: BaseStyle.textStyleNunitoSansSemiBold(
              14,
              AppColors.colorDarkBlue,
            ),
            controller: textEditingController,
            itemSubmitted: (String item) {
              onSelectedValue!(item);
            },
            textSubmitted: (String value) {
              onSubmit!(value);
            },
            key: searchFieldAutoCompleteKey,
            suggestions: searchOptions,
            itemBuilder: (context, String item) {
              return Container(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 10),
                child: Text(
                  item,
                  style: BaseStyle.textStyleNunitoSansSemiBold(
                    12,
                    AppColors.colorDarkBlue,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              );
            },
            itemSorter: (String a, String b) {
              return a.compareTo(b);
            },
            itemFilter: (String item, query) {
              return item.toLowerCase().startsWith(query.toLowerCase());
            }),
      ],
    );
  }
}
