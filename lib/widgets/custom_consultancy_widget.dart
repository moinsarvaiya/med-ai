import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/widgets/custom_card_widget.dart';

import '../constant/ui_constant.dart';

class CustomConsultancyWidget extends StatelessWidget {
  final String tag;
  final Function()? onClick;

  const CustomConsultancyWidget({Key? key, required this.tag, this.onClick}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardWidget(
      shadowOpacity: shadow,
      backgroundColor: AppColors.colorDarkBlue,
      widget: InkWell(
        splashColor: AppColors.white.withOpacity(0.5),
        onTap: onClick,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              tag,
              style: BaseStyle.textStyleNunitoSansSemiBold(16, AppColors.white),
            ),
            const Icon(
              Icons.arrow_forward_ios_rounded,
              size: 20,
              color: AppColors.colorGreen,
            ),
          ],
        ).paddingOnly(left: 20, right: 15, top: 14, bottom: 14),
      ),
    );
  }
}
