class DrEarningHistoryModel {
  String? promoCode;
  String? currency;
  String? referenceId;
  String? status;
  String? amount;
  String? promoDiscount;
  int? paymentId;
  String? trxID;
  String? date;
  String? paymentIdStr;

  DrEarningHistoryModel(
      {this.promoCode,
      this.currency,
      this.referenceId,
      this.status,
      this.amount,
      this.promoDiscount,
      this.paymentId,
      this.trxID,
      this.date,
      this.paymentIdStr});

  DrEarningHistoryModel.fromJson(Map<String, dynamic> json) {
    promoCode = json['promo_code'];
    currency = json['currency'];
    referenceId = json['reference_id'];
    status = json['status'];
    amount = json['amount'];
    promoDiscount = json['promo_discount'];
    paymentId = json['payment_id'];
    trxID = json['trxID'];
    date = json['date'];
    paymentIdStr = json['payment_id_str'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['promo_code'] = this.promoCode;
    data['currency'] = this.currency;
    data['reference_id'] = this.referenceId;
    data['status'] = this.status;
    data['amount'] = this.amount;
    data['promo_discount'] = this.promoDiscount;
    data['payment_id'] = this.paymentId;
    data['trxID'] = this.trxID;
    data['date'] = this.date;
    data['payment_id_str'] = this.paymentIdStr;
    return data;
  }
}
