class DrInstitutionModel {
  DrInstitutionModel({
    this.id,
    this.degree,
    this.institutionName,
    this.accreditationBody,
    this.country,
  });

  DrInstitutionModel.fromJson(dynamic json) {
    id = json['id'];
    degree = json['degree'];
    institutionName = json['institutionName'];
    accreditationBody = json['accreditationBody'];
    country = json['country'];
  }

  int? id;
  String? degree;
  String? institutionName;
  String? accreditationBody;
  String? country;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['degree'] = degree;
    map['id'] = id;
    map['institutionName'] = institutionName;
    map['accreditationBody'] = accreditationBody;
    map['country'] = country;
    return map;
  }
}
