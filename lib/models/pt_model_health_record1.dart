class PtModelHealthRecord1 {
  PtModelHealthRecord1({
    this.question,
    this.option,
  });

  PtModelHealthRecord1.fromJson(dynamic json) {
    question = json['question'];
    option = json['option'];
  }

  String? question;
  int? option;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['question'] = question;
    map['option'] = option;
    return map;
  }
}
