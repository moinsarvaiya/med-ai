class PtAvailableDoctorModel {
  String? docProfileImage;
  String? docName;
  String? docDegree;
  String? docType;
  String? docRating;
  String? docTotalReview;

  PtAvailableDoctorModel({
    this.docProfileImage,
    this.docName,
    this.docDegree,
    this.docType,
    this.docRating,
    this.docTotalReview,
  });

  PtAvailableDoctorModel.fromJson(dynamic json) {
    docProfileImage = json['docProfileImage'];
    docName = json['docName'];
    docDegree = json['docDegree'];
    docType = json['docType'];
    docRating = json['docRating'];
    docTotalReview = json['docTotalReview'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['docProfileImage'] = docProfileImage;
    map['docName'] = docName;
    map['docDegree'] = docDegree;
    map['docType'] = docType;
    map['docRating'] = docRating;
    map['docTotalReview'] = docTotalReview;
    return map;
  }
}
