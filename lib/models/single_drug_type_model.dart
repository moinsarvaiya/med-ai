class SingleDrugTypeListModel {
  String? genericName;
  String? brandName;
  String? company;
  String? strength;
  String? url;

  SingleDrugTypeListModel({this.genericName, this.brandName, this.company, this.strength, this.url});

  SingleDrugTypeListModel.fromJson(Map<String, dynamic> json) {
    genericName = json['generic_name'];
    brandName = json['brand_name'];
    company = json['company'];
    strength = json['strength'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['generic_name'] = this.genericName;
    data['brand_name'] = this.brandName;
    data['company'] = this.company;
    data['strength'] = this.strength;
    data['url'] = this.url;
    return data;
  }
}
