class MedicineModel {
  String? drugName;
  Schedule? schedule;
  int? timeInterval;
  String? foodIntake;
  String? duration;
  String? specialInstruction;
  String? administrationMethod;

  MedicineModel(
      {this.drugName,
      this.schedule,
      this.timeInterval,
      this.foodIntake,
      this.duration,
      this.specialInstruction,
      this.administrationMethod});

  MedicineModel.fromJson(Map<String, dynamic> json) {
    drugName = json['drug_name'];
    schedule = json['schedule'] != null ? new Schedule.fromJson(json['schedule']) : null;
    timeInterval = json['time_interval'];
    foodIntake = json['food_intake'];
    duration = json['duration'];
    specialInstruction = json['special_instruction'];
    administrationMethod = json['administration_method'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['drug_name'] = this.drugName;
    if (this.schedule != null) {
      data['schedule'] = this.schedule!.toJson();
    }
    data['time_interval'] = this.timeInterval;
    data['food_intake'] = this.foodIntake;
    data['duration'] = this.duration;
    data['special_instruction'] = this.specialInstruction;
    data['administration_method'] = this.administrationMethod;
    return data;
  }
}

class Schedule {
  String? breakfast;
  String? lunch;
  String? dinner;

  Schedule({this.breakfast, this.lunch, this.dinner});

  Schedule.fromJson(Map<String, dynamic> json) {
    breakfast = json['breakfast'];
    lunch = json['lunch'];
    dinner = json['dinner'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['breakfast'] = this.breakfast;
    data['lunch'] = this.lunch;
    data['dinner'] = this.dinner;
    return data;
  }
}
