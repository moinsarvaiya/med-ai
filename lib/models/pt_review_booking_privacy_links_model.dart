class PtReviewBookingPrivacyLinksModel {
  String? name;
  String? linkText;
  bool? selected;
  String? url;

  PtReviewBookingPrivacyLinksModel({
    this.name,
    this.linkText,
    this.selected,
    this.url,
  });

  PtReviewBookingPrivacyLinksModel.fromJson(dynamic json) {
    name = json['name'];
    linkText = json['linkText'];
    selected = json['selected'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    map['linkText'] = linkText;
    map['selected'] = selected;
    map['url'] = url;
    return map;
  }
}
