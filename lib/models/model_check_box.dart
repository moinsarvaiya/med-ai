class ModelCheckBox {
  ModelCheckBox({
    this.name,
    this.selected,
  });

  ModelCheckBox.fromJson(dynamic json) {
    name = json['name'];
    selected = json['selected'];
  }

  String? name;
  bool? selected;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    map['selected'] = selected;
    return map;
  }
}
