class PtSpecialityItemModel {
  PtSpecialityItemModel({
    this.question,
    this.option,
  });

  PtSpecialityItemModel.fromJson(dynamic json) {
    question = json['question'];
    option = json['option'];
  }

  String? question;
  bool? option;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['question'] = question;
    map['option'] = option;
    return map;
  }
}
