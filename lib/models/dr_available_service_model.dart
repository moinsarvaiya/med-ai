class DrAvailableServiceModel {
  String? serviceName;
  bool? isSelected;
  Enum? enums;

  DrAvailableServiceModel({
    this.serviceName,
    this.enums,
    this.isSelected,
  });

  DrAvailableServiceModel.fromJson(Map<String, dynamic> json) {
    serviceName = json['serviceName'];
    isSelected = json['isSelected'];
    enums = json['enums'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['serviceName'] = serviceName;
    data['isSelected'] = isSelected;
    data['enums'] = enums;
    return data;
  }
}
