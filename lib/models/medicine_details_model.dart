class MedicineDetailsModel {
  int slNo;
  String name;
  String type;

  MedicineDetailsModel({
    required this.slNo,
    required this.name,
    required this.type,
  });

  MedicineDetailsModel.fromMap(Map<String, dynamic> medicine)
      : slNo = medicine['sl_no'],
        name = medicine['name'],
        type = medicine['type'];

  Map<String, dynamic> toMap() {
    return {
      'sl_no': slNo,
      'name': name,
      'type': type,
    };
  }
}
