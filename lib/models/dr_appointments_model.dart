class DrAppointmentsModel {
  String? date;
  int? count;
  List<Appointments>? appointments;

  DrAppointmentsModel({this.date, this.count, this.appointments});

  DrAppointmentsModel.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    count = json['count'];
    if (json['appointments'] != null) {
      appointments = <Appointments>[];
      json['appointments'].forEach((v) {
        appointments!.add(new Appointments.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['count'] = this.count;
    if (this.appointments != null) {
      data['appointments'] = this.appointments!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Appointments {
  String? name;
  int? age;
  String? gender;
  String? personId;
  int? consultationId;
  String? consultationIdStr;
  String? prevRecordId;
  String? idType;
  String? consultationStatus;
  String? date;
  String? time;
  String? division;
  String? country;
  String? phone;
  List<String>? chiefComplaints;
  List<String>? symptoms;

  Appointments(
      {this.name,
      this.age,
      this.gender,
      this.personId,
      this.consultationId,
      this.consultationIdStr,
      this.prevRecordId,
      this.idType,
      this.consultationStatus,
      this.date,
      this.time,
      this.division,
      this.country,
      this.phone,
      this.chiefComplaints,
      this.symptoms});

  Appointments.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    age = json['age'];
    gender = json['gender'];
    personId = json['person_id'];
    consultationId = json['consultation_id'];
    consultationIdStr = json['consultation_id_str'];
    prevRecordId = json['prev_record_id'];
    idType = json['id_type'];
    consultationStatus = json['consultation_status'];
    date = json['date'];
    time = json['time'];
    division = json['division'];
    country = json['country'];
    phone = json['phone'];
    chiefComplaints = json['chief_complaints'].cast<String>();
    symptoms = json['symptoms'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['age'] = this.age;
    data['gender'] = this.gender;
    data['person_id'] = this.personId;
    data['consultation_id'] = this.consultationId;
    data['consultation_id_str'] = this.consultationIdStr;
    data['prev_record_id'] = this.prevRecordId;
    data['id_type'] = this.idType;
    data['consultation_status'] = this.consultationStatus;
    data['date'] = this.date;
    data['time'] = this.time;
    data['division'] = this.division;
    data['country'] = this.country;
    data['phone'] = this.phone;
    data['chief_complaints'] = this.chiefComplaints;
    data['symptoms'] = this.symptoms;
    return data;
  }
}
