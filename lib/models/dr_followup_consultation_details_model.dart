class DrFollowupConsultationDetailsModel {
  String? code;
  ConsultationDetail? consultationDetail;

  DrFollowupConsultationDetailsModel({this.code, this.consultationDetail});

  DrFollowupConsultationDetailsModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    consultationDetail = json['consultation_detail'] != null ? new ConsultationDetail.fromJson(json['consultation_detail']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    if (this.consultationDetail != null) {
      data['consultation_detail'] = this.consultationDetail!.toJson();
    }
    return data;
  }
}

class ConsultationDetail {
  String? consultationStatus;
  List<String>? currentSymptoms;
  String? consultationId;
  String? idType;
  String? consultationDate;
  String? predictedDisease;
  String? consultationType;
  String? specialInstruction;
  String? nextConsultationDate;
  String? patientComment;
  String? advice;
  List<String>? diagnosedDisease;
  String? clinicalNote;
  List<MedicineList>? prevMedicineList;
  DoctorDetails? doctorDetails;
  String? testInstructions;
  List<DiagnosticsStatus>? diagnosticsStatus;
  List<MedicationStatus>? medicationStatus;
  List<String>? medicineList;
  List<String>? diagnosticsList;
  List<String>? previousSymptoms;
  List<String>? addedSymptoms;
  List<String>? removedSymptoms;
  PreviousVitalSigns? previousVitalSigns;
  NewVitalSigns? newVitalSigns;
  String? name;
  String? district;
  int? personId;
  int? age;
  String? gender;
  String? country;
  String? phone;

  ConsultationDetail(
      {this.consultationStatus,
      this.currentSymptoms,
      this.consultationId,
      this.idType,
      this.consultationDate,
      this.predictedDisease,
      this.consultationType,
      this.specialInstruction,
      this.nextConsultationDate,
      this.patientComment,
      this.advice,
      this.diagnosedDisease,
      this.clinicalNote,
      this.prevMedicineList,
      this.doctorDetails,
      this.testInstructions,
      this.diagnosticsStatus,
      this.medicationStatus,
      this.medicineList,
      this.diagnosticsList,
      this.previousSymptoms,
      this.addedSymptoms,
      this.removedSymptoms,
      this.previousVitalSigns,
      this.newVitalSigns,
      this.name,
      this.district,
      this.personId,
      this.age,
      this.gender,
      this.country,
      this.phone});

  ConsultationDetail.fromJson(Map<String, dynamic> json) {
    consultationStatus = json['consultation_status'];
    currentSymptoms = json['current_symptoms'].cast<String>();
    consultationId = json['consultation_id'];
    idType = json['id_type'];
    consultationDate = json['consultation_date'];
    predictedDisease = json['predicted_disease'];
    consultationType = json['consultation_type'];
    specialInstruction = json['special_instruction'];
    nextConsultationDate = json['next_consultation_date'];
    patientComment = json['patient_comment'];
    advice = json['advice'];
    diagnosedDisease = json['diagnosed_disease'].cast<String>();
    clinicalNote = json['clinical_note'];
    if (json['prev_medicine_list'] != null) {
      prevMedicineList = <MedicineList>[];
      json['prev_medicine_list'].forEach((v) {
        prevMedicineList!.add(new MedicineList.fromJson(v));
      });
    }
    doctorDetails = json['doctor_details'] != null ? new DoctorDetails.fromJson(json['doctor_details']) : null;
    testInstructions = json['test_instructions'];
    if (json['diagnostics_status'] != null) {
      diagnosticsStatus = <DiagnosticsStatus>[];
      json['diagnostics_status'].forEach((v) {
        diagnosticsStatus!.add(new DiagnosticsStatus.fromJson(v));
      });
    }
    if (json['medication_status'] != null) {
      medicationStatus = <MedicationStatus>[];
      json['medication_status'].forEach((v) {
        medicationStatus!.add(new MedicationStatus.fromJson(v));
      });
    }
    medicineList = json['medicine_list'].cast<String>();
    diagnosticsList = json['diagnostics_list'].cast<String>();
    previousSymptoms = json['previous_symptoms'].cast<String>();
    addedSymptoms = json['added_symptoms'].cast<String>();
    removedSymptoms = json['removed_symptoms'].cast<String>();
    previousVitalSigns = json['previous_vital_signs'] != null ? new PreviousVitalSigns.fromJson(json['previous_vital_signs']) : null;
    newVitalSigns = json['new_vital_signs'] != null ? new NewVitalSigns.fromJson(json['new_vital_signs']) : null;
    name = json['name'];
    district = json['district'];
    personId = json['person_id'];
    age = json['age'];
    gender = json['gender'];
    country = json['country'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['consultation_status'] = this.consultationStatus;
    data['current_symptoms'] = this.currentSymptoms;
    data['consultation_id'] = this.consultationId;
    data['id_type'] = this.idType;
    data['consultation_date'] = this.consultationDate;
    data['predicted_disease'] = this.predictedDisease;
    data['consultation_type'] = this.consultationType;
    data['special_instruction'] = this.specialInstruction;
    data['next_consultation_date'] = this.nextConsultationDate;
    data['patient_comment'] = this.patientComment;
    data['advice'] = this.advice;
    data['diagnosed_disease'] = this.diagnosedDisease;
    data['clinical_note'] = this.clinicalNote;
    if (this.prevMedicineList != null) {
      data['prev_medicine_list'] = this.prevMedicineList!.map((v) => v.toJson()).toList();
    }
    if (this.doctorDetails != null) {
      data['doctor_details'] = this.doctorDetails!.toJson();
    }
    data['test_instructions'] = this.testInstructions;
    if (this.diagnosticsStatus != null) {
      data['diagnostics_status'] = this.diagnosticsStatus!.map((v) => v.toJson()).toList();
    }
    if (this.medicationStatus != null) {
      data['medication_status'] = this.medicationStatus!.map((v) => v.toJson()).toList();
    }
    data['medicine_list'] = this.medicineList;
    data['diagnostics_list'] = this.diagnosticsList;
    data['previous_symptoms'] = this.previousSymptoms;
    data['added_symptoms'] = this.addedSymptoms;
    data['removed_symptoms'] = this.removedSymptoms;
    if (this.previousVitalSigns != null) {
      data['previous_vital_signs'] = this.previousVitalSigns!.toJson();
    }
    if (this.newVitalSigns != null) {
      data['new_vital_signs'] = this.newVitalSigns!.toJson();
    }
    data['name'] = this.name;
    data['district'] = this.district;
    data['person_id'] = this.personId;
    data['age'] = this.age;
    data['gender'] = this.gender;
    data['country'] = this.country;
    data['phone'] = this.phone;
    return data;
  }
}

class MedicineList {
  String? medicine;
  String? breakfast;
  String? lunch;
  String? dinner;
  String? hourlyInterval;
  String? foodIntake;
  String? duration;
  String? specialInstruction;
  String? administrationMethod;

  MedicineList(
      {this.medicine,
      this.breakfast,
      this.lunch,
      this.dinner,
      this.hourlyInterval,
      this.foodIntake,
      this.duration,
      this.specialInstruction,
      this.administrationMethod});

  MedicineList.fromJson(Map<String, dynamic> json) {
    medicine = json['medicine'];
    breakfast = json['breakfast'];
    lunch = json['lunch'];
    dinner = json['dinner'];
    hourlyInterval = json['hourly_interval'];
    foodIntake = json['food_intake'];
    duration = json['duration'];
    specialInstruction = json['special_instruction'];
    administrationMethod = json['administration_method'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['medicine'] = this.medicine;
    data['breakfast'] = this.breakfast;
    data['lunch'] = this.lunch;
    data['dinner'] = this.dinner;
    data['hourly_interval'] = this.hourlyInterval;
    data['food_intake'] = this.foodIntake;
    data['duration'] = this.duration;
    data['special_instruction'] = this.specialInstruction;
    data['administration_method'] = this.administrationMethod;
    return data;
  }
}

class DoctorDetails {
  String? name;
  String? specialization;

  DoctorDetails({this.name, this.specialization});

  DoctorDetails.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    specialization = json['specialization'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['specialization'] = this.specialization;
    return data;
  }
}

class DiagnosticsStatus {
  String? testName;
  String? testResult;
  List<String>? testImg;

  DiagnosticsStatus({this.testName, this.testResult, this.testImg});

  DiagnosticsStatus.fromJson(Map<String, dynamic> json) {
    testName = json['test_name'];
    testResult = json['test_result'];
    testImg = json['test_img'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['test_name'] = this.testName;
    data['test_result'] = this.testResult;
    data['test_img'] = this.testImg;
    return data;
  }
}

class MedicationStatus {
  String? drugName;
  String? status;

  MedicationStatus({this.drugName, this.status});

  MedicationStatus.fromJson(Map<String, dynamic> json) {
    drugName = json['drug_name'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['drug_name'] = this.drugName;
    data['status'] = this.status;
    return data;
  }
}

class PreviousVitalSigns {
  String? prevHeight;
  String? prevWeight;
  String? prevTemperature;
  String? prevBp;
  String? prevPulse;
  String? prevBreathing;
  String? prevMenstrualCycle;

  PreviousVitalSigns(
      {this.prevHeight, this.prevWeight, this.prevTemperature, this.prevBp, this.prevPulse, this.prevBreathing, this.prevMenstrualCycle});

  PreviousVitalSigns.fromJson(Map<String, dynamic> json) {
    prevHeight = json['prev_height'];
    prevWeight = json['prev_weight'];
    prevTemperature = json['prev_temperature'];
    prevBp = json['prev_bp'];
    prevPulse = json['prev_pulse'];
    prevBreathing = json['prev_breathing'];
    prevMenstrualCycle = json['prev_menstrual_cycle'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['prev_height'] = this.prevHeight;
    data['prev_weight'] = this.prevWeight;
    data['prev_temperature'] = this.prevTemperature;
    data['prev_bp'] = this.prevBp;
    data['prev_pulse'] = this.prevPulse;
    data['prev_breathing'] = this.prevBreathing;
    data['prev_menstrual_cycle'] = this.prevMenstrualCycle;
    return data;
  }
}

class NewVitalSigns {
  String? newHeight;
  int? newWeight;
  String? newTemperature;
  String? newBp;
  String? newPulse;
  String? newBreathing;
  String? newMenstrualCycle;

  NewVitalSigns(
      {this.newHeight, this.newWeight, this.newTemperature, this.newBp, this.newPulse, this.newBreathing, this.newMenstrualCycle});

  NewVitalSigns.fromJson(Map<String, dynamic> json) {
    newHeight = json['new_height'];
    newWeight = json['new_weight'];
    newTemperature = json['new_temperature'];
    newBp = json['new_bp'];
    newPulse = json['new_pulse'];
    newBreathing = json['new_breathing'];
    newMenstrualCycle = json['new_menstrual_cycle'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['new_height'] = this.newHeight;
    data['new_weight'] = this.newWeight;
    data['new_temperature'] = this.newTemperature;
    data['new_bp'] = this.newBp;
    data['new_pulse'] = this.newPulse;
    data['new_breathing'] = this.newBreathing;
    data['new_menstrual_cycle'] = this.newMenstrualCycle;
    return data;
  }
}
