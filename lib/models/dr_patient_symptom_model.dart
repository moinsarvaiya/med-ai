class DrPatientSymptomModel {
  String? serviceName;
  bool? isSelected;
  bool? isSpecialSymptom;
  Enum? enums;

  DrPatientSymptomModel({
    this.serviceName,
    this.enums,
    this.isSelected,
    this.isSpecialSymptom,
  });

  DrPatientSymptomModel.fromJson(Map<String, dynamic> json) {
    serviceName = json['serviceName'];
    isSelected = json['isSelected'];
    enums = json['enums'];
    enums = json['isSpecialSymptom'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['serviceName'] = serviceName;
    data['isSelected'] = isSelected;
    data['enums'] = enums;
    data['isSpecialSymptom'] = isSpecialSymptom;
    return data;
  }
}
