class SpecialSymptomsRequestBody {
  bool fever;
  bool cough;
  bool pain;
  int symptomLevel;
  String site;
  String onset;
  String frequency;
  String painCharacter;
  String painExacerbating;
  String duration;
  String coughType;
  String coughTrigger;

  SpecialSymptomsRequestBody({
    required this.fever,
    required this.cough,
    required this.pain,
    required this.symptomLevel,
    required this.site,
    required this.onset,
    required this.frequency,
    required this.painCharacter,
    required this.painExacerbating,
    required this.duration,
    required this.coughType,
    required this.coughTrigger,
  });

  factory SpecialSymptomsRequestBody.fromJson(Map<String, dynamic> json) {
    return SpecialSymptomsRequestBody(
      fever: json['fever'] ?? false,
      cough: json['cough'] ?? false,
      pain: json['pain'] ?? false,
      symptomLevel: json['symptomLevel'] ?? 0,
      site: json['site'] ?? '',
      onset: json['onset'] ?? '',
      frequency: json['frequency'] ?? '',
      painCharacter: json['pain_character'] ?? '',
      painExacerbating: json['pain_exacerbating'] ?? '',
      duration: json['duration'] ?? '',
      coughType: json['cough_type'] ?? '',
      coughTrigger: json['cough_trigger'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'fever': fever,
      'cough': cough,
      'pain': pain,
      'symptomLevel': symptomLevel,
      'site': site,
      'onset': onset,
      'frequency': frequency,
      'pain_character': painCharacter,
      'pain_exacerbating': painExacerbating,
      'duration': duration,
      'cough_type': coughType,
      'cough_trigger': coughTrigger,
    };
  }
}

class SubmitSymptomRequestBodyModel {
  SpecialSymptomsRequestBody specialSymptoms;
  String lang;

  SubmitSymptomRequestBodyModel({
    required this.specialSymptoms,
    required this.lang,
  });

  factory SubmitSymptomRequestBodyModel.fromJson(Map<String, dynamic> json) {
    return SubmitSymptomRequestBodyModel(
      specialSymptoms: SpecialSymptomsRequestBody.fromJson(json['special_symptoms'] ?? {}),
      lang: json['lang'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'special_symptoms': specialSymptoms.toJson(),
      'lang': lang,
    };
  }
}
