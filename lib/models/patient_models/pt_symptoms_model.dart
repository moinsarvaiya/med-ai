class PtSymptomsModel {
  String? symptomName;
  bool? isSelected;
  bool? isSpecialSymptom;

  PtSymptomsModel({
    this.symptomName,
    this.isSelected,
    this.isSpecialSymptom,
  });
}
