// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pt_multi_user_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PtMultiUserDetailsModel _$PtMultiUserDetailsModelFromJson(Map<String, dynamic> json) => PtMultiUserDetailsModel(
      city_vill: json['city_vill'] as String?,
      first_name: json['first_name'] as String?,
      monthly_expenditure: json['monthly_expenditure'] as String?,
      referral_code: json['referral_code'] as String?,
      age: json['age'] as int?,
      account_id: json['account_id'] as String?,
      last_name: json['last_name'] as String?,
      consent_image: json['consent_image'] as String?,
      occupation: json['occupation'] as String?,
      ethnicity: json['ethnicity'] as String?,
      education: json['education'] as String?,
      gender: json['gender'] as String?,
      phone: json['phone'] as String?,
      zip_post_code: json['zip_post_code'] as String?,
      blood_group: json['blood_group'] as String?,
      person_id: json['person_id'] as String?,
      address_line: json['address_line'] as String?,
      consent_given: json['consent_given'] as String?,
      update_data_stamp: json['update_data_stamp'] as String?,
      person_location: json['person_location'] as String?,
      region: json['region'] as String?,
      person_type: json['person_type'] as String?,
      insert_date_stamp: json['insert_date_stamp'] as String?,
      is_primary_user: json['is_primary_user'] as String?,
      country: json['country'] as String?,
      marital_status: json['marital_status'] as String?,
      DOB: json['DOB'] as String?,
      profile_pic: json['profile_pic'] as String?,
    );

Map<String, dynamic> _$PtMultiUserDetailsModelToJson(PtMultiUserDetailsModel instance) => <String, dynamic>{
      'city_vill': instance.city_vill,
      'first_name': instance.first_name,
      'monthly_expenditure': instance.monthly_expenditure,
      'referral_code': instance.referral_code,
      'age': instance.age,
      'account_id': instance.account_id,
      'last_name': instance.last_name,
      'consent_image': instance.consent_image,
      'occupation': instance.occupation,
      'ethnicity': instance.ethnicity,
      'education': instance.education,
      'gender': instance.gender,
      'phone': instance.phone,
      'zip_post_code': instance.zip_post_code,
      'blood_group': instance.blood_group,
      'person_id': instance.person_id,
      'address_line': instance.address_line,
      'consent_given': instance.consent_given,
      'update_data_stamp': instance.update_data_stamp,
      'person_location': instance.person_location,
      'region': instance.region,
      'person_type': instance.person_type,
      'insert_date_stamp': instance.insert_date_stamp,
      'is_primary_user': instance.is_primary_user,
      'country': instance.country,
      'marital_status': instance.marital_status,
      'DOB': instance.DOB,
      'profile_pic': instance.profile_pic,
    };
