import 'package:json_annotation/json_annotation.dart';

part 'pt_disease_list_model.g.dart';

@JsonSerializable()
class PtDiseaseListModel {
  String? disease_name;
  double? weight;

  PtDiseaseListModel({
    this.disease_name,
    this.weight,
  });

  factory PtDiseaseListModel.fromJson(Map<String, dynamic> json) => _$PtDiseaseListModelFromJson(json);

  Map<String, dynamic> toJson() => _$PtDiseaseListModelToJson(this);
}
