// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pt_book_appointment_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PtBookAppointmentModel _$PtBookAppointmentModelFromJson(Map<String, dynamic> json) => PtBookAppointmentModel(
      code: json['code'] as String?,
      profile_count: json['profile_count'] as String?,
      book_appointment_profile_list: (json['book_appointment_profile_list'] as List<dynamic>?)
          ?.map((e) => BookAppointmentProfileList.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PtBookAppointmentModelToJson(PtBookAppointmentModel instance) => <String, dynamic>{
      'code': instance.code,
      'profile_count': instance.profile_count,
      'book_appointment_profile_list': instance.book_appointment_profile_list,
    };

BookAppointmentProfileList _$BookAppointmentProfileListFromJson(Map<String, dynamic> json) => BookAppointmentProfileList(
      name: json['name'] as String?,
      person_id: json['person_id'] as String?,
      country: json['country'] as String?,
      age: json['age'] as int?,
      gender: json['gender'] as String?,
      approval_status: json['approval_status'] as String?,
      referral_code: json['referral_code'] as String?,
      division: json['division'] as String?,
      mobile: json['mobile'] as String?,
      is_primary_user: json['is_primary_user'] as String?,
      profile_img: json['profile_img'] as String?,
    );

Map<String, dynamic> _$BookAppointmentProfileListToJson(BookAppointmentProfileList instance) => <String, dynamic>{
      'name': instance.name,
      'person_id': instance.person_id,
      'country': instance.country,
      'age': instance.age,
      'gender': instance.gender,
      'approval_status': instance.approval_status,
      'referral_code': instance.referral_code,
      'division': instance.division,
      'mobile': instance.mobile,
      'is_primary_user': instance.is_primary_user,
      'profile_img': instance.profile_img,
    };
