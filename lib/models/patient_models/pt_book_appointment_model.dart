import 'package:json_annotation/json_annotation.dart';

part 'pt_book_appointment_model.g.dart';

@JsonSerializable()
class PtBookAppointmentModel {
  String? code;
  String? profile_count;
  List<BookAppointmentProfileList>? book_appointment_profile_list;

  PtBookAppointmentModel({
    this.code,
    this.profile_count,
    this.book_appointment_profile_list,
  });

  factory PtBookAppointmentModel.fromJson(Map<String, dynamic> json) => _$PtBookAppointmentModelFromJson(json);

  Map<String, dynamic> toJson() => _$PtBookAppointmentModelToJson(this);
}

@JsonSerializable()
class BookAppointmentProfileList {
  String? name;
  String? person_id;
  String? country;
  int? age;
  String? gender;
  String? approval_status;
  String? referral_code;
  String? division;
  String? mobile;
  String? is_primary_user;
  String? profile_img;

  BookAppointmentProfileList({
    this.name,
    this.person_id,
    this.country,
    this.age,
    this.gender,
    this.approval_status,
    this.referral_code,
    this.division,
    this.mobile,
    this.is_primary_user,
    this.profile_img,
  });

  factory BookAppointmentProfileList.fromJson(Map<String, dynamic> json) => _$BookAppointmentProfileListFromJson(json);

  Map<String, dynamic> toJson() => _$BookAppointmentProfileListToJson(this);
}
