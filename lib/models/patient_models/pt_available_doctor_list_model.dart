import 'package:json_annotation/json_annotation.dart';

part 'pt_available_doctor_list_model.g.dart';

@JsonSerializable()
class PtAvailableDoctorListModel {
  String? name;
  String? doctor_id;
  String? specialization;
  String? affiliation;
  int? patient_treated;
  List<String>? days;
  String? visit_fee;
  String? follow_up_fee;
  String? followup_within;
  String? doctor_type;
  String? training_course;
  String? qualification;
  String? availability;
  String? profile_img;
  List<String>? consultation_hour;

  PtAvailableDoctorListModel({
    this.name,
    this.doctor_id,
    this.specialization,
    this.affiliation,
    this.patient_treated,
    this.days,
    this.visit_fee,
    this.follow_up_fee,
    this.followup_within,
    this.doctor_type,
    this.training_course,
    this.qualification,
    this.availability,
    this.profile_img,
    this.consultation_hour,
  });

  factory PtAvailableDoctorListModel.fromJson(Map<String, dynamic> json) => _$PtAvailableDoctorListModelFromJson(json);

  Map<String, dynamic> toJson() => _$PtAvailableDoctorListModelToJson(this);
}
