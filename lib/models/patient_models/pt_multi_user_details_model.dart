import 'package:json_annotation/json_annotation.dart';

part 'pt_multi_user_details_model.g.dart';

@JsonSerializable()
class PtMultiUserDetailsModel {
  String? city_vill;
  String? first_name;
  String? monthly_expenditure;
  String? referral_code;
  int? age;
  String? account_id;
  String? last_name;
  String? consent_image;
  String? occupation;
  String? ethnicity;
  String? education;
  String? gender;
  String? phone;
  String? zip_post_code;
  String? blood_group;
  String? person_id;
  String? address_line;
  String? consent_given;
  String? update_data_stamp;
  String? person_location;
  String? region;
  String? person_type;
  String? insert_date_stamp;
  String? is_primary_user;
  String? country;
  String? marital_status;
  String? DOB;
  String? profile_pic;

  PtMultiUserDetailsModel({
    this.city_vill,
    this.first_name,
    this.monthly_expenditure,
    this.referral_code,
    this.age,
    this.account_id,
    this.last_name,
    this.consent_image,
    this.occupation,
    this.ethnicity,
    this.education,
    this.gender,
    this.phone,
    this.zip_post_code,
    this.blood_group,
    this.person_id,
    this.address_line,
    this.consent_given,
    this.update_data_stamp,
    this.person_location,
    this.region,
    this.person_type,
    this.insert_date_stamp,
    this.is_primary_user,
    this.country,
    this.marital_status,
    this.DOB,
    this.profile_pic,
  });

  factory PtMultiUserDetailsModel.fromJson(Map<String, dynamic> json) => _$PtMultiUserDetailsModelFromJson(json);

  Map<String, dynamic> toJson() => _$PtMultiUserDetailsModelToJson(this);
}
