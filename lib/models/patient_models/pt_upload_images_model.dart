class PtUploadedImagesModel {
  String? url;
  String? filePath;

  PtUploadedImagesModel({
    this.url,
    this.filePath,
  });
}
