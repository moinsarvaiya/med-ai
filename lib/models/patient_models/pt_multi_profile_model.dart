import 'package:json_annotation/json_annotation.dart';

part 'pt_multi_profile_model.g.dart';

@JsonSerializable()
class PtMultiProfileModel {
  String? code;
  String? profile_count;
  List<ProfileList>? profile_list;

  PtMultiProfileModel({
    this.code,
    this.profile_count,
    this.profile_list,
  });

  factory PtMultiProfileModel.fromJson(Map<String, dynamic> json) => _$PtMultiProfileModelFromJson(json);

  Map<String, dynamic> toJson() => _$PtMultiProfileModelToJson(this);
}

@JsonSerializable()
class ProfileList {
  String? name;
  String? person_id;
  String? country;
  int? age;
  String? gender;
  String? approval_status;
  String? referral_code;
  String? division;
  String? mobile;
  String? is_primary_user;
  String? profile_img;

  ProfileList({
    this.name,
    this.person_id,
    this.country,
    this.age,
    this.gender,
    this.approval_status,
    this.referral_code,
    this.division,
    this.mobile,
    this.is_primary_user,
    this.profile_img,
  });

  factory ProfileList.fromJson(Map<String, dynamic> json) => _$ProfileListFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileListToJson(this);
}
