class PtDynamicSymptomRequestParamsModel {
  bool answer;
  String lang;
  String gender;
  RequestParamsTempInfo requestParamsTempInfo;
  Map<String, dynamic> radioObject;
  List<String> allSymptoms;
  List<String> userTypedSymptoms;

  PtDynamicSymptomRequestParamsModel({
    required this.answer,
    required this.lang,
    required this.gender,
    required this.requestParamsTempInfo,
    required this.radioObject,
    required this.allSymptoms,
    required this.userTypedSymptoms,
  });

  factory PtDynamicSymptomRequestParamsModel.fromJson(Map<String, dynamic> json) {
    return PtDynamicSymptomRequestParamsModel(
      answer: json['answer'] ?? false,
      lang: json['lang'] ?? "",
      gender: json['gender'] ?? "",
      requestParamsTempInfo: RequestParamsTempInfo.fromJson(json['temp_info'] ?? {}),
      radioObject: (json['radioObject'] as Map<String, dynamic>?) ?? {},
      allSymptoms: (json['all_symptoms'] as List<dynamic>).map((e) => e.toString()).toList(),
      userTypedSymptoms: (json['user_typed_symptoms'] as List<dynamic>).map((e) => e.toString()).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'answer': answer,
      'lang': lang,
      'gender': gender,
      'temp_info': requestParamsTempInfo.toJson(),
      'radioObject': radioObject,
      'all_symptoms': allSymptoms,
      'user_typed_symptoms': userTypedSymptoms,
    };
  }
}

class RequestParamsTempInfo {
  List<String> allSymMatched;
  List<String> allGivenSymptoms;
  List<int> disCount;
  List<String> alreadyAsked;
  Map<String, dynamic> res;
  int count;
  List<dynamic> bigList;
  String ans;
  List<String> allSymBen;
  List<dynamic> bigListBen;
  List<String> userTypedSymptom;
  String gender;
  List<dynamic> answeredNoSymptoms;

  RequestParamsTempInfo({
    required this.allSymMatched,
    required this.allGivenSymptoms,
    required this.disCount,
    required this.alreadyAsked,
    required this.res,
    required this.count,
    required this.bigList,
    required this.ans,
    required this.allSymBen,
    required this.bigListBen,
    required this.userTypedSymptom,
    required this.gender,
    required this.answeredNoSymptoms,
  });

  factory RequestParamsTempInfo.fromJson(Map<String, dynamic> json) {
    return RequestParamsTempInfo(
      allSymMatched: (json['all_sym_matched'] as List<dynamic>).map((e) => e.toString()).toList(),
      allGivenSymptoms: (json['all_given_symptoms'] as List<dynamic>).map((e) => e.toString()).toList(),
      disCount: (json['dis_count'] as List<dynamic>).map((e) => e as int).toList(),
      alreadyAsked: (json['already_asked'] as List<dynamic>).map((e) => e.toString()).toList(),
      res: json['res'] ?? {},
      count: json['count'] ?? 0,
      bigList: json['big_list'] ?? [],
      ans: json['ans'] ?? "",
      allSymBen: (json['all_sym_ben'] as List<dynamic>).map((e) => e.toString()).toList(),
      bigListBen: json['big_list_ben'] ?? [],
      userTypedSymptom: (json['user_typed_symptom'] as List<dynamic>).map((e) => e.toString()).toList(),
      gender: json['gender'] ?? "",
      answeredNoSymptoms: json['answered_no_symptoms'] ?? [],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'all_sym_matched': allSymMatched,
      'all_given_symptoms': allGivenSymptoms,
      'dis_count': disCount,
      'already_asked': alreadyAsked,
      'res': res,
      'count': count,
      'big_list': bigList,
      'ans': ans,
      'all_sym_ben': allSymBen,
      'big_list_ben': bigListBen,
      'user_typed_symptom': userTypedSymptom,
      'gender': gender,
      'answered_no_symptoms': answeredNoSymptoms,
    };
  }
}
