class PtDynamicSymptomFirstQueRequestParamsModel {
  bool answer;
  String lang;
  String gender;
  FirstQueTempInfo firstQueTempInfo;
  Map<String, dynamic> radioObject;
  List<String> allSymptoms;
  List<String> userTypedSymptoms;

  PtDynamicSymptomFirstQueRequestParamsModel({
    required this.answer,
    required this.lang,
    required this.gender,
    required this.firstQueTempInfo,
    required this.radioObject,
    required this.allSymptoms,
    required this.userTypedSymptoms,
  });

  factory PtDynamicSymptomFirstQueRequestParamsModel.fromJson(Map<String, dynamic> json) {
    return PtDynamicSymptomFirstQueRequestParamsModel(
      answer: json['answer'] ?? false,
      lang: json['lang'] ?? "",
      gender: json['gender'] ?? "",
      firstQueTempInfo: FirstQueTempInfo.fromJson(json['temp_info'] ?? {}),
      radioObject: json['radioObject'] ?? {},
      allSymptoms: (json['all_symptoms'] as List<dynamic>).map((e) => e.toString()).toList(),
      userTypedSymptoms: (json['user_typed_symptoms'] as List<dynamic>).map((e) => e.toString()).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'answer': answer,
      'lang': lang,
      'gender': gender,
      'temp_info': firstQueTempInfo.toJson(),
      'radioObject': radioObject,
      'all_symptoms': allSymptoms,
      'user_typed_symptoms': userTypedSymptoms,
    };
  }
}

class FirstQueTempInfo {
  Map<String, dynamic> res;
  List<dynamic> allSym;
  List<dynamic> disCount;
  List<dynamic> alreadyAsked;
  List<dynamic> allSymBen;
  List<dynamic> bigListBen;
  List<dynamic> bigList;
  List<dynamic> userTypedSymptom;

  FirstQueTempInfo({
    required this.res,
    required this.allSym,
    required this.disCount,
    required this.alreadyAsked,
    required this.allSymBen,
    required this.bigListBen,
    required this.bigList,
    required this.userTypedSymptom,
  });

  factory FirstQueTempInfo.fromJson(Map<String, dynamic> json) {
    return FirstQueTempInfo(
      res: json['res'] ?? {},
      allSym: (json['all_sym'] as List<dynamic>).map((e) => e.toString()).toList(),
      disCount: (json['dis_count'] as List<dynamic>).map((e) => e.toString()).toList(),
      alreadyAsked: (json['already_asked'] as List<dynamic>).map((e) => e.toString()).toList(),
      allSymBen: (json['all_sym_ben'] as List<dynamic>).map((e) => e.toString()).toList(),
      bigListBen: (json['big_list_ben'] as List<dynamic>).map((e) => e.toString()).toList(),
      bigList: (json['big_list'] as List<dynamic>).map((e) => e.toString()).toList(),
      userTypedSymptom: (json['user_typed_symptom'] as List<dynamic>).map((e) => e.toString()).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'res': res,
      'all_sym': allSym,
      'dis_count': disCount,
      'already_asked': alreadyAsked,
      'all_sym_ben': allSymBen,
      'big_list_ben': bigListBen,
      'big_list': bigList,
      'user_typed_symptom': userTypedSymptom,
    };
  }
}
