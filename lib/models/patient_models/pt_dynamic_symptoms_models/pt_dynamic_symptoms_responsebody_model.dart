class PtDynamicSymptomsResponseBodyModel {
  String? dynamicQuestion;
  bool? lastQuestion;
  ResponseBodyTempInfo? responseBodytempInfo;
  List<String>? symptoms;
  String? plainSymptom;
  List<String>? userTypedSymptom;
  List<dynamic>? answeredNoSymptoms;
  String? disease;

  PtDynamicSymptomsResponseBodyModel({
    this.dynamicQuestion,
    this.lastQuestion,
    this.responseBodytempInfo,
    this.symptoms,
    this.plainSymptom,
    this.userTypedSymptom,
    this.answeredNoSymptoms,
    this.disease,
  });

  factory PtDynamicSymptomsResponseBodyModel.fromJson(Map<String, dynamic> json) {
    return PtDynamicSymptomsResponseBodyModel(
      dynamicQuestion: json['dynamic_question'] ?? "",
      lastQuestion: json['last_question'] ?? false,
      responseBodytempInfo: ResponseBodyTempInfo.fromJson(json['temp_info'] ?? {}),
      symptoms: (json['symptoms'] as List<dynamic>).map((e) => e.toString()).toList(),
      plainSymptom: json['plain_symptom'] ?? "",
      userTypedSymptom: (json['user_typed_symptom'] as List<dynamic>).map((e) => e.toString()).toList(),
      answeredNoSymptoms: json['answered_no_symptoms'] ?? [],
      disease: json['disease'] ?? "",
    );
  }
}

class ResponseBodyTempInfo {
  List<String>? allSymMatched;
  List<String>? allGivenSymptoms;
  List<int>? disCount;
  List<String>? alreadyAsked;
  Map<String, dynamic>? res;
  int? count;
  List<dynamic>? bigList;
  String? ans;
  List<String>? allSymBen;
  List<dynamic>? bigListBen;
  List<String>? userTypedSymptom;
  String? gender;
  List<dynamic>? answeredNoSymptoms;

  ResponseBodyTempInfo({
    this.allSymMatched,
    this.allGivenSymptoms,
    this.disCount,
    this.alreadyAsked,
    this.res,
    this.count,
    this.bigList,
    this.ans,
    this.allSymBen,
    this.bigListBen,
    this.userTypedSymptom,
    this.gender,
    this.answeredNoSymptoms,
  });

  factory ResponseBodyTempInfo.fromJson(Map<String, dynamic> json) {
    return ResponseBodyTempInfo(
      allSymMatched: (json['all_sym_matched'] as List<dynamic>).map((e) => e.toString()).toList(),
      allGivenSymptoms: (json['all_given_symptoms'] as List<dynamic>).map((e) => e.toString()).toList(),
      disCount: (json['dis_count'] as List<dynamic>).map((e) => e as int).toList(),
      alreadyAsked: (json['already_asked'] as List<dynamic>).map((e) => e.toString()).toList(),
      res: json['res'] ?? {},
      count: json['count'] ?? 0,
      bigList: json['big_list'] ?? [],
      ans: json['ans'] ?? "",
      allSymBen: (json['all_sym_ben'] as List<dynamic>).map((e) => e.toString()).toList(),
      bigListBen: json['big_list_ben'] ?? [],
      userTypedSymptom: (json['user_typed_symptom'] as List<dynamic>).map((e) => e.toString()).toList(),
      gender: json['gender'] ?? "",
      answeredNoSymptoms: json['answered_no_symptoms'] ?? [],
    );
  }
}
