// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pt_available_doctor_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PtAvailableDoctorListModel _$PtAvailableDoctorListModelFromJson(Map<String, dynamic> json) => PtAvailableDoctorListModel(
      name: json['name'] as String?,
      doctor_id: json['doctor_id'] as String?,
      specialization: json['specialization'] as String?,
      affiliation: json['affiliation'] as String?,
      patient_treated: json['patient_treated'] as int?,
      days: (json['days'] as List<dynamic>?)?.map((e) => e as String).toList(),
      visit_fee: json['visit_fee'] as String?,
      follow_up_fee: json['follow_up_fee'] as String?,
      followup_within: json['followup_within'] as String?,
      doctor_type: json['doctor_type'] as String?,
      training_course: json['training_course'] as String?,
      qualification: json['qualification'] as String?,
      availability: json['availability'] as String?,
      profile_img: json['profile_img'] as String?,
      consultation_hour: (json['consultation_hour'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$PtAvailableDoctorListModelToJson(PtAvailableDoctorListModel instance) => <String, dynamic>{
      'name': instance.name,
      'doctor_id': instance.doctor_id,
      'specialization': instance.specialization,
      'affiliation': instance.affiliation,
      'patient_treated': instance.patient_treated,
      'days': instance.days,
      'visit_fee': instance.visit_fee,
      'follow_up_fee': instance.follow_up_fee,
      'followup_within': instance.followup_within,
      'doctor_type': instance.doctor_type,
      'training_course': instance.training_course,
      'qualification': instance.qualification,
      'availability': instance.availability,
      'profile_img': instance.profile_img,
      'consultation_hour': instance.consultation_hour,
    };
