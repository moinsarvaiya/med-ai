// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pt_disease_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PtDiseaseListModel _$PtDiseaseListModelFromJson(Map<String, dynamic> json) => PtDiseaseListModel(
      disease_name: json['disease_name'] as String?,
      weight: json['weight'] as double?,
    );

Map<String, dynamic> _$PtDiseaseListModelToJson(PtDiseaseListModel instance) => <String, dynamic>{
      'disease_name': instance.disease_name,
      'weight': instance.weight,
    };
