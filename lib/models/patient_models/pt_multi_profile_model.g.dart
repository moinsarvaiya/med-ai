// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pt_multi_profile_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PtMultiProfileModel _$PtMultiProfileModelFromJson(Map<String, dynamic> json) => PtMultiProfileModel(
      code: json['code'] as String?,
      profile_count: json['profile_count'] as String?,
      profile_list: (json['profile_list'] as List<dynamic>?)?.map((e) => ProfileList.fromJson(e as Map<String, dynamic>)).toList(),
    );

Map<String, dynamic> _$PtMultiProfileModelToJson(PtMultiProfileModel instance) => <String, dynamic>{
      'code': instance.code,
      'profile_count': instance.profile_count,
      'profile_list': instance.profile_list,
    };

ProfileList _$ProfileListFromJson(Map<String, dynamic> json) => ProfileList(
      name: json['name'] as String?,
      person_id: json['person_id'] as String?,
      country: json['country'] as String?,
      age: json['age'] as int?,
      gender: json['gender'] as String?,
      approval_status: json['approval_status'] as String?,
      referral_code: json['referral_code'] as String?,
      division: json['division'] as String?,
      mobile: json['mobile'] as String?,
      is_primary_user: json['is_primary_user'] as String?,
      profile_img: json['profile_img'] as String?,
    );

Map<String, dynamic> _$ProfileListToJson(ProfileList instance) => <String, dynamic>{
      'name': instance.name,
      'person_id': instance.person_id,
      'country': instance.country,
      'age': instance.age,
      'gender': instance.gender,
      'approval_status': instance.approval_status,
      'referral_code': instance.referral_code,
      'division': instance.division,
      'mobile': instance.mobile,
      'is_primary_user': instance.is_primary_user,
      'profile_img': instance.profile_img,
    };
