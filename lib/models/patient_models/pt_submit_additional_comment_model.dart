class PtSubmitAdditionalCommentModel {
  final int pid;
  final String prevRecordId;
  final int age;
  final String gender;
  final String height;
  final String weight;
  final String operatorId;
  final String gps;
  final List<String> symptoms;
  final List<String> userTypedSymptom;
  final List<String> answeredNoSymptoms;
  final String temperature;
  final String bp;
  final String pulseRate;
  final String breathingRate;
  final String lastMenstrualCycle;
  final String patientComment;
  final List<String> additionalImage;

  PtSubmitAdditionalCommentModel({
    required this.pid,
    required this.prevRecordId,
    required this.age,
    required this.gender,
    required this.height,
    required this.weight,
    required this.operatorId,
    required this.gps,
    required this.symptoms,
    required this.userTypedSymptom,
    required this.answeredNoSymptoms,
    required this.temperature,
    required this.bp,
    required this.pulseRate,
    required this.breathingRate,
    required this.lastMenstrualCycle,
    required this.patientComment,
    required this.additionalImage,
  });

  factory PtSubmitAdditionalCommentModel.fromJson(Map<String, dynamic>? json) {
    return PtSubmitAdditionalCommentModel(
      pid: json?['pid'] as int? ?? 0,
      prevRecordId: json?['prev_record_id'] as String? ?? '',
      age: json?['age'] as int? ?? 0,
      gender: json?['gender'] as String? ?? '',
      height: json?['height'] as String? ?? '',
      weight: json?['weight'] as String? ?? '',
      operatorId: json?['operator_id'] as String? ?? '',
      gps: json?['gps'] as String? ?? '',
      symptoms: (json?['symptoms'] as List<dynamic>?)?.map((e) => e.toString()).toList() ?? [],
      userTypedSymptom: (json?['user_typed_symptom'] as List<dynamic>?)?.map((e) => e.toString()).toList() ?? [],
      answeredNoSymptoms: (json?['answered_no_symptoms'] as List<dynamic>?)?.map((e) => e.toString()).toList() ?? [],
      temperature: json?['temperature'] as String? ?? '',
      bp: json?['bp'] as String? ?? '',
      pulseRate: json?['pulse_rate'] as String? ?? '',
      breathingRate: json?['breathing_rate'] as String? ?? '',
      lastMenstrualCycle: json?['last_menstrual_cycle'] as String? ?? '',
      patientComment: json?['patient_comment'] as String? ?? '',
      additionalImage: (json?['additional_image'] as List<dynamic>?)?.map((e) => e.toString()).toList() ?? [],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'pid': pid,
      'prev_record_id': prevRecordId,
      'age': age,
      'gender': gender,
      'height': height,
      'weight': weight,
      'operator_id': operatorId,
      'gps': gps,
      'symptoms': symptoms,
      'user_typed_symptom': userTypedSymptom,
      'answered_no_symptoms': answeredNoSymptoms,
      'temperature': temperature,
      'bp': bp,
      'pulse_rate': pulseRate,
      'breathing_rate': breathingRate,
      'last_menstrual_cycle': lastMenstrualCycle,
      'patient_comment': patientComment,
      'additional_image': additionalImage,
    };
  }
}
