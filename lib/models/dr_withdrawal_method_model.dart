class DrWithdrawalMethodModel {
  String? accountName;
  String? mfsName;
  int? txMethodId;
  String? insertDateStamp;
  String? accountNumber;
  String? updateDataStamp;
  String? txMethodType;
  String? txMethodName;
  String? bankName;
  String? branchName;
  String? routingNumber;

  DrWithdrawalMethodModel(
      {this.accountName,
      this.mfsName,
      this.txMethodId,
      this.insertDateStamp,
      this.accountNumber,
      this.updateDataStamp,
      this.txMethodType,
      this.txMethodName,
      this.bankName,
      this.branchName,
      this.routingNumber});

  DrWithdrawalMethodModel.fromJson(Map<String, dynamic> json) {
    accountName = json['account_name'];
    mfsName = json['mfs_name'];
    txMethodId = json['tx_method_id'];
    insertDateStamp = json['insert_date_stamp'];
    accountNumber = json['account_number'];
    updateDataStamp = json['update_data_stamp'];
    txMethodType = json['tx_method_type'];
    txMethodName = json['tx_method_name'];
    bankName = json['bank_name'];
    branchName = json['branch_name'];
    routingNumber = json['routing_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['account_name'] = this.accountName;
    data['mfs_name'] = this.mfsName;
    data['tx_method_id'] = this.txMethodId;
    data['insert_date_stamp'] = this.insertDateStamp;
    data['account_number'] = this.accountNumber;
    data['update_data_stamp'] = this.updateDataStamp;
    data['tx_method_type'] = this.txMethodType;
    data['tx_method_name'] = this.txMethodName;
    data['bank_name'] = this.bankName;
    data['branch_name'] = this.branchName;
    data['routing_number'] = this.routingNumber;
    return data;
  }
}
