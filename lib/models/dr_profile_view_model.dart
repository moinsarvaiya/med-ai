class DrProfileViewModel {
  String? code;
  DoctorProfile? doctorProfile;

  DrProfileViewModel({this.code, this.doctorProfile});

  DrProfileViewModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    doctorProfile = json['doctor_profile'] != null ? new DoctorProfile.fromJson(json['doctor_profile']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    if (this.doctorProfile != null) {
      data['doctor_profile'] = this.doctorProfile!.toJson();
    }
    return data;
  }
}

class DoctorProfile {
  String? firstName;
  String? lastName;
  int? age;
  String? gender;
  String? regNo;
  String? registrationYear;
  String? specialization;
  String? designation;
  String? regBody;
  String? currentInstitute;
  String? country;
  String? district;
  String? mobile;
  String? affiliation;
  String? trainingCourse;
  String? languagesSpeaks;
  String? doctorType;
  String? approvalStatus;
  String? email;
  List<String>? degrees;
  Schedule? schedule;

  DoctorProfile({
    this.firstName,
    this.lastName,
    this.age,
    this.gender,
    this.regNo,
    this.registrationYear,
    this.specialization,
    this.designation,
    this.regBody,
    this.currentInstitute,
    this.country,
    this.district,
    this.mobile,
    this.affiliation,
    this.trainingCourse,
    this.languagesSpeaks,
    this.doctorType,
    this.approvalStatus,
    this.email,
    this.degrees,
    this.schedule,
  });

  DoctorProfile.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
    age = json['age'];
    gender = json['gender'];
    regNo = json['reg_no'];
    registrationYear = json['registration_year'];
    specialization = json['specialization'];
    designation = json['designation'];
    regBody = json['reg_body'];
    currentInstitute = json['current_institute'];
    country = json['country'];
    district = json['district'];
    mobile = json['mobile'];
    affiliation = json['affiliation'];
    trainingCourse = json['training_course'];
    languagesSpeaks = json['languages_speaks'];
    doctorType = json['doctor_type'];
    approvalStatus = json['approval_status'];
    email = json['email'];
    degrees = json['degrees'].cast<String>();
    schedule = json['schedule'] != null ? new Schedule.fromJson(json['schedule']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['age'] = this.age;
    data['gender'] = this.gender;
    data['reg_no'] = this.regNo;
    data['registration_year'] = this.registrationYear;
    data['specialization'] = this.specialization;
    data['designation'] = this.designation;
    data['reg_body'] = this.regBody;
    data['current_institute'] = this.currentInstitute;
    data['country'] = this.country;
    data['district'] = this.district;
    data['mobile'] = this.mobile;
    data['affiliation'] = this.affiliation;
    data['training_course'] = this.trainingCourse;
    data['languages_speaks'] = this.languagesSpeaks;
    data['doctor_type'] = this.doctorType;
    data['approval_status'] = this.approvalStatus;
    data['email'] = this.email;
    data['degrees'] = this.degrees;
    if (this.schedule != null) {
      data['schedule'] = this.schedule!.toJson();
    }
    return data;
  }
}

class Schedule {
  String? visitFee;
  String? visitDuration;
  String? breakDuration;
  String? followUpFee;
  String? availability;
  List<String>? days;
  List<String>? consultationHour;

  Schedule({
    this.visitFee,
    this.visitDuration,
    this.breakDuration,
    this.followUpFee,
    this.availability,
    this.days,
    this.consultationHour,
  });

  Schedule.fromJson(Map<String, dynamic> json) {
    visitFee = json['visit_fee'];
    visitDuration = json['visit_duration'];
    breakDuration = json['break'];
    followUpFee = json['follow_up_fee'];
    availability = json['availability'];
    days = json['days'].cast<String>();
    consultationHour = json['consultation_hour'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['visit_fee'] = this.visitFee;
    data['visit_duration'] = this.visitDuration;
    data['break'] = this.breakDuration;
    data['follow_up_fee'] = this.followUpFee;
    data['availability'] = this.availability;
    data['days'] = this.days;
    data['consultation_hour'] = this.consultationHour;
    return data;
  }
}
