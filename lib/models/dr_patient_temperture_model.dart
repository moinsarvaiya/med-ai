class DrPatientTemperatureModel {
  String? temperature;
  int? temperatureValue;
  bool? isSelected;

  DrPatientTemperatureModel({this.temperature, this.temperatureValue, this.isSelected});

  DrPatientTemperatureModel.fromJson(Map<String, dynamic> json) {
    temperature = json['temperature'];
    temperatureValue = json['temperatureValue'];
    isSelected = json['isSelected'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['temperature'] = temperature;
    data['temperatureValue'] = temperatureValue;
    data['isSelected'] = isSelected;
    return data;
  }
}
