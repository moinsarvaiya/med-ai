class DrWithdrawalHistoryModel {
  String? currency;
  String? status;
  String? amount;
  String? date;
  int? withdrawalId;
  String? operatorId;
  String? trxID;
  String? withdrawalIdStr;
  TransactionMethodData? transactionMethodData;

  DrWithdrawalHistoryModel(
      {this.currency,
      this.status,
      this.amount,
      this.date,
      this.withdrawalId,
      this.operatorId,
      this.trxID,
      this.withdrawalIdStr,
      this.transactionMethodData});

  DrWithdrawalHistoryModel.fromJson(Map<String, dynamic> json) {
    currency = json['currency'];
    status = json['status'];
    amount = json['amount'];
    date = json['date'];
    withdrawalId = json['withdrawal_id'];
    operatorId = json['operator_id'];
    trxID = json['trxID'];
    withdrawalIdStr = json['withdrawal_id_str'];
    transactionMethodData =
        json['transaction_method_data'] != null ? new TransactionMethodData.fromJson(json['transaction_method_data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['currency'] = this.currency;
    data['status'] = this.status;
    data['amount'] = this.amount;
    data['date'] = this.date;
    data['withdrawal_id'] = this.withdrawalId;
    data['operator_id'] = this.operatorId;
    data['trxID'] = this.trxID;
    data['withdrawal_id_str'] = this.withdrawalIdStr;
    if (this.transactionMethodData != null) {
      data['transaction_method_data'] = this.transactionMethodData!.toJson();
    }
    return data;
  }
}

class TransactionMethodData {
  String? accountName;
  String? mfsName;
  int? txMethodId;
  String? accountNumber;
  String? txMethodType;
  String? txMethodName;
  String? bankName;
  String? branchName;
  String? routingNumber;
  String? txMethodIdStr;

  TransactionMethodData(
      {this.accountName,
      this.mfsName,
      this.txMethodId,
      this.accountNumber,
      this.txMethodType,
      this.txMethodName,
      this.bankName,
      this.branchName,
      this.routingNumber,
      this.txMethodIdStr});

  TransactionMethodData.fromJson(Map<String, dynamic> json) {
    accountName = json['account_name'];
    mfsName = json['mfs_name'];
    txMethodId = json['tx_method_id'];
    accountNumber = json['account_number'];
    txMethodType = json['tx_method_type'];
    txMethodName = json['tx_method_name'];
    bankName = json['bank_name'];
    branchName = json['branch_name'];
    routingNumber = json['routing_number'];
    txMethodIdStr = json['tx_method_id_str'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['account_name'] = this.accountName;
    data['mfs_name'] = this.mfsName;
    data['tx_method_id'] = this.txMethodId;
    data['account_number'] = this.accountNumber;
    data['tx_method_type'] = this.txMethodType;
    data['tx_method_name'] = this.txMethodName;
    data['bank_name'] = this.bankName;
    data['branch_name'] = this.branchName;
    data['routing_number'] = this.routingNumber;
    data['tx_method_id_str'] = this.txMethodIdStr;
    return data;
  }
}
