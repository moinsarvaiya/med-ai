class DocAvailabilityDaysModel {
  String? dayName;
  bool? isAvailable;

  DocAvailabilityDaysModel({this.dayName, this.isAvailable});

  DocAvailabilityDaysModel.fromJson(Map<String, dynamic> json) {
    dayName = json['dayName'];
    isAvailable = json['isAvailable'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['dayName'] = dayName;
    data['isAvailable'] = isAvailable;
    return data;
  }
}
