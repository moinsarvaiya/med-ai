class PtDoctorTimeSlotModel {
  PtDoctorTimeSlotModel({
    this.startTime,
    this.endTime,
    this.isAvailable,
  });

  PtDoctorTimeSlotModel.fromJson(dynamic json) {
    startTime = json['startTime'];
    endTime = json['endTime'];
    isAvailable = json['isAvailable'];
  }

  String? startTime;
  String? endTime;
  bool? isAvailable;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['startTime'] = startTime;
    map['endTime'] = endTime;
    map['isAvailable'] = isAvailable;
    return map;
  }
}
