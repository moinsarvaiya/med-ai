class DrFollowupSoapModel {
  String? code;
  SoapInfo? soapInfo;
  PrescriptionInfo? prescriptionInfo;

  DrFollowupSoapModel({this.code, this.soapInfo, this.prescriptionInfo});

  DrFollowupSoapModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    soapInfo = json['soap_info'] != null ? new SoapInfo.fromJson(json['soap_info']) : null;
    prescriptionInfo = json['prescription_info'] != null ? new PrescriptionInfo.fromJson(json['prescription_info']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    if (this.soapInfo != null) {
      data['soap_info'] = this.soapInfo!.toJson();
    }
    if (this.prescriptionInfo != null) {
      data['prescription_info'] = this.prescriptionInfo!.toJson();
    }
    return data;
  }
}

class SoapInfo {
  PersonalInformation? personalInformation;
  ComplaintsSymptoms? complaintsSymptoms;
  MedicalHistory? medicalHistory;
  PreExistingCond? preExistingCond;
  VitalSigns? vitalSigns;
  Recommendations? recommendations;
  String? contactNumber;
  String? contactMethod;
  String? patientComment;

  SoapInfo(
      {this.personalInformation,
      this.complaintsSymptoms,
      this.medicalHistory,
      this.preExistingCond,
      this.vitalSigns,
      this.recommendations,
      this.contactNumber,
      this.contactMethod,
      this.patientComment});

  SoapInfo.fromJson(Map<String, dynamic> json) {
    personalInformation = json['personal_information'] != null ? new PersonalInformation.fromJson(json['personal_information']) : null;
    complaintsSymptoms = json['complaints_&_symptoms'] != null ? new ComplaintsSymptoms.fromJson(json['complaints_&_symptoms']) : null;
    medicalHistory = json['medical_history'] != null ? new MedicalHistory.fromJson(json['medical_history']) : null;
    preExistingCond = json['pre_existing_cond'] != null ? new PreExistingCond.fromJson(json['pre_existing_cond']) : null;
    vitalSigns = json['vital_signs'] != null ? new VitalSigns.fromJson(json['vital_signs']) : null;
    recommendations = json['recommendations'] != null ? new Recommendations.fromJson(json['recommendations']) : null;
    contactNumber = json['contact_number'];
    contactMethod = json['contact_method'];
    patientComment = json['patient_comment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.personalInformation != null) {
      data['personal_information'] = this.personalInformation!.toJson();
    }
    if (this.complaintsSymptoms != null) {
      data['complaints_&_symptoms'] = this.complaintsSymptoms!.toJson();
    }
    if (this.medicalHistory != null) {
      data['medical_history'] = this.medicalHistory!.toJson();
    }
    if (this.preExistingCond != null) {
      data['pre_existing_cond'] = this.preExistingCond!.toJson();
    }
    if (this.vitalSigns != null) {
      data['vital_signs'] = this.vitalSigns!.toJson();
    }
    if (this.recommendations != null) {
      data['recommendations'] = this.recommendations!.toJson();
    }
    data['contact_number'] = this.contactNumber;
    data['contact_method'] = this.contactMethod;
    data['patient_comment'] = this.patientComment;
    return data;
  }
}

class PersonalInformation {
  String? name;
  String? personId;
  int? age;
  String? gender;
  String? district;
  String? country;
  String? phone;

  PersonalInformation({this.name, this.personId, this.age, this.gender, this.district, this.country, this.phone});

  PersonalInformation.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    personId = json['person_id'];
    age = json['age'];
    gender = json['gender'];
    district = json['district'];
    country = json['country'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['person_id'] = this.personId;
    data['age'] = this.age;
    data['gender'] = this.gender;
    data['district'] = this.district;
    data['country'] = this.country;
    data['phone'] = this.phone;
    return data;
  }
}

class ComplaintsSymptoms {
  List<String>? chiefComplaints;
  List<String>? symptoms;

  ComplaintsSymptoms({this.chiefComplaints, this.symptoms});

  ComplaintsSymptoms.fromJson(Map<String, dynamic> json) {
    chiefComplaints = json['chief_complaints'].cast<String>();
    symptoms = json['symptoms'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['chief_complaints'] = this.chiefComplaints;
    data['symptoms'] = this.symptoms;
    return data;
  }
}

class MedicalHistory {
  List<String>? medicalCondition;
  String? allergy;
  List<String>? familyHistory;
  List<String>? personalHistory;

  MedicalHistory({this.medicalCondition, this.allergy, this.familyHistory, this.personalHistory});

  MedicalHistory.fromJson(Map<String, dynamic> json) {
    medicalCondition = json['medical_condition'].cast<String>();
    allergy = json['allergy'];
    familyHistory = json['family_history'].cast<String>();
    personalHistory = json['personal_history'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['medical_condition'] = this.medicalCondition;
    data['allergy'] = this.allergy;
    data['family_history'] = this.familyHistory;
    data['personal_history'] = this.personalHistory;
    return data;
  }
}

class PreExistingCond {
  String? medicalCondition;
  String? medication;

  PreExistingCond({this.medicalCondition, this.medication});

  PreExistingCond.fromJson(Map<String, dynamic> json) {
    medicalCondition = json['medical_condition'];
    medication = json['medication'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['medical_condition'] = this.medicalCondition;
    data['medication'] = this.medication;
    return data;
  }
}

class VitalSigns {
  String? bp;
  String? temp;
  String? pulse;
  String? breathingRate;

  VitalSigns({this.bp, this.temp, this.pulse, this.breathingRate});

  VitalSigns.fromJson(Map<String, dynamic> json) {
    bp = json['bp'];
    temp = json['temp'];
    pulse = json['pulse'];
    breathingRate = json['breathing_rate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bp'] = this.bp;
    data['temp'] = this.temp;
    data['pulse'] = this.pulse;
    data['breathing_rate'] = this.breathingRate;
    return data;
  }
}

class Recommendations {
  String? provisionalDiagnosis;
  List<String>? recommendedDiagnosticTests;
  List<String>? recommendedMedicine;

  Recommendations({this.provisionalDiagnosis, this.recommendedDiagnosticTests, this.recommendedMedicine});

  Recommendations.fromJson(Map<String, dynamic> json) {
    provisionalDiagnosis = json['provisional_diagnosis'];
    recommendedDiagnosticTests = json['recommended_diagnostic_tests'].cast<String>();
    recommendedMedicine = json['recommended_medicine'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['provisional_diagnosis'] = this.provisionalDiagnosis;
    data['recommended_diagnostic_tests'] = this.recommendedDiagnosticTests;
    data['recommended_medicine'] = this.recommendedMedicine;
    return data;
  }
}

class PrescriptionInfo {
  Decision? decision;
  Prescription? prescription;

  PrescriptionInfo({this.decision, this.prescription});

  PrescriptionInfo.fromJson(Map<String, dynamic> json) {
    decision = json['decision'] != null ? new Decision.fromJson(json['decision']) : null;
    prescription = json['prescription'] != null ? new Prescription.fromJson(json['prescription']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.decision != null) {
      data['decision'] = this.decision!.toJson();
    }
    if (this.prescription != null) {
      data['prescription'] = this.prescription!.toJson();
    }
    return data;
  }
}

class Decision {
  List<String>? identifiedDisease;
  String? clinicalNote;

  Decision({this.identifiedDisease, this.clinicalNote});

  Decision.fromJson(Map<String, dynamic> json) {
    identifiedDisease = json['identified_disease'].cast<String>();
    clinicalNote = json['clinical_note'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['identified_disease'] = this.identifiedDisease;
    data['clinical_note'] = this.clinicalNote;
    return data;
  }
}

class Prescription {
  List<String>? medication;
  List<String>? diagnosticTests;
  String? testInstruction;

  Prescription({this.medication, this.diagnosticTests, this.testInstruction});

  Prescription.fromJson(Map<String, dynamic> json) {
    medication = json['medication'].cast<String>();
    diagnosticTests = json['diagnostic_tests'].cast<String>();
    testInstruction = json['test_instruction'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['medication'] = this.medication;
    data['diagnostic_tests'] = this.diagnosticTests;
    data['test_instruction'] = this.testInstruction;
    return data;
  }
}
