class PtDoctorCallingMethodModel {
  String? methodIcon;
  String? methodName;
  bool? isSelected;

  PtDoctorCallingMethodModel({
    this.methodIcon,
    this.methodName,
    this.isSelected,
  });

  PtDoctorCallingMethodModel.fromJson(dynamic json) {
    methodIcon = json['methodIcon'];
    methodName = json['methodName'];
    isSelected = json['isSelected'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['methodIcon'] = methodIcon;
    map['methodName'] = methodName;
    map['isSelected'] = isSelected;
    return map;
  }
}
