class PtHomeDataModel {
  PtHomeDataModel({
    this.name,
    this.image,
    this.navigation,
    this.enumProfileSelection,
  });

  PtHomeDataModel.fromJson(dynamic json) {
    name = json['name'];
    image = json['image'];
    navigation = json['navigation'];
    enumProfileSelection = json['enumProfileSelection'];
  }

  String? name;
  String? image;
  String? navigation;
  Enum? enumProfileSelection;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    map['image'] = image;
    map['navigation'] = navigation;
    map['enumProfileSelection'] = enumProfileSelection;
    return map;
  }
}
