import 'package:get/get.dart';

class ModelDiagnosticInfo {
  ModelDiagnosticInfo({
    this.diagnosticName,
    this.option,
    this.resultSummary,
    this.images,
  });

  ModelDiagnosticInfo.fromJson(dynamic json) {
    diagnosticName = json['diagnosticName'];
    option = json['option'];
    resultSummary = json['resultSummary'];
    images = json['images'] != null ? json['images'].cast<String>() : [];
  }

  String? diagnosticName;
  int? option;
  String? resultSummary;
  RxList<String>? images;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['diagnosticName'] = diagnosticName;
    map['option'] = option;
    map['resultSummary'] = resultSummary;
    map['images'] = images;
    return map;
  }
}
