class DrPrescribeMedicineModel {
  String? brandName;
  String? dosageType;
  String? threeTimesDay;
  String? hourDifference;
  String? mealInstruction;
  String? duration;
  String? specialInstruction;

  DrPrescribeMedicineModel({
    this.brandName,
    this.dosageType,
    this.threeTimesDay,
    this.hourDifference,
    this.mealInstruction,
    this.duration,
    this.specialInstruction,
  });

  DrPrescribeMedicineModel.fromJson(dynamic json) {
    brandName = json['brandName'];
    dosageType = json['dosageType'];
    threeTimesDay = json['medicineTiming'];
    hourDifference = json['hourDifference'];
    mealInstruction = json['mealInstruction'];
    duration = json['duration'];
    specialInstruction = json['specialInstruction'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['brandName'] = brandName;
    map['dosageType'] = dosageType;
    map['medicineTiming'] = threeTimesDay;
    map['hourDifference'] = hourDifference;
    map['mealInstruction'] = mealInstruction;
    map['duration'] = duration;
    map['specialInstruction'] = specialInstruction;
    return map;
  }
}
