import '../constant/app_strings_key.dart';

Map<String, String> bn_BN = {
  AppStringKey.appName: 'MedAI',
  AppStringKey.toContinue: 'Choose Your Language',
  AppStringKey.success: 'Success',
  AppStringKey.error: 'Error',
  AppStringKey.back: 'পেছনে',
  AppStringKey.somethingWentWrong: 'Something went wrong!',
  AppStringKey.noDataFound: 'No Data Found',
  AppStringKey.visit: 'ভিজিট',
  AppStringKey.followUp: 'ফলো আপ',

//button texts
  AppStringKey.btnGetStarted: 'Get Started',
  AppStringKey.btnWithdrawBalance: 'ব্যালেন্স উত্তোলন',
  AppStringKey.btnAddNewMethod: 'নতুন পদ্ধতি যোগ করুন',
  AppStringKey.btnSelectThisMethod: 'এই পদ্ধতি নির্বাচন করুন',
  AppStringKey.btnSetMethod: 'পদ্ধতি সেট করুন',
  AppStringKey.btnWithdrawalRequest: 'ব্যালেন্স উত্তোলনের আবেদন',
  AppStringKey.btnSearch: 'খোঁজা',
  AppStringKey.btnBackToNewConsultancy: 'নতুন কনসাল্টেন্সিতে ফিরে যান',
  AppStringKey.btnBackToDashboard: 'ড্যাশবোর্ডে ফিরে যান',
  AppStringKey.btnUpdate: 'আপডেট ',
  AppStringKey.btnViewEdit: 'দেখুন এবং এডিট করুন',
  AppStringKey.btnAddThisRecord: 'এই রেকর্ড যোগ করুন',
  AppStringKey.btnEditRecord: 'সম্পাদনা রেকর্ড',
  AppStringKey.btnUpdateThisRecord: 'এই রেকর্ড আপডেট করুন',
  AppStringKey.btnCancel: 'বাতিল',
  AppStringKey.btnUpload: 'আপলোড',
  AppStringKey.btnCheckUsingAIAssistant: 'AI ব্যবহার করে পরীক্ষা করুন',

  AppStringKey.titleLogin: 'Login In To\nYour Account',
  AppStringKey.titleOnBoarding1: 'Welcome to\nMedAi!',
  AppStringKey.titleOnBoarding2: 'Find your doctor and Make an appointment with ease!',
  AppStringKey.titleOnBoarding3: 'Download your prescription and test results!',
  AppStringKey.contentOnBoarding1: 'Global Healthcare in Local languages',
  AppStringKey.contentOnBoarding2: 'Global Healthcare in Local languages',
  AppStringKey.contentOnBoarding3: 'Global Healthcare in Local languages',
  AppStringKey.selectUserDescription: 'Global Healthcare in Local languages',

//common
  AppStringKey.strWelcome: 'স্বাগতম!',
  AppStringKey.strDoctor: 'ডাক্তার',
  AppStringKey.strPatient: 'রোগী',
  AppStringKey.strPatients: 'রোগী',
  AppStringKey.strRevenue: 'রাজস্ব',
  AppStringKey.strCapacity: 'ক্ষমতা',
  AppStringKey.strMale: 'পুরুষ',
  AppStringKey.strFemale: 'মহিলা',
  AppStringKey.strAnd: 'এবং',
  AppStringKey.strDay: 'দিন',
  AppStringKey.strNext: 'পরবর্তী',
  AppStringKey.strPayBook: 'পে এবং বুক',
  AppStringKey.strNextSubmit: 'সংরক্ষণ করুন এবং জমা দিন',
  AppStringKey.strSaveNext: 'সংরক্ষণ করুন এবং এগিয়ে যান',
  AppStringKey.strSkip: 'এড়িয়ে যান',
  AppStringKey.strEdit: 'এডিট',
  AppStringKey.strDelete: 'ডিলিট ',
  AppStringKey.strDate: 'তারিখ',
  AppStringKey.strSave: 'সংরক্ষণ করুন',
  AppStringKey.strSubmit: 'জমা দিন',
  AppStringKey.strAvailable: 'এভেইলেবল',
  AppStringKey.strUnavailable: 'আনএভেইলেবল',
  AppStringKey.strUnread: 'অপঠিত',
  AppStringKey.strComplete: 'সম্পূর্ণ',
  AppStringKey.strLocation: 'অবস্থান',
  AppStringKey.strConsultancy: 'কনসালটেন্সি',
  AppStringKey.strConsultancyHistoryDecs: 'History of last 30 days (2 consultations)',
  AppStringKey.strAppointmentSchdule: 'অ্যাপয়েন্টমেন্টের সময়সূচী',
  AppStringKey.strComplaintsSymptoms: 'সমস্যা এবং উপসর্গ',
  AppStringKey.strChiefComplaints: 'প্রধান সমস্যা',
  AppStringKey.strSymptoms: 'উপসর্গ',
  AppStringKey.strPredication: 'পূর্বাভাস',
  AppStringKey.strRecommendation: 'সুপারিশ*',
  AppStringKey.strRecommendationSpecialist: 'সুপারিশকৃত বিশেষজ্ঞ',
  AppStringKey.strMedicalHistory: 'পূর্বের স্বাস্থ্য বিবরণ',
  AppStringKey.strMedicalCondition: 'চিকিৎসাধীন অবস্থা',
  AppStringKey.strAllergies: 'এলার্জি',
  AppStringKey.strFamilyHistory: 'পারিবারিক ইতিহাস',
  AppStringKey.strPersonalHistory: 'ব্যক্তিগত ইতিহাস',
  AppStringKey.strMedication: 'ওষুধ',
  AppStringKey.strPreConditions: 'বর্তমান শারীরিক অবস্থা',
  AppStringKey.strVitalSigns: 'গুরুত্বপূর্ণ লক্ষণ',
  AppStringKey.strBP: 'BP',
  AppStringKey.strTemp: 'Temp',
  AppStringKey.strPulseRate: 'স্পন্দনের হার',
  AppStringKey.strBreathingsRate: 'নি:শ্বাসের হার',
  AppStringKey.strRecommendations: 'সুপারিশ',
  AppStringKey.strProvisionalDiagnosis: 'প্রাথমিক নির্ণীত রোগ',
  AppStringKey.strRecommendedMedicines: 'প্রস্তাবিত ওষুধ',
  AppStringKey.strDecision: 'সিদ্ধান্ত',
  AppStringKey.strIdentifiedDisease: 'নির্ণীত রোগ',
  AppStringKey.strClinicalNote: 'ক্লিনিকাল নোট',
  AppStringKey.strPrescribedMedication: 'নির্ধারিত ওষুধ',
  AppStringKey.strPrescribedDiagnostics: 'নির্ধারিত ডায়াগনস্টিক টেস্ট',
  AppStringKey.strBrand: 'ব্র্যান্ড',
  AppStringKey.strTime: 'সময়',
  AppStringKey.strAppointment: 'অ্যাপয়েন্টমেন্ট  ',
  AppStringKey.strFoodIntake: 'কখন খাবেন?',
  AppStringKey.strDuration: 'সময়কাল',
  AppStringKey.strType: 'ধরন (Optional)',
  AppStringKey.strTypeWithoutOptional: 'প্রকার',
  AppStringKey.strHowOftenItComes: 'কত ঘনঘন',
  AppStringKey.strHowItStarted: 'কিভাবে শুরু হয়',
  AppStringKey.strTraveledRecentlyDesc: 'আপনি কি সম্প্রতি এই এলাকায় \nবসবাস করেছেন বা ভ্রমণ করেছেন?',
  AppStringKey.strDoYouHavePersistentCough: 'আপনার কি একটানা কাশি হয়?',
  AppStringKey.strAdditionalCommentDesc: 'অবিরাম জ্বর, অবিরাম কাশি',
  AppStringKey.strAdditionalCommentOrNotes: 'অতিরিক্ত মন্তব্য বা নোট',
  AppStringKey.strAdditionalCommentImages: 'ছবি যোগ করুন (Optional)',
  AppStringKey.strReviewDoctorSuggest: 'ডাক্তার নিম্নোক্ত টেস্ট গুলোর পরামর্শ দিতে পারেন',
  AppStringKey.strReviewEditQuestions: 'এডিট উপসর্গ',
  AppStringKey.strDisclaimer: 'বিশেষ দ্রষ্টব্য*',
  AppStringKey.strAfter: 'পরে',
  AppStringKey.strBefore: 'আগে',
  AppStringKey.strAdd: 'যোগ করুন',
  AppStringKey.strNA: 'N/A',
  AppStringKey.strRemove: 'মুছে ফেলুন',
  AppStringKey.strAlert: 'সতর্কতা',
  AppStringKey.strStay: 'থাকুন',
  AppStringKey.strGoBack: 'ফিরে যাও',
  AppStringKey.strMorning: 'সকাল',
  AppStringKey.strNoon: 'দুপুর',
  AppStringKey.strNight: 'রাত',
  AppStringKey.strImages: 'ছবি',
  AppStringKey.strCreatePatientDesc: 'রোগীর প্রোফাইল সম্পূর্ণ করতে\nঅনুগ্রহ করে আরও কিছু তথ্য দিন',
  AppStringKey.strAvailableDoc: 'অ্যাপের এভেইলেবল\nডাক্তার',
  AppStringKey.strNrbyHospitals: 'নিকটস্থ\nহাসপাতাল',
  AppStringKey.strNrbyAmbulances: 'নিকটস্থ\nঅ্যাম্বুলেন্স',
  AppStringKey.strNrbyBloodBank: 'নিকটস্থ\nব্লাড ব্যাঙ্ক',
  AppStringKey.strNrbyPhysiotherapist: 'নিকটস্থ\nফিজিওথেরাপিস্ট',
  AppStringKey.strDocCabins: 'ডাক্তারের\nচেম্বার',
  AppStringKey.strDocIsAvailable: 'বর্তমানে ডাক্তার এভেইলেবল আছেন',
  AppStringKey.strDocIsNotAvailable: 'বর্তমানে ডাক্তার পাওয়া যাচ্ছে না',
  AppStringKey.strBookAnAppointment: 'অ্যাপয়েন্টমেন্ট বুক করুন ',
  AppStringKey.strSaveExit: 'সংরক্ষণ করুন এবং বাহির হন',
  AppStringKey.strReturnHomePage: 'আপনি হোমপেজে ফিরে যেতে চান??',
  AppStringKey.strHighToLow: 'উচ্চ থেকে নিম্ন',
  AppStringKey.strLowToHigh: 'কম থেকে বেশি',
  AppStringKey.strAreYouSureDelete: 'আপনি কি নিশ্চিত এই পদ্ধতি মুছে ফেলতে চান?',
  AppStringKey.strPriceRange: 'মূল্য পরিসীমা',
  AppStringKey.strSpecialty: 'বিশেষত্ব',
  AppStringKey.strGender: 'লিংগ',
  AppStringKey.strFilterBy: 'ফিল্টার করুন',
  AppStringKey.strFilter: 'ফিল্টার',
  AppStringKey.strSort: 'সাজান',
  AppStringKey.strSortBy: 'ক্রমানুসার',
  AppStringKey.strSortByPrice: 'মূল্য অনুসারে সাজান',
  AppStringKey.strSortByPatient: 'রোগী দেখা অনুসারে সাজান',
  AppStringKey.strClearAll: 'সব পরিষ্কার করে দাও',
  AppStringKey.strApply: 'আবেদন করুন',
  AppStringKey.strVideoCall: 'অ্যাপ ভিডিওতে\nকল করুন',
  AppStringKey.strAudioCall: 'অ্যাপ ভয়েসে\nকল করুন',
  AppStringKey.strDirectCall: 'সরাসরি কল',
  AppStringKey.strTimeSlotSelection: 'সময় স্লট নির্বাচন',
  AppStringKey.strCallingMethod: 'পছন্দের কলিং পদ্ধতি',
  AppStringKey.strBookingTime: 'বুকিং সময়',
  AppStringKey.strCallingMethodHeading: 'কলিং পদ্ধতি',
  AppStringKey.strAppointmentFee: 'অ্যাপয়েন্টমেন্ট ফি',
  AppStringKey.strDiscountAmount: 'ডিসকাউন্ট মুল্য',
  AppStringKey.strFinalAmount: 'ফাইনাল পরিমাণ',
  AppStringKey.strFeeDetails: 'ফি বিবরণ',
  AppStringKey.strPromoCode: 'প্রমো কোড',
  AppStringKey.strPromoCodeApplySuccess: 'প্রমো কোড সফলভাবে প্রয়োগ করা হয়েছে',
  AppStringKey.strVerified: 'Verified',
  AppStringKey.strDirectCallLable: 'Direct call:  ',
  AppStringKey.noUpcomingAppointment: 'No Upcoming Appointment',
  AppStringKey.strThanksBooking: 'বুকিংয়ের জন্য ধন্যবাদ',
  AppStringKey.strConfirmSoon: 'আপনার বুকিং সময় শীঘ্রই ডাক্তার দ্বারা \n নিশ্চিত করা হবে',
  AppStringKey.strBookingID: 'বুকিং আইডি:',
  AppStringKey.strPainNO:
      "এই ট্রাইজিং কেবল অ-জরুরী লক্ষণ পরীক্ষার জন্য। \n যদি আপনার সম্প্রতি একটি আঘাত ছিল যা হ'ল কনফিউশন/লেসারেশন/এভুলশন/বিচ্ছেদ/বার্ন/ক্রাশ/অবনমিত/অনুপ্রবেশ/জাহাজ, স্নায়ু, অঙ্গ, হাড়, হাড় বা জয়েন্টগুলি জড়িত থাকার কারণে আরও কিছু নির্দিষ্ট আঘাতের কারণ আপনার নিকটতম মেডিকেল ইমার্জেন্সি বিভাগের সাথে যোগাযোগ করুন । ",

  AppStringKey.strNearbyHospital: 'কাছাকাছি হাসপাতাল',
  AppStringKey.strNearbyAmbulances: 'কাছাকাছি অ্যাম্বুলেন্স',
  AppStringKey.strNearbyBloodBanks: 'কাছাকাছি রক্ত ব্যাংক',
  AppStringKey.strNearbyPhysiotherapist: 'কাছাকাছি ফিজিওথেরাপিস্ট',
  AppStringKey.strNearbyChambers: 'ডাক্তারের চেম্বারস',
  AppStringKey.strGoodAfternoon: 'শুভ অপরাহ্ন',
  AppStringKey.strRefundPolicy: 'প্রত্যর্পণ নীতি',
  AppStringKey.strTermsCondition: 'শর্তাবলী',
  AppStringKey.strPrivacyPolicy: 'গোপনীয়তা নীতি',
  AppStringKey.strAvailableAppDoctor: 'উপলব্ধ অ্যাপ্লিকেশন ডাক্তার',

//hints
  AppStringKey.hintEmailOrMobile: 'ইমেল/মোবাইল নম্বর লিখুন',
  AppStringKey.hintOtp: 'এখানে আপনার OTP কোড লিখুন',
  AppStringKey.hintFindThePatients: 'রোগীদের খুঁজুন',
  AppStringKey.hintSearchPatients: 'রোগীর ফোন/ইমেল/নাম দ্বারা অনুসন্ধান করুন',
  AppStringKey.hintSearchMedicine: 'ওষুধ, ব্লাড ব্যাঙ্ক, হাসপাতাল, অ্যাম্বুলেন্স',
  AppStringKey.hintSearchDoctorHospital: 'সঠিক ডাক্তার, হাসপাতাল সন্ধান করুন',
  AppStringKey.hintSearchDoctor: 'ডাক্তার খুঁজুন',
  AppStringKey.hintBDT: 'BDT',
  AppStringKey.hintInstitutionName: 'প্রতিষ্ঠানের নাম লিখুন',
  AppStringKey.hintAccreditationBody: 'অ্যাক্রিডিটেশন বডি লিখুন',
  AppStringKey.hintCountry: 'দেশের নাম লিখুন',
  AppStringKey.hintSelectCountry: 'দেশ নির্বাচন করুন',
  AppStringKey.hintSelect: 'সিলেক্ট',
  AppStringKey.hintRegisteredNo: 'নিবন্ধিত নম্বর লিখুন',
  AppStringKey.hintRegisteredBody: 'নিবন্ধিত বডি লিখুন',
  AppStringKey.hintRegisteredYear: 'নিবন্ধিত বছর লিখুন',
  AppStringKey.hintWorking: 'কর্মরত - সংস্থা / হাসপাতাল / ক্লিনিক',
  AppStringKey.hintLanguages: 'আপনার ভাষা অনুসন্ধান করুন',
  AppStringKey.hintSelectLanguages: 'আপনার ভাষা নির্বাচন করুন',
  AppStringKey.hintSelectGender: 'আপনার লিঙ্গ নির্বাচন',
  AppStringKey.hintMaritalStatus: 'আপনার বৈবাহিক অবস্থা অনুসন্ধান করুন',
  AppStringKey.hintSelectMaritalStatus: 'আপনার বৈবাহিক অবস্থা নির্বাচন করুন',
  AppStringKey.hintEducation: 'আপনার শিক্ষা অনুসন্ধান করুন',
  AppStringKey.hintSelectEducation: 'আপনার শিক্ষা নির্বাচন করুন',
  AppStringKey.hintOccupation: 'আপনার পেশা অনুসন্ধান করুন',
  AppStringKey.hintSelectOccupation: 'আপনার পেশা নির্বাচন করুন',
  AppStringKey.hintEthnicity: 'আপনার জাতিগত অনুসন্ধান করুন',
  AppStringKey.hintSelectEthnicity: 'আপনার জাতি নির্বাচন করুন',
  AppStringKey.hintMonthlyEarning: 'আপনার মাসিক উপার্জন অনুসন্ধান করুন',
  AppStringKey.hintSelectMonthlyEarning: 'আপনার মাসিক উপার্জন নির্বাচন করুন',
  AppStringKey.hintSelectWhere: 'কোথায় নির্বাচন করুন',
  AppStringKey.hintMobile: 'আপনার মোবাইল নম্বর লিখুন',
  AppStringKey.hintEmail: 'আপনার ইমেইল লিখুন',
  AppStringKey.hintAge: 'আপনার বয়স লিখুন',
  AppStringKey.hintArea: 'আপনার এলাকায়  লিখুন',
  AppStringKey.hintSelectArea: 'আপনার এলাকা নির্বাচন করুন',
  AppStringKey.hintSelectCity: 'আপনার শহর নির্বাচন করুন',
  AppStringKey.hintReferralCode: 'রেফারেল কোড লিখুন (optional)',
  AppStringKey.hintSpecialization: 'আপনার বিশেষীকরণ লিখুন',
  AppStringKey.hintDesignation: 'আপনার পদবী লিখুন',
  AppStringKey.hintAffiliation: 'অ্যাফিলিয়েশন লিখুন',
  AppStringKey.hintTraining: 'প্রশিক্ষণ লিখুন',
  AppStringKey.hintDegreeName: 'ডিগ্রির নাম লিখুন',
  AppStringKey.hintSelectDegreeName: 'ডিগ্রীর নাম নির্বাচন করুন',
  AppStringKey.hintSelectConsultationDuration: 'পরামর্শের সময়কাল নির্বাচন করুন',
  AppStringKey.hintSelectAppointmentDuration: 'অ্যাপয়েন্টমেন্টের সময়কাল নির্বাচন করুন',
  AppStringKey.hintSelectBreakDuration: 'বিরতির সময়কাল নির্বাচন করুন',
  AppStringKey.hintMethodSaveAs: 'এই পদ্ধতির একটি নাম দিন',
  AppStringKey.hintDiseaseName: 'রোগের নাম টাইপ করুন',
  AppStringKey.hintNote: 'নোট টাইপ করুন',
  AppStringKey.hintSymptoms: 'এখানে লক্ষণ টাইপ করুন',

  AppStringKey.hintFullName: 'আপনার পুরো নাম লিখুন',
  AppStringKey.hintFirstName: 'আপনার নামের প্রথম অংশ লিখুন',
  AppStringKey.hintLastName: 'আপনার শেষ নাম লিখুন',
  AppStringKey.hintDivision: 'আপনার বিভাগের নাম লিখুন',
  AppStringKey.hintSelectDivision: 'আপনার বিভাগ নির্বাচন করুন',
  AppStringKey.hintDistrict: 'আপনার জেলার নাম লিখুন',
  AppStringKey.hintSelectDistrict: 'আপনার জেলা নির্বাচন করুন',
  AppStringKey.hintAddress: 'আপনার ঠিকানা লিখুন',
  AppStringKey.hintPostCode: 'আপনার পোস্ট কোড লিখুন',
  AppStringKey.hintKG: 'Kg',
  AppStringKey.hintFeet: 'Feet',
  AppStringKey.hintInch: 'Inch',
  AppStringKey.hintLowest: 'Lowest',
  AppStringKey.hintHighest: 'Highest',
  AppStringKey.hintType: 'লিখুন',
  AppStringKey.hintSymptomChecker: 'এখানে আপনার লক্ষণ লিখুন বা বলুন!',
  AppStringKey.hintSpecialInstruction: 'এখানে অতিরিক্ত নির্দেশনা লিখুন',
  AppStringKey.hintSelectDay: 'দিন নির্বাচন করুন',
  AppStringKey.hintDiagnosedDiagnosis: 'নির্ণয় করা রোগ লিখুন',
  AppStringKey.hintAddDiagnosticsTests: 'ডায়াগনস্টিক টেস্ট লিখুন',
  AppStringKey.hintEnterYourComment: 'আপনার মন্তব্য লিখুন',
  AppStringKey.hintMoreFeatureSearch: 'সঠিক ডাক্তার, হাসপাতাল খুঁজুন',
  AppStringKey.hintWriteHere: 'এখানে লিখুন',
  AppStringKey.hintBrandGenericName: 'ব্র্যান্ড বা জেনেরিক নাম',
  AppStringKey.hintMedicineDosageType: 'ট্যাবলেট, ক্যাপসুল, আই ড্রপ etc...',
  AppStringKey.hintEnterPromoCode: 'প্রমো কোড লিখুন',
  AppStringKey.hintAmount: 'আপনার ইচ্ছার পরিমাণ লিখুন',
  AppStringKey.hintDiseaseAccordingPrescription: 'প্রেসক্রিপশন অনুযায়ী রোগ টাইপ করুন',
  AppStringKey.hintDoctorName: 'ডক্টর নাম টাইপ করুন',
  AppStringKey.hintVisitingDate: 'ক্যালেন্ডার খুলতে এখানে আলতো চাপুন',
  AppStringKey.hintTestName: 'পরীক্ষার নাম টাইপ করুন',
  AppStringKey.hintTestResult: 'টাইপ পরীক্ষার ফলাফল',
  AppStringKey.hintAdditionalDisease: 'আপনার রোগ প্রবেশ করুন',

// titles
  AppStringKey.titleOtpScreen: 'MedAi-তে স্বাগতম',
  AppStringKey.subTitleOtpScreen: 'আপনার বিস্তারিত লিখুন',
  AppStringKey.titleNotifications: 'বিজ্ঞপ্তি',
  AppStringKey.titleVirtualConsultancyScreen: 'দারুণ! আপনার ক্যালেন্ডার ভার্চুয়াল পরামর্শের জন্য সেট করা আছে!',
  AppStringKey.titlePhysicalConsultancyScreen: 'দারুণ! আপনার ক্যালেন্ডার শারীরিক পরামর্শের জন্য সেট করা আছে!',
  AppStringKey.titleSetYourSchedule: 'আপনার সময়সূচী সেট করুন',
  AppStringKey.titleSetYourScheduleDesc: 'আপনার ক্যালেন্ডারের \nবিস্তারিত সেটআপ করুন',
  AppStringKey.titleAddDegree: 'ডিগ্রী যোগ করুন',
  AppStringKey.titleEditDegree: 'ডিগ্রী এডিট করুন',
  AppStringKey.titleEditProfile: 'এডিট প্রোফাইল',
  AppStringKey.titleViewProfile: 'প্রোফাইল দেখুন',
  AppStringKey.titleAddProfile: 'প্রোফাইল যুক্ত করুন',
  AppStringKey.titleCreatePatient: 'রোগী তৈরি করুন',
  AppStringKey.titleMyWallet: 'আপনার উপার্জন এবং\nউত্তোলন',
  AppStringKey.titleConsultancyHistory: 'পরামর্শ ইতিহাস',
  AppStringKey.titleSeeWithdrawalMethods: 'এখানে আপনার প্রত্যাহার পদ্ধতি দেখুন!',
  AppStringKey.titleSetWithdrawalMethod: 'এখানে আপনার প্রত্যাহারের পদ্ধতি সেট করুন!',
  AppStringKey.titleSetWithdrawBalance: 'এখান থেকে আপনার ব্যালেন্স তুলে নিন!',
  AppStringKey.titleDescVitalHealth: 'সঠিক নির্দেশনার জন্য কিছু গুরুত্বপূর্ণ স্বাস্থ্য মেট্রিক্স প্রদান করুন',
  AppStringKey.titleDescSymptomChecker: 'দ্রুত এবং সঠিক নির্ণয়ের জন্য অনুগ্রহ করে আমাদের আপনার লক্ষণগুলি বলুন৷',
  AppStringKey.titleSelectSymptom: 'আপনার যদি এই লক্ষণগুলির কোনটি থাকে তবে নির্বাচন করুন',
  AppStringKey.titleDynamicSymptom:
      'আপনি প্রায় সেখানে! \nআমাদের আরও ভালোভাবে নির্ণয় করতে সাহায্য করার জন্য, আমরা আরও কিছু প্রশ্ন করতে যাচ্ছি',
  AppStringKey.titleVirtualDesc: 'দারুণ! আপনার ক্যালেন্ডার\nভার্চুয়াল পরামর্শের জন্য সেট করা আছে!',
  AppStringKey.titleSearchPatientProfile: 'রোগীর প্রোফাইল অনুসন্ধান করুন',
  AppStringKey.titlePatientList: 'রোগীর তালিকা',
  AppStringKey.titleSelectProfile: 'প্রোফাইল নির্বাচন করুন',
  AppStringKey.titleFollowUpDiagnosticStatus: 'আপনার ডায়াগনস্টিক তথ্য জমা দিন',

  AppStringKey.titleSearchHospital: 'বিস্তারিত জানার জন্য নীচের থেকে প্রতিষ্ঠান নির্বাচন করুন.',
  AppStringKey.titleSearchMedicine: 'বিস্তারিত জানার জন্য তালিকা থেকে ঔষধ নির্বাচন করুন',
  AppStringKey.titleConsultancyHistoryDetail: 'রোগীর ইতিহাস',
  AppStringKey.titleInstitution: 'প্রতিষ্ঠান',
  AppStringKey.titleDecision: 'সিদ্ধান্ত',
  AppStringKey.titlePrescribe: 'প্রেসক্রাইব করুন',
  AppStringKey.titleAddNewMedicine: 'নতুন ওষুধ যোগ করুন',
  AppStringKey.titleBreakSymptom: 'উপসর্গটি ভেঙে লিখুন',
  AppStringKey.titleFever: 'জ্বর',
  AppStringKey.titleCough: 'কাশি',
  AppStringKey.titlePain: 'ব্যথা',
  AppStringKey.titleReview: 'পুনঃমূল্যায়ন',
  AppStringKey.titleMedicineAppointment: 'বিশদ জন্য তালিকা থেকে ওষুধ চয়ন করুন',
  AppStringKey.titleAdviceAndNextFollowUpDate: 'পরামর্শ এবং পরবর্তী ফলো-আপ তারিখ',
  AppStringKey.titleUpdateVitalSignsHere: 'এখানে গুরুত্বপূর্ণ লক্ষণ আপডেট করুন!',
  AppStringKey.titlePaymentHistory: 'অর্থ প্রদান ইতিহাস',
  AppStringKey.titleOtherFamilyProfile: 'অন্যান্য/পারিবারিক প্রোফাইল',
  AppStringKey.titleCompleteYourHealthProfile: 'আপনার স্বাস্থ্য প্রোফাইল সম্পূর্ণ করুন',
  AppStringKey.titlePatientProfile: 'রোগী প্রোফাইল',
  AppStringKey.titleAvailableAppDoctor: 'উপলব্ধ অ্যাপ্লিকেশন ডাক্তার',
  AppStringKey.titleFollowUpVital: 'সঠিক দিকনির্দেশনার জন্য দয়া করে কিছু গুরুত্বপূর্ণ স্বাস্থ্য মেট্রিক সরবরাহ করুন',
  AppStringKey.titleFollowUpMedicineStatus: 'আপনার ওষুধ কোর্সের তথ্য জমা দিন',
  AppStringKey.titleConfirmBooking: 'আপনার বুকিং নিশ্চিত করুন',
  AppStringKey.titleReviewBooking: 'আপনার বুকিং পর্যালোচনা',
  AppStringKey.titlePtPreviousSymptoms: 'দয়া করে এমন কোনও লক্ষণগুলি সরিয়ে ফেলুন যা আর কার্যকর হয় না!',
  AppStringKey.titleAddYourPreviousCheckupRecord: 'আপনার পূর্ববর্তী চেকআপ রেকর্ড যুক্ত করুন',
  AppStringKey.titleViewYourPreviousCheckupRecord: 'আপনার পূর্ববর্তী চেকআপ রেকর্ড দেখুন',
  AppStringKey.titleEditYourPreviousCheckupRecord: 'আপনার পূর্ববর্তী চেকআপ রেকর্ড সম্পাদনা করুন',
  AppStringKey.titleUpdateYourPreviousCheckupRecord: 'আপনার পূর্ববর্তী চেকআপ রেকর্ড আপডেট করুন',
  AppStringKey.titleAddNewTestReport: 'নতুন পরীক্ষার প্রতিবেদন যুক্ত করুন',
  AppStringKey.titleBookAppointment: 'বুক \n অ্যাপয়েন্টমেন্ট',
  AppStringKey.titleInstantVideoConsult: 'তাত্ক্ষণিক \n ভিডিও পরামর্শ',
  AppStringKey.titlePatientExperience: 'রোগী \n অভিজ্ঞতা',

//validation
  AppStringKey.validationEnterYourDegree: 'ডিগ্রি নির্বাচন করুন',
  AppStringKey.validationEnterYourCountry: 'দেশ নির্বাচন করুন',
  AppStringKey.validationSelectAppointmentDuration: 'অ্যাপয়েন্টমেন্ট সময়কাল নির্বাচন করুন',
  AppStringKey.validationSelectGap: 'সান্ত্বনার মধ্যে ফাঁক নির্বাচন করুন',
  AppStringKey.validationSelectMethod: 'পদ্ধতি নির্বাচন করুন',
  AppStringKey.validationSelectBreathing: 'শ্বাস প্রশ্বাস নির্বাচন করুন',

//labels
  AppStringKey.labelTermsAndPrivacyStartPoint: 'চালিয়ে যাওয়ার মাধ্যমে, আপনি আমাদের সাথে সম্মত হন',
  AppStringKey.labelTermsOfService: 'সেবা পাবার শর্ত',
  AppStringKey.labelPrivacyAndPolicy: 'গোপনীয়তা নীতি',
  AppStringKey.labelGetOtp: 'ওটিপি পান',
  AppStringKey.labelVerify: 'যাচাই করুন',
  AppStringKey.labelResendOtp: 'ওটিপি পুনরায় প্রেরণ করুন',
  AppStringKey.labelDoNotShareOTP: 'আপনার ওটিপি ভাগ করবেন না',
  AppStringKey.labelHome: 'বাড়ি',
  AppStringKey.labelProfile: 'প্রোফাইল',
  AppStringKey.labelAppointment: 'অ্যাপয়েন্টমেন্ট',
  AppStringKey.labelSettings: 'সেটিংস',
  AppStringKey.labelMoreServices: 'আরও পরিষেবা',
  AppStringKey.labelMyPatients: 'আমার রোগী',
  AppStringKey.labelAddNewPatients: 'নতুন রোগী যুক্ত করুন',
  AppStringKey.labelMyWallet: 'আমার ওয়ালেট',
  AppStringKey.labelStatisticalSummary: 'পরিসংখ্যান সংক্ষিপ্তসার',
  AppStringKey.labelMyPatientsAndAppointment: 'আমার রোগী এবং অ্যাপয়েন্টমেন্ট',
  AppStringKey.labelTime: 'সময়',
  AppStringKey.labelDuration: 'সময়কাল',
  AppStringKey.labelGap: 'ফাঁক',
  AppStringKey.labelAvailability: 'উপস্থিতি',
  AppStringKey.labelAvailabilityHours: 'উপলব্ধ ঘন্টা:',
  AppStringKey.labelAvailabilityDays: 'উপলব্ধ দিন:',
  AppStringKey.labelPhysicalConsultancy: 'শারীরিক পরামর্শ',
  AppStringKey.labelVirtualConsultancy: 'ভার্চুয়াল পরামর্শ',
  AppStringKey.labelEditProfile: 'এডিট প্রোফাইল',
  AppStringKey.labelPaymentPerConsultancy: 'পরামর্শ প্রতি প্রদান',
  AppStringKey.labelPaymentPerFollowUpConsultancy: 'ফলো-আপ পরামর্শ প্রতি অর্থ প্রদান',
  AppStringKey.labelFollowupApplyWithin: 'ফলোআপ এর মধ্যে প্রয়োগ',
  AppStringKey.labelDegreeName: 'ডিগ্রি নাম',
  AppStringKey.labelInstitutionName: 'প্রতিষ্ঠানের নাম',
  AppStringKey.labelInstitution: 'প্রতিষ্ঠান',
  AppStringKey.labelAccreditationBody: 'স্বীকৃতি বডি',
  AppStringKey.labelCountry: 'দেশ',
  AppStringKey.labelFullName: 'পুরো নাম',
  AppStringKey.labelFirstName: 'নামের প্রথমাংশ',
  AppStringKey.labelLastName: 'নামের শেষাংশ',
  AppStringKey.labelPulse: 'স্পন্দন (ঐচ্ছিক)',
  AppStringKey.labelWeight: 'ওজন (ঐচ্ছিক)',
  AppStringKey.labelHeight: 'উচ্চতা (ঐচ্ছিক)',
  AppStringKey.labelBreathing: 'শ্বাস প্রশ্বাস (ঐচ্ছিক)',
  AppStringKey.labelBloodPressure: 'রক্তচাপ (ঐচ্ছিক)',
  AppStringKey.labelInch: 'ইঞ্চি (ঐচ্ছিক)',
  AppStringKey.labelRegisteredNo: 'নিবন্ধিত নম্বর',
  AppStringKey.labelRegisteredBody: 'নিবন্ধিত শরীর',
  AppStringKey.labelRegisteredYear: 'নিবন্ধিত বছর',
  AppStringKey.labelWorking: 'কাজ - সংস্থা / হাসপাতাল / ক্লিনিক',
  AppStringKey.labelConsolationDuration: 'সান্ত্বনা সময়কাল',
  AppStringKey.labelConsultationDuration: 'পরামর্শ সময়কাল',
  AppStringKey.labelGapBetweenConsolation: 'সান্ত্বনার মধ্যে ব্যবধান',
  AppStringKey.labelDivision: 'বিভাগ',
  AppStringKey.labelDistrict: 'জেলা',
  AppStringKey.labelAddress: 'ঠিকানা',
  AppStringKey.labelPostCode: 'পোস্ট কোড',
  AppStringKey.labelMobile: 'মোবাইল',
  AppStringKey.labelLanguages: 'ভাষা কথা বলে (ঐচ্ছিক)',
  AppStringKey.labelEmail: 'ইমেল',
  AppStringKey.labelSpecialization: 'বিশেষীকরণ',
  AppStringKey.labelDesignation: 'উপাধি',
  AppStringKey.labelAffiliation: 'অধিভুক্তি',
  AppStringKey.labelTraining: 'প্রশিক্ষণ',
  AppStringKey.labelTrainedIn: 'প্রশিক্ষিত',
  AppStringKey.labelAllDegrees: 'সমস্ত ডিগ্রি',
  AppStringKey.labelDegree: 'ডিগ্রি',
  AppStringKey.labelAddDegree: '+ ডিগ্রি যোগ করুন',
  AppStringKey.labelTotalEarning: 'মোট \n উপার্জন',
  AppStringKey.labelTotalWithdrawl: 'মোট \n উইথড্রোল',
  AppStringKey.labelTotalWithdrawal: 'মোট \n উইথড্রোল',
  AppStringKey.labelAvailableBalance: 'মোট \n ব্যালেন্স',
  AppStringKey.labelEarning: 'উপার্জন',
  AppStringKey.labelWithdrawals: 'উত্তোলন',
  AppStringKey.labelInformation: 'তথ্য',
  AppStringKey.labelReviews: 'পর্যালোচনা',
  AppStringKey.labelWithdrawls: 'প্রত্যাহার',
  AppStringKey.labelAlias: 'ওরফে',
  AppStringKey.labelMethodType: 'পদ্ধতির ধরণ',
  AppStringKey.labelAccountName: 'হিসাবের নাম',
  AppStringKey.labelAccountNo: 'হিসাব নাম্বার',
  AppStringKey.labelAccountNumber: 'হিসাব নাম্বার',
  AppStringKey.labelAmount: 'পরিমাণ',
  AppStringKey.labelMethodSaveAs: 'পদ্ধতি হিসাবে সংরক্ষণ করুন',
  AppStringKey.labelSetMethod: 'পদ্ধতি সেট করুন',
  AppStringKey.labelSelectMethod: 'পদ্ধতি নির্বাচন করুন',
  AppStringKey.labelMethodSelected: 'নির্বাচিত পদ্ধতি',
  AppStringKey.labelPayment: 'অর্থ প্রদান',
  AppStringKey.labelPaymentPerFollowUp: 'ফলোআপ প্রতি প্রদান',
  AppStringKey.labelFollowUpApplyWithin: 'এর মধ্যে প্রয়োগ করুন',
  AppStringKey.labelSearchGlobal: 'মেডিসিন, ব্লাড ব্যাংক, হাসপাতাল, অ্যাম্বুলেন্স',
  AppStringKey.labelSearchHospital: 'অনুসন্ধান হাসপাতাল',
  AppStringKey.labelSearchBloodBank: 'ব্লাড ব্যাংক অনুসন্ধান করুন',
  AppStringKey.labelSearchDiagnosticCenter: 'ডায়াগনস্টিক সেন্টার অনুসন্ধান করুন',
  AppStringKey.labelSearchTraumaCenter: 'ট্রমা সেন্টার অনুসন্ধান করুন',
  AppStringKey.labelServiceNeed: 'আপনার কোন পরিষেবা দরকার?',
  AppStringKey.labelChooseServicesFor: 'জন্য অতিরিক্ত পরিষেবা চয়ন করুন',
  AppStringKey.labelBrand: 'ব্র্যান্ড',
  AppStringKey.labelGeneric: 'জেনেরিক',
  AppStringKey.labelCompany: 'প্রতিষ্ঠান',
  AppStringKey.labelDesignAdministration: 'নকশা এবং প্রশাসন',
  AppStringKey.labelMedicineDetails: 'ওষুধের বিশদ',
  AppStringKey.labelNewConsultancy: 'নতুন কনসাল্টেন্সি',
  AppStringKey.labelExistingPatientConsultancy: 'বিদ্যমান রোগী পরামর্শ',
  AppStringKey.labelMyPatient: 'আমার রোগী',
  AppStringKey.labelCancelAppointment: 'অ্যাপয়েন্টমেন্ট বাতিল করুন',
  AppStringKey.labelCancelAllAppointment: 'সমস্ত অ্যাপয়েন্টমেন্ট বাতিল করুন',
  AppStringKey.labelSymptoms: 'লক্ষণ',
  AppStringKey.labelChiefComplaint: 'প্রধান অভিযোগ',
  AppStringKey.labelInAppVideoCall: 'অ্যাপ্লিকেশন ভিডিও কল',
  AppStringKey.labelClientNote: 'ক্লায়েন্ট নোট',
  AppStringKey.labelPreviousDecision: 'পূর্ববর্তী সিদ্ধান্ত',
  AppStringKey.labelPrescription: 'প্রেসক্রিপশন',
  AppStringKey.labelMedicineTiming: 'ওষুধের সময়',
  AppStringKey.labelMealInstruction: 'খাবারের নির্দেশনা',
  AppStringKey.labelSpecialInstruction: 'বিশেষ নির্দেশনা',
  AppStringKey.labelMedicineStatus: 'ওষুধের স্থিতি',
  AppStringKey.labelDiagnosticTestStatus: 'ডায়াগনস্টিক পরীক্ষার স্থিতি',
  AppStringKey.labelPreviousComplaintsAndSymptoms: 'পূর্ববর্তী অভিযোগ এবং লক্ষণ',
  AppStringKey.labelPreviousSymptoms: 'পূর্ববর্তী লক্ষণ',
  AppStringKey.labelNewSymptoms: 'নতুন লক্ষণ',
  AppStringKey.labelCurrentSymptoms: 'বর্তমান লক্ষণ',
  AppStringKey.labelNewComplaintsAndSymptoms: 'নতুন অভিযোগ ও লক্ষণ',
  AppStringKey.labelPreExistingConditions: 'পূর্বনির্ধারিত শর্ত',
  AppStringKey.labelRecommendedDiagnosticTests: 'প্রস্তাবিত ডায়াগনস্টিক পরীক্ষা',
  AppStringKey.labelBrandGenericName: 'ব্র্যান্ড/জেনেরিক নাম',
  AppStringKey.labelMedicineDosageType: 'ওষুধ/ডোজ প্রকার',
  AppStringKey.labelBeforeOrAfterMealInstruction: 'খাবারের নির্দেশের আগে/পরে',
  AppStringKey.labelContinuesMedicine: 'ওষুধ চালিয়ে যান',
  AppStringKey.labelSelectThis: 'এটি নির্বাচন করুন',
  AppStringKey.labelViewDetails: 'বিস্তারিত দেখুন',
  AppStringKey.labelDiagnosticsTests: 'ডায়াগনস্টিকস পরীক্ষা',
  AppStringKey.labelDiagnosticsList: 'ডায়াগনস্টিকস তালিকা',
  AppStringKey.labelDoctorAdvise: 'ডাক্তার পরামর্শ দিন',
  AppStringKey.labelNextFollowUp: 'পরবর্তী ফলোআপ',
  AppStringKey.labelAppointmentDataSubmit: 'অ্যাপয়েন্টমেন্ট ডেটা uc',
  AppStringKey.labelWithDrawRequestSubmit: 'আপনার অনুরোধটি uc',
  AppStringKey.labelUpdateSymptomsHere: 'এখানে লক্ষণগুলি আপডেট করুন!',
  AppStringKey.labelFollowUpUpdateSymptomsHere: 'আপনি যদি আরও লক্ষণগুলি এখানে যোগ করেন',
  AppStringKey.labelFollowUpUpdateExtraSymptomsHere: 'আপনার যদি এই লক্ষণগুলির কোনও থাকে তবে নির্বাচন করুন',
  AppStringKey.labelViewImageHere: 'চিত্রটি এখানে দেখুন',
  AppStringKey.labelScheduleSubmit: 'আপনার সময়সূচী রোগীর জন্য \n সফলভাবে সেট করা হয়েছে',
  AppStringKey.labelClinicalDecision: 'ক্লিনিকাল সিদ্ধান্ত',
  AppStringKey.labelDiagnosedDiagnosis: 'নির্ণয় নির্ণয়',
  AppStringKey.labelAddNewDiagnosticsTests: 'নতুন ডায়াগনস্টিকস পরীক্ষা যুক্ত করুন',
  AppStringKey.labelGoBackAlert: 'আপনি যদি ফিরে যান তবে ডেটা সরানো হবে \n আপনি ফিরে যেতে চান?',
  AppStringKey.labelDiseases: 'রোগ',
  AppStringKey.labelCallingMethods: 'কলিং পদ্ধতি',
  AppStringKey.labelBreathingRate: 'শ্বাস প্রশ্বাসের হার',
  AppStringKey.labelVisitFollowUpHistory: 'দেখুন এবং ফলো-আপ ইতিহাস',
  AppStringKey.labelDownloadPrescription: 'প্রেসক্রিপশন ডাউনলোড করুন',
  AppStringKey.labelBookFollowUp: 'বই ফলোআপ',
  AppStringKey.labelResultSummary: 'ফলাফল সংক্ষিপ্তসার',
  AppStringKey.labelTimeOfDay: 'দিনের সময় নির্বাচন করুন',
  AppStringKey.labelHourDifferenceDay: 'ঘন্টা পার্থক্য/ দিন',
  AppStringKey.labelRegisteredInNo: 'নিবন্ধিত। না',
  AppStringKey.labelDegrees: 'ডিগ্রী',
  AppStringKey.labelAffiliatedWith: 'অনুমোদিত',
  AppStringKey.labelWorkingAt: 'কর্মরত',
  AppStringKey.labelAccountType: 'অ্যাকাউন্ট ধরন',
  AppStringKey.labelMyAppointments: 'আমার অ্যাপয়েন্টমেন্ট',
  AppStringKey.labelYourCheckupRecords: 'আপনার চেকআপ রেকর্ড',
  AppStringKey.labelDiseaseName: 'রোগের নাম',
  AppStringKey.labelAddNewRecord: 'নতুন রেকর্ড যুক্ত করুন',
  AppStringKey.labelPainBecomeExtreme: 'কখন ব্যথা চরম হয়ে যায়',
  AppStringKey.labelDoctorsName: 'ডাক্তারের নাম',
  AppStringKey.labelDiagnostics: 'কারণ নির্ণয়',
  AppStringKey.labelPrescriptionImages: 'প্রেসক্রিপশন ইমেজ',
  AppStringKey.labelChooseAnOption: 'একটি বিকল্প নির্বাচন করুন',
  AppStringKey.labelTakePhoto: 'একটি ছবি তুলুন',
  AppStringKey.labelChooseFromGallery: 'গ্যালারী থেকে চয়ন করুন',
  AppStringKey.labelTestName: 'পরীক্ষার নাম',
  AppStringKey.labelTestResult: 'পরীক্ষার ফলাফল',
  AppStringKey.labelTestImages: 'পরীক্ষার চিত্র',
  AppStringKey.labelAlreadyAdded: 'এটা \'s already added!',
  AppStringKey.labelPressAgainToExit: 'প্রস্থান করতে আবার টিপুন!',
  AppStringKey.labelExit: 'প্রস্থান',
  AppStringKey.labelConfused: 'বিভ্রান্ত?',
  AppStringKey.labelFindNow: 'এখন খুঁজুন',
  AppStringKey.labelFindDoctor: 'আসুন আমরা আপনাকে সঠিক ডাক্তার খুঁজে পাই',
  AppStringKey.labelNotFeelingTooWell: 'খুব ভাল লাগছে না?',
  AppStringKey.subLabelNotFeelingTooWell: 'শীর্ষ বিশেষজ্ঞদের সাথে সাধারণ লক্ষণগুলি চিকিত্সা করুন',
  AppStringKey.labelAdditionalDisease: 'অতিরিক্ত রোগ',

  AppStringKey.labelProfileAge: 'বয়স',
  AppStringKey.labelProfileGender: 'লিঙ্গ',
  AppStringKey.labelProfileRegisterNo: 'নিবন্ধিত নম্বর',
  AppStringKey.labelProfileEducation: 'শিক্ষা',
  AppStringKey.labelProfileAffiliated: 'সম্বন্ধযুক্ত',
  AppStringKey.labelProfileWorking: 'কাজ',
  AppStringKey.labelProfileTrainedIn: 'প্রশিক্ষিত',
  AppStringKey.labelType: 'প্রকার',
  AppStringKey.labelContact: 'যোগাযোগ',
  AppStringKey.labelBloodGroup: 'রক্তের গ্রুপ',
  AppStringKey.labelArea: 'অঞ্চল',
  AppStringKey.labelReferralCodeCreatePatient: 'রেফারেল কোড',
  AppStringKey.labelMaritalStatus: 'বৈবাহিক অবস্থা',
  AppStringKey.labelMaritalStatusWithOptional: 'বৈবাহিক অবস্থা (ঐচ্ছিক)',
  AppStringKey.labelEducation: 'শিক্ষা',
  AppStringKey.labelEducationWithOptional: 'শিক্ষা (ঐচ্ছিক)',
  AppStringKey.labelOccupation: 'পেশা',
  AppStringKey.labelOccupationWithOptional: 'পেশা (ঐচ্ছিক)',
  AppStringKey.labelEthnicity: 'জাতিগততা',
  AppStringKey.labelEthnicityWithOptional: 'জাতিগততা (ঐচ্ছিক)',
  AppStringKey.labelMonthlyEarning: 'মাসিক উপার্জন',
  AppStringKey.labelMonthlyEarningWithOptional: 'মাসিক উপার্জন (ঐচ্ছিক)',
  AppStringKey.labelTemperatureWithOptional: 'তাপমাত্রা (ঐচ্ছিক)',
  AppStringKey.labelTemperature: 'তাপমাত্রা',
  AppStringKey.labelTemperatureOptinal: 'তাপমাত্রা (ঐচ্ছিক)',
  AppStringKey.labelTryAgain: 'আবার চেষ্টা কর',
  AppStringKey.labelCheckInternet: 'আপনার ইন্টারনেট \n সংযোগ পরীক্ষা করুন এবং আবার চেষ্টা করুন',
  AppStringKey.labelNoInternetConnection: 'কোনও ইন্টারনেট সংযোগ নেই',
  AppStringKey.labelRemoveDiagnosticTestPart1: 'আপনি কি অপসারণ করতে চান?',
  AppStringKey.labelRemoveDiagnosticTestPart2: 'এই পরীক্ষা থেকে?',
  AppStringKey.labelAddNewProfile: 'নতুন প্রোফাইল যুক্ত করুন',
  AppStringKey.labelRegularMedicine: 'নিয়মিত ওষুধ',
  AppStringKey.labelAllergy: 'অ্যালার্জি',
  AppStringKey.labelOtherComment: 'অন্যান্য মন্তব্য',
  AppStringKey.labelHeight2: 'উচ্চতা',
  AppStringKey.labelBreathingsRate: 'শ্বাস প্রশ্বাসের হার',
  AppStringKey.labelWidth: 'প্রস্থ',
  AppStringKey.labelCurrentNewSymptoms: 'আপনি যদি আরও লক্ষণগুলি এখানে যোগ করেন।',
  AppStringKey.subLabelCurrentNewSymptoms: 'আপনার যদি এই লক্ষণগুলির কোনও থাকে তবে নির্বাচন করুন',
  AppStringKey.labelLevelOfPain: 'ব্যথা স্তর',
  AppStringKey.labelPainRecent: 'সাম্প্রতিক আঘাত থেকে এই ব্যথা কি?',
  AppStringKey.labelWhere: 'কোথায়',

// setting option label
  AppStringKey.labelMyProfile: 'আমার প্রোফাইল',
  AppStringKey.labelSetYourSchedule: 'আপনার সময়সূচী সেট করুন',
  AppStringKey.labelChangeAvailability: 'প্রাপ্যতা পরিবর্তন করুন',
  AppStringKey.labelChangeLanguage: 'ভাষা পরিবর্তন করুন',
  AppStringKey.labelReferralCode: 'রেফারেল কোড: DRS12S',
  AppStringKey.labelReferralCodeWithoutValue: 'রেফারেল কোড',
  AppStringKey.labelShareApp: 'অ্যাপ্লিকেশন শেয়ার করা',
  AppStringKey.labelDeleteAcc: 'একটি অ্যাকাউন্ট মুছুন',
  AppStringKey.labelLogout: 'প্রস্থান',
  AppStringKey.labelRemarks: 'মন্তব্য',
  AppStringKey.labelPaid: 'প্রদত্ত',
  AppStringKey.labelPending: 'বিচারাধীন',
  AppStringKey.labelRefunded: 'ফেরত দেওয়া',
  AppStringKey.labelReferenceID: 'রেফারেন্স আইডি',
  AppStringKey.labelTrxID: 'ট্রেক্সিড',
  AppStringKey.labelAppointmentID: 'অ্যাপয়েন্টমেন্ট আইডি',
  AppStringKey.labelDoctorName: 'ডাক্তারের নাম',
  AppStringKey.labelOtherProfile: 'অন্যান্য প্রোফাইল',
  AppStringKey.labelPaymentHistory: 'পেমেন্ট ইতিহাস',
  AppStringKey.labelFutureAppointment: 'ভবিষ্যত \n অ্যাপয়েন্টমেন্ট',
  AppStringKey.labelCompletedAppointment: 'সম্পূর্ণ \n অ্যাপয়েন্টমেন্ট',
  AppStringKey.labelVisitingDate: 'দেখার তারিখ',
  AppStringKey.labelEntryDate: 'প্রবেশের তারিখ',

//app bar string
  AppStringKey.titleSettings: 'সেটিংস',
  AppStringKey.titleMyProfile: 'আমার প্রোফাইল',
  AppStringKey.titleMoreFeatures: 'আরো বৈশিষ্ট্য',

//dialog string
  AppStringKey.deleteDesc: 'আপনার অ্যাকাউন্ট স্থায়ীভাবে মুছে ফেলা হবে! \n আমরা আপনার ডেটা ফিরিয়ে আনতে সক্ষম হব না।',
  AppStringKey.cancelSingleAppointmentDesc: 'আপনি কি এই অ্যাপয়েন্টমেন্ট বাতিল করতে চান?',
  AppStringKey.cancelAllAppointmentDesc: 'আপনি কি এই সমস্ত অ্যাপয়েন্টমেন্ট বাতিল করতে চান?',
  AppStringKey.deleteWarning: 'সতর্কতা!',
  AppStringKey.labelWarning: 'সতর্কতা!',
  AppStringKey.logoutTitle: 'প্রস্থান',
  AppStringKey.deleteYes: 'হ্যাঁ',
  AppStringKey.deleteNo: 'না',
  AppStringKey.logoutDesc: 'আপনি লগ আউট করতে চান?',
  AppStringKey.deleteProfileDesc: 'আপনি কি নিশ্চিত যে আপনি এই প্রোফাইলটি মুছবেন?',

//services
  AppStringKey.strServiceCCU: 'সিসিইউ',
  AppStringKey.strServiceICU: 'আইসিইউ',
  AppStringKey.strServiceAmbulanceService: 'অ্যাম্বুলেন্স পরিষেবা',
  AppStringKey.strServiceDialysis: 'ডায়ালাইসিস',
  AppStringKey.strServiceDialysisCenter: 'ডায়ালাইসিস সেন্টার',
  AppStringKey.strServiceAmbulance: 'অ্যাম্বুলেন্স',
  AppStringKey.strServiceBloodBank: 'ব্লাড ব্যাংক',
  AppStringKey.strServiceDiagnosticCenter: 'ডায়াগনস্টিক সেন্টার',
  AppStringKey.strServiceGeneralHospital: 'সাদারন হসপিটাল',
  AppStringKey.strServiceMedicine: 'ওষুধ',
  AppStringKey.strServiceSpecializedClinic: 'বিশেষায়িত ক্লিনিক',
  AppStringKey.strServiceSpecializedHospital: 'বিশেষ হাসপাতাল',
  AppStringKey.strServiceTraumaCenter: 'ট্রমা সেন্টার',

// symptoms
  AppStringKey.strSymptomBloating: 'ফোলাভাব',
  AppStringKey.strSymptomCough: 'কাশি',
  AppStringKey.strSymptomFever: 'জ্বর',
  AppStringKey.strSymptomNeusea: 'নিউউসিয়া বা বমি বমিভাব',
  AppStringKey.strSymptomUnexplained: 'অব্যক্ত ক্লান্তি',
  AppStringKey.strSymptomPain: 'ব্যথা',
  AppStringKey.strSymptomDiarrhoea: 'ডায়রিয়া',
  AppStringKey.strSymptomRunny: 'সর্দি',
  AppStringKey.strSymptomSore: 'গলা ব্যথা',
  AppStringKey.strSymptomDizziness: 'মাথা ঘোরা',
  AppStringKey.strSymptomNightSweat: 'রাতের ঘাম',
  AppStringKey.strSymptomCoughingBlood: 'রক্ত কাশি',
  AppStringKey.strSymptomPersistent: 'ক্রমাগত কাশি',

//dynamics
  AppStringKey.intentArgument: 'Intent',
  AppStringKey.forwardArgument: 'forwardIntent',
  AppStringKey.backwardArgument: 'backwardIntent',

//SnackBar
  AppStringKey.cantBeEmpty: 'This Field Can Not Be Empty!',
  AppStringKey.chooseFromDropDown: 'Please Select Option From Dropdown!',

  AppStringKey.infoTableName: "info_database",
  AppStringKey.verifyDoc: "যাচাইকৃত ডাক্তার",
  AppStringKey.appVersion: "অ্যাপ সংস্করণ - 1.0.0",
  AppStringKey.hint7days: "7 দিন",
  AppStringKey.yearsOld: "বছর পুরনো",
  AppStringKey.wantToRemovePerscription: "আপনি প্রেসক্রিপশন থেকে Napasin অপসারণ করতে চান?",
  AppStringKey.basedOnSymtoms: "রোগীর উপসর্গের উপর ভিত্তি করে রোগীর পেটের আলসার সংক্রান্ত সমস্যা থাকতে পারে",
  AppStringKey.noMedicineAdded: "এখনো কোনো ওষুধ যোগ করা হয়নি",
  AppStringKey.visit: "ভিজিট করুন",
  AppStringKey.chiefCompaint: "প্রধান অভিযোগ",
  AppStringKey.only5ImagesAdded: "মাত্র 5টি ছবি যোগ করা যাবে!",
  AppStringKey.dontKnow: "জানি না",
  AppStringKey.strEnterYourMobile: "আপনার মোবাইল নম্বর লিখুন",
  AppStringKey.strEnterYourEmail: "আপনার ইমেল ঠিকানা লিখুন দয়া করে",
  AppStringKey.hintEnterYourMobile: "আপনার মোবাইল নাম্বার প্রবেশ করুন",
  AppStringKey.hintEnterYourEmail: "তুমার ইমেইল প্রবেশ করাও",
  AppStringKey.strEmail: "ইমেইল",
  AppStringKey.strMobile: "মুঠোফোন",
  AppStringKey.strVisit: "পরিদর্শন",
  AppStringKey.strFollowUp: "অনুসরণ করুন",
  AppStringKey.strSelect: "নির্বাচন করুন",
  AppStringKey.strConsultADoctor: "একজন ডাক্তারের সাথে পরামর্শ করুন",
  AppStringKey.strPrev: "পূর্ববর্তী",
  AppStringKey.strBookNow: "এখন বুক করুন",
  AppStringKey.strOkay: "ঠিক আছে",
  AppStringKey.strEnterThePain: "একটি সাম্প্রতিক আঘাত থেকে ব্যথা প্রবেশ করুন",
  AppStringKey.strIHaveRead: "আমি পড়েছি এবং এর সাথে একমত",
  AppStringKey.strOpenInGoogleMap: "গুগল ম্যাপে খুলুন",
  AppStringKey.strAreYouSureDelete: "আপনি কি এই রেকর্ডটি মুছতে চান?",
  AppStringKey.strTitleInternetConnection: 'ইন্টারনেট সংযোগ নেই',
  AppStringKey.validationEnterFullName: 'আপনার সম্পূর্ণ নাম লিখুন',
  AppStringKey.validationSelectCountry: 'দেশ নির্বাচন করুন',
  AppStringKey.validationSelectDivision: 'অনুগ্রহ করে বিভাগ নির্বাচন করুন',
  AppStringKey.validationSelectDistrict: 'অনুগ্রহ করে জেলা নির্বাচন করুন',
  AppStringKey.validationEnterAddress: 'আপনার ঠিকানা লিখুন',
  AppStringKey.validationEnterPostCode: 'আপনার পোস্ট কোড লিখুন',
  AppStringKey.validationEnterMobile: 'আপনার মোবাইল নম্বর লিখুন',
  AppStringKey.validationSelectGender: 'আপনার লিঙ্গ নির্বাচন করুন',
  AppStringKey.validationSelectBloodGroop: 'আপনার রক্তের গ্রুপ নির্বাচন করুন',
  AppStringKey.validationSelectArea: 'আপনার এলাকা নির্বাচন করুন',
  AppStringKey.validationSelectLanguage: 'আপনার ভাষা নির্বাচন করুন',
  AppStringKey.validationSelectMaritalStatus: 'আপনার বৈবাহিক অবস্থা নির্বাচন করুন',
  AppStringKey.validationSelectEducation: 'আপনার শিক্ষা নির্বাচন করুন',
  AppStringKey.validationSelectOccupation: 'আপনার পেশা নির্বাচন করুন',
  AppStringKey.validationSelectEthnicity: 'আপনার জাতিগত নির্বাচন করুন',
  AppStringKey.validationSelectMonthlyEarning: 'আপনার মাসিক উপার্জন নির্বাচন করুন',
  AppStringKey.validationEnterAge: 'আপনার বয়স লিখুন',
  AppStringKey.validationEnterReferralCode: 'আপনার রেফারেল কোড লিখুন',
  AppStringKey.validationUploadProfilePicture: 'প্রোফাইল ছবি আপলোড করুন',
  AppStringKey.strLabelFileDownloadedSuccessfullySavedIn: 'ফাইল সফলভাবে ডাউনলোড করা হয়েছে এবং সংরক্ষিত হয়েছে৷',
  AppStringKey.strLabelPermissionRequired: 'অনুমতি প্রয়োজন',
  AppStringKey.strLabelCancel: 'বাতিল',
  AppStringKey.strSettings: 'সেটিংস',
  AppStringKey.strPleaseSelectOption: 'বিকল্প নির্বাচন করুন',
  AppStringKey.strImageUploadSuccessfully: 'ছবি আপলোড সফলভাবে',
  AppStringKey.strImageDeleteSuccessfully: 'ছবি সফলভাবে মুছে ফেলা হয়েছে',
  AppStringKey.strTimeSlotNotAvailable: 'কোন টাইম স্লট পাওয়া যায় না',
  AppStringKey.strPleaseEnterPromoCode: 'প্রচার কোড লিখুন দয়া করে',
  AppStringKey.strVideoCallTxt: 'ভিডিও কল',
  AppStringKey.strVoiceCallTxt: 'ভয়েস কল',
  AppStringKey.strPleaseRecordYourVoice: 'আপনার লক্ষণ রেকর্ড করুন এবং সাবমিট টিপুন',
};
