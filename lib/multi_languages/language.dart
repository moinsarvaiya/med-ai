import 'package:get/get_navigation/src/root/internacionalization.dart';

import 'bn_BN.dart';
import 'en_EN.dart';

class Languages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_EN': en_EN,
        'bn_BN': bn_BN,
      };
}
