import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

import 'app.dart';

void main() async {
  await GetStorage.init("login_preferences");
  await GetStorage.init("visit_preferences");
  await GetStorage.init("followup_preferences");
  await GetStorage.init("doctor_preferences");
  await GetStorage.init("patient_preferences");
  runApp(const MyApp());
}
