import 'dart:convert';
import 'dart:developer' as devLog;

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/utils/app_preference/login_preferences.dart';

class ApiCallObject {
  late http.Response response;
  final String tag = "API-Interface ::::";
  final String _bearerToken = LoginPreferences().sharedPrefRead(StorageKeys.token) ?? "";

  Future<http.Response> getData(String endpoint, [bool token = true]) async {
    if (kDebugMode) {
      devLog.log("Endpoint: $endpoint", name: tag);
    }
    try {
      response = await http.get(
        Uri.parse(endpoint),
        headers: token == false ? _setRegularHeaders() : _setAuthenticationHeaders(_bearerToken),
      );
      if (response.statusCode == 200) {
        if (kDebugMode) {
          devLog.log("ResponseBody: ${jsonDecode(response.body)}", name: tag);
        }
        return response;
      } else {
        throw response.body.toString();
      }
    } on http.ClientException catch (error) {
      throw Exception(error.toString());
    }
  }

  Future<http.Response> postData(String endpoint, Object object, [bool token = true]) async {
    if (kDebugMode) {
      devLog.log("EndPoint === $endpoint", name: tag);
      devLog.log("RequestBody: ${jsonEncode(object)}", name: tag);
    }
    try {
      response = await http.post(
        Uri.parse(endpoint),
        body: jsonEncode(object),
        headers: token == false ? _setRegularHeaders() : _setAuthenticationHeaders(_bearerToken),
      );
      if (response.statusCode == 200) {
        if (kDebugMode) {
          devLog.log("KDebugModeResponseBody: ${jsonDecode(response.body)}", name: tag);
          devLog.log("EndPoint: $endpoint", name: tag);
        }
        return response;
      } else {
        devLog.log("ResponseBody: ${jsonDecode(response.body)}", name: tag);
        return response;
      }
    } on http.ClientException catch (error) {
      devLog.log("HTTPClientException: $error", name: tag);
      throw Exception(error.toString());
    }
  }

  _setRegularHeaders() => {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      };

  _setAuthenticationHeaders(token) => {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };
}
