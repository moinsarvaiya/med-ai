class ApiEndpoints {
  // static const BASE_URL = "";
  static const demoBaseUrl = "";

  static const baseUrl = demoBaseUrl;

  static const urlVerifyToken = "$baseUrl/accounts/token/verify";
  static const urlContinuePhone = "$baseUrl/accounts/user/continue_with_phone";
  static const urlContinueEmail = "$baseUrl/accounts/user/continue_with_email";

  ///accounts/verify_otp
  static const urlVerifyOtp = "$baseUrl/accounts/verify_otp";
  static const urlResendOtp = "$baseUrl/accounts/user/send_otp";

  //doctor profile
  static const urlGetDoctorList = "$baseUrl/all_user/get_doctor_list";
  static const urlGetProfileImage = "$baseUrl/all_user/get_profile_picture";
  static const urlGetDoctorProfile = "$baseUrl/doctors/view_profile";
  static const urlDoctorCreateProfile = "$baseUrl/doctors/profile_registration";
  static const urlDoctorUpdateProfile = "$baseUrl/doctors/update_profile";
  static const urlDeleteUserAccount = "$baseUrl/accounts/user/delete_account";


  //Cancellation API
  static const urlDoctorCancelAllFollowupConsultancy = "$baseUrl/all_user/cancellation/cancel_all_follow_up_consultancy";
  static const urlDoctorCancelAllNewConsultancy = "$baseUrl/all_user/cancellation/cancel_all_new_consultancy";
  static const urlDoctorCancelOneAppointment = "$baseUrl/all_user/cancellation/cancel_one_appointment";


  //Other Services
  static const urlGetSymptomSuggestions = "$baseUrl/all_user/symptoms/get_symptom_suggestions";
  static const urlGetDiagnosticTestList = "$baseUrl/all_user/get_diagnostic_test_list";
  static const urlOtherServicesDetails = "$baseUrl/all_user/get_other_services_details";
  static const urlAdviseNextDate = "$baseUrl/all_user/consultation/advice_and_next_consultation_date";

  //Doctor Appointments
  static const urlDoctorGetUpcomingAppointments = "$baseUrl/doctors/get_upcoming_appointments";

  //Consultation
  static const urlGetConsultationDetails = "$baseUrl/patients/consultation_details";

  static const mobileNo = "01122334455";
  static const consultancyType = "1";

  // Doctor Visit Consultancy
  static const urlDoctorGetVisitSoap = "$baseUrl/doctors/visit/get_soap";
  static const urlGetDiseaseList = "$baseUrl/all_user/get_disease_list";
  static const urlVisitDoctorDecision = "$baseUrl/all_user/visit/hospital_doctor_info";
  static const urlVisitAddPrescribeMedicine = "$baseUrl/all_user/visit/add_prescribed_medicine";
  static const urlVisitAddPrescribeDiagnostics = "$baseUrl/all_user/visit/add_prescribed_diagnosis";
  static const urlVisitStatusUpdate = "$baseUrl/doctors/visit/update_consultation_status";
  static const urlGetDrugList = "$baseUrl/all_user/get_drug_list";
  static const urlGetSingleDrugTypeList = "$baseUrl/all_user/get_drug_data";
  static const urlGetDrugDetails = "$baseUrl/all_user/get_drug_data_details";
}
