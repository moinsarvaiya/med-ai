import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:med_ai/routes/route_paths.dart';

import 'multi_languages/language.dart';
import 'routes/routes.dart';
import 'utils/theme/app_theme.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with AppThemeMixin {
  /// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.white, // navigation bar color
        systemNavigationBarIconBrightness: Brightness.dark,
        statusBarColor: Colors.white, // status bar color
        statusBarIconBrightness: Brightness.dark));
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return GetMaterialApp(
      translations: Languages(),
      locale: Get.deviceLocale,
      fallbackLocale: const Locale('en', 'US'),
      themeMode: ThemeMode.light,
      title: AppStringKey.appName.tr,
      theme: appTheme(context),
      debugShowCheckedModeBanner: false,
      getPages: Routes.pages,
      initialRoute: Platform.isIOS ? RoutePaths.ONBOARDING : RoutePaths.SPLASH,
      defaultTransition: Transition.native,
    );
  }
}
