import 'package:get/get.dart';
import 'package:med_ai/screens/common/no_internet/no_internet_controller.dart';
import 'package:med_ai/screens/common/onboarding/onboarding_controller.dart';
import 'package:med_ai/screens/common/otp_verification/otp_verification_controller.dart';
import 'package:med_ai/screens/common/select_user/select_user_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_add_new_medicine/dr_add_new_medicine_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_appointment_medicine_details/dr_appointment_medicine_details_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_appointment_medicine_list/dr_appointment_medicine_list_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_clinical_decision/dr_clinical_decision_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_diagnostics_test_images/dr_diagnostics_test_images_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_follow_up_complete_appointment/dr_follow_up_complete_appointment_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_follow_up_date/dr_follow_up_date_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_followup_appointment/dr_followup_appointment_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_followup_update_symptoms/dr_followup_update_symptoms_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_prescribe_medicine/dr_prescribe_medicine_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_update_vital_signs/dr_update_vital_signs_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_visit_appointment/dr_visit_appointment_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_visit_complete_appointment/dr_visit_complete_appointment_controller.dart';
import 'package:med_ai/screens/doctor/dr_appointments/dr_visit_update_diagnostic_test/dr_visit_update_diagnostic_test_controller.dart';
import 'package:med_ai/screens/doctor/dr_consultancy_history_detail/dr_consultancy_history_detail_controller.dart';
import 'package:med_ai/screens/doctor/dr_create_patient_flow/dr_additional_comments/dr_additional_comment_controller.dart';
import 'package:med_ai/screens/doctor/dr_create_patient_flow/dr_dynamic_symptom/dr_dynamic_symptom_controller.dart';
import 'package:med_ai/screens/doctor/dr_existing_consultancy_flow/dr_patient_details/dr_patient_detail_controller.dart';
import 'package:med_ai/screens/doctor/dr_existing_consultancy_flow/dr_patient_list/dr_patient_list_controller.dart';
import 'package:med_ai/screens/doctor/dr_home/dr_home_controller.dart';
import 'package:med_ai/screens/doctor/dr_notification/dr_notification_controller.dart';
import 'package:med_ai/screens/doctor/dr_number_email_verification_flow/dr_otp_verification/dr_otp_verification_controller.dart';

import '../screens/common/global_search_flow/global_search/global_search_controller.dart';
import '../screens/common/global_search_flow/search_blood_bank/search_blood_bank_controller.dart';
import '../screens/common/global_search_flow/search_diagnostic_center/search_diagnostic_center_controller.dart';
import '../screens/common/global_search_flow/search_filter_service/search_filter_service_controller.dart';
import '../screens/common/global_search_flow/search_hospital_list/search_hospital_list_controller.dart';
import '../screens/common/global_search_flow/search_medicine_details/search_medicine_details_controller.dart';
import '../screens/common/global_search_flow/search_medicine_list/search_medicine_list_controller.dart';
import '../screens/common/global_search_flow/search_trauma_center/search_trauma_center_controller.dart';
import '../screens/common/login/login_controller.dart';
import '../screens/doctor/dr_appointments/dr_appointment_data_submitted/dr_appointment_data_submitted_controller.dart';
import '../screens/doctor/dr_appointments/dr_followup_identified_disease/dr_followup_identified_disease_controller.dart';
import '../screens/doctor/dr_appointments/dr_followup_prescribe_medicine/dr_followup_prescribe_medicine_controller.dart';
import '../screens/doctor/dr_appointments/dr_followup_update_diagnostics_test/dr_followup_update_diagnostics_test_controller.dart';
import '../screens/doctor/dr_appointments/dr_identified_disease/dr_identified_disease_controller.dart';
import '../screens/doctor/dr_appointments/dr_my_patient/dr_my_patient_controller.dart';
import '../screens/doctor/dr_appointments/dr_new_consultancy/dr_new_consultancy_controller.dart';
import '../screens/doctor/dr_appointments/dr_visit_next_date/dr_visit_next_date_controller.dart';
import '../screens/doctor/dr_appointments/dr_visit_update_symptoms/dr_visit_update_symptoms_controller.dart';
import '../screens/doctor/dr_bottom_navigation/dr_bottom_navigation_controller.dart';
import '../screens/doctor/dr_consultancy_history/dr_consultancy_history_controller.dart';
import '../screens/doctor/dr_create_patient_flow/dr_create_patient/dr_create_patient_controller.dart';
import '../screens/doctor/dr_create_patient_flow/dr_patient_reviews/dr_patient_review_controller.dart';
import '../screens/doctor/dr_create_patient_flow/dr_special_symptom/dr_special_symptom_controller.dart';
import '../screens/doctor/dr_create_patient_flow/dr_symptom_checker/dr_symptom_checker_controller.dart';
import '../screens/doctor/dr_create_patient_flow/dr_symptom_checker_step_2/dr_symptom_checker_step_2_controller.dart';
import '../screens/doctor/dr_create_patient_flow/dr_vital_health_metrics/dr_vital_health_metrics_controller.dart';
import '../screens/doctor/dr_number_email_verification_flow/dr_verify_email_number/dr_verify_email_number_controller.dart';
import '../screens/doctor/dr_schedule_flow/dr_physical_consultancy_detail/dr_physical_consultancy_detail_controller.dart';
import '../screens/doctor/dr_schedule_flow/dr_schedule_submitted/dr_schedule_submitted_controller.dart';
import '../screens/doctor/dr_schedule_flow/dr_set_your_calendar_physical/dr_set_your_calendar_physical_controller.dart';
import '../screens/doctor/dr_schedule_flow/dr_set_your_calendar_virtual/dr_set_your_calendar_virtual_controller.dart';
import '../screens/doctor/dr_schedule_flow/dr_set_your_schedule/dr_set_your_schedule_controller.dart';
import '../screens/doctor/dr_schedule_flow/dr_virtual_consultancy/dr_virtual_consultancy_controller.dart';
import '../screens/doctor/dr_setting_flow/dr_add_degree/dr_add_degree_controller.dart';
import '../screens/doctor/dr_setting_flow/dr_edit_profile/dr_edit_profile_controller.dart';
import '../screens/doctor/dr_setting_flow/dr_profile/dr_profile_controller.dart';
import '../screens/doctor/dr_setting_flow/dr_settings/dr_settings_controller.dart';
import '../screens/doctor/dr_wallet_flow/dr_my_wallet/dr_my_wallet_controller.dart';
import '../screens/doctor/dr_wallet_flow/dr_see_withdrawal_methods/dr_see_withdrawal_methods_controller.dart';
import '../screens/doctor/dr_wallet_flow/dr_set_withdrawal_method/dr_set_withdrawal_method_controller.dart';
import '../screens/doctor/dr_wallet_flow/dr_withdraw_balance/dr_withdraw_balance_controller.dart';
import '../screens/doctor/dr_wallet_flow/dr_withdraw_request_submitted/dr_withdraw_request_submitted_controller.dart';

class LoginBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginController>(() => LoginController());
  }
}

class OnBoardingBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OnBoardingController>(() => OnBoardingController());
  }
}

class SelectUserBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SelectUserController>(() => SelectUserController());
  }
}

class OtpVerificationBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OtpVerificationController>(() => OtpVerificationController());
  }
}

class DrHomeBindings extends Bindings {
  @override
  void dependencies() {
    //Get.lazyPut<DrHomeController>(() => DrHomeController());
    Get.put(DrHomeController());
  }
}

class DrBottomNavigationBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrBottomNavigationController>(() => DrBottomNavigationController());
    Get.lazyPut<DrHomeController>(() => DrHomeController());
    Get.lazyPut<DrNewConsultancyController>(() => DrNewConsultancyController());
    Get.lazyPut<DrSettingsController>(() => DrSettingsController());
    Get.lazyPut<DrMyWalletController>(() => DrMyWalletController());
  }
}

class DrProfileBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrProfileController>(() => DrProfileController());
  }
}

class DrSettingsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrSettingsController>(() => DrSettingsController());
  }
}

class DrMyWalletBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrMyWalletController>(() => DrMyWalletController());
  }
}

class DrNotificationBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrNotificationController>(() => DrNotificationController());
  }
}

class DrVirtualConsultancyBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrVirtualConsultancyController>(() => DrVirtualConsultancyController());
  }
}

class DrSetYourScheduleBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrSetYourScheduleController>(() => DrSetYourScheduleController());
  }
}

class DrSetYourCalendarVirtualBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrSetYourCalendarVirtualController>(() => DrSetYourCalendarVirtualController());
  }
}

class DrAddDegreeBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrAddDegreeController>(() => DrAddDegreeController());
  }
}

class DrEditProfileBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrEditProfileController>(() => DrEditProfileController());
  }
}

class DrCreatePatientBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrCreatePatientController>(() => DrCreatePatientController());
  }
}

class DrConsultancyHistoryBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrConsultancyHistoryController>(() => DrConsultancyHistoryController());
  }
}

class DrSeeWithdrawalMethodsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrSeeWithdrawalMethodsController>(() => DrSeeWithdrawalMethodsController());
  }
}

class DrSetWithdrawalMethodBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrSetWithdrawalMethodController>(() => DrSetWithdrawalMethodController());
  }
}

class DrWithdrawBalanceBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrWithdrawBalanceController>(() => DrWithdrawBalanceController());
  }
}

class DrConsultancyHistoryDetailBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrConsultancyHistoryDetailController>(() => DrConsultancyHistoryDetailController());
  }
}

class DrSetYourCalendarPhysicalBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrSetYourCalendarPhysicalController>(() => DrSetYourCalendarPhysicalController());
  }
}

class DrPhysicalConsultancyDetailBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrPhysicalConsultancyDetailController>(() => DrPhysicalConsultancyDetailController());
  }
}

class GlobalSearchBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<GlobalSearchController>(() => GlobalSearchController());
  }
}

class SearchFilterServiceBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchFilterServiceController>(() => SearchFilterServiceController());
  }
}

class SearchHospitalListBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchHospitalListController>(() => SearchHospitalListController());
  }
}

class DrVitalHealthMetricsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrVitalHealthMetricsController>(() => DrVitalHealthMetricsController());
  }
}

class SearchBloodBankBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchBloodBankController>(() => SearchBloodBankController());
  }
}

class SearchDiagnosticCenterBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchDiagnosticCenterController>(() => SearchDiagnosticCenterController());
  }
}

class SearchTraumaCenterBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchTraumaCenterController>(() => SearchTraumaCenterController());
  }
}

class SearchMedicineListBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchMedicineListController>(() => SearchMedicineListController());
  }
}

class SearchMedicineDetailsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchMedicineDetailsController>(() => SearchMedicineDetailsController());
  }
}

class DrSymptomCheckerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrSymptomCheckerController>(() => DrSymptomCheckerController());
  }
}

class DrSymptomCheckerStep2Bindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrSymptomCheckerStep2Controller>(() => DrSymptomCheckerStep2Controller());
  }
}

class DrMyPatientBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrMyPatientController>(() => DrMyPatientController());
  }
}

class DrNewConsultancyBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrNewConsultancyController>(() => DrNewConsultancyController());
  }
}

class DrVisitAppointmentBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrVisitAppointmentController>(() => DrVisitAppointmentController());
  }
}

class DrFollowUpAppointmentBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrFollowUpAppointmentController>(() => DrFollowUpAppointmentController());
  }
}

class DrIdentifiedDiseaseBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrIdentifiedDiseaseController>(() => DrIdentifiedDiseaseController());
  }
}

class DrFollowupIdentifiedDiseaseBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrFollowupIdentifiedDiseaseController>(() => DrFollowupIdentifiedDiseaseController());
  }
}

class DrPrescribeMedicineBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrPrescribeMedicineController>(() => DrPrescribeMedicineController());
  }
}

class DrFollowupPrescribeMedicineBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrFollowupPrescribeMedicineController>(() => DrFollowupPrescribeMedicineController());
  }
}

class DrAddNewMedicineBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrAddNewMedicineController>(() => DrAddNewMedicineController());
  }
}

class DrSpecialSymptomBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrSpecialSymptomController>(() => DrSpecialSymptomController());
  }
}

class DrDynamicSymptomBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrDynamicSymptomController>(() => DrDynamicSymptomController());
  }
}

class DrAdditionalCommentBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrAdditionalCommentController>(() => DrAdditionalCommentController());
  }
}

class DrPatientReviewBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrPatientReviewController>(() => DrPatientReviewController());
  }
}

class DrVerifyEmailNumberBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrVerifyEmailNumberController>(() => DrVerifyEmailNumberController());
  }
}

class DrOTPVerificationBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrOtpVerificationController>(() => DrOtpVerificationController());
  }
}

class DrECPatientListBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrECPatientListController>(() => DrECPatientListController());
  }
}

class DrAppointmentMedicineListBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrAppointmentMedicineListController>(() => DrAppointmentMedicineListController());
  }
}

class DrAppointmentMedicineDetailsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrAppointmentMedicineDetailsController>(() => DrAppointmentMedicineDetailsController());
  }
}

class DrFollowUpUpdateDiagnosticsTestBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrFollowUpUpdateDiagnosticsTestController>(() => DrFollowUpUpdateDiagnosticsTestController());
  }
}

class DrVisitNextDateBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrVisitNextDateController>(() => DrVisitNextDateController());
  }
}

class DrFollowUpDateBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrFollowUpDateController>(() => DrFollowUpDateController());
  }
}

class DrAppointmentDataSubmittedBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrAppointmentDataSubmittedController>(() => DrAppointmentDataSubmittedController());
  }
}

class DrPatientDetailBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrPatientDetailController>(() => DrPatientDetailController());
  }
}

class DrWithdrawRequestSubmittedBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrWithdrawRequestSubmittedController>(() => DrWithdrawRequestSubmittedController());
  }
}

class NoInternetBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NoInternetController>(() => NoInternetController());
  }
}

class DrScheduleSubmitBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrScheduleSubmittedController>(() => DrScheduleSubmittedController());
  }
}

class DrVisitUpdateSymptomsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrVisitUpdateSymptomsController>(() => DrVisitUpdateSymptomsController());
  }
}

class DrUpdateVitalSignsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrUpdateVitalSignsController>(() => DrUpdateVitalSignsController());
  }
}

class DrVisitCompleteAppointmentBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrVisitCompleteAppointmentController>(() => DrVisitCompleteAppointmentController());
  }
}

class DrFollowUpCompleteAppointmentBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrFollowUpCompleteAppointmentController>(() => DrFollowUpCompleteAppointmentController());
  }
}

class DrFollowupUpdateSymptomsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrFollowupUpdateSymptomsController>(() => DrFollowupUpdateSymptomsController());
  }
}

class DrDiagnosticsTestImagesBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrDiagnosticsTestImagesController>(() => DrDiagnosticsTestImagesController());
  }
}

class DrClinicalDecisionBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrClinicalDecisionController>(() => DrClinicalDecisionController());
  }
}

class DrVisitUpdateDiagnosticTestBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DrVisitUpdateDiagnosticTestController>(() => DrVisitUpdateDiagnosticTestController());
  }
}
