import 'package:flutter/material.dart';
import 'package:get/get.dart';

// import '../utils/preference/preference_manager.dart';

class BaseController extends GetxController {
  BuildContext? context;

  // var storage = Get.find<StorageManager>();

  void setContext(BuildContext mContext) {
    context = mContext;
    update();
  }
}
