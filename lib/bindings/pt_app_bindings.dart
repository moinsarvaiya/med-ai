import 'package:get/get.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_appointment_list/pt_appointment_list_controller.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_completed_appointment_details/pt_completed_appointment_details_controller.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_current_new_symptoms/pt_current_new_symptoms_controller.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_follow_up_diagnostic_status/pt_follow_up_diagnostic_status_controller.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_follow_up_medicine_status/pt_follow_up_medicine_status_controller.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_follow_up_review/pt_follow_up_review_controller.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_follow_up_vital/pt_follow_up_vital_controller.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_future_appointment_details/pt_future_appointment_details_controller.dart';
import 'package:med_ai/screens/patient/my_appointment_flow/pt_visit_followup_history/pt_visit_followup_history_controller.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_additional_comments/pt_additional_comment_controller.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_booking_reviews/pt_booking_review_controller.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_doctor_available_days/pt_doctor_available_days_controller.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_doctor_selection/pt_doctor_selection_controller.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_dynamic_symptom/pt_dynamic_symptom_controller.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_pain_symptom/pt_pain_symptom_controller.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_symptom_checker_step_2/pt_symptom_checker_step_2_controller.dart';
import 'package:med_ai/screens/patient/pt_book_appointment_flow/pt_vital_health_metrics/pt_vital_health_metrics_controller.dart';
import 'package:med_ai/screens/patient/pt_home/pt_home_controller.dart';
import 'package:med_ai/screens/patient/pt_more_services_flow/pt_available_doctor_profile_detail/pt_available_doctor_profile_detail_controller.dart';
import 'package:med_ai/screens/patient/pt_more_services_flow/pt_available_doctors/pt_available_doctor_controller.dart';
import 'package:med_ai/screens/patient/pt_more_services_flow/pt_near_by_hospital/pt_near_by_hospital_controller.dart';
import 'package:med_ai/screens/patient/pt_previous_medication/pt_add_previous_medication_record/pt_add_previous_medication_record_controller.dart';
import 'package:med_ai/screens/patient/pt_previous_medication/pt_previous_medication_diagnostics/pt_previous_medication_diagnostics_controller.dart';
import 'package:med_ai/screens/patient/pt_previous_medication/pt_previous_medication_records/pt_previous_medication_records_controller.dart';
import 'package:med_ai/screens/patient/pt_select_patient_profile/pt_select_patient_profile_controller.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_edit_patient_profile/pt_edit_patient_profile_controller.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_home_symptoms/pt_home_symptoms_controller.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_number_email_verification_flow/pt_otp_verification/pt_otp_verification_controller.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_number_email_verification_flow/pt_verify_email_number/pt_verify_email_number_controller.dart';
import 'package:med_ai/screens/patient/pt_setting_flow/pt_profile/pt_profile_controller.dart';

import '../screens/patient/my_appointment_flow/pt_previous_symptoms/pt_previous_symptoms_controller.dart';
import '../screens/patient/pt_book_appointment_flow/pt_cough_symptom/pt_cough_symptom_controller.dart';
import '../screens/patient/pt_book_appointment_flow/pt_doctor_time_slot_selection/pt_doctor_time_slot_selection_controller.dart';
import '../screens/patient/pt_book_appointment_flow/pt_review_booking/pt_review_booking_controller.dart';
import '../screens/patient/pt_book_appointment_flow/pt_special_symptom/pt_special_symptom_controller.dart';
import '../screens/patient/pt_book_appointment_flow/pt_symptom_checker/pt_symptom_checker_controller.dart';
import '../screens/patient/pt_book_appointment_flow/pt_thank_you_booking/pt_thank_you_booking_controller.dart';
import '../screens/patient/pt_bottom_navigation/pt_bottom_navigation_controller.dart';
import '../screens/patient/pt_health_record_flow/pt_health_record1/pt_health_record1_controller.dart';
import '../screens/patient/pt_health_record_flow/pt_health_record2/pt_health_record2_controller.dart';
import '../screens/patient/pt_more_services_flow/pt_more_services/pt_more_services_controller.dart';
import '../screens/patient/pt_payment_history_flow/pt_payment_details/pt_payment_details_controller.dart';
import '../screens/patient/pt_payment_history_flow/pt_payment_history/pt_payment_history_controller.dart';
import '../screens/patient/pt_setting_flow/pt_multi_profile/pt_multi_profile_controller.dart';
import '../screens/patient/pt_setting_flow/pt_settings/pt_setting_controller.dart';

class PtPaymentHistoryBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtPaymentHistoryController>(() => PtPaymentHistoryController());
  }
}

class PtSettingBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtSettingController>(() => PtSettingController());
  }
}

class PtBottomNavigationBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtBottomNavigationController>(() => PtBottomNavigationController());
    Get.lazyPut<PtSelectPatientProfileController>(() => PtSelectPatientProfileController());
    Get.lazyPut<PtSettingController>(() => PtSettingController());
    Get.lazyPut<PtHomeController>(() => PtHomeController());
    Get.lazyPut<PtMoreServicesController>(() => PtMoreServicesController());
  }
}

class PtProfileBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtProfileController>(() => PtProfileController());
  }
}

class PtHomeBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtHomeController>(() => PtHomeController());
  }
}

class PtMoreServicesBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtMoreServicesController>(() => PtMoreServicesController());
  }
}

class PtVerifyEmailNumberBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtVerifyEmailNumberController>(() => PtVerifyEmailNumberController());
  }
}

class PtOTPVerificationBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtOtpVerificationController>(() => PtOtpVerificationController());
  }
}

class PtEditProfileBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtEditPatientProfileController>(() => PtEditPatientProfileController());
  }
}

class PtAvailableDoctorBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtAvailableDoctorController>(() => PtAvailableDoctorController());
  }
}

class PtAvailableDoctorProfileDetailBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtAvailableDoctorProfileDetailController>(() => PtAvailableDoctorProfileDetailController());
  }
}

class PtNearByHospitalBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtNearByHospitalController>(() => PtNearByHospitalController());
  }
}

class PtPaymentDetailsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtPaymentDetailsController>(() => PtPaymentDetailsController());
  }
}

class PtMultiProfileBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtMultiProfileController>(() => PtMultiProfileController());
  }
}

class PtSelectPatientProfileBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtSelectPatientProfileController>(() => PtSelectPatientProfileController());
  }
}

class PtHealthRecord1Bindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtHealthRecord1Controller>(() => PtHealthRecord1Controller());
  }
}

class PtHealthRecord2Bindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtHealthRecord2Controller>(() => PtHealthRecord2Controller());
  }
}

class PtAppointmentListBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtAppointmentListController>(() => PtAppointmentListController());
  }
}

class PtFutureAppointmentDetailsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtFutureAppointmentDetailsController>(() => PtFutureAppointmentDetailsController());
  }
}

class PtVisitFollowupHistoryBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtVisitFollowupHistoryController>(() => PtVisitFollowupHistoryController());
  }
}

class PtCompletedAppointmentDetailsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PtCompletedAppointmentDetailsController());
  }
}

class PtVitalHealthMatrixBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtVitalHealthMetricsController>(() => PtVitalHealthMetricsController());
  }
}

class PtSymptomCheckerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtSymptomCheckerController>(() => PtSymptomCheckerController());
  }
}

class PtSpecialSymptomsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtSpecialSymptomController>(() => PtSpecialSymptomController());
  }
}

class PtSymptomsCheckerStep2Bindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtSymptomCheckerStep2Controller>(() => PtSymptomCheckerStep2Controller());
  }
}

class PtDynamicSymptomBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtDynamicSymptomController>(() => PtDynamicSymptomController());
  }
}

class PtAdditionalCommentsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtAdditionalCommentController>(() => PtAdditionalCommentController());
  }
}

class PtBookingReviewsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtBookingReviewController>(() => PtBookingReviewController());
  }
}

class PtDoctorSelectionBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtDoctorSelectionController>(() => PtDoctorSelectionController());
  }
}

class PtFollowUpVitalBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtFollowUpVitalController>(() => PtFollowUpVitalController());
  }
}

class PtDoctorAvailableDaysBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtDoctorAvailableDaysController>(() => PtDoctorAvailableDaysController());
  }
}

class PtDoctorTimeSlotSelectionBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtDoctorTimeSlotSelectionController>(() => PtDoctorTimeSlotSelectionController());
  }
}

class PtReviewBookingBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtReviewBookingController>(() => PtReviewBookingController());
  }
}

class PtFollowUpMedicineStatusBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtFollowUpMedicineStatusController>(() => PtFollowUpMedicineStatusController());
  }
}

class PtFollowUpDiagnosticStatusBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtFollowUpDiagnosticStatusController>(() => PtFollowUpDiagnosticStatusController());
  }
}

class PtPreviousSymptomsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtPreviousSymptomsController>(() => PtPreviousSymptomsController());
  }
}

class PtCoughSymptomBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtCoughSymptomController>(() => PtCoughSymptomController());
  }
}

class PtCurrentNewSymptomsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtCurrentNewSymptomsController>(() => PtCurrentNewSymptomsController());
  }
}

class PtThankYouBookingBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtThankYouBookingController>(() => PtThankYouBookingController());
  }
}

class PtFollowUpReviewBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtFollowUpReviewController>(() => PtFollowUpReviewController());
  }
}

class PtPainSymptomBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtPainSymptomController>(() => PtPainSymptomController());
  }
}

class PtPreviousMedicationBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtPreviousMedicationRecordsController>(() => PtPreviousMedicationRecordsController());
  }
}

class PtAddPreviousMedicationRecordBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtAddPreviousMedicationRecordController>(() => PtAddPreviousMedicationRecordController());
  }
}

class PtPreviousMedicationDiagnosticsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtPreviousMedicationDiagnosticsController>(() => PtPreviousMedicationDiagnosticsController());
  }
}

class PtHomeSymptomsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PtHomeSymptomsController>(() => PtHomeSymptomsController());
  }
}
