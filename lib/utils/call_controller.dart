import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

import '../constant/settings.dart';

class CallController extends GetxController {
  RxInt myremoteUid = 1.obs;
  RxBool localUserJoined = false.obs;
  RxBool muted = false.obs;
  RxBool videoPaused = false.obs;
  RxBool switchMainView = false.obs;
  RxBool mutedVideo = false.obs;
  RxBool reConnectingRemoteView = false.obs;
  RxBool isFront = false.obs;
  late RtcEngine engine;
  final GlobalKey<State> _keyLoader = GlobalKey<State>();

  @override
  void onClose() {
    super.onClose();
    clear();
  }

  clear() {
    engine.leaveChannel();
    isFront.value = false;
    reConnectingRemoteView.value = false;
    videoPaused.value = false;
    muted.value = false;
    mutedVideo.value = false;
    switchMainView.value = false;
    localUserJoined.value = false;
    update();
  }

  Future<void> initilize() async {
    Future.delayed(Duration.zero, () async {
      await _initAgoraRtcEngine();
      _addAgoraEventHandlers();
      await engine.setClientRole(role: ClientRoleType.clientRoleBroadcaster);
      VideoEncoderConfiguration configuration = const VideoEncoderConfiguration();
      await engine.setVideoEncoderConfiguration(configuration);
      await engine.leaveChannel();
      await engine.joinChannel(
        token: token,
        channelId: channgeId,
        uid: 0,
        options: const ChannelMediaOptions(),
      );
      update();
    });
  }

  Future<void> _initAgoraRtcEngine() async {
    engine = createAgoraRtcEngine();
    await engine.initialize(const RtcEngineContext(
      appId: appId,
      channelProfile: ChannelProfileType.channelProfileLiveBroadcasting,
    ));
    await engine.enableVideo();
    // await engine.startPreview();
    await engine.setClientRole(role: ClientRoleType.clientRoleBroadcaster);
  }

  void _addAgoraEventHandlers() {
    showLoadingDialog(Get.context!, _keyLoader);
    engine.registerEventHandler(
      RtcEngineEventHandler(
        onJoinChannelSuccess: (RtcConnection connection, int elapsed) {
          print('#DEBUG_TESTING onJoinChannelSuccess');
          localUserJoined.value = true;
          update();
        },
        onUserJoined: (RtcConnection connection, int remoteUid, int elapsed) {
          print('#DEBUG_TESTING onUserJoined');
          localUserJoined.value = true;
          myremoteUid.value = remoteUid;
          update();
          Get.back();
        },
        onUserOffline: (RtcConnection connection, int remoteUid, UserOfflineReasonType reason) {
          if (reason == UserOfflineReasonType.userOfflineDropped) {
            WakelockPlus.disable();
            myremoteUid.value = 0;
            onCallEnd();
            update();
          } else {
            myremoteUid.value = 0;
            onCallEnd();
            update();
          }
        },
        onRemoteVideoStats: (RtcConnection connection, RemoteVideoStats remoteVideoStats) {
          if (remoteVideoStats.receivedBitrate == 0) {
            videoPaused.value = true;
            update();
          } else {
            videoPaused.value = false;
            update();
          }
        },
        onUserMuteVideo: (RtcConnection connection, int value, bool isConnected) {
          if (isConnected) {
            videoPaused.value = true;
            update();
          } else {
            videoPaused.value = false;
            update();
          }
        },
        onError: (errorCode, _) {
          print('#DEBUG_TESTING $errorCode');
          // Handle errors, such as connection failures, here
          // Use errorCode to identify the specific error scenario
        },
        onTokenPrivilegeWillExpire: (RtcConnection connection, String token) {},
        onLeaveChannel: (RtcConnection connection, stats) {
          clear();
          onCallEnd();
          update();
        },
      ),
    );
  }

  void onVideoOff() {
    mutedVideo.value = !mutedVideo.value;
    engine.muteLocalVideoStream(mutedVideo.value);
    update();
  }

  void onCallEnd() {
    clear();
    update();
    Get.back();
  }

  void onToggleMute() {
    muted.value = !muted.value;
    engine.muteLocalAudioStream(muted.value);
    update();
  }

  void onToggleMuteVideo() {
    mutedVideo.value = !mutedVideo.value;
    engine.muteLocalVideoStream(mutedVideo.value);
    update();
  }

  void onSwitchCamera() {
    engine.switchCamera().then((value) => {}).catchError((err) {});
  }

  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: SimpleDialog(
            key: key,
            backgroundColor: Colors.white,
            children: const <Widget>[
              Center(
                child: Column(
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Please Wait....",
                      style: TextStyle(color: Colors.black),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
