import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';

import '../constant/app_colors.dart';
import '../constant/app_images.dart';
import 'NotificationController.dart';

class LocalNotificationService {
  static Future<void> initialize(BuildContext context) async {
    AwesomeNotifications().initialize(null, [
      NotificationChannel(
        channelKey: 'video_calling',
        channelName: 'Call Channel',
        channelDescription: 'Channel of calling',
        defaultColor: AppColors.colorDarkBlue,
        ledColor: Colors.white,
        importance: NotificationImportance.Max,
        channelShowBadge: true,
        locked: true,
        defaultRingtoneType: DefaultRingtoneType.Ringtone,
        icon: AppImages.appLogo,
        // enableLights: true,
        enableVibration: true,
        // playSound: true,
      ),
    ]);
  }

  static void display() async {
    AwesomeNotifications().createNotification(
      content: NotificationContent(
        id: 101,
        channelKey: 'video_calling',
        color: Colors.white,
        title: "Doctor Calling...",
        body: 'Doctor Philip calling...',
        category: NotificationCategory.Call,
        wakeUpScreen: true,
        fullScreenIntent: true,
        autoDismissible: false,
        backgroundColor: Colors.orange,
        // displayOnBackground: true,
        // displayOnForeground: true,
      ),
      actionButtons: [
        NotificationActionButton(
          key: 'ACCEPT',
          label: 'Accept Call',
          color: Colors.green,
          autoDismissible: true,
        ),
        NotificationActionButton(
          key: 'REJECT',
          label: 'Reject Call',
          color: Colors.red,
          autoDismissible: true,
        )
      ],
    );
    AwesomeNotifications().setListeners(
      onActionReceivedMethod: (ReceivedAction receivedAction) async {
        await NotificationController.onActionReceivedMethod(receivedAction);
      },
      onNotificationCreatedMethod: (ReceivedNotification receivedNotification) async {
        await NotificationController.onNotificationCreatedMethod(receivedNotification);
      },
      onNotificationDisplayedMethod: (ReceivedNotification receivedNotification) async {
        await NotificationController.onNotificationDisplayedMethod(receivedNotification);
      },
      onDismissActionReceivedMethod: (ReceivedAction receivedAction) async {
        await NotificationController.onDismissActionReceivedMethod(receivedAction);
      },
    );
  }
}
