import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:med_ai/constant/app_strings_key.dart';
import 'package:path_provider/path_provider.dart';

import '../../constant/storage_keys.dart';
import '../app_preference/login_preferences.dart';
import '../utilities.dart';

class FileDownload {
  static final Dio _dio = Dio();

  static void downLoadFile(String url) async {
    // bool isPermissionGranted = await CheckPermission.getStoragePermission(
    //     AppStringKey.strStoragePermissionName);
    // if (isPermissionGranted) {
    // final baseStorage = await getDownloadDirectory();
    final baseStorage = Directory('/storage/emulated/0/Download');
    final fileName = url.split('/').last;
    final file = File("${baseStorage.path}/$fileName");

    // Map<String, dynamic> headers = {
    //   HttpHeaders.contentTypeHeader: 'application/json',
    //   'Authorization': 'Bearer ${LoginPreferences().sharedPrefRead(StorageKeys.token)}',
    //   'Accept': 'application/json',
    // };

    try {
      await _dio.download(
        url,
        file.path,
       /* options: Options(
          headers: headers, // Pass the headers in the options parameter
          followRedirects: false, // (Optional) Set followRedirects to false if needed
        ),*/
      );
      Utilities.showMessage("${AppStringKey.strLabelFileDownloadedSuccessfullySavedIn} ${file.path}");
    } catch (e) {
      Utilities.showErrorMessage(AppStringKey.somethingWentWrong);
    }

    try {
      var response = await http.get(
        Uri.parse(url),
        headers: {
          HttpHeaders.authorizationHeader: 'Bearer ${LoginPreferences().sharedPrefRead(StorageKeys.token)}',
          // Set the auth token in the header
        },
      );

      if (response.statusCode == 200) {
        // Get the app's directory path for storing the downloaded PDF
        // Directory appDocDir = await getApplicationDocumentsDirectory();
        // String filePath = '${appDocDir.path}/downloaded_pdf.pdf';
        //
        // // Write the response content to a file
        // File pdfFile = File(filePath);

        final baseStorage = Directory('/storage/emulated/0/Download');
        final fileName = url.split('/').last;
        print("object $fileName");
        print("object $url");
        final file = File("${baseStorage.path}/$fileName");

        await file.writeAsBytes(response.bodyBytes);

        // Once the file is downloaded, filePath contains the downloaded PDF location
        print('PDF downloaded at: $file');
      } else {
        // Handle API error if any
        print('Failed to download PDF. Status code: ${response.statusCode}');
      }
    } catch (e) {
      // Handle network or other errors
      print('Error downloading PDF: $e');
    }
    // }
  }

  static Future<Directory> getDownloadDirectory() async {
    if (Platform.isAndroid) {
      return await getApplicationDocumentsDirectory();
    } else if (Platform.isIOS) {
      return await getApplicationDocumentsDirectory();
    } else {
      throw Exception('Unsupported platform');
    }
  }
}
