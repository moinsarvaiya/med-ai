import 'dart:convert';
import 'dart:developer' as devLog;
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:med_ai/constant/app_colors.dart';
import 'package:med_ai/constant/app_strings.dart';
import 'package:med_ai/constant/storage_keys.dart';
import 'package:med_ai/models/medicine_model.dart';
import 'package:path/path.dart' as path;

import '../app_preference/doctor_preferences.dart';
import '../app_preference/followup_preferences.dart';
import '../app_preference/login_preferences.dart';
import '../app_preference/patient_preferences.dart';
import '../app_preference/visit_preferences.dart';

class CustomFunctions {
  static void customSnackBar(String? title, String message) {
    Get.snackbar(
      title!,
      message,
      colorText: AppColors.white,
      backgroundColor: AppColors.colorDarkBlue,
      icon: const Icon(
        Icons.add_alert_rounded,
        color: AppColors.white,
      ),
      snackPosition: SnackPosition.TOP,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      reverseAnimationCurve: Curves.easeOutBack,
      //duration: const Duration(seconds: 3),
      margin: const EdgeInsets.only(left: 10, right: 10, top: 30),
      overlayBlur: 0.5,
    );
  }

  static void customSuccessSnackBar(String message) {
    Get.snackbar(
      AppStrings.success,
      message,
      colorText: AppColors.white,
      backgroundColor: AppColors.colorRed,
      icon: const Icon(
        Icons.add_alert_rounded,
        color: AppColors.white,
      ),
      snackPosition: SnackPosition.TOP,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      reverseAnimationCurve: Curves.easeOutBack,
      //duration: const Duration(seconds: 3),
      margin: const EdgeInsets.only(left: 10, right: 10, top: 30),
      overlayBlur: 0.5,
    );
  }

  static void customErrorSnackBar(String message) {
    Get.snackbar(
      AppStrings.error,
      message,
      colorText: AppColors.white,
      backgroundColor: AppColors.colorRed,
      icon: const Icon(
        Icons.add_alert_rounded,
        color: AppColors.white,
      ),
      snackPosition: SnackPosition.TOP,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      reverseAnimationCurve: Curves.easeOutBack,
      //duration: const Duration(seconds: 3),
      margin: const EdgeInsets.only(left: 10, right: 10, top: 30),
      overlayBlur: 0.5,
    );
  }

  static String setStringFromListString(var list) {
    devLog.log("List : ${list.length}");
    var resultString = "";
    var flag = false;
    if (list.length > 0) {
      for (String data in list) {
        resultString += data;
        resultString += ", ";
        flag = true;
      }
    } else {
      resultString = AppStrings.noDataFound;
    }
    if (flag) {
      resultString = resultString.substring(0, resultString.length - 2);
    }
    devLog.log("ResultString: $resultString", name: "TAG");
    return resultString;
  }

  static String removeHoursFromString(String difference) {
    var resultString = difference.substring(0, difference.length - 6);
    return resultString;
  }

  static Schedule getScheduleSorted(String schedule) {
    String breakfast = 'No';
    String lunch = 'No';
    String dinner = 'No';

    switch (schedule) {
      case '0+0+0':
        break;
      case '0+0+1':
        dinner = 'Yes';
        break;
      case '0+1+0':
        lunch = 'Yes';
        break;
      case '0+1+1':
        lunch = 'Yes';
        dinner = 'Yes';
        break;
      case '1+0+0':
        breakfast = 'Yes';
        break;
      case '1+0+1':
        breakfast = 'Yes';
        dinner = 'Yes';
        break;
      case '1+1+0':
        breakfast = 'Yes';
        lunch = 'Yes';
        break;
      case '1+1+1':
        breakfast = 'Yes';
        lunch = 'Yes';
        dinner = 'Yes';
        break;
    }

    Schedule scheduleModel = Schedule(
      breakfast: breakfast,
      lunch: lunch,
      dinner: dinner,
    );

    return scheduleModel;
  }

  static void loadingConfig(bool loadingState) {
    Future.delayed(const Duration(seconds: 5));
  }

  static SpinKitPumpingHeart showLoadingDialog() {
    const spinKit = SpinKitPumpingHeart(
      color: AppColors.colorDarkBlue,
      size: 50.0,
    );
    return spinKit;
  }

  static void stopLoadingDialog() {}

  static void clearAllPreferences() {
    LoginPreferences().sharedPrefEraseAllData();
    VisitPreferences().sharedPrefEraseAllData();
    FollowupPreferences().sharedPrefEraseAllData();
    DoctorPreferences().sharedPrefEraseAllData();
    PatientPreferences().sharedPrefEraseAllData();
  }

  static void resetToDefaultLanguage() {
    Get.updateLocale(const Locale('en', 'EN'));
  }

  static Future<File?> showImagePickBottomSheet(BuildContext context) async {
    // FocusScope.of(context).unfocus();
    File? file;
    await showModalBottomSheet(
      context: context,
      builder: (context) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: IntrinsicHeight(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      await pickImage(ImageSource.camera, context).then((value) {
                        file = value;
                        Navigator.pop(context);
                      });
                    },
                    child: Column(
                      children: [
                        const Icon(
                          CupertinoIcons.camera_fill,
                          size: 40,
                          color: AppColors.blue,
                        ),
                        Text(
                          'Camera',
                          style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                                color: AppColors.blue,
                              ),
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      await pickImage(ImageSource.gallery, context).then((value) {
                        file = value;
                        Get.back();
                      });
                    },
                    child: Column(
                      children: [
                        const Icon(
                          Icons.photo_library,
                          size: 40,
                          color: AppColors.blue,
                        ),
                        Text(
                          "Gallery",
                          style: Theme.of(context).textTheme.bodyLarge!.copyWith(color: AppColors.blue),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
    return file;
  }

  static Future<File?> pickImage(ImageSource imageSource, BuildContext context) async {
    try {
      final XFile? image = await ImagePicker().pickImage(source: imageSource);
      if (image != null) {
        return File(image.path);
      }
    } catch (e) {
      // showAlert(
      //   'This image is not supported,So just choose another image',
      // );
    }
    return null;
  }

  static String convertIntoBase64(File file) {
    List<int> imageBytes = file.readAsBytesSync();
    String base64File = base64Encode(imageBytes);
    return base64File;
  }

  static String getPatientBirthDate(String age) {
    int ageInYears = int.tryParse(age) ?? 0;
    DateTime currentDate = DateTime.now();
    int birthYear = currentDate.year - ageInYears;
    DateTime birthDate = DateTime(birthYear, currentDate.month, currentDate.day);
    String formattedBirthDate = DateFormat('yyyy-MM-dd').format(birthDate);
    return formattedBirthDate;
  }

  static String changeFileName(String filePath, String newFileName) {
    final directory = path.dirname(filePath);
    final extension = path.extension(filePath);
    final newFilePath = path.join(directory, newFileName + extension);
    return newFilePath;
  }

  static String generateName(int index) {
    return '${LoginPreferences().sharedPrefRead(StorageKeys.personId)}_${DateTime.now().millisecondsSinceEpoch}_visit_additional_$index.jpg';
  }

  static String generateRoomNameWithTimestamp() {
    return 'Room_${DateTime.now().millisecondsSinceEpoch}'; // Example: Room_1636732431256;
  }
}
