import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../constant/app_strings_key.dart';

class CheckPermission {
  static Future<bool> check(List<Permission> permissions, String permissionName, {bool isDone = false}) async {
    if (Platform.isAndroid) {
      DeviceInfoPlugin plugin = DeviceInfoPlugin();
      AndroidDeviceInfo android = await plugin.androidInfo;
      if (android.version.sdkInt >= 33 /*&& permissionName == 'Storage'*/) {
        return true;
      } else {
        bool allGranted = true;

        for (var permission in permissions) {
          if (!(await checkPermission(permission))) {
            allGranted = false;
            PermissionStatus status = await permission.request();
            if (status.isDenied /*&& permissionName == 'Storage'*/) {
              await Permission.storage.request();
            }
            if (status.isPermanentlyDenied && !isDone) {
              allGranted = await _openAppSettings(permissionName);
            }
          }
        }
        return allGranted;
      }
    } else {
      bool allGranted = true;

      for (var permission in permissions) {
        if (!(await checkPermission(permission))) {
          allGranted = false;
          PermissionStatus status = await permission.request();
          if (status.isPermanentlyDenied && !isDone) {
            allGranted = await _openAppSettings(permissionName);
          }
        }
      }

      return allGranted;
    }
  }

  static Future<bool> getStoragePermission(String permissionName) async {
    DeviceInfoPlugin plugin = DeviceInfoPlugin();
    AndroidDeviceInfo android = await plugin.androidInfo;
    if (android.version.sdkInt < 33) {
      if (await Permission.storage.request().isGranted) {
        return true;
      } else if (await Permission.storage.request().isPermanentlyDenied) {
        return await _openAppSettings(permissionName);
      } else if (await Permission.audio.request().isDenied) {
        await Permission.storage.request();
      }
    } else {
      if (await Permission.manageExternalStorage.request().isGranted) {
        return true;
      } else if (await Permission.manageExternalStorage.request().isPermanentlyDenied) {
        return await _openAppSettings(permissionName);
      } else if (await Permission.manageExternalStorage.request().isDenied) {
        await Permission.manageExternalStorage.request();
      }
    }
    return false;
  }

  static Future<bool> checkPermission(Permission permission) async {
    PermissionStatus status = await permission.status;
    return status.isGranted;
  }

  static Future<bool> _openAppSettings(String permissionName) async {
    return await showDialog(
          context: Get.context!,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text(AppStringKey.strLabelPermissionRequired),
              content: Text('Please grant the $permissionName permission to continue.'),
              actions: <Widget>[
                TextButton(
                  child: const Text(AppStringKey.strLabelCancel),
                  onPressed: () {
                    Get.back();
                  },
                ),
                TextButton(
                  child: const Text(AppStringKey.strSettings),
                  onPressed: () {
                    Get.back();
                    openAppSettings();
                  },
                ),
              ],
            );
          },
        ) ??
        false;
  }
}
