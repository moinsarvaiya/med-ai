import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:med_ai/constant/app_images.dart';
import 'package:med_ai/constant/base_style.dart';
import 'package:med_ai/widgets/space_horizontal.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constant/app_colors.dart';
import '../constant/app_strings_key.dart';
import '../widgets/custom_buttons.dart';
import '../widgets/space_vertical.dart';

// Useful utilies of the app
class Utilities {
  static Future<bool> isConnectedNetwork() async {
    try {
      final List<InternetAddress> result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      return false;
    }
  }

  static Color fromHex(String hexString) {
    final StringBuffer buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  static TimeOfDay stringToTimeOfDay(String tod) {
    final cleanedTimeString = tod.replaceAll('\u202F', ''); // Remove non-breaking space character

    int hour = int.parse(cleanedTimeString.split(':')[0]);
    final minute = cleanedTimeString.split(':').length > 1 ? int.parse(cleanedTimeString.split(':')[1].substring(0, 2)) : 0;
    final isAM = cleanedTimeString.substring(cleanedTimeString.length - 2).toLowerCase() == 'am';

    if (!isAM && hour != 12) {
      hour += 12;
    } else if (isAM && hour == 12) {
      hour = 0;
    }

    return TimeOfDay(hour: hour, minute: minute);
  }

  static TimeOfDay stringToTimeOfDayFor24Hours(String tod) {
    final cleanedTimeString = tod.replaceAll('\u202F', ''); // Remove non-breaking space character

    int hour = int.parse(cleanedTimeString.split(':')[0]);
    final minute = cleanedTimeString.split(':').length > 1 ? int.parse(cleanedTimeString.split(':')[1].substring(0, 2)) : 0;
    return TimeOfDay(hour: hour, minute: minute);
  }

  static String timeOfDayToString(TimeOfDay timeOfDay) {
    return "${timeOfDay.hour.toString().padLeft(2, '0')}:${timeOfDay.minute.toString().padLeft(2, '0')}";
  }

  static String formatTimeOfDay(TimeOfDay time, String format) {
    DateTime now = DateTime.now();
    DateTime dateTime = DateTime(now.year, now.month, now.day, time.hour, time.minute);
    DateFormat formatter = DateFormat(format);
    return formatter.format(dateTime);
  }

  static showErrorMessage(String message, [String title = "Error"]) {
    Get.rawSnackbar(
      title: title,
      message: message,
      backgroundColor: AppColors.colorDarkBlue,
    );
  }

  static showExitMessage(String message, [String title = AppStringKey.labelExit]) {
    Get.rawSnackbar(
      title: title,
      message: message,
      backgroundColor: AppColors.colorDarkBlue,
      snackPosition: SnackPosition.TOP,
      duration: const Duration(
        seconds: 1,
      ),
    );
  }

  static showMessage(String message, [String title = "Success"]) {
    Get.rawSnackbar(title: title.isEmpty ? null : title, message: message);
  }

  static Future<File?> pickImage(ImageSource imageSource) async {
    final XFile? image = await ImagePicker().pickImage(source: imageSource);
    if (image != null) {
      return File(image.path);
    }
    return null;
  }

  static String greeting() {
    var hour = DateTime.now().hour;
    if (hour < 12) {
      return 'Morning';
    }
    if (hour < 17) {
      return 'Afternoon';
    }
    return 'Evening';
  }

  static String getDay() {
    return DateFormat('EEEE').format(DateTime.now());
  }

  static String getTodayDate() {
    return DateFormat('dd MMM, yyyy').format(DateTime.now());
  }

  // A helper function to get the correct suffix for the day
  static String getSuffix(int day) {
    if (day >= 11 && day <= 13) {
      return 'th';
    }
    switch (day % 10) {
      case 1:
        return 'st';
      case 2:
        return 'nd';
      case 3:
        return 'rd';
      default:
        return 'th';
    }
  }

  // static String getDateFormat(String format) => switch (format) {
  //       "DD MMMM YYYY" => 'dd MMMM y',
  //       "DD MMM YYYY" => 'dd MMM y',
  //       "DD-MM-YY" => 'dd-MM-yy',
  //       "YYYY-MM-DD" => 'yyyy-MM-dd',
  //       "YYYY MMMM DD" => 'yyyy MMMM dd',
  //       "YYYY MMM DD" => 'yyyy MMM dd',
  //       "MMMM DD YYYY" => 'MMMM d y',
  //       _ => 'MMMM d y',
  //     };

  static bool is24HourFormat(String time) {
    return !time.contains('a');
  }

  static String formatHHMMSSTimer(int seconds) {
    int hours = (seconds / 3600).truncate();
    seconds = (seconds % 3600).truncate();
    int minutes = (seconds / 60).truncate();

    String hoursStr = (hours).toString().padLeft(2, '0');
    String minutesStr = (minutes).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    if (hours == 0) {
      return "$minutesStr:$secondsStr";
    }

    return "$hoursStr:$minutesStr:$secondsStr";
  }

  static Future<File?> showImagePickBottomSheet(BuildContext context) async {
    FocusScope.of(context).unfocus();
    File? file;
    await showModalBottomSheet(
      context: context,
      builder: (context) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: IntrinsicHeight(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      await pickImage(ImageSource.camera).then((value) {
                        file = value;
                        Navigator.pop(context);
                      });
                    },
                    child: Column(
                      children: [
                        const Icon(
                          Icons.camera,
                          size: 40,
                          color: AppColors.blue,
                        ),
                        Text(
                          "Camera",
                          style: Theme.of(context).textTheme.titleLarge!.copyWith(color: AppColors.blue),
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      await pickImage(ImageSource.gallery).then((value) {
                        file = value;
                        Navigator.pop(context);
                      });
                    },
                    child: Column(
                      children: [
                        const Icon(
                          Icons.image,
                          size: 40,
                          color: AppColors.blue,
                        ),
                        Text(
                          "Gallery",
                          style: Theme.of(context).textTheme.titleLarge!.copyWith(color: AppColors.blue),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
    return file;
  }

  static addTextToClipBoard(BuildContext context, String text) {
    Clipboard.setData(ClipboardData(text: text)).then((_) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Copied to your clipboard !')));
    });
  }

  static Future<String> getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }

  static commonDialog(
    BuildContext context,
    String title,
    String description,
    String positiveButton,
    String negativeButton,
    Function positiveButtonCallBack,
    Function negativeButtonCallBack,
  ) {
    showDialog(
      context: context,
      builder: (BuildContext context) => Center(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              DefaultTextStyle(
                style: BaseStyle.textStyleNunitoSansBold(
                  16,
                  AppColors.colorDarkBlue,
                ),
                child: Text(
                  title,
                ),
              ),
              const Divider(
                color: AppColors.colorLightSkyBlue,
              ),
              Align(
                alignment: Alignment.center,
                child: DefaultTextStyle(
                  style: BaseStyle.textStyleNunitoSansRegular(
                    16,
                    AppColors.colorDarkBlue,
                  ),
                  child: Text(
                    description,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              const SpaceVertical(15),
              Material(
                child: Row(
                  children: [
                    Expanded(
                      child: SubmitButton(
                        title: positiveButton,
                        displayBorder: true,
                        borderColor: AppColors.colorDarkBlue,
                        backgroundColor: Colors.white,
                        titleColor: AppColors.colorDarkBlue,
                        onClick: () {
                          positiveButtonCallBack();
                        },
                      ),
                    ),
                    const SpaceHorizontal(10),
                    Expanded(
                      child: SubmitButton(
                        title: negativeButton,
                        onClick: () {
                          negativeButtonCallBack();
                        },
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  static pendingProfileDialog(
    BuildContext context,
  ) {
    showDialog(
      context: context,
      builder: (BuildContext context) => Center(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Alert',
                style: BaseStyle.textStyleNunitoSansBold(
                  16,
                  AppColors.colorDarkBlue,
                ),
              ),
              const Divider(
                color: AppColors.colorLightSkyBlue,
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  'Your Approval is Pending. \nPlease wait till its done!',
                  style: BaseStyle.textStyleNunitoSansRegular(
                    16,
                    AppColors.colorDarkBlue,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              const SpaceVertical(15),
              Material(
                child: SizedBox(
                  width: 150,
                  child: SubmitButton(
                    title: "Okay",
                    onClick: () {
                      Get.back();
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  static pendingProfileCompleteDialog(
    BuildContext context,
  ) {
    showDialog(
      context: context,
      builder: (BuildContext context) => Center(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Alert',
                style: BaseStyle.textStyleNunitoSansBold(
                  16,
                  AppColors.colorDarkBlue,
                ),
              ),
              const Divider(
                color: AppColors.colorLightSkyBlue,
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  'Your profile is not completed yet. \nPlease complete your profile first!',
                  style: BaseStyle.textStyleNunitoSansRegular(
                    16,
                    AppColors.colorDarkBlue,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              const SpaceVertical(15),
              Material(
                child: SizedBox(
                  width: 150,
                  child: SubmitButton(
                    title: "Okay",
                    onClick: () {
                      Get.back();
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  static void removeFocus() {
    FocusManager.instance.primaryFocus?.unfocus();
  }

  static Widget getPlaceHolder({double? height, double? width}) {
    return Container(
      color: Colors.white,
      child: Image.asset(
        AppImages.placeholder,
        width: width,
        height: height,
        fit: BoxFit.cover,
      ),
    );
  }

  static Widget userVerifiedWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          AppImages.verified,
          width: 16,
          height: 16,
        ),
        const SpaceHorizontal(3),
        Text(
          AppStringKey.strVerified,
          style: BaseStyle.textStyleNunitoSansSemiBold(
            10,
            AppColors.colorGreen,
          ),
        ),
      ],
    );
  }

  static openDialPad(String phoneNumber) async {
    Uri url = Uri(scheme: "tel", path: phoneNumber);
    if (await canLaunchUrl(url)) {
      await launchUrl(url);
    } else {
      print("Can't open dial pad.");
    }
  }

  static openBrowser(String url) async {
    final Uri googleUrl = Uri.parse(url);
    if (!await launchUrl(
      googleUrl,
      mode: LaunchMode.inAppWebView,
    )) {
      throw Exception('Could not launch $googleUrl');
    }
  }

  static String availableDayFormat_yyyyMMdd(String inputDate) {
    final inputDateFormat = DateFormat("E, yyyy-MM-dd");
    final parsedDate = inputDateFormat.parse(inputDate);
    final outputDateFormat = DateFormat("yyyy-MM-dd");
    return outputDateFormat.format(parsedDate);
  }

  static String formatDuration(Duration duration) {
    final hours = duration.inHours;
    final minutes = duration.inMinutes.remainder(60);
    return '$hours:${minutes.toString().padLeft(2, '0')}';
  }

  static String getDifferenceTime(String timeRange) {
    final timeFormat = DateFormat("h:mm a");
    final times = timeRange.split(" - ");
    if (times.length != 2) {
      throw Exception("Invalid time range format");
    }
    final startTime = timeFormat.parse(times[0]);
    final endTime = timeFormat.parse(times[1]);
    final difference = endTime.difference(startTime);

    if (difference.inSeconds > 60) {
      return "${formatDuration(difference)} Min";
    } else {
      return "${difference.inSeconds} Second";
    }
  }

  static String reviewScreenDateFormat_ddMMMyyyy(String inputDate) {
    final inputDateFormat = DateFormat("E, yyyy-MM-dd");
    final parsedDate = inputDateFormat.parse(inputDate);
    final outputDateFormat = DateFormat("dd MMM yyyy");
    return outputDateFormat.format(parsedDate);
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(BuildContext context, Widget child, ScrollableDetails details) {
    return child;
  }
}
