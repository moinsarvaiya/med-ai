import 'package:get_storage/get_storage.dart';

// Use to store data in local storage
class DoctorPreferences {
  final doctorData = GetStorage("doctor_preferences");

  static final DoctorPreferences _singleton = DoctorPreferences._();

  factory DoctorPreferences() {
    return _singleton;
  }

  DoctorPreferences._();

  void sharedPrefWrite(String key, dynamic value) => doctorData.write(key, value);

  dynamic sharedPrefRead(String key) => doctorData.read(key);

  void sharedPrefRemove(String key) => doctorData.remove(key);

  void sharedPrefEraseAllData() => doctorData.erase();
}
