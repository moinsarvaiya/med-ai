import 'package:get_storage/get_storage.dart';

// Use to store data in local storage
class PatientPreferences {
  final patientData = GetStorage("patient_preferences");

  static final PatientPreferences _singleton = PatientPreferences._();

  factory PatientPreferences() {
    return _singleton;
  }

  PatientPreferences._();

  void sharedPrefWrite(String key, dynamic value) => patientData.write(key, value);

  dynamic sharedPrefRead(String key) => patientData.read(key);

  void sharedPrefRemove(String key) => patientData.remove(key);

  void sharedPrefEraseAllData() => patientData.erase();
}
