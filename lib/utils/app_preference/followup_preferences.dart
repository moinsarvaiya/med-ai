import 'package:get_storage/get_storage.dart';

// Use to store data in local storage
class FollowupPreferences {
  final followupData = GetStorage("followup_preferences");

  static final FollowupPreferences _singleton = FollowupPreferences._();

  factory FollowupPreferences() {
    return _singleton;
  }

  FollowupPreferences._();

  void sharedPrefWrite(String key, dynamic value) => followupData.write(key, value);

  dynamic sharedPrefRead(String key) => followupData.read(key);

  void sharedPrefRemove(String key) => followupData.remove(key);

  void sharedPrefEraseAllData() => followupData.erase();
}
