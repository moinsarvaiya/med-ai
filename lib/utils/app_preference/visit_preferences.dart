import 'package:get_storage/get_storage.dart';

// Use to store data in local storage
class VisitPreferences {
  final visitData = GetStorage("visit_preferences");

  static final VisitPreferences _singleton = VisitPreferences._();

  factory VisitPreferences() {
    return _singleton;
  }

  VisitPreferences._();

  void sharedPrefWrite(String key, dynamic value) => visitData.write(key, value);

  dynamic sharedPrefRead(String key) => visitData.read(key);

  void sharedPrefRemove(String key) => visitData.remove(key);

  void sharedPrefEraseAllData() => visitData.erase();
}
