import 'package:get_storage/get_storage.dart';

// Use to store data in local storage
class LoginPreferences {
  final userData = GetStorage("login_preferences");

  static final LoginPreferences _singleton = LoginPreferences._();

  factory LoginPreferences() {
    return _singleton;
  }

  LoginPreferences._();

  void sharedPrefWrite(String key, dynamic value) => userData.write(key, value);

  dynamic sharedPrefRead(String key) => userData.read(key);

  void sharedPrefRemove(String key) => userData.remove(key);

  void sharedPrefEraseAllData() => userData.erase();
}
