import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class FirebaseNotificationService {
  static Future<void> firebaseMessagingInitialization(BuildContext context) async {
    FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
    FirebaseMessaging messaging = FirebaseMessaging.instance;

    ///App is terminated or close and user taps and it open app from terminated state
    messaging.getInitialMessage().then(
      (message) {
        if (message != null) {
          if (message.data.containsKey('url')) {}
        }
      },
    );

    ///App in foreground
    FirebaseMessaging.onMessage.listen((RemoteMessage? message) {
      if (message != null) {
        if (Platform.isAndroid) {
          ///Firebase not send heads up notification when app in foreground, for that use flutter local notification to display heads up notification
          // LocalNotificationService.display(message);
        }
      }
    });

    ///App in background and user taps on notification
    FirebaseMessaging.onMessageOpenedApp.listen(
      (message) {
        if (message.data.containsKey('url')) {}
      },
    );

    ///Heads ap notification for iOS
    messaging.setForegroundNotificationPresentationOptions(alert: true, badge: true, sound: true);
    enableIOSNotifications();

    await messaging.requestPermission(
      announcement: false,
      criticalAlert: true,
      provisional: false,
    );
  }

  static enableIOSNotifications() async {
    await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );
  }
}

/// This function called when app is in the background or terminated.
Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  //print(message.data);
}
