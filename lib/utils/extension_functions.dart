import 'package:get/get_rx/src/rx_types/rx_types.dart';

extension RxListExtension<E> on RxList<E> {
  List<E> toNormalList() {
    return toList();
  }
}

extension DefaultIfEmptyExtension on String {
  String defaultIfEmpty(String defaultValue) {
    return isEmpty ? defaultValue : this;
  }
}

extension DynamicListToStringListExtension on List<dynamic> {
  List<String> toListOfStrings() {
    return map((dynamic item) => item.toString()).toList();
  }
}
